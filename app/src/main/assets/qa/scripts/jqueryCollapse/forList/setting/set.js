// JavaScript Document


$(document).ready(function() {
        $("#css3-animated-example").collapse({
          accordion: false,
          open: function() {
            this.addClass("open");
            this.css({ height: this.children().outerHeight() });
          },
          close: function() {
            this.css({ height: "0px" });
            this.removeClass("open");
          }
        });

        $("#btn-open-all").click(function() {
          $("#css3-animated-example").trigger("open")
        })
				$("#btn-close-all").click(function() {
          $("#css3-animated-example").trigger("close")
        })
});