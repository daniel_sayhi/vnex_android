package tw.sayhi.vnex;

public class Config
{
    public static final String company = "vnex";
    public static final String apid = "ROV02"; // for user profile version
    public static final String lang = "vi_VN"; /* lang, vi_VN, in_ID, zh_TW */
    public static final String sender_country = "VNM";
    public static final String payment_type = "CVS"; // BARCODE|CVS
    public static final long payment_duration = 3 * 60 * 60 * 1000L; // 3 hours
    public static final long history_duration = 90 * 24 * 60 * 60 * 1000L; // 3 months = 90 days
    public static final String selfie_file_name = "vnex_selfie.jpg";
    public static final String selfie_file_name_norm = "vnex_selfie_800x600.jpg";
    public static final String arc_file_name = "vnex_arc.jpg";
    public static final String arc_file_name_norm = "vnex_arc_800x600.jpg";
    public static final String arc_back_file_name = "vnex_arc_back.jpg";
    public static final String arc_back_file_name_norm = "vnex_arc_back_800x600.jpg";
    public static final String receipt_file_name = "seven_receipt.jpg";
    public static final String receipt_file_name_norm = "seven_receipt_200x80.png";
    public static final String sign_file_name = "vnex_sign.jpg";
    public static final String sign_file_name_norm = "vnex_sign_200x80.png";
    public static final int photo_width = 800;
    public static final int photo_height = 600;
    public static final int sign_width = 200;
    public static final int sign_height = 80;
    public static final int barcode_width = 600;
    public static final int barcode_height = 100;
    // 紅利點數(Cash Points)
    public static final String CP_REGISTER = "10";
    public static final String CP_SHARE = "10";
    public static final String CP_REMIT = "5";
    public static final String CP_AT_STORE = "5";
}
