package tw.sayhi.vnex.misc;

import android.app.Activity;

import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tw.sayhi.shared.Api;
import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.ImageURL;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.vnex.api.DataImageLink;
import tw.sayhi.vnex.api.RespCloudImages;

import static tw.sayhi.shared.util.ImageURL.latchInitImageLoader;

public class CloudImages
{
    // For banners and popup images
    public static RespCloudImages cloudImages;
    public static String cloudImagesUrl = "https://sayhi-market-web.appspot.com/VnexImages?TimeStamp=";
    public static CountDownLatch latchLoadImageDesc = new CountDownLatch(1);
    public static String respImageDesc = "";
    public static long timeLoadImageDesc = 0;
    public static final long LOAD_IMAGE_DESC_INTERVAL = 10 * 60 * 1000; // 10 minutes
    public static void loadImageDesc(final Activity act, final CountDownLatch latchLoad)
    {
//        MyLog.i("loadImageDesc()");
        new Thread() {
            @Override
            public void run() {
                // Making URL unique every time such that URL cache is not used.
                long curr_time = System.currentTimeMillis();
                if(!Utils.isNull(respImageDesc) && (curr_time - timeLoadImageDesc) < LOAD_IMAGE_DESC_INTERVAL) {
//                    MyLog.i("image desc not loaded. Too soon");
                    return;
                }
                CountDownLatch myLatch;
                int t = MyLog.timeStart();
                String resp = getUrl(cloudImagesUrl + System.currentTimeMillis());
                if(!Utils.isNull(respImageDesc) && respImageDesc.equals(resp)) {
                    latchLoadImageDesc.countDown();
                    MyLog.timeEnd(t, "image desc is the same");
                    return;
                }
                timeLoadImageDesc = curr_time;
                cloudImages = new RespCloudImages(resp);
                if(!cloudImages.success) {
                    MyLog.timeEnd(t, "cloudImagesUrl failed.");
                    cloudImages = embedCloudImages();
                } else {
                    MyLog.timeEnd(t, "cloudImagesUrl succeed.");
                    respImageDesc = resp;
                }
                Utils.waitFor(latchInitImageLoader);
                latchLoad.countDown();
            }
        }.start();
    }
    public static String getUrl(String sourceUrl)
    {
        String resp = "";
        try {
//            resp = Api.getUrl(sourceUrl, 20 * 1000);
            resp = Api.getUrl(sourceUrl);
        } catch(Exception e) {
        }
        return resp;
    }
    public static RespCloudImages embedCloudImages()
    {
        String resp = "{\n" +
                "    \"android_version\" : \"61\",\n" +
                "    \"android_force_update\" : \"false\",\n" +
                "    \"ios_version\" : \"55\",\n" +
                "    \"ios_force_update\" : \"false\",\n" +
                "    \"old_data\" :\n" +
                "    {\n" +
                "        \"start_date\" : \"2021/01/01\",\n" +
                "        \"default_link\" : \"\",\n" +
                "        \"banners\" : [\n" +
                "            {\n" +
                "                \"image\" : \"https://i.ibb.co/prW3PGG/banner-1.jpg\",\n" +
                "                \"link\" : \"\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"image\" : \"https://i.ibb.co/BskSNW4/banner-2.jpg\",\n" +
                "                \"link\" : \"\"\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    \"new_data\" :\n" +
                "    {\n" +
                "        \"start_date\" : \"2021/01/01\",\n" +
                "        \"default_link\" : \"\",\n" +
                "        \"banners\" : [\n" +
                "            {\n" +
                "                \"image\" : \"https://i.ibb.co/prW3PGG/banner-1.jpg\",\n" +
                "                \"link\" : \"\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"image\" : \"https://i.ibb.co/BskSNW4/banner-2.jpg\",\n" +
                "                \"link\" : \"\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}   ";
        return new RespCloudImages(resp);
    }
}
