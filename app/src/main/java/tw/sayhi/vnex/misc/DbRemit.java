package tw.sayhi.vnex.misc;

        import android.content.ContentValues;
        import android.content.Context;
        import android.database.Cursor;
        import android.database.MatrixCursor;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteDatabase.CursorFactory;
        import android.database.sqlite.SQLiteOpenHelper;
        import android.util.Log;

        import org.json.JSONObject;

        import java.util.Map;

        import tw.sayhi.shared.Utils;
        import tw.sayhi.shared.util.MyLog;

public class DbRemit extends SQLiteOpenHelper
{
    public static String DB_NAME = "vnex_db_2.0.39";
    public static int DB_VER = 1;
    private static final String TAG = DbRemit.class.getSimpleName();
    public static boolean sDebug = false;
    // Reserved fields for all tables
    // Usage: for example
    //  public static final String NEW_FIELD = DbRemit.CREATOR;
    //  And use NEW_FIELD thereon.
    public static final String RESERVED1 = "reserved1";
    public static final String PROFILE_TYPE = "reserved2";
    public static final String IBON = "reserved3";
    public static final String FAMILYMART = "reserved4";
    public static final String HILIFE = "reserved5";
    public static final String RESERVED6 = "reserved6";
    public static final String RESERVED7 = "reserved7";
    public static final String RESERVED8 = "reserved8";
    public static final String RESERVED9 = "reserved9";


    public static SQLiteDatabase sDb;


    public static void init(Context ctx)
    {
        if (sDb == null || !sDb.isOpen()) {
            sDb = new DbRemit(ctx, null).getWritableDatabase();
        }
    }


    public DbRemit(Context ctx, CursorFactory factory) {
        super(ctx, DB_NAME, factory, DB_VER);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        DbSender._create(db);
        DbOrder._create(db);
//        DbPayee._create(db);
        DbPassword._create(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
        Log.d(TAG, "DB upgrade: " + oldVer + " -> " + newVer);
    }

    public final static void insertRow(Cursor from, MatrixCursor to) {
        final String columns[] = from.getColumnNames(), values[] = new String[columns.length];
        final int size = columns.length;

        for (int i = 0; i < size; i++) {
            values[i] = getStringFromColumn(from, columns[i]);
        }
        to.addRow(values);

    }

    private static String getStringFromColumn(Cursor c, String columnName) {
        int index = c.getColumnIndexOrThrow(columnName);
        String value = c.getString(index);
        if (value != null && value.length() > 0) {
            return value;
        } else {
            return null;
        }
    }

    public static String getString(Cursor c, String column)
    {
        if(isNull(c)) {
            return "";
        }
        String str = c.getString(c.getColumnIndex(column));
        if (str == null)
            str = "";
        return str;
    }
    public static String getString(JSONObject obj, String key)
    {
        if(obj == null) {
            return "";
        }
        String str = obj.optString(key);
        if (str == null)
            str = "";
        return str;
    }
    public static double getDouble(Cursor c, String column) {
        return c.getDouble(c.getColumnIndex(column));
    }
    public static long getLong(Cursor c, String column) {
        return c.getLong(c.getColumnIndex(column));
    }
    public static boolean isNull(Cursor c)
    {
        if(c != null && c.getCount() > 0)
            return false;
        return true;
    }
    public static void close(Cursor c)
    {
        if(c != null) {
            c.close();
        }
    }
    public static int getCount(Cursor c)
    {
        if(c == null) {
            return 0;
        }
        return c.getCount();
    }
    public static Cursor first(Cursor cursor) {
        if(!isNull(cursor)) {
            cursor.moveToFirst();
        }
        return cursor;
    }
    public static long getHashCode(String[] columns, ContentValues cv)
    {
        long hash = 0;
        for(int i = 0; i < columns.length; i++) {
            String key = columns[i];
            String value = (String)cv.get(key);
            if(Utils.isNull(value))
                continue;
            byte[] ba = value.getBytes();
            for(int j = 0; j < ba.length; j++) {
                hash = (3 * hash + ba[j]);
            }
        }
        return hash;
    }
    public static void debugStringArray(String[] array, String prefix)
    {
        if(!sDebug) return;
        for (String str : array) {
            MyLog.i(prefix + " " + str);
        }
    }
    public static void debugCV(ContentValues cv, String prefix)
    {
        if(!sDebug) return;
        for (Map.Entry<String, Object> entry : cv.valueSet()) {
            MyLog.i(prefix + " " + entry.getKey() + ": " + entry.getValue());
        }
    }
}
