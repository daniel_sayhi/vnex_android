package tw.sayhi.vnex.misc;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.api.RespQueryRemitOrder;
import tw.sayhi.vnex.api.RespRemitOrder;

public class DbOrder
{
    // some values
    public static final String ORDER_FAIL = "0";
    public static final String ORDER_SUCCESS = "1";
    public static final String ORDER_REVIEW = "2";
    public static final String ORDER_PAID = "3";
    public static final String TRANS_SUCCESS = "4";
    public static final String TRANS_FAIL = "5";
    public static final String ORDER_EXPIRED = "100";

    public static final String USER_REVIEWED_FLAG = "user_reviewed_flag";

    public static final String PAY_VNEX = "pay_vnex";
    public static final String PAY_SEVEN = "pay_seven";

    public static final String NOTIFY_NONE = "0";
    public static final String NOTIFY_PENDING = "1";

    // Schema starts here
    public static final String PK = "_id";
    // Remit OrderStatus Status
    public static final String EUSO_ORDER = "euso_order";
    public static final String UUID = "uuid";
    public static final String STATUS = "status";
    public static final String PAY = "pay";
    public static final String NOTIFIED = "notified";
    public static final String CREATE_TIME = "create_time";
    public static final String RO_MESSAGE = "ro_message";
    public static final String ACTIVE = "active";
    public static final String MERCHANT_TRADE_NO = "merchant_trade_no";
    public static final String BARCODE1 = "barcode1";
    public static final String BARCODE2 = "barcode2";
    public static final String BARCODE3 = "barcode3";
    public static final String PAYMENT_NO = "payment_no";
    public static final String PAY_CHANNEL = "pay_channel";
    public static final String UPLOAD_RECEIPT = "upload_receipt";
    public static final String PICKUP_CODE = "pickup_code";
    // Remit OrderStatus Data
    public static final String COMPANY = "company";
    public static final String APID = "apid";
    public static final String DEVICE_ID = "device_id";
    public static final String LANG = "lang";
    public static final String SENDER_NAME = "sender_name";
    public static final String SENDER_ARC = "sender_arc";
    public static final String SENDER_ARC_TERM = "sender_arc_term";
    public static final String SENDER_TEL = "sender_tel";
    public static final String SENDER_MOBILE = "sender_mobile";
    public static final String SENDER_COUNTRY = "sender_country";
    public static final String SENDER_BIRTHDAY = "sender_birthday";
    public static final String RECIPINENT_NAME = "recipinent_name";
    public static final String RECIPINENT_PERSONAL_ID = "recipinent_personal_id";
    public static final String RECIPINENT_TEL = "recipinent_tel";
    public static final String RECIPINENT_ADDRESS1 = "recipinent_address1";
    public static final String RECIPINENT_ADDRESS2 = "recipinent_address2";
    public static final String RECIPINENT_BIRTHDAY_NOTE = "recipinent_birthday_note";
    public static final String BANK_NAME = "bank_name";
    public static final String BANK_BRANCH = "bank_branch";
    public static final String BANK_ACCOUNT = "bank_account";
    public static final String DISCOUNT_FEE = "discount_fee";
    public static final String SERVICE_FEE = "service_fee";
    public static final String AMOUNT = "amount";
    public static final String TOTAL_AMOUNT = "total_amount";
    public static final String TOTAL_AMOUNT_COMMA = "total_amount_comma";
    public static final String EXCHANGE_RATE_FLAG = "exchange_rate_flag";
    public static final String EXCHANGE_RATE = "exchange_rate";
    public static final String REFERENCE_ER = "referenceER";
    public static final String TRANS_TYPE = "trans_type";
    public static final String NOTE = "note";
    public static final String COUPON_CODE = "coupon_code";
    public static final String COUPON_VALUE = "coupon_value";
    public static final String PAYMENT_TYPE = "payment_type";
    public static final String SENDER_INFO_MODIFY = "sender_info_modify";
    public static final String RED_POINT_DISCOUNT = "red_point_discount";
    // others
    public static final String LAST_QUERY_TIME = "last_query_time";
    public static final String REMIT_AGAIN = "remit_again";


    public static final String[] COLUMNS = new String[] {
            PK, EUSO_ORDER, UUID, STATUS, PAY, NOTIFIED, CREATE_TIME, RO_MESSAGE, ACTIVE,
            MERCHANT_TRADE_NO, BARCODE1, BARCODE2, BARCODE3, PAYMENT_NO, PAY_CHANNEL, UPLOAD_RECEIPT, PICKUP_CODE,
            COMPANY, APID, DEVICE_ID, LANG,
            SENDER_NAME, SENDER_ARC, SENDER_ARC_TERM, SENDER_TEL, SENDER_MOBILE, SENDER_COUNTRY, SENDER_BIRTHDAY,
            RECIPINENT_NAME, RECIPINENT_PERSONAL_ID, RECIPINENT_TEL, RECIPINENT_ADDRESS1, RECIPINENT_ADDRESS2,
            RECIPINENT_BIRTHDAY_NOTE,
            BANK_NAME, BANK_BRANCH, BANK_ACCOUNT,
            DISCOUNT_FEE, SERVICE_FEE, AMOUNT, TOTAL_AMOUNT, TOTAL_AMOUNT_COMMA,
            EXCHANGE_RATE_FLAG, EXCHANGE_RATE, REFERENCE_ER, TRANS_TYPE,
            NOTE, COUPON_CODE, COUPON_VALUE,
            PAYMENT_TYPE, SENDER_INFO_MODIFY, LAST_QUERY_TIME, RED_POINT_DISCOUNT,
            DbRemit.RESERVED1,
            DbRemit.PROFILE_TYPE,
            DbRemit.IBON,
            DbRemit.FAMILYMART,
            DbRemit.HILIFE,
            DbRemit.RESERVED6,
            DbRemit.RESERVED7,
            DbRemit.RESERVED8,
            DbRemit.RESERVED9
    };
    public static final String mTableString = " (" +
            PK + " TEXT PRIMARY KEY, " +
            EUSO_ORDER + " TEXT, " +
            UUID + " TEXT, " +
            STATUS + " TEXT, " +
            PAY + " TEXT, " +
            NOTIFIED + " TEXT, " +
            CREATE_TIME + " TEXT, " +
            RO_MESSAGE + " TEXT, " +
            ACTIVE + " TEXT, " +
            MERCHANT_TRADE_NO + " TEXT, " +
            BARCODE1 + " TEXT, " +
            BARCODE2 + " TEXT, " +
            BARCODE3 + " TEXT, " +
            PAYMENT_NO + " TEXT, " +
            PAY_CHANNEL + " TEXT, " +
            UPLOAD_RECEIPT + " TEXT, " +
            PICKUP_CODE + " TEXT, " +
            COMPANY + " TEXT, " +
            APID + " TEXT, " +
            DEVICE_ID + " TEXT, " +
            LANG + " TEXT, " +
            SENDER_NAME + " TEXT, " +
            SENDER_ARC + " TEXT, " +
            SENDER_ARC_TERM + " TEXT, " +
            SENDER_TEL + " TEXT, " +
            SENDER_MOBILE + " TEXT, " +
            SENDER_COUNTRY + " TEXT, " +
            SENDER_BIRTHDAY + " TEXT, " +
            RECIPINENT_NAME + " TEXT, " +
            RECIPINENT_PERSONAL_ID + " TEXT, " +
            RECIPINENT_TEL + " TEXT, " +
            RECIPINENT_ADDRESS1 + " TEXT, " +
            RECIPINENT_ADDRESS2 + " TEXT, " +
            RECIPINENT_BIRTHDAY_NOTE + " TEXT, " +
            BANK_NAME + " TEXT, " +
            BANK_BRANCH + " TEXT, " +
            BANK_ACCOUNT + " TEXT, " +
            DISCOUNT_FEE + " TEXT, " +
            SERVICE_FEE + " TEXT, " +
            AMOUNT + " TEXT, " +
            TOTAL_AMOUNT + " TEXT, " +
            TOTAL_AMOUNT_COMMA + " TEXT, " +
            EXCHANGE_RATE_FLAG + " TEXT, " +
            EXCHANGE_RATE + " TEXT, " +
            REFERENCE_ER + " TEXT, " +
            TRANS_TYPE + " TEXT, " +
            NOTE + " TEXT, " +
            COUPON_CODE + " TEXT, " +
            COUPON_VALUE + " TEXT, " +
            PAYMENT_TYPE + " TEXT, " +
            SENDER_INFO_MODIFY + " TEXT, " +
            LAST_QUERY_TIME + " LONG, " +
            RED_POINT_DISCOUNT + " TEXT, " +
            DbRemit.RESERVED1 + " TEXT, " +
            DbRemit.PROFILE_TYPE + " TEXT, " +
            DbRemit.IBON + " TEXT, " +
            DbRemit.FAMILYMART + " TEXT, " +
            DbRemit.HILIFE + " TEXT, " +
            DbRemit.RESERVED6 + " TEXT, " +
            DbRemit.RESERVED7 + " TEXT, " +
            DbRemit.RESERVED8 + " TEXT, " +
            DbRemit.RESERVED9 + " TEXT " +
            ")";
    private static final String TABLE_NAME = "remit_order";

    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + mTableString);
    }
    public static void delete()
    {
        synchronized (DbRemit.class) {
            DbRemit.sDb.delete(TABLE_NAME, null, null);
        }
    }
    public static void delete(String euso_order)
    {
        synchronized (DbRemit.class) {
            DbRemit.sDb.delete(TABLE_NAME, EUSO_ORDER + "=?", new String[]{euso_order});
        }
    }
    public static Cursor read(String euso_order)
    {
        synchronized (DbRemit.class) {
            Cursor cursor = DbRemit.sDb.query(TABLE_NAME, COLUMNS, EUSO_ORDER + "=?",
                    new String[]{euso_order}, null, null, null);
            return DbRemit.first(cursor);
        }
    }
    public static Cursor readBy(String column, String value, String orderBy)
    {
        if(Utils.isNull(orderBy)) {
            orderBy = CREATE_TIME + " DESC";
        }
        synchronized (DbRemit.class) {
            Cursor cursor = DbRemit.sDb.query(TABLE_NAME, COLUMNS, column + "=?",
                    new String[]{value}, null, null, orderBy);
            return DbRemit.first(cursor);
        }
    }
    public static Cursor readAll(String orderBy)
    {
        DbRemit.sDb.beginTransaction();

        Cursor cursor = DbRemit.sDb.query(TABLE_NAME, COLUMNS, null, null, null,
                null, orderBy);

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
        return DbRemit.first(cursor);
    }
    public static void create(ContentValues cv, String total_amount_comma, RespRemitOrder ro)
    {
        DbRemit.debugStringArray(COLUMNS, "0.");
        DbRemit.debugCV(cv, "1.");
        // remove keys not in DB
        ArrayList<String> no_use = new ArrayList<>(10);
        for (Map.Entry<String, Object> entry : cv.valueSet()) {
            if(!Utils.inArray(COLUMNS, entry.getKey())) {
                no_use.add(entry.getKey());
            }
        }
        for(String key : no_use) {
            cv.remove(key);
        }
        DbRemit.debugCV(cv, "2.");
        cv.put(PK, ro.euso_order);
        cv.put(EUSO_ORDER, ro.euso_order);
        cv.put(UUID, ro.uuid);
        cv.put(STATUS, ro.status);
        cv.put(PAY, "0");
        cv.put(NOTIFIED, NOTIFY_NONE);
        long create_time = System.currentTimeMillis();
        cv.put(CREATE_TIME, Utils.SDF_DASH.format(create_time));
        cv.put(RO_MESSAGE, ro.message);
        cv.put(ACTIVE, "1");
        cv.put(MERCHANT_TRADE_NO, "");
        cv.put(BARCODE1, "");
        cv.put(BARCODE2, "");
        cv.put(BARCODE3, "");
        cv.put(PAYMENT_NO, "");
        cv.put(PAY_CHANNEL, "");
        cv.put(UPLOAD_RECEIPT, "0");
        cv.put(PICKUP_CODE, "");
        // RemitOrder Data
        cv.put(TOTAL_AMOUNT_COMMA, total_amount_comma);
        cv.put(LAST_QUERY_TIME, -1);
        DbRemit.debugCV(cv, "3.");

        DbRemit.sDb.beginTransaction();

        int tmp = DbRemit.sDb.update(TABLE_NAME, cv, PK + "=?",
                new String[] {ro.euso_order});
        if (tmp == 0) {
            DbRemit.sDb.insert(TABLE_NAME, null, cv);
        }

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
    }
    public static void create(String company, String apid, String uuid, String device_id, String lang,
                              String sender_name, String sender_arc, String sender_arc_term, String sender_tel, String sender_birthday,
                              String sender_mobile, String sender_country, String recipinent_name,
                              String recipinent_personal_id, String recipinent_tel, String recipinent_address1,
                              String recipinent_address2, String recipinent_birthday_note, String bank_name,
                              String bank_branch, String bank_account, String discount_fee, String service_fee,
                              String amount, String total_amount, String total_amount_comma,
                              String exchange_rate_flag, String exchange_rate,
                              String referenceER, String trans_type, String note, String coupon_code,
                              String coupon_value, String payment_type, String sender_info_modify,
                              RespRemitOrder ro, String red_point_discount)
    {
        ContentValues cv = new ContentValues();
        // RemitOrder result
        cv.put(PK, ro.euso_order);
        cv.put(EUSO_ORDER, ro.euso_order);
        cv.put(UUID, ro.uuid);
        cv.put(STATUS, ro.status);
        cv.put(PAY, "0");
        cv.put(NOTIFIED, NOTIFY_NONE);
        long create_time = System.currentTimeMillis();
        cv.put(CREATE_TIME, Utils.SDF_DASH.format(create_time));
        cv.put(RO_MESSAGE, ro.message);
        cv.put(ACTIVE, "1");
        cv.put(MERCHANT_TRADE_NO, "");
        cv.put(BARCODE1, "");
        cv.put(BARCODE2, "");
        cv.put(BARCODE3, "");
        cv.put(PAYMENT_NO, "");
        cv.put(PAY_CHANNEL, "");
        cv.put(UPLOAD_RECEIPT, "0");
        cv.put(PICKUP_CODE, "");
        // RemitOrder Data
        cv.put(COMPANY, company);
        cv.put(APID, apid);
        cv.put(DEVICE_ID, device_id);
        cv.put(LANG, lang);
        cv.put(SENDER_NAME, sender_name);
        cv.put(SENDER_ARC, sender_arc);
        cv.put(SENDER_ARC_TERM, sender_arc_term);
        cv.put(SENDER_TEL, sender_tel);
        cv.put(SENDER_MOBILE, sender_mobile);
        cv.put(SENDER_COUNTRY, sender_country);
        cv.put(SENDER_BIRTHDAY, sender_birthday);
        cv.put(RECIPINENT_NAME, recipinent_name);
        cv.put(RECIPINENT_PERSONAL_ID, recipinent_personal_id);
        cv.put(RECIPINENT_TEL, recipinent_tel);
        cv.put(RECIPINENT_ADDRESS1, recipinent_address1);
        cv.put(RECIPINENT_ADDRESS2, recipinent_address2);
        cv.put(RECIPINENT_BIRTHDAY_NOTE, recipinent_birthday_note);
        cv.put(BANK_NAME, bank_name);
        cv.put(BANK_BRANCH, bank_branch);
        cv.put(BANK_ACCOUNT, bank_account);
        cv.put(DISCOUNT_FEE, discount_fee);
        cv.put(SERVICE_FEE, service_fee);
        cv.put(AMOUNT, amount);
        cv.put(TOTAL_AMOUNT, total_amount);
        cv.put(TOTAL_AMOUNT_COMMA, total_amount_comma);
        cv.put(EXCHANGE_RATE_FLAG, exchange_rate_flag);
        cv.put(EXCHANGE_RATE, exchange_rate);
        cv.put(REFERENCE_ER, referenceER);
        cv.put(TRANS_TYPE, trans_type);
        cv.put(NOTE, note);
        cv.put(COUPON_CODE, coupon_code);
        cv.put(COUPON_VALUE, coupon_value);
        cv.put(PAYMENT_TYPE, payment_type);
        cv.put(SENDER_INFO_MODIFY, sender_info_modify);
        cv.put(LAST_QUERY_TIME, -1);
        cv.put(RED_POINT_DISCOUNT, red_point_discount);

        DbRemit.sDb.beginTransaction();

        int tmp = DbRemit.sDb.update(TABLE_NAME, cv, PK + "=?",
                new String[] {ro.euso_order});
        if (tmp == 0) {
            DbRemit.sDb.insert(TABLE_NAME, null, cv);
        }

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
    }
    public static void update(RespQueryRemitOrder ro)
    {
        DbRemit.sDb.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(STATUS, ro.status);
        cv.put(PAY, ro.pay);
        cv.put(PICKUP_CODE, ro.pickup_code);
        cv.put(BARCODE1, ro.barcode1);
        cv.put(BARCODE2, ro.barcode2);
        cv.put(BARCODE3, ro.barcode3);
        cv.put(PAYMENT_NO, ro.paymentNo);
        cv.put(MERCHANT_TRADE_NO, ro.merchantTradeNo);
        cv.put(DbRemit.IBON, ro.iBon);
        cv.put(DbRemit.FAMILYMART, ro.Family);
        cv.put(DbRemit.HILIFE, ro.Hilife);

        DbRemit.sDb.update(TABLE_NAME, cv, EUSO_ORDER + "=?",
                new String[] {ro.euso_odrer});

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
    }
    public static void update(String euso_order, String key, String value)
    {
        DbRemit.sDb.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(key, value);

        int affected_rows = DbRemit.sDb.update(TABLE_NAME, cv, EUSO_ORDER + "=?",
                new String[] {euso_order});

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
    }
    public static void update(String euso_order, String key, long value)
    {
        DbRemit.sDb.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(key, value);

        int affected_rows = DbRemit.sDb.update(TABLE_NAME, cv, EUSO_ORDER + "=?",
                new String[] {euso_order});

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
    }
    public static int update_old(JSONArray ja) throws JSONException
    {
        int count = ja.length();

        DbRemit.sDb.beginTransaction();

        long curr_time = System.currentTimeMillis();
        for(int i = 0; i < ja.length(); i++) {
            JSONObject obj = ja.optJSONObject(i);
            // get basic data
            String euso_odrer = DbRemit.getString(obj, "euso_order");
            String sender_arc = DbRemit.getString(obj, "sender_arc");
            String uuid = DbRemit.getString(obj, "uuid");
            if(Utils.isNull(euso_odrer))
                continue;
            String create_time = DbRemit.getString(obj, "create_time");
            String status = DbRemit.getString(obj, "status");
            long l_create_time = Utils.getTimeStamp(Utils.SDF_DASH, create_time);
            long time_lag = curr_time - l_create_time;
            String pay = DbRemit.getString(obj, "pay");
            if(Pref.isOrderPaid(euso_odrer))
                pay = "1";
            ContentValues cv = new ContentValues();
            // determine status
            if(time_lag > Config.history_duration) {
                delete(euso_odrer);
                continue;
            } else if(pay.equals("1")) {
                status = ORDER_PAID;
                cv.put(ACTIVE, "0");
            } else if(time_lag > Config.payment_duration) {
                status = ORDER_EXPIRED;
                cv.put(ACTIVE, "0");
            }
            cv.put(STATUS, status);
            // RemitOrder result
            cv.put(PK, euso_odrer);
            cv.put(EUSO_ORDER, euso_odrer);
            cv.put(UUID, uuid);
            cv.put(PAY, pay);
            cv.put(NOTIFIED, NOTIFY_NONE);
            cv.put(CREATE_TIME, create_time);
//            cv.put(RO_MESSAGE, ro.message);
//            cv.put(ACTIVE, "1");
//            cv.put(MERCHANT_TRADE_NO, "");
//            cv.put(BARCODE1, "");
//            cv.put(BARCODE2, "");
//            cv.put(BARCODE3, "");
//            cv.put(PAYMENT_NO, "");
//            cv.put(PAY_CHANNEL, "");
//            cv.put(UPLOAD_RECEIPT, "0");
            cv.put(PICKUP_CODE, DbRemit.getString(obj, PICKUP_CODE));
            // RemitOrder Data
//            cv.put(COMPANY, company);
//            cv.put(APID, apid);
//            cv.put(DEVICE_ID, device_id);
//            cv.put(LANG, lang);
            cv.put(SENDER_NAME, DbRemit.getString(obj, "sender_name"));
            cv.put(SENDER_ARC, sender_arc);
            cv.put(SENDER_ARC_TERM, DbRemit.getString(obj, "sender_arc_term"));
//            cv.put(SENDER_TEL, sender_tel);
            cv.put(SENDER_MOBILE, DbRemit.getString(obj, "sender_mobile"));
            cv.put(SENDER_BIRTHDAY, DbRemit.getString(obj, "sender_birthday"));
//            cv.put(SENDER_COUNTRY, sender_country);
            cv.put(RECIPINENT_NAME, DbRemit.getString(obj, "recipinent_name"));
            cv.put(RECIPINENT_PERSONAL_ID, DbRemit.getString(obj, "recipinent_personal_id"));
            cv.put(RECIPINENT_TEL, DbRemit.getString(obj, "recipinent_tel"));
            cv.put(RECIPINENT_ADDRESS1, DbRemit.getString(obj, "recipinent_address1"));
            cv.put(RECIPINENT_ADDRESS2, DbRemit.getString(obj, "recipinent_address2"));
            cv.put(RECIPINENT_BIRTHDAY_NOTE, DbRemit.getString(obj, "recipinent_birthday_note"));
            cv.put(BANK_NAME, DbRemit.getString(obj, "bank_name"));
            cv.put(BANK_BRANCH, DbRemit.getString(obj, "bank_branch"));
            cv.put(BANK_ACCOUNT, DbRemit.getString(obj, "bank_account"));
            cv.put(DISCOUNT_FEE, DbRemit.getString(obj, "discount_fee"));
            cv.put(COUPON_VALUE, DbRemit.getString(obj, "coupon_value"));
            cv.put(SERVICE_FEE, DbRemit.getString(obj, "service_fee"));
            cv.put(AMOUNT, DbRemit.getString(obj, "amount"));
            String total_amount = DbRemit.getString(obj, "total_amount");
            cv.put(TOTAL_AMOUNT, total_amount);
            String commaTotal = String.format(Locale.TAIWAN,"%,.0f", Utils.parseFloat(total_amount));
            cv.put(TOTAL_AMOUNT_COMMA, commaTotal);
            cv.put(EXCHANGE_RATE_FLAG, DbRemit.getString(obj, "exchange_rate_flag"));
            cv.put(EXCHANGE_RATE, DbRemit.getString(obj, "exchange_rate"));
            cv.put(REFERENCE_ER, DbRemit.getString(obj, "referenceER"));
            cv.put(TRANS_TYPE, DbRemit.getString(obj, "trans_type"));
            cv.put(RED_POINT_DISCOUNT, DbRemit.getString(obj, "red_point_discount"));
//            cv.put(NOTE, note);
//            cv.put(COUPON_CODE, coupon_code);
//            cv.put(COUPON_VALUE, coupon_value);
            cv.put(PAYMENT_TYPE, DbRemit.getString(obj, "payment_type"));
//            cv.put(SENDER_INFO_MODIFY, sender_info_modify);
            cv.put(LAST_QUERY_TIME, curr_time);

            String payment_no = "";
            JSONArray ja_pay = obj.optJSONArray("ecPayOrderList");
            if(ja_pay != null) {
                JSONObject jo_pay = ja_pay.optJSONObject(0);
                if (jo_pay != null) {
                    payment_no = jo_pay.optString("paymentNo");
                }
            }
            cv.put(PAYMENT_NO, payment_no);

            int tmp = DbRemit.sDb.update(TABLE_NAME, cv, PK + "=?",
                    new String[]{euso_odrer});
            if (tmp == 0) {
                DbRemit.sDb.insert(TABLE_NAME, null, cv);
            }
        }
        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
        return count;
    }
    public static int update_last(JSONArray ja) throws JSONException
    {
        int count = ja.length();

        for(int i = 0; i < ja.length(); i++) {
            JSONObject obj = ja.optJSONObject(i);
            update(obj);
        }
        return count;
    }
    public static void update(JSONObject obj) throws JSONException
    {
        long curr_time = System.currentTimeMillis();
        // get basic data
        String euso_odrer = DbRemit.getString(obj, "euso_order");
        String sender_arc = DbRemit.getString(obj, "sender_arc");
        String uuid = DbRemit.getString(obj, "uuid");
        if(Utils.isNull(euso_odrer))
            return;
        String create_time = DbRemit.getString(obj, "create_time");
        long l_create_time = Utils.getTimeStamp(Utils.SDF_DASH, create_time);
        long time_lag = curr_time - l_create_time;
        String pay = DbRemit.getString(obj, "pay");
        String remittance_order = DbRemit.getString(obj, "remittance_order");
        String trans_code = DbRemit.getString(obj, "trans_code");
        if(Pref.isOrderPaid(euso_odrer))
            pay = "1";
        ContentValues cv = new ContentValues();
        // determine status
        if(time_lag > Config.history_duration) {
            delete(euso_odrer);
            return;
        }
        String status = DbRemit.getString(obj, "status");
        cv.put(ACTIVE, "1");
        if(pay.equals("1")) {
            status = ORDER_PAID;
            if(!Utils.isNull(trans_code)) {
                if(trans_code.equals("0")) {
                    // waiting for transaction
                } else if(trans_code.equals("1")) {
                    status = TRANS_SUCCESS;
                    cv.put(ACTIVE, "0");
                } else { // 9 : 已退款 20 : 待再次交易 -99100 : 匯款失敗, 待再次處理 99100 : 匯款資料已修正
                    status = TRANS_FAIL;
                    cv.put(ACTIVE, "0");
                }
            } else if(!Utils.isNull(remittance_order)) {
                status = TRANS_SUCCESS;
                cv.put(ACTIVE, "0");
            }
        } else { // not paid
            if(time_lag > Config.payment_duration) {
                status = ORDER_EXPIRED;
                cv.put(ACTIVE, "0");
            }
        }
        cv.put(STATUS, status);
        // RemitOrder result
        cv.put(PK, euso_odrer);
        cv.put(EUSO_ORDER, euso_odrer);
        cv.put(UUID, uuid);
        cv.put(PAY, pay);
        cv.put(NOTIFIED, NOTIFY_NONE);
        cv.put(CREATE_TIME, create_time);
//            cv.put(RO_MESSAGE, ro.message);
//            cv.put(ACTIVE, "1");
//            cv.put(MERCHANT_TRADE_NO, "");
//            cv.put(BARCODE1, "");
//            cv.put(BARCODE2, "");
//            cv.put(BARCODE3, "");
//            cv.put(PAYMENT_NO, "");
//            cv.put(PAY_CHANNEL, "");
//            cv.put(UPLOAD_RECEIPT, "0");
//            cv.put(PICKUP_CODE, "");
        // RemitOrder Data
//            cv.put(COMPANY, company);
//            cv.put(APID, apid);
//            cv.put(DEVICE_ID, device_id);
//            cv.put(LANG, lang);
        cv.put(SENDER_NAME, DbRemit.getString(obj, "sender_name"));
        cv.put(SENDER_ARC, sender_arc);
        cv.put(SENDER_ARC_TERM, DbRemit.getString(obj, "sender_arc_term"));
//            cv.put(SENDER_TEL, sender_tel);
        cv.put(SENDER_MOBILE, DbRemit.getString(obj, "sender_mobile"));
        cv.put(SENDER_BIRTHDAY, DbRemit.getString(obj, "sender_birthday"));
//            cv.put(SENDER_COUNTRY, sender_country);
        cv.put(RECIPINENT_NAME, DbRemit.getString(obj, "recipinent_name"));
        cv.put(RECIPINENT_PERSONAL_ID, DbRemit.getString(obj, "recipinent_personal_id"));
        cv.put(RECIPINENT_TEL, DbRemit.getString(obj, "recipinent_tel"));
//            cv.put(RECIPINENT_ADDRESS1, DbRemit.getString(obj, "recipinent_address1"));
//            cv.put(RECIPINENT_ADDRESS2, recipinent_address2);
//            cv.put(RECIPINENT_BIRTHDAY_NOTE, DbRemit.getString(obj, "recipinent_birthday_note"));
        cv.put(BANK_NAME, DbRemit.getString(obj, "bank_name"));
        cv.put(BANK_BRANCH, DbRemit.getString(obj, "bank_branch"));
        cv.put(BANK_ACCOUNT, DbRemit.getString(obj, "bank_account"));
        cv.put(DISCOUNT_FEE, DbRemit.getString(obj, "discount_fee"));
        cv.put(COUPON_VALUE, DbRemit.getString(obj, "coupon_value"));
        cv.put(SERVICE_FEE, DbRemit.getString(obj, "service_fee"));
        cv.put(AMOUNT, DbRemit.getString(obj, "amount"));
        String total_amount = DbRemit.getString(obj, "total_amount");
        cv.put(TOTAL_AMOUNT, total_amount);
        String commaTotal = String.format(Locale.TAIWAN,"%,.0f", Utils.parseFloat(total_amount));
        cv.put(TOTAL_AMOUNT_COMMA, commaTotal);
        cv.put(EXCHANGE_RATE_FLAG, DbRemit.getString(obj, "exchange_rate_flag"));
        cv.put(EXCHANGE_RATE, DbRemit.getString(obj, "exchange_rate"));
        cv.put(REFERENCE_ER, DbRemit.getString(obj, "referenceER"));
        cv.put(TRANS_TYPE, DbRemit.getString(obj, "trans_type"));
        cv.put(RED_POINT_DISCOUNT, DbRemit.getString(obj, "red_point_discount"));
//            cv.put(NOTE, note);
//            cv.put(COUPON_CODE, coupon_code);
//            cv.put(COUPON_VALUE, coupon_value);
        cv.put(PAYMENT_TYPE, DbRemit.getString(obj, "payment_type"));
//            cv.put(SENDER_INFO_MODIFY, sender_info_modify);
        cv.put(LAST_QUERY_TIME, curr_time);

        String payment_no = "";
        JSONArray ja_pay = obj.optJSONArray("ecPayOrderList");
        if(ja_pay != null) {
            JSONObject jo_pay = ja_pay.optJSONObject(0);
            if (jo_pay != null) {
                payment_no = jo_pay.optString("paymentNo");
            }
        }
        cv.put(PAYMENT_NO, payment_no);

        DbRemit.sDb.beginTransaction();

        int tmp = DbRemit.sDb.update(TABLE_NAME, cv, PK + "=?",
                new String[]{euso_odrer});
        if (tmp == 0) {
            DbRemit.sDb.insert(TABLE_NAME, null, cv);
        }
        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
        return;
    }
    // Refer to DB.createRemitOrderCore()
    public static void setPref(String euso_order)
    {
        Cursor cursor = read(euso_order);
        OrderData os = new OrderData(cursor);
        PayInfo.trans_type = os.trans_type;
        Pref.set(PayInfo.PAYEE_NAME, os.name);
        Pref.set(PayInfo.PAYEE_ID, os.id);
        Pref.set(PayInfo.PAYEE_PHONE, os.phone);
//        Pref.set(PayInfo.PAYEE_ADDRESS, os.address);
        Pref.set(PayInfo.PAYEE_BIRTHDAY, os.birthday);
        Pref.set(PayInfo.BANK_NAME, os.bank_name);
        Pref.set(PayInfo.BANK_BRANCH, os.bank_branch);
        Pref.set(PayInfo.BANK_ACCOUNT, os.bank_account);
        DbRemit.close(cursor);
    }
    public static void clearPayeeGlobal()
    {
        Pref.set(PayInfo.PAYEE_NAME, "");
        Pref.set(PayInfo.PAYEE_BIRTHDAY, "");
        Pref.set(PayInfo.PAYEE_PHONE, "");
        Pref.set(PayInfo.PAYEE_ID, "");
        Pref.set(PayInfo.BANK_NAME, "");
        Pref.set(PayInfo.BANK_BRANCH, "");
        Pref.set(PayInfo.BANK_ACCOUNT, "");
        Pref.set(PayInfo.PAYEE_AREA, "");
        Pref.set(PayInfo.PAYEE_PROVINCE, "");
        Pref.set(PayInfo.PAYEE_CITY, "");
        Pref.set(PayInfo.PAYEE_ADDRESS, "");
        Pref.set(PayInfo.PAYEE_AREA_VI, "");
    }
    public static boolean setPayeeGlobal(Activity act, String sender_arc, String euso_order)
    {
        Cursor c = null;
        if(!Utils.isNull(sender_arc)) {
            c = DbOrder.readBy(SENDER_ARC, sender_arc, CREATE_TIME + " DESC");
        } else if(!Utils.isNull(euso_order)) {
            c = DbOrder.read(euso_order);
        }
        if(DbRemit.isNull(c)) {
            clearPayeeGlobal();
            return true;
        }
        // set payee data according to last order
        PayInfo.trans_type = DbRemit.getString(c, DbOrder.TRANS_TYPE);
        Pref.set(PayInfo.PAYEE_NAME, DbRemit.getString(c, DbOrder.RECIPINENT_NAME));
        Pref.set(PayInfo.PAYEE_BIRTHDAY, DbRemit.getString(c, DbOrder.RECIPINENT_BIRTHDAY_NOTE));
        Pref.set(PayInfo.PAYEE_PHONE, DbRemit.getString(c, DbOrder.RECIPINENT_TEL));
        Pref.set(PayInfo.PAYEE_ID, DbRemit.getString(c, DbOrder.RECIPINENT_PERSONAL_ID));
        Pref.set(PayInfo.BANK_NAME, DbRemit.getString(c, DbOrder.BANK_NAME));
        Pref.set(PayInfo.BANK_BRANCH, DbRemit.getString(c, DbOrder.BANK_BRANCH));
        Pref.set(PayInfo.BANK_ACCOUNT, DbRemit.getString(c, DbOrder.BANK_ACCOUNT));
        String address1 = DbRemit.getString(c, DbOrder.RECIPINENT_ADDRESS1);
        String[] addr = address1.split("\\|\\|\\|");
        if(addr.length < 3) {
            return false;
        }
        Pref.set(PayInfo.PAYEE_AREA, addr[0]);
        Pref.set(PayInfo.PAYEE_PROVINCE, addr[1]);
        Pref.set(PayInfo.PAYEE_CITY, addr[2]);
        Pref.set(PayInfo.PAYEE_ADDRESS, DbRemit.getString(c, DbOrder.RECIPINENT_ADDRESS2));
        String payee_area_vi = "";
        switch (addr[0]) {
            case "NORTH":
                payee_area_vi = act.getString(R.string.north);
                break;
            case "MIDDLE":
                payee_area_vi = act.getString(R.string.middle);
                break;
            case "SOUTH":
                payee_area_vi = act.getString(R.string.south);
                break;
        }
        Pref.set(PayInfo.PAYEE_AREA_VI, payee_area_vi);
        return true;
    }
    public static ContentValues getOrderValues(JSONObject obj)
    {
        ContentValues cv = new ContentValues();

        long curr_time = System.currentTimeMillis();
        String euso_odrer = DbRemit.getString(obj, "euso_order");
        String sender_arc = DbRemit.getString(obj, "sender_arc");
        String uuid = DbRemit.getString(obj, "uuid");
        String create_time = DbRemit.getString(obj, "create_time");
        long l_create_time = Utils.getTimeStamp(Utils.SDF_DASH, create_time);
        long time_lag = curr_time - l_create_time;
        String pay = DbRemit.getString(obj, "pay");
        String remittance_order = DbRemit.getString(obj, "remittance_order");
        String trans_code = DbRemit.getString(obj, "trans_code");
        String fake_status = QueryApiService.getPref(euso_odrer);
        switch (fake_status)
        {
            case ORDER_PAID:
            case TRANS_SUCCESS:
            case TRANS_FAIL:
                pay = "1";
                break;
        }
        // determine status
        String status = DbRemit.getString(obj, "status");
//            String status = ORDER_SUCCESS;
        if(Utils.nonNull(fake_status)) {
            if(!status.equals(fake_status)) {
                MyLog.e("status changed from " +  status + " to " + fake_status);
            }
            status = fake_status;
        } else {
            if(pay.equals("1")) {
                status = ORDER_PAID;
                if(!Utils.isNull(trans_code)) {
                    if(trans_code.equals("0")) {
                        // waiting for transaction
                    } else if(trans_code.equals("1")) {
                        status = TRANS_SUCCESS;
                    } else { // 9 : 已退款 20 : 待再次交易 -99100 : 匯款失敗, 待再次處理 99100 : 匯款資料已修正
                        status = TRANS_FAIL;
                    }
                } else if(!Utils.isNull(remittance_order)) {
                    status = TRANS_SUCCESS;
                }
            } else { // not paid
                if(time_lag > Config.payment_duration) {
                    status = ORDER_EXPIRED;
                }
            }
        }
//        MyLog.i("status = " + status);
        switch (status)
        {
            case ORDER_FAIL:
            case TRANS_SUCCESS:
            case TRANS_FAIL:
            case ORDER_EXPIRED:
                cv.put(ACTIVE, "0");
                break;
            default:
                cv.put(ACTIVE, "1");
//                MyLog.e("euso_order " + euso_odrer + " is active.");
                break;
        }
        cv.put(STATUS, status);
        // RemitOrder result
        cv.put(PK, euso_odrer);
        cv.put(EUSO_ORDER, euso_odrer);
        cv.put(UUID, uuid);
        cv.put(PAY, pay);
        cv.put(NOTIFIED, NOTIFY_NONE);
        cv.put(CREATE_TIME, create_time);
//            cv.put(RO_MESSAGE, ro.message);
//            cv.put(ACTIVE, "1");
//            cv.put(MERCHANT_TRADE_NO, "");
//            cv.put(BARCODE1, "");
//            cv.put(BARCODE2, "");
//            cv.put(BARCODE3, "");
//            cv.put(PAYMENT_NO, "");
//            cv.put(PAY_CHANNEL, "");
//            cv.put(UPLOAD_RECEIPT, "0");
//            cv.put(PICKUP_CODE, "");
        // RemitOrder Data
//            cv.put(COMPANY, company);
//            cv.put(APID, apid);
//            cv.put(DEVICE_ID, device_id);
//            cv.put(LANG, lang);
        cv.put(SENDER_NAME, DbRemit.getString(obj, "sender_name"));
        cv.put(SENDER_ARC, sender_arc);
        cv.put(SENDER_ARC_TERM, DbRemit.getString(obj, "sender_arc_term"));
//            cv.put(SENDER_TEL, sender_tel);
        cv.put(SENDER_MOBILE, DbRemit.getString(obj, "sender_mobile"));
//            cv.put(SENDER_BIRTHDAY, DbRemit.getString(obj, "sender_birthday"));
//            cv.put(SENDER_COUNTRY, sender_country);
        cv.put(RECIPINENT_NAME, DbRemit.getString(obj, "recipinent_name"));
        cv.put(RECIPINENT_PERSONAL_ID, DbRemit.getString(obj, "recipinent_personal_id"));
        cv.put(RECIPINENT_TEL, DbRemit.getString(obj, "recipinent_tel"));
//            cv.put(RECIPINENT_ADDRESS1, DbRemit.getString(obj, "recipinent_address1"));
//            cv.put(RECIPINENT_ADDRESS2, recipinent_address2);
//            cv.put(RECIPINENT_BIRTHDAY_NOTE, DbRemit.getString(obj, "recipinent_birthday_note"));
        cv.put(BANK_NAME, DbRemit.getString(obj, "bank_name"));
        cv.put(BANK_BRANCH, DbRemit.getString(obj, "bank_branch"));
        cv.put(BANK_ACCOUNT, DbRemit.getString(obj, "bank_account"));
        cv.put(DISCOUNT_FEE, DbRemit.getString(obj, "discount_fee"));
        cv.put(COUPON_VALUE, DbRemit.getString(obj, "coupon_value"));
        cv.put(SERVICE_FEE, DbRemit.getString(obj, "service_fee"));
        cv.put(AMOUNT, DbRemit.getString(obj, "amount"));
        String total_amount = DbRemit.getString(obj, "total_amount");
        cv.put(TOTAL_AMOUNT, total_amount);
        String commaTotal = String.format(Locale.TAIWAN,"%,.0f", Utils.parseFloat(total_amount));
        cv.put(TOTAL_AMOUNT_COMMA, commaTotal);
        cv.put(EXCHANGE_RATE_FLAG, DbRemit.getString(obj, "exchange_rate_flag"));
        cv.put(EXCHANGE_RATE, DbRemit.getString(obj, "exchange_rate"));
        cv.put(REFERENCE_ER, DbRemit.getString(obj, "referenceER"));
        cv.put(TRANS_TYPE, DbRemit.getString(obj, "trans_type"));
        cv.put(DbOrder.RED_POINT_DISCOUNT, DbRemit.getString(obj, "red_point_discount"));
//            cv.put(NOTE, note);
//            cv.put(COUPON_CODE, coupon_code);
//            cv.put(COUPON_VALUE, coupon_value);
        cv.put(PAYMENT_TYPE, DbRemit.getString(obj, "payment_type"));
//            cv.put(SENDER_INFO_MODIFY, sender_info_modify);
        cv.put(LAST_QUERY_TIME, curr_time);

        String payment_no = "";
        JSONArray ja_pay = obj.optJSONArray("ecPayOrderList");
        if(ja_pay != null) {
            JSONObject jo_pay = ja_pay.optJSONObject(0);
            if (jo_pay != null) {
                payment_no = jo_pay.optString("paymentNo");
            }
        }
        cv.put(PAYMENT_NO, payment_no);

        return cv;
    }
    // Returns whether there is a pending order or not
    public static boolean update(JSONArray ja) throws JSONException
    {
        boolean bPendingOrder = false;

        DbRemit.sDb.beginTransaction();

        for(int i = 0; i < ja.length(); i++) {
            JSONObject obj = ja.optJSONObject(i);
            // get basic data

            ContentValues cv = getOrderValues(obj);

            if(cv.get(ACTIVE).equals("1")) {
                bPendingOrder = true;
            }

            String euso_odrer = DbRemit.getString(obj, EUSO_ORDER);
            int tmp = DbRemit.sDb.update(TABLE_NAME, cv, PK + "=?",
                    new String[]{euso_odrer});
            if (tmp == 0) {
                DbRemit.sDb.insert(TABLE_NAME, null, cv);
            }
        }
        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
        return bPendingOrder;
    }
    public static int getStatusNotify(String status)
    {
        int id_status = -1;
        switch (status)
        {
            case ORDER_PAID:
                id_status = R.string.order_paid;
                break;
            case TRANS_SUCCESS:
                id_status = R.string.trans_succeed;
                break;
            case TRANS_FAIL:
                id_status = R.string.trans_fail;
                break;
            default:
                break;
        }
        return id_status;
    }
}
