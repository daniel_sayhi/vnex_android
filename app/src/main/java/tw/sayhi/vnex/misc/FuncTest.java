package tw.sayhi.vnex.misc;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.widget.DatePicker;

import java.util.Calendar;

import tw.sayhi.vnex.PayInfo;

public class FuncTest
{
    public static void go(Activity act)
    {
//        normalizeVietText();
    }
    public static void normalizeVietText()
    {
        // -60, -112
        String s = "boôơaâăêeuưiyrlcdóồớáấắềéụứíýnhsđòộờàầằếèúừìỳmtxgõổởãẫẵễẽũữĩỷvkpỏỗợảẩẳểẻủ ửỉỹ₫qọốỡạậặệẹùựịỵ";
        String normal = PayInfo.normVietText("Phí phục vụ NTD:");
        byte[] ba = normal.getBytes();
        byte[] ba2 = new byte[ba.length - 1];
        ba2[0] = 'D';
        for(int i = 2; i < ba.length; i++) {
            ba2[i - 1] = ba[i];
        }
        String str = null;
        try {
            str = new String(ba2, "UTF-8");
        } catch (Exception e)
        {
        }
        String str2 = str;
//        String strip = PayInfo.stripDiacritics("Đóng tiền");
        String lower = PayInfo.normVietText( "đóng tiền");
//        String strip = StringUtils.stripAccents("Đóng tiền");
        String up = "Đóng tiền".toLowerCase();
    }
    public static void dateDialog(Activity act)
    {
        Calendar newCalendar = Calendar.getInstance();
        int year = newCalendar.get(Calendar.YEAR);
        int month = newCalendar.get(Calendar.MONTH);
        int day = newCalendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(act, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
//                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.show();
    }
}
