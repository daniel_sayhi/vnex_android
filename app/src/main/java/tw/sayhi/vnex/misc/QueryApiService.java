package tw.sayhi.vnex.misc;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Messenger;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.BuildConfig;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.DebugOptions;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.api.DB2;
import tw.sayhi.vnex.api.EusoApi;

import static android.app.PendingIntent.FLAG_IMMUTABLE;
import static tw.sayhi.vnex.misc.DbOrder.ORDER_PAID;
import static tw.sayhi.vnex.misc.DbOrder.TRANS_FAIL;
import static tw.sayhi.vnex.misc.DbOrder.TRANS_SUCCESS;

public class QueryApiService extends Service
{
    Service mService = this;
    public static boolean sDebug = true;
    public static boolean bDoQueryService = true;
    public static final String USE_TEST_URL = "USE_TEST_URL";
    public static final String HAS_PENDING_ORDER = "HAS_PENDING_ORDER";
    public static final String ORDER_STATUS_CHANGED = "order_status_changed";
    public static final String QUERY_TRANS_HISTORY = "query_trans_history";
    public static final String  FAKE_REVIEW_STATUS = "fake_review_status";
    public static final String  NOTIFICATION_MESSAGE = "notification_message";
    // Constants: should not change in other classes
    public static int TIME_INTERVAL = 1 * 60 * 1000; // every 1 minute
    public static int MIN_QUERY_INTERVAL = 1 * 60 * 1000; // 1 minute
    public static int SKIP_INTERVAL = 10 * 60 * 1000; // start querying after 10 minutes
    public static int QUERY_INTERVAL = 5 * 60 * 1000; // every 5 minutes
    // For Messenger
    public static final int QUERY_STATUS = 1;
    public static final int QUERY_REMIT_ORDER = 2;
    public static final int QUERY_ALL_REMIT_ORDER = 3;
    public static final int QUERY_STATUS_AND_ALL_ORDER = 4;
    public static final int QUERY_STATUS_AND_ORDER = 5;
    private static Messenger mMessenger;
    private HandlerThread mHandlerThread;
    private Handler mHandler;
    public static long lastQueryProfileSuccessful = 0;
    public static long lastQueryAllRemitOrderSuccessful = 0;
    public static SharedPreferences mPref;
    public static boolean firstTime = true;
    @Override
    public void onCreate()
    {
        super.onCreate();

        mPref = PreferenceManager.getDefaultSharedPreferences(this);
        String use_test_url = getPref(USE_TEST_URL);
        if(Utils.isNull(use_test_url)) {
            use_test_url = (DebugOptions.bUseTestURL ? "1" : "0");
            setPref(USE_TEST_URL, use_test_url);
        }
        DbRemit.init(this);
        if(BuildConfig.DEBUG) {
            TIME_INTERVAL = 35 * 1000;
            SKIP_INTERVAL = 15 * 1000;
            QUERY_INTERVAL = 11 * 1000;
            MIN_QUERY_INTERVAL = 20 * 1000;
        }
        MyLog.e("MIN_QUERY_INTERVAL = " + MIN_QUERY_INTERVAL / 1000 + " seconds");
        MyLog.e("QUERY_INTERVAL = " + QUERY_INTERVAL / 1000 + " seconds");
        new Thread() {
            @Override
            public void run() {
//                MyLog.i("sender_arc = " + getPref(DbSender.ARC_NO));
                while(true) {
                    if(Utils.nonNull(getPref(DbSender.ARC_NO))) {
                        break;
                    }
                    Utils.sleep(500);
                }
//                MyLog.i("sender_arc = " + getPref(DbSender.ARC_NO));
//                MyLog.i("Do service = " + (bDoQueryService ? "YES" : "NO"));
                while(bDoQueryService) {
                    String sender_arc = getPref(DbSender.ARC_NO);
                    String uuid = getPref(DbSender.UUID);
                    if(Utils.nonNull(sender_arc)) {
                        String use_test_url = getPref(USE_TEST_URL);
                        EusoApi.init(null, use_test_url.equals("1"));
                        OnQueryAllRemitOrder(mService, uuid, true);
                        onQueryStatus(mService, sender_arc, true);
                    }
                    Utils.sleep(QUERY_INTERVAL);
                }
            }
        }.start();
    }
    @Override
    public void onDestroy() {
        if(mHandlerThread != null) {
            mHandlerThread.quit();
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
//        return mMessenger.getBinder();
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MyLog.e("onStartCommand()");
        return START_STICKY;
    }
    @Override
    public void onTaskRemoved(Intent rootIntent)
    {
        try {
            _onTaskRemoved(rootIntent);
        } catch (Exception e) {
            DB.reportException("QueryApiService_onTaskRemoved", e);
        }
    }
    public void _onTaskRemoved(Intent rootIntent)
    {
        MyLog.e("");
        Intent restartService = new Intent(getApplicationContext(),
                this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT | FLAG_IMMUTABLE);

        //Restart the service once it has been killed android

        AlarmManager alarmService = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() +100, restartServicePI);
    }
    @Override
    public void onStart(Intent intent, int startid)
    {
        MyLog.e("onStart()");
    }
    public synchronized static void onQueryStatus(Context context, String sender_arc, boolean notify)
    {
        DbSender.Data data = DbSender.read(DbSender.ARC_NO, sender_arc);
        if(data == null) {
            MyLog.i("data == null");
            return;
        }
        if(data.status.equals(DbSender.SENDER_REGISTERED) ||
                data.status.equals(DbSender.SENDER_EDITED)) {
            long curr_time = System.currentTimeMillis();
            if((curr_time - data.last_query_time) < MIN_QUERY_INTERVAL ||
                    (curr_time - data.create_time) < SKIP_INTERVAL) {
                MyLog.i("Skip query since not long passed.");
                return;
            }
            DB.queryStatus(null, data.sender_arc, data.uuid, data.password);
            if(DB.bQueryStatus == false) {
                return;
            }
            curr_time = System.currentTimeMillis();
            DbSender.update(sender_arc, DbSender.LAST_QUERY_TIME, curr_time);
            MyLog.i("    now status = " + DbSender.mStatusMap.get(DB.querySenderStatus.status));
            // update Database and Globals
            boolean status_changed = !data.status.equals(DB.querySenderStatus.status);
            if(status_changed) {
                DbSender.update(sender_arc, DbSender.STATUS, DB.querySenderStatus.status);
                DB.setStatus(DB.querySenderStatus.status);
                setPref(sender_arc + DbSender.STATUS, DB.querySenderStatus.status);
            } else {
                setPref(sender_arc + DbSender.STATUS, "");
            }
            String title = context.getString(R.string.app_name) + " Arc No. " + data.sender_arc;
            switch(DB.querySenderStatus.status) {
                case DbSender.SENDER_EDITED:
                case DbSender.SENDER_REGISTERED:
                    break;
                case DbSender.SENDER_SUCCESS:
                    if(notify) {
                        BaseActivity.createNotification(context, title,
                                context.getString(R.string.notify_profile_verified), createPendingIntent(context));
                    }
                    break;
                case DbSender.SENDER_FAIL:
                    if(notify) {
                        BaseActivity.createNotification(context, title,
                                context.getString(R.string.review_fail), createPendingIntent(context));
                    }
                    break;
                case DbSender.SENDER_DELETE:
                    break;
                default:
                    break;
            }
        } else {
            MyLog.i("data is not edited.");
        }
    }
    public static void createNotification(Context context, JSONArray ja)
    {
        // Find out the orders got paid in this query.
        final OrderData data = new OrderData();
        for(int i = 0; i < ja.length(); i++) {
            JSONObject obj = ja.optJSONObject(i);
            String euso_odrer = DbRemit.getString(obj, "euso_order");
            String pay = DbRemit.getString(obj, "pay");
            String fake_status = QueryApiService.getPref(euso_odrer);
            switch (fake_status)
            {
                case ORDER_PAID:
                case TRANS_SUCCESS:
                case TRANS_FAIL:
                    pay = "1";
                    MyLog.e("Fake order status:" + euso_odrer + " " + fake_status);
                    break;
            }
            if(/*Pref.isDeletedEusoOrder(euso_odrer) || isDeletedEusoOrder(euso_odrer) ||*/ !pay.equals("1"))
                continue;
            Cursor c = DbOrder.read(euso_odrer);
            if(DbRemit.isNull(c))
                continue;
            data.setData(c); // data is old
            ContentValues cv = DbOrder.getOrderValues(obj);
            boolean prev_active = data.active.equals("1");
            boolean curr_active = cv.get(DbOrder.ACTIVE).equals("1");
            if(data.pay.equals("0") || (prev_active && !curr_active))
            {
                String curr_status = (String)cv.get(DbOrder.STATUS);
                int id_status = DbOrder.getStatusNotify(curr_status);
                if(id_status > 0) {
                    addOrderStatusChanged(euso_odrer);
                    String message = euso_odrer + " " + context.getString(id_status);
                    setPref(euso_odrer + NOTIFICATION_MESSAGE, message);
                    MyLog.e(message);
                    BaseActivity.createNotification(context, context.getString(R.string.app_name), message,
                            createPendingIntent(context));
                }
            }
            DbRemit.close(c);
        }
    }
    public static synchronized boolean OnQueryAllRemitOrder(Context context, String uuid, boolean is_background)
    {
        // Don't try again when last query is successful and not long ago (1 min)
        long curr_time = System.currentTimeMillis();
        if (curr_time - lastQueryAllRemitOrderSuccessful < MIN_QUERY_INTERVAL) {
            MyLog.i("Skip query since not long passed.");
            return false;
        }
        boolean has_pending_order = getPref(HAS_PENDING_ORDER).equals("1");
        if((lastQueryAllRemitOrderSuccessful > 0) && is_background && !has_pending_order) {
            MyLog.i("No pending orders.");
            return false;
        }
        DB2 db2 = new DB2();
        db2.queryAllRemitOrder(null, uuid);
        if(!db2.bAllRemitOrder)
            return false;
        lastQueryAllRemitOrderSuccessful = System.currentTimeMillis();
        JSONArray ja = db2.allRemitOrder.jo.optJSONArray(db2.allRemitOrder.response_text);
        if(ja == null)
            return false;
        if(is_background) {
            createNotification(context, ja);
        }
        try {
            boolean pending_order = DbOrder.update(ja);
            setPref(HAS_PENDING_ORDER, (pending_order ? "1" : "0"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return true;
    }
    // NOTE: Make sure "pay" is "0" before calling this routine
    public static void orderPaid(OrderData os, String sender_arc, String uuid)
    {
        DbOrder.update(os.euso_order, DbOrder.ACTIVE, "0");
        DbOrder.update(os.euso_order, DbOrder.PAY, "1");
        DbOrder.update(os.euso_order, DbOrder.STATUS, ORDER_PAID);
    }
    /*
        public static void addDownloadPoint(DbSender.Data sender)
        {
            if(sender == null || !sender.creator || sender.download_point_used)
                return;
            DB.adjustRedPoint(null, sender.arc_no, sender.uuid, PayInfo.RED_POINT_DOWNLOAD);
            if(DB.bAdjustRedPoint) {
                DbSender.update(sender.arc_no, DbSender.DOWNLOAD_POINT_USED, "1");
            }
        }
    */
    public static boolean canQuery(long create_time, long last_query_time, long curr_time)
    {
        if(last_query_time < 0) { // not yet queried
            return ((curr_time - create_time) > SKIP_INTERVAL);
        } else {
            return ((curr_time - last_query_time) > QUERY_INTERVAL);
        }
    }
    void showDialog(int title_id, int body_id)
    {
/*
        boolean allow = (ContextCompat.checkSelfPermission(this, Manifest.permission.SYSTEM_ALERT_WINDOW)
                == PackageManager.PERMISSION_GRANTED);
        if(!allow) return;
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(getString(title_id))
                .setMessage(getString(body_id))
                .create();

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alertDialog.show();
*/
    }
    public static void setPref(String key, String value) {
        synchronized (QueryApiService.class) {
            mPref.edit().putString(key, value).commit();
        }
    }
    public static String getPref(String key) {
        String value = "";
        synchronized (QueryApiService.class) {
            value = mPref.getString(key, "");
        }
        return value;
    }
    public static void login(String sender_arc, String uuid)
    {
        setPref(DbSender.ARC_NO, sender_arc);
        setPref(DbSender.UUID, uuid);
        setPref(HAS_PENDING_ORDER, "1");
    }
    public static void logout()
    {
        setPref(DbSender.ARC_NO, "");
        setPref(DbSender.UUID, "");
        setPref(HAS_PENDING_ORDER, "0");
    }
    public static void addOrderStatusChanged(String euso_order)
    {
        if(isOrderStatusChanged(euso_order))
            return;
        String paid = getPref(ORDER_STATUS_CHANGED);
        if(Utils.isNull(paid)) {
            paid = euso_order;
        } else {
            paid = (paid + " " + euso_order);
        }
        setPref(ORDER_STATUS_CHANGED, paid);
    }
    public static boolean isOrderStatusChanged(String euso_order)
    {
        String paid = getPref(ORDER_STATUS_CHANGED);
        return paid.contains(euso_order);
    }
    public static String getOrderStatusChanged()
    {
        String paid = getPref(ORDER_STATUS_CHANGED);
        if(Utils.isNull(paid)) {
            return "";
        }
        String[] orders = paid.split(" ");
        String new_paid = "";
        for(int i = 1; i < orders.length; i++) {
            if(Utils.isNull(new_paid)) {
                new_paid = orders[i];
            } else {
                new_paid = (new_paid + " " + orders[i]);
            }
        }
        setPref(ORDER_STATUS_CHANGED, new_paid);
        return orders[0];
    }
    public static void setFakeStatus(String status)
    {
        setPref(FAKE_REVIEW_STATUS, status);
    }
    public static String getFakeStatus()
    {
        return getPref(FAKE_REVIEW_STATUS);
    }
    public static boolean needQueryAllRemitOrder(String sender_arc)
    {
        if(lastQueryAllRemitOrderSuccessful == 0)
            return true;
        final Cursor c = DbOrder.readAll(DbOrder.CREATE_TIME + " DESC");
        if (DbRemit.isNull(c)) {
            DbRemit.close(c);
            return false;
        }
        final OrderData os = new OrderData();
        boolean has_active_order = false;
        for(int i = 0; i < c.getCount(); i++) {
            c.moveToPosition(i);
            os.setData(c);
            if(!os.sender_arc.equals(sender_arc))
                continue;
            long curr_time = System.currentTimeMillis();
            long time_passed = curr_time - os.l_create_time;
            if(os.active.equals("0")) {
                if(time_passed > Config.history_duration) { // 3 months
                    DbOrder.delete(os.euso_order);
                }
                continue;
            } else { // ACTIVE order
                has_active_order = true;
                break;
            }
        }
        DbRemit.close(c);
        return has_active_order;
    }
    public static PendingIntent createPendingIntent(Context context)
    {

        Intent intent;
        String package_name = context.getPackageName();
        if(Utils.isAppRunning(context, package_name) && BaseActivity.mClass != null) {
            intent = new Intent(context, BaseActivity.mClass);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        } else {
            intent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        return pendingIntent;
    }
}
