package tw.sayhi.vnex.misc;

import android.location.Location;

import java.util.ArrayList;

public class EECStore
{
    public class Store
    {
        public String name, address, address_in, link, phone, open;
        public double latitude, longitude;
        public Store(String n, String a, String l, String p, String o, double lat, double lng, String a_in)
        {
            name = n;
            address = a;
            address_in = a_in;
            link = l;
            phone = p;
            open = o;
            latitude = lat;
            longitude = lng;
        }
    }
    public ArrayList<Store> mArray = new ArrayList<>();
    public void init(Location loc)
    {
        if(mArray.size() > 0) {
            mArray.clear();
        }
        ArrayList<Store> array = new ArrayList<>();
        array.add(new Store("Tổng công ty VNEX Đài Bắc", "台北市中山區雙城街18巷11之1號1樓", "https://goo.gl/maps/dFT96sxgvKMu4kMC7",
                "02-25957900", "9:00 AM", 25.0654997,121.5212082, "1 F, No. 11-1, Ln. 18, Shuangcheng St., Zhongshan Dist., Taipei City 10460, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Xinzhuang - VHY", "新北市新莊區自立街84號", "https://goo.gl/maps/qVyAZKttWeVQKrP38",
                "02-2276-8591", "9:00 AM", 25.0452625,121.452442, "No.84, Zili St., Xinzhuang Dist., New Taipei City 242, Taiwan"));
        array.add(new Store("Chi nhánh Zhu Bei - VCB", "新竹縣竹北市仁愛街92-1號1樓", "https://goo.gl/maps/cTqM16krQ31y149B9",
                "03-555-8522", "", 24.8382303,121.0094437, "1 F, No. 92-1, Ren’ai St., Zhubei City, Hsinchu County 30241, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Lu Zhu 1 -  VLC1", "桃園縣蘆竹鄉南山路1段76號", "https://www.google.com.tw/maps/place/338%E6%A1%83%E5%9C%92%E5%B8%82%E8%98%86%E7%AB%B9%E5%8D%80%E5%8D%97%E5%B1%B1%E8%B7%AF%E4%B8%80%E6%AE%B52-1%E8%99%9F/@25.050389,121.2899553,17z/data=!3m1!4b1!4m5!3m4!1s0x3442a07689cd3197:0xafb677417b1edceb!8m2!3d25.050389!4d121.292144?hl=zh-TW",
                "03-212-0230", "", 25.050389,121.2899553, "No. 76, Sec. 1, Nanshan Rd., Luzhu Township, Taoyuan County 33861, Taiwan (R.O.C.)"));
//        array.add(new Store("Chi nhánh Lu Zhu 2 - VLC2", "桃園縣蘆竹鄉南山路2段463號1樓", "https://goo.gl/maps/rwQuwMXRxSVmZZfY9",
//                "03-3245550", "", 25.072182,121.2828873, "1 F, No. 463, Sec. 2, Nanshan Rd., Luzhu Township, Taoyuan County 33852, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Zhong Li - VZL", "桃園市中壢區元化路2-6號1樓", "https://goo.gl/maps/WkTfGV7DUfndAyrRA",
                "03-425-1380", "8:30 AM", 24.9547939,121.2239948, "1 F, No. 2-6, Yanwu Rd., Taoyuan City, Taoyuan County, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Nei Li - VNL", "桃園市中壢區吉林路68-3號1樓", "https://goo.gl/maps/xTtHob1rxaCTGF6t7",
                "03-435-1852", "", 24.979705,121.2455854, "1 F, No. 68-3, Yanwu Rd., Taoyuan City, Taoyuan County, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Shu Lin - VSL", "新北市樹林區中正路608號", "https://goo.gl/maps/tqjqGQ78YSqbm6je9",
                "02-26880725", "9:00 AM", 25.0157796,121.4082551, "No. 608, Zhongzheng Rd., Shulin Dist., New Taipei City 23867, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Ping Zhen - VPZ", "桃園縣平鎮市南豐路216號", "https://goo.gl/maps/Gvo7KqcRa8rQqJgE6",
                "03-419-2165", "", 24.8998379,121.2032682, "No. 216, Nanfeng Rd., Pingzhen City, Taoyuan County 32459, Taiwan (R.O.C.)"));
//        array.add(new Store("Chi nhánh Đào Viên1 - VTY", "桃園市延平路19號 （桃園後火車站）", "https://goo.gl/maps/1bz73QjKGZLnu6jx6",
//                "03-3625113", "9:00 AM", 24.9870442,121.3122599, "No. 19, Yanping Rd., Taoyuan City, Taoyuan County 33067, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Đào Viên2 - VTY2", "桃園市延平路10號 （桃園後火車站）", "https://goo.gl/maps/Akmye8KZ9N3hpBF39",
                "03-361-2981", "9:00 AM", 24.963763,121.2311133, "No.10, Yanping Rd., Taoyuan Dist., Taoyuan City 330, Taiwan"));
        array.add(new Store("Chi nhánh Yuanlin - VYL", "彰化縣員林市民權街46號", "https://goo.gl/maps/GBGbUdRQABB33XR76",
                "04-838-0156", "", 23.959643,120.5681703, "No.46, Minquan St., Yuanlin City, Changhua County 510, Taiwan"));
        array.add(new Store("Chi nhánh Đài Trung 1 - VTC", "台中市西屯區台灣大道一段72號1樓", "https://goo.gl/maps/gTU1b5pVJB2rPFFu5",
                "04-22231599", "9:00 AM", 24.1389804,120.6815091, "1 F, No. 72, Sec. 1, Taiwan Blvd., Central Dist., Taichung City 40046, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Đài Trung 2 - VTC2", "台中市中區臺灣大道一段110號", "https://goo.gl/maps/MUW8PyBwrcH58eXD8",
                "04-2226-8190", "9:00 AM", 24.1394119,120.6812823, "No.110, Sec. 1, Taiwan Blvd., Central Dist., Taichung City 400, Taiwan"));
        array.add(new Store("Chi nhánh Lugang - VLG", "彰化縣鹿港鎮東石里臨海路三段411號1樓", "https://goo.gl/maps/bHUSTyTLQ1eiYPSSA",
                "04-778-5331", "", 24.0717671,120.4138166, "1F., No.411, Sec. 3, Linhai Rd., Lukang Township, Changhua County 505, Taiwan"));
        array.add(new Store("Chi nhánh Feng Yuan - VFY", "台中市豐原區三豐路一段37號", "https://goo.gl/maps/wS2qM8ivgRjRbUBJ6",
                "04-2528-2730", "", 24.2560859,120.7202952, "No. 37, Liancun Rd., Fengyuan Dist., Taichung City, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Ga Tou Liu - VTL", "雲林縣斗六市漢口路199號1+2樓", "https://goo.gl/maps/qC8dNhZoFR3JnUFRA",
                "05-537-5063", "", 23.7134237,120.5390373, "1 F, No. 199, Hankou Rd., Douliu City, Yunlin County 64048, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Gia Nghĩa - VJY", "嘉義市民雄鄉建國路三段102號", "https://goo.gl/maps/yvJe5cEGGzYwUzXu6",
                "05-221-0052", "", 23.5219614,120.4379097, "No. 102, Sec. 3, Jianguo Rd., Minxiong Township, Chiayi County 62157, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Nan Tou - VNT", "南投縣南投市成功三路279號", "https://goo.gl/maps/KThZVFKWSpDwHaY97",
                "04-9225-0051", "9:00 AM", 23.9258075,120.6607373, "No. 279, Chenggong 3rd Rd., Nantou City, Nantou County 54066, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Chang Hua - VCH", "彰化市長安街86號", "https://goo.gl/maps/JzAGWG3Zp9GbjkdV7",
                "04-727-0128", "", 24.080631,120.5372393, "No.86, Chang’an St., Changhua City, Changhua County 500, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Đài Nam - VTN", "台南市北區北忠街68號", "https://goo.gl/maps/Ae7knmSLXCZJpVut5",
                "06-223-8329", "", 22.9991116,120.2085627, "No. 68, Beizhong St., North Dist., Tainan City 70449, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Guantian -VGT", "台南官田區勝利路106號", "https://goo.gl/maps/TVKwyJEmHJXqwXH56",
                "06-579-5913", "9:00 AM", 23.1948044,120.3142681, "No.106, Shengli Rd., Guantian Dist., Tainan City 720, Taiwan"));
        array.add(new Store("Chi nhánh Cao Hùng - VKH", "高雄市新興區八德一路380號", "https://goo.gl/maps/dUdmGFYfNPSCkVam6",
                "07-2351772", "9:00 AM", 22.6360615,120.3009152, "No. 380, Bade 1st Rd., Xinxing Dist., Kaohsiung City 80050, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Qian Zhen - VQZ", "高雄市前鎮區和誠街151號1樓", "https://goo.gl/maps/mfoR6RwHzpmpFJat5",
                "07-812-2971", "9:00 AM", 22.579638,120.3185803, "1 F, No. 151, Hecheng St., Qianzhen Dist., Kaohsiung City 80668, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Ren Wu - VRW", "高雄市仁武區仁祥街87號1樓 1 F", "https://goo.gl/maps/8qJ3ysB1mSQGA5fJA",
                "07-371-9513", "", 22.7024912,120.3436801, "No.87, Renxiang St., Renwu Dist., Kaohsiung City 814, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Bình Đông - VPT", "屏東市光復路156之1號", "https://goo.gl/maps/LxU4WH4BFnhuZRx37",
                "08-733-9760", "10:30 AM", 22.6701466,120.4810064, "No. 156-1, Guangfu Rd., Pingtung City, Pingtung County 90078, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Tunghai - VTH", "台中市西屯區順和二街52號1樓", "https://goo.gl/maps/QuYEffx8NraMvEhB9",
                "04-23505219", "", 24.1765541,120.5869033, "1F., No. 52, Shunhe 2nd St., Xitun Dist., Taichung City 407, Taiwan (R.O.C.)"));
//        array.add(new Store("Chi nhánh Hsinchu - VSZ", "新竹市牛埔東路324號1樓", "https://goo.gl/maps/wJzziECCmhQ4zeQF6",
//                "03-530-1395", "", 24.7955075,120.9396749, "1F., No. 324, Niupu E. Rd., Xiangshan Dist., Hsinchu City 300, Taiwan (R.O.C.)"));
        array.add(new Store("Chi nhánh Mailiao - VML", "雲林縣麥寮鄉表福路3號", "https://goo.gl/maps/nAvVmBjSLZwia6SY6",
                "05-693-0115", "", 23.7475825,120.2535603, "No. 3, Biaofu Road, Mailiao Township, Yunlin County, 638, Taiwan (R.O.C.)"));

        sortArray(loc, array, mArray);
/*
        for(int i = 0; i < array.size(); i++) {
            mArray.add(array.get(i));
        }
*/
    }
    void sortArray(Location loc, ArrayList<Store> from, ArrayList<Store> to)
    {
        if(loc == null) {
            for(int i = 0; i < from.size(); i++) {
                Store store = from.get(i);
                to.add(store);
            }
            return;
        }
        double lat = loc.getLatitude();
        double lng = loc.getLongitude();
//        lat = 24.1378647; 台中市
//        lng = 120.6830978;
        while(from.size() > 0) {
            double min = 99999;
            int target = 0;
            for(int i = 0; i < from.size(); i++) {
                double d_lat = lat - from.get(i).latitude;
                double d_lng = lng - from.get(i).longitude;
                double dist = (d_lat * d_lat + d_lng * d_lng);
                if(dist < min) {
                    min = dist;
                    target = i;
                }
            }
            Store store = from.remove(target);
            to.add(store);
        }
    }
}
