package tw.sayhi.vnex.misc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tw.sayhi.shared.Utils;

public class Valid
{
    public static boolean isNewArcNo(String arc_no)
    {
        if(arc_no.trim().length() != 10) {
            return false;
        }
        String uc = arc_no.toUpperCase();
        Pattern pattern = Pattern.compile("[A-Z]\\d{9}");
        Matcher matcher = pattern.matcher(uc);
        if (matcher.find()) {
            return true;
        }
        return false;
    }
    public static boolean isArcNo(String arc_no)
    {
        if(arc_no.trim().length() != 10) {
            return false;
        }
        String uc = arc_no.toUpperCase();
        Pattern pattern = Pattern.compile("[A-Z][A-D]\\d{8}");
        Matcher matcher = pattern.matcher(uc);
        if (matcher.find()) {
            return true;
        }
        return false;
    }
    public static boolean isPassword(String password)
    {
        if(Utils.isNull(password) || password.length() < 4 || password.length() > 8 ||
                !password.matches("[A-Za-z0-9]+")) {
            return false;
        }
        return true;
    }
    public static boolean isMobile(String phone)
    {
        Pattern pattern = Pattern.compile("^09\\d{8}$");
        Matcher matcher = pattern.matcher(phone);
        if (matcher.find()) {
            return true;
        }
        pattern = Pattern.compile("^9\\d{8}$");
        matcher = pattern.matcher(phone);
        if (matcher.find()) {
            return true;
        }
        return false;
    }
    public static boolean isLocalPhone(String phone)
    {
        String norm = phone.replaceAll("[-|\\(|\\)]", "");
        Pattern pattern = Pattern.compile("^\\d{7,11}$");
        Matcher matcher = pattern.matcher(norm);
        if(matcher.find()) {
            return true;
        }
        return false;
    }
}
