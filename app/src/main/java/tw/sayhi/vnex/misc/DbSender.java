package tw.sayhi.vnex.misc;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.HashMap;
import java.util.Map;

import tw.sayhi.shared.util.MyLog;
import tw.sayhi.vnex.api.RespSenderProfile;

public class DbSender
{
    public static final String SENDER_DELETE = "-1";
    public static final String SENDER_FAIL = "0";
    public static final String SENDER_SUCCESS = "1";
    public static final String SENDER_EDITED = "2";
    public static final String SENDER_REGISTERED = "3";
    public static Map<String, String> mStatusMap = new HashMap<String, String>() {{
        put(SENDER_DELETE, "DELETE");
        put(SENDER_FAIL, "FAIL");
        put(SENDER_SUCCESS, "SUCCESS");
        put(SENDER_EDITED, "EDITED");
        put(SENDER_REGISTERED, "REGISTERED");
    }};
    // field name
    public static final String PK = "_id";
    public static final String NAME = "name";
    public static final String ARC_NO = "arc_no";
    public static final String PASSWORD = "password";
    public static final String EXPIRY_DATE = "expiry_date";
    public static final String UUID = "uuid";
    public static final String PHONE = "phone";
    public static final String BIRTHDAY = "birthday";
    public static final String PROMOTE_NO = "promote_no";
    public static final String PROMOTE_TO = "promote_to";
    public static final String CREATE_TIME = "create_time";
    public static final String STATUS = "status";
    public static final String NOTIFIED = "notified";
    public static final String DOWNLOAD_POINT_USED = "download_point_used";
    public static final String LAST_QUERY_TIME = "last_query_time";
    public static final String REMIT_SUCCESS_COUNT = "remit_success_count";
    public static final String SHARE_POINT_USED = "share_point_used";
    public static final String FACE_IMAGE = "face_image";
    public static final String ARC_IMAGE = "arc_image";
    public static final String ARC_01_IMAGE = "arc_01_image";
    public static final String[] COLUMNS = new String[] {
            PK, NAME, ARC_NO, PASSWORD, EXPIRY_DATE, UUID, PHONE, BIRTHDAY, PROMOTE_NO, PROMOTE_TO, CREATE_TIME, STATUS,
            NOTIFIED, DOWNLOAD_POINT_USED, LAST_QUERY_TIME, REMIT_SUCCESS_COUNT, SHARE_POINT_USED,
            FACE_IMAGE, ARC_IMAGE, ARC_01_IMAGE,
            DbRemit.RESERVED1,
            DbRemit.PROFILE_TYPE,
            DbRemit.IBON,
            DbRemit.FAMILYMART,
            DbRemit.HILIFE,
            DbRemit.RESERVED6,
            DbRemit.RESERVED7,
            DbRemit.RESERVED8,
            DbRemit.RESERVED9
    };
    public static class Data
    {
        public String name, sender_arc, password, expiry_date, uuid, phone, birthday, promote_no, promote_to, status, notified;
        public boolean creator, download_point_used, share_point_used;
        public long create_time, last_query_time, remit_success_count;
        public String face_image, arc_image, arc_01_image;
        public String profile_type;

        public Data(Cursor c)
        {
            name = DbRemit.getString(c, NAME);
            sender_arc = DbRemit.getString(c, ARC_NO);
            password = DbRemit.getString(c, PASSWORD);
            expiry_date = DbRemit.getString(c, EXPIRY_DATE);
            uuid = DbRemit.getString(c, UUID);
            phone = DbRemit.getString(c, PHONE);
            birthday = DbRemit.getString(c, BIRTHDAY);
            promote_no = DbRemit.getString(c, PROMOTE_NO);
            promote_to = DbRemit.getString(c, PROMOTE_TO);
            status = DbRemit.getString(c, STATUS);
            notified = DbRemit.getString(c, NOTIFIED);
            create_time = DbRemit.getLong(c, CREATE_TIME);
            last_query_time = DbRemit.getLong(c, LAST_QUERY_TIME);
            remit_success_count = DbRemit.getLong(c, REMIT_SUCCESS_COUNT);
            creator = (DbRemit.getString(c, DbRemit.RESERVED1).equals("1"));
            download_point_used = (DbRemit.getString(c, DOWNLOAD_POINT_USED).equals("1"));
            share_point_used = (DbRemit.getString(c, SHARE_POINT_USED).equals("1"));
            face_image = DbRemit.getString(c, FACE_IMAGE);
            arc_image = DbRemit.getString(c, ARC_IMAGE);
            arc_01_image = DbRemit.getString(c, ARC_01_IMAGE);
            profile_type = DbRemit.getString(c, DbRemit.PROFILE_TYPE);
        }
    }
    public static final String mTableString = "( " +
            PK + " TEXT PRIMARY KEY, " +
            NAME + " TEXT, " +
            ARC_NO + " TEXT, " +
            PASSWORD + " TEXT, " +
            EXPIRY_DATE + " TEXT, " +
            UUID + " TEXT, " +
            PHONE + " TEXT, " +
            BIRTHDAY + " TEXT, " +
            PROMOTE_NO + " TEXT, " +
            PROMOTE_TO + " TEXT, " +
            CREATE_TIME + " LONG, " +
            STATUS + " TEXT, " +
            NOTIFIED + " TEXT, " +
            DOWNLOAD_POINT_USED + " TEXT, " +
            LAST_QUERY_TIME + " LONG, " +
            REMIT_SUCCESS_COUNT + " LONG, " +
            SHARE_POINT_USED + " TEXT, " +
            FACE_IMAGE + " TEXT, " +
            ARC_IMAGE + " TEXT, " +
            ARC_01_IMAGE + " TEXT, " +
            DbRemit.RESERVED1 + " TEXT, " +
            DbRemit.PROFILE_TYPE + " TEXT, " +
            DbRemit.IBON + " TEXT, " +
            DbRemit.FAMILYMART + " TEXT, " +
            DbRemit.HILIFE + " TEXT, " +
            DbRemit.RESERVED6 + " TEXT, " +
            DbRemit.RESERVED7 + " TEXT, " +
            DbRemit.RESERVED8 + " TEXT, " +
            DbRemit.RESERVED9 + " TEXT " +
            ")";
    private static final String TABLE_NAME = "sender_data";

    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + mTableString);
    }
    public static void delete()
    {
        synchronized (DbRemit.class) {
            DbRemit.sDb.delete(TABLE_NAME, null, null);
        }
    }
    public static void delete(String key, String value)
    {
        synchronized (DbRemit.class) {
            DbRemit.sDb.delete(TABLE_NAME, key + "=?", new String[]{value});
        }
    }
    public static Data read(String key, String value)
    {
        Data data = null;
        synchronized (DbSender.class) {
            Cursor cursor = DbRemit.sDb.query(TABLE_NAME, COLUMNS, key + "=?",
                    new String[]{value}, null, null, null);
            if(!DbRemit.isNull(cursor)) {
                cursor.moveToFirst();
                data = new Data(cursor);
                DbRemit.close(cursor);
            }
            return data;
        }
    }
    public static Cursor readAll()
    {
        return readAll(CREATE_TIME + " DESC");
    }
    public static Cursor readAll(String orderBy)
    {
        DbRemit.sDb.beginTransaction();

        Cursor cursor = DbRemit.sDb.query(TABLE_NAME, COLUMNS, null, null, null,
                null, orderBy);

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
        return DbRemit.first(cursor);
    }
    public static String create(boolean isRegister,
                                String name, String arc_no, String password, String expiry_date, String uuid,
                                String phone, String birthday, String promote_no, String promote_to, String status)
    {
        ContentValues cv = new ContentValues();
        long create_time = System.currentTimeMillis();
        cv.put(PK, arc_no);
        cv.put(NAME, name);
        cv.put(ARC_NO, arc_no);
        cv.put(PASSWORD, password);
        cv.put(EXPIRY_DATE, expiry_date);
        cv.put(UUID, uuid);
        cv.put(PHONE, phone);
        cv.put(BIRTHDAY, birthday);
        cv.put(PROMOTE_NO, promote_no);
        cv.put(PROMOTE_TO, promote_to);
        cv.put(CREATE_TIME, create_time);
        cv.put(STATUS, status);
        cv.put(NOTIFIED, "0");
        cv.put(LAST_QUERY_TIME, -1);
        cv.put(REMIT_SUCCESS_COUNT, 0);

        DbRemit.sDb.beginTransaction();

        int tmp = DbRemit.sDb.update(TABLE_NAME, cv, PK + "=?",
                new String[] {arc_no});
        if (tmp == 0) {
            DbRemit.sDb.insert(TABLE_NAME, null, cv);
        }

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();

        return status;
    }
    public static void update(RespSenderProfile sender)
    {
        Data data = read(ARC_NO, sender.sender_arc);
        if(data != null) {
            return;
        }
        ContentValues cv = new ContentValues();
        cv.put(PK, sender.sender_arc);
        cv.put(NAME, sender.sender_name);
        cv.put(ARC_NO, sender.sender_arc);
        cv.put(PASSWORD, sender.password);
        cv.put(EXPIRY_DATE, sender.sender_arc_term);
        cv.put(UUID, sender.uuid);
        cv.put(PHONE, sender.sender_mobile);
        cv.put(BIRTHDAY, sender.sender_birthday);
        cv.put(PROMOTE_NO, sender.promote_no);
        cv.put(CREATE_TIME, sender.l_create_time);
        cv.put(STATUS, sender.status);
        cv.put(NOTIFIED, "0");
        cv.put(LAST_QUERY_TIME, -1);
        cv.put(REMIT_SUCCESS_COUNT, 0);
        cv.put(FACE_IMAGE, sender.face_image);
        cv.put(ARC_IMAGE, sender.arc_image);
        cv.put(ARC_01_IMAGE, sender.arc_01_image);
        cv.put(DbRemit.PROFILE_TYPE, sender.profile_type);

        DbRemit.sDb.beginTransaction();

        int tmp = DbRemit.sDb.update(TABLE_NAME, cv, PK + "=?",
                new String[] {sender.sender_arc});
        if (tmp == 0) {
            DbRemit.sDb.insert(TABLE_NAME, null, cv);
        }

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();

    }
    public static Data update(RespSenderProfile sender, String password)
    {
        ContentValues cv = new ContentValues();
        cv.put(PK, sender.sender_arc);
        cv.put(NAME, sender.sender_name);
        cv.put(ARC_NO, sender.sender_arc);
        cv.put(PASSWORD, password);
        cv.put(EXPIRY_DATE, sender.sender_arc_term);
        cv.put(UUID, sender.uuid);
        cv.put(PHONE, sender.sender_mobile);
        cv.put(BIRTHDAY, sender.sender_birthday);
        cv.put(PROMOTE_NO, sender.promote_no);
        cv.put(CREATE_TIME, sender.l_create_time);
        cv.put(STATUS, sender.status);
        MyLog.e("DbSender.update() status = " + sender.status);
        cv.put(NOTIFIED, "0");
        cv.put(LAST_QUERY_TIME, -1);
        cv.put(REMIT_SUCCESS_COUNT, 0);
        cv.put(FACE_IMAGE, sender.face_image);
        cv.put(ARC_IMAGE, sender.arc_image);
        cv.put(ARC_01_IMAGE, sender.arc_01_image);
        cv.put(DbRemit.PROFILE_TYPE, sender.profile_type);

        DbRemit.sDb.beginTransaction();

        int tmp = DbRemit.sDb.update(TABLE_NAME, cv, PK + "=?",
                new String[] {sender.sender_arc});
        if (tmp == 0) {
            DbRemit.sDb.insert(TABLE_NAME, null, cv);
        }

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();

        Data data = read(ARC_NO, sender.sender_arc);

        return data;
    }
    public static void update(String arc_no, String key, String value)
    {
        DbRemit.sDb.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(key, value);

        int affected_rows = DbRemit.sDb.update(TABLE_NAME, cv, PK + "=?",
                new String[] {arc_no});

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
    }
    public static void update(String arc_no, String key, long value)
    {
        DbRemit.sDb.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(key, value);

        int affected_rows = DbRemit.sDb.update(TABLE_NAME, cv, PK + "=?",
                new String[] {arc_no});

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
    }
}
