package tw.sayhi.vnex.misc;

import android.database.Cursor;

import tw.sayhi.shared.Utils;

public class OrderData
{
    public String euso_order, sender_arc, uuid, status, pay, notified, pay_channel, upload_receipt, active;
    public String payee_pk, create_time;
    // Amount related data
    public long amount, total_amount, service_fee, discount_fee, red_point_discount;
    public long l_create_time, last_query_time;
    // Payee data
    public String trans_type, name, phone, birthday, id, bank_name, bank_branch, bank_account;
    public String pickup_code;
    public String ibon, familymark, hilife;
    public String recipinent_name, recipinent_personal_id, recipinent_tel;
    public String total_amount_comma;
    public OrderData() {}
    public OrderData(Cursor c)
    {
        setData(c);
    }
    public void setData(Cursor c)
    {
        euso_order = DbRemit.getString(c, DbOrder.EUSO_ORDER);
        sender_arc = DbRemit.getString(c, DbOrder.SENDER_ARC);
//        payee_pk = DbRemit.getString(c, DbPayee.PAYEE_PK);
        uuid = DbRemit.getString(c, DbOrder.UUID);
        status = DbRemit.getString(c, DbOrder.STATUS);
        pay = DbRemit.getString(c, DbOrder.PAY);
        notified = DbRemit.getString(c, DbOrder.NOTIFIED);
        create_time = DbRemit.getString(c, DbOrder.CREATE_TIME);
        l_create_time = Utils.getTimeStamp(Utils.SDF_DASH, create_time);
        pay_channel = DbRemit.getString(c, DbOrder.PAY_CHANNEL);
        upload_receipt = DbRemit.getString(c, DbOrder.UPLOAD_RECEIPT);
        active = DbRemit.getString(c, DbOrder.ACTIVE);
        // Payee data
        trans_type = DbRemit.getString(c, DbOrder.TRANS_TYPE);
        name = DbRemit.getString(c, DbOrder.RECIPINENT_NAME);
        phone = DbRemit.getString(c, DbOrder.RECIPINENT_TEL);
        id = DbRemit.getString(c, DbOrder.RECIPINENT_PERSONAL_ID);
        birthday = DbRemit.getString(c, DbOrder.RECIPINENT_BIRTHDAY_NOTE);
        bank_name = DbRemit.getString(c, DbOrder.BANK_NAME);
        bank_branch = DbRemit.getString(c, DbOrder.BANK_BRANCH);
        bank_account = DbRemit.getString(c, DbOrder.BANK_ACCOUNT);
        // others
        last_query_time = DbRemit.getLong(c, DbOrder.LAST_QUERY_TIME);
        amount = Utils.parseLong(DbRemit.getString(c, DbOrder.AMOUNT));
        total_amount = Utils.parseLong(DbRemit.getString(c, DbOrder.TOTAL_AMOUNT));
        service_fee = Utils.parseLong(DbRemit.getString(c, DbOrder.SERVICE_FEE));
        discount_fee = Utils.parseLong(DbRemit.getString(c, DbOrder.DISCOUNT_FEE));
        String s_red_point_discount = DbRemit.getString(c, DbOrder.RED_POINT_DISCOUNT);
        if(!Utils.isNull(s_red_point_discount)) {
            red_point_discount = Utils.parseLong(s_red_point_discount);
        } else {
            red_point_discount = 0;
        }
        pickup_code = DbRemit.getString(c, DbOrder.PICKUP_CODE);
        ibon = DbRemit.getString(c, DbRemit.IBON);
        familymark = DbRemit.getString(c, DbRemit.FAMILYMART);
        hilife = DbRemit.getString(c, DbRemit.HILIFE);

        recipinent_name = DbRemit.getString(c, DbOrder.RECIPINENT_NAME);
        recipinent_personal_id = DbRemit.getString(c, DbOrder.RECIPINENT_PERSONAL_ID);
        recipinent_tel = DbRemit.getString(c, DbOrder.RECIPINENT_TEL);
        total_amount_comma = DbRemit.getString(c, DbOrder.TOTAL_AMOUNT_COMMA);
    }
}
