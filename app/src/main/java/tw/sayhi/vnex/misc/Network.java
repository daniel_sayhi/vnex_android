package tw.sayhi.vnex.misc;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import org.json.JSONException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.RequiresApi;
import tw.sayhi.shared.Api;
import tw.sayhi.shared.GlobalStatic;
import tw.sayhi.shared.Utils;
import tw.sayhi.shared.result.ResultAuth;
import tw.sayhi.shared.result.ResultEmbedReg;
import tw.sayhi.shared.util.DevInfo;
import tw.sayhi.shared.util.HttpURL;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.shared.widget.ActionDialog;
import tw.sayhi.vnex.R;

public class Network
{
    public static final int NOT_FINISHED = -1;
    public static final int SUCCESS = 0;
    public static final int FATAL_ERROR = 1;
    public static String sErrorMessage;
    public static void checkNetwork(final Activity activity, final CountDownLatch latch)
    {
        // Daniel Yan : June 11, 2016
        // This looks to be an infinite loop, but, once the "cancel" button has clicked, System.exit()
        // will be called.
        new Thread() {
            @Override
            public void run() {
                if (!DevInfo.isNetworkAvailable()) {
                    String sError = "無法使用網路，請確定您有連線上網";
                    MyLog.e(sError);
                    /*
                    Utils.dialogSetNetwork(activity, new Runnable() {
                        @Override
                        public void run() {
                            Etag.checkNetwork(activity);
                        }
                    });
                    */
                    Utils.dialogNetworkProblem(activity);
                } else
                    latch.countDown();
            }
        }.start();
    }
    /*
    public static int register(Activity activity, CountDownLatch latch)
    {
        Utils.waitFor(latch);
        // Check account. Register if needed.
        String msisdn = Pref.getMsisdn(null);
        if (msisdn == null) {
            String adId;
            try {
                adId = AdvertisingIdClient.getAdvertisingIdInfo(activity.getApplicationContext()).getId();
                if (adId == null) {
                    sErrorMessage = "無法取得 Google ID";
                    MyLog.e(sErrorMessage);
                    Utils.dialogFinish(activity, "", sErrorMessage);
                    return FATAL_ERROR;
                }
            } catch (GooglePlayServicesNotAvailableException |
                    GooglePlayServicesRepairableException | IOException e) {
                e.printStackTrace();
                sErrorMessage = "無法取得 Ad. ID";
                MyLog.e(sErrorMessage);
                Utils.dialogFinish(activity, "", sErrorMessage);
                return FATAL_ERROR;
            }
            try {
                ResultEmbedReg r = Api.embedReg(GlobalStatic.appId, adId,
                        DevInfo.getImei(),
                        "Android_" + DevInfo.getVersionName());
                if (r.status.equals("SUCCESS")) {
                    Pref.setMsisdn(r.msisdn);
                    Pref.setCode(adId);
                } else {
                    sErrorMessage = "未知錯誤 (" + r.status + ", " + r.debug + ")";
                    Utils.dialogFinish(activity, "認證錯誤", sErrorMessage);
                    return FATAL_ERROR;
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                Utils.dialogNetworkProblem(activity);
                return FATAL_ERROR;
            }
        }

        // Authentication
        try {
            int i = MyLog.timeStart();
            ResultAuth r = Api.auth(GlobalStatic.appId, null,
                    Pref.getCode(null), DevInfo.getImei(),
                    Pref.getMsisdn(null),
                    "Android_" + DevInfo.getVersionName());
            MyLog.timeEnd(i, "Api.auth()");
            if (r.status.equals("SUCCESS")) {
                Pref.setInviteUrl(r.inviteUrl);
                Pref.setSession(r.session);
            } else {
                sErrorMessage = "未知錯誤 (" + r.status + ", " + r.debug + ")";
                Utils.dialogFinish(activity, "", sErrorMessage);
                return FATAL_ERROR;
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            Utils.dialogNetworkProblem(activity);
            return FATAL_ERROR;
        }
        MyLog.i("session = " + Pref.getSession());
        return SUCCESS;
    } */
    public static boolean chkUpdate(final Activity act, CountDownLatch latch) {
        boolean needUpdate = false;
        String packageName = act.getPackageName();
        final String updateURL = Api.GPLAY_URL + packageName;
        if(latch != null)
            Utils.waitFor(latch);
        int t = MyLog.timeStart();
        try {
            int curVerCode = act.getPackageManager().getPackageInfo(packageName, 0).versionCode;
            String resp = HttpURL.sendGet(updateURL);
            Pattern pattern = Pattern.compile("\"(\\d+\\.\\d+\\.\\d+)\"");
            Matcher matcher = pattern.matcher(resp);
            String gplayVersion = "";
            if (matcher.find()) {
                gplayVersion = matcher.group(1);
                int lastestVerCode = Integer.parseInt(
                        gplayVersion.substring(gplayVersion.lastIndexOf('.') + 1));
                MyLog.timeEnd(t, "(" + curVerCode + ", " + lastestVerCode + ")");
                needUpdate = (curVerCode < lastestVerCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            MyLog.timeEnd(t, "chkUpdate() Exception");
            return false;
        }
        if (needUpdate == false)
            return false;
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ActionDialog(act, tw.sayhi.shared.R.string.app_name, "", R.string.app_update, null,
                        R.drawable.info, R.string.update, R.string.cancel) {
                    @Override
                    public void onOk() {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateURL));
                        act.startActivity(intent);
                    }
                };
            }
        });
        return true;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean isInternetAvailable() {
        boolean available = false;
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress("www.google.com", 80), 2000);
            available = true;
        } catch (IOException e) {
            // Either we have a timeout or unreachable host or failed DNS lookup
        }
        return available;
    }
    public static boolean needUpdateApp(final Activity act, int latestVerCode) {
        boolean needUpdate = false;
        String packageName = act.getPackageName();
        int t = MyLog.timeStart();
        try {
            int curVerCode = act.getPackageManager().getPackageInfo(packageName, 0).versionCode;
            needUpdate = (curVerCode < latestVerCode);
        } catch (Exception e) {
        }
        return needUpdate;
    }
    public static void updateApp(final Activity act, boolean force_update, final Runnable runnable) {
        final int id_cancel = force_update ? -1 : R.string.cancel;
        String packageName = act.getPackageName();
        final String updateURL = Api.GPLAY_URL + packageName;
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ActionDialog(act, tw.sayhi.shared.R.string.app_name, "", R.string.app_update, null,
                        R.drawable.info, R.string.update, id_cancel) {
                    @Override
                    public void onOk() {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateURL));
                        act.startActivity(intent);
                    }
                    @Override
                    public void onCancel() {
                        if(runnable != null) {
                            runnable.run();
                        }
                    }
                };
            }
        });
    }
}
