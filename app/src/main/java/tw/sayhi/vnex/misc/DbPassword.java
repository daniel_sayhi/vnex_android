package tw.sayhi.vnex.misc;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DbPassword
{
    public static Data mData;
    public static final String INITIAL = "initial";
    public static final String FORGET_PASSWORD = "forget_password";
    public static final long MAX_WRONG_PASSWORD = 3;

    public static final String PK = "_id";
    public static final String SENDER_ARC = "sender_arc";
    public static final String STATUS = "status";
    public static final String PASSWORD = "password";
    public static final String FAILED_TIMES = "failed_times";
    public static final String[] COLUMNS = new String[] {
            PK, SENDER_ARC, STATUS, PASSWORD, FAILED_TIMES,
            DbRemit.RESERVED1,
            DbRemit.PROFILE_TYPE,
            DbRemit.IBON,
            DbRemit.FAMILYMART,
            DbRemit.HILIFE,
            DbRemit.RESERVED6,
            DbRemit.RESERVED7,
            DbRemit.RESERVED8,
            DbRemit.RESERVED9
    };
    public static class Data
    {
        public String sender_arc, status, password;
        public long failed_times;

        public Data(Cursor c)
        {
            if(DbRemit.isNull(c))
                return;
            sender_arc = DbRemit.getString(c, SENDER_ARC);
            status = DbRemit.getString(c, STATUS);
            password = DbRemit.getString(c, PASSWORD);
            failed_times = DbRemit.getLong(c, FAILED_TIMES);
        }
    }
    public static final String mTableString = "( " +
            PK + " TEXT PRIMARY KEY, " +
            SENDER_ARC + " TEXT, " +
            STATUS + " TEXT, " +
            PASSWORD + " TEXT, " +
            FAILED_TIMES + " LONG, " +
            DbRemit.RESERVED1 + " TEXT, " +
            DbRemit.PROFILE_TYPE + " TEXT, " +
            DbRemit.IBON + " TEXT, " +
            DbRemit.FAMILYMART + " TEXT, " +
            DbRemit.HILIFE + " TEXT, " +
            DbRemit.RESERVED6 + " TEXT, " +
            DbRemit.RESERVED7 + " TEXT, " +
            DbRemit.RESERVED8 + " TEXT, " +
            DbRemit.RESERVED9 + " TEXT " +
            ")";
    private static final String TABLE_NAME = "password_data";

    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + mTableString);
    }
    public static void delete()
    {
        synchronized (DbRemit.class) {
            DbRemit.sDb.delete(TABLE_NAME, null, null);
        }
    }
    public static void delete(String sender_arc)
    {
        synchronized (DbRemit.class) {
            DbRemit.sDb.delete(TABLE_NAME, SENDER_ARC + "=?", new String[]{sender_arc});
        }
    }
    public static Data read(String sender_arc)
    {
        synchronized (DbPassword.class) {
            Cursor cursor = DbRemit.sDb.query(TABLE_NAME, COLUMNS, SENDER_ARC + "=?",
                    new String[]{sender_arc}, null, null, null);
            DbRemit.first(cursor);
            mData = null;
            if(!DbRemit.isNull(cursor)) {
                mData = new Data(cursor);
            }
            DbRemit.close(cursor);
            return mData;
        }
    }
    public static Cursor readAll(String orderBy)
    {
        DbRemit.sDb.beginTransaction();

        Cursor cursor = DbRemit.sDb.query(TABLE_NAME, COLUMNS, null, null, null,
                null, orderBy);

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
        return DbRemit.first(cursor);
    }
    public static void create(String sender_arc, String password, long failed_times)
    {
        ContentValues cv = new ContentValues();
        cv.put(PK, sender_arc);
        cv.put(SENDER_ARC, sender_arc);
        cv.put(STATUS, INITIAL);
        cv.put(PASSWORD, password);
        cv.put(FAILED_TIMES, failed_times);

        DbRemit.sDb.beginTransaction();

        int tmp = DbRemit.sDb.update(TABLE_NAME, cv, PK + "=?",
                new String[] {sender_arc});
        if (tmp == 0) {
            DbRemit.sDb.insert(TABLE_NAME, null, cv);
        }

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
    }
    public static void update(String sender_arc, String key, String value)
    {
        DbRemit.sDb.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(key, value);

        int affected_rows = DbRemit.sDb.update(TABLE_NAME, cv, SENDER_ARC + "=?",
                new String[] {sender_arc});

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
    }
    public static void update(String sender_arc, String key, long value)
    {
        DbRemit.sDb.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(key, value);

        int affected_rows = DbRemit.sDb.update(TABLE_NAME, cv, SENDER_ARC + "=?",
                new String[] {sender_arc});

        DbRemit.sDb.setTransactionSuccessful();
        DbRemit.sDb.endTransaction();
    }
}
