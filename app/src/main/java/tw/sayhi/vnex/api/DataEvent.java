package tw.sayhi.vnex.api;

import org.json.JSONObject;

public class DataEvent
{
    public String note, sub, modifier, modify_time, origin, coupon_name, column_val_01, column_val_02, coupon_limit_trans_type;
    public String company, id, coupon_batch_no, sql_orderby_column, trans_type, creator, coupon_code, create_time;
    public String column_name_02, column_name_01, column_name, agent_no, sql_query_column, branch_code, order_uid;
    public String coupon_limit_branch_code, coupon_quick_select, coupon_start_time, column_val, coupon_type, coupon_end_time;
    public String status;
    // public JSONArray branch_codes;
    public int coupon_count, sum_coupon, coupon_limit, coupon_value_limit_high, coupon_value_limit_low;
    public int coupon_value, order_value;

    public DataEvent(JSONObject obj)
    {
        note = obj.optString("note");
        sub = obj.optString("sub");
        coupon_count = obj.optInt("coupon_count");
        modifier = obj.optString("modifier");
        modify_time = obj.optString("modify_time");
        origin = obj.optString("origin");
        sum_coupon = obj.optInt("sum_coupon");
        coupon_name = obj.optString("coupon_name");
        coupon_limit = obj.optInt("coupon_limit");
        column_val_01 = obj.optString("column_val_01");
        column_val_02 = obj.optString("column_val_02");
        // 匯款金額超過此限，coupon才可使用
        coupon_value_limit_high = obj.optInt("coupon_value_limit_high");
        coupon_limit_trans_type = obj.optString("coupon_limit_trans_type");
        company = obj.optString("company");
        id = obj.optString("id");
        coupon_batch_no = obj.optString("coupon_batch_no");
        sql_orderby_column = obj.optString("sql_orderby_column");
        trans_type = obj.optString("trans_type");
        coupon_value_limit_low = obj.optInt("coupon_value_limit_low");
        creator = obj.optString("creator");
        // used in createRemitOrder()
        coupon_code = obj.optString("coupon_code");
        create_time = obj.optString("create_time");
        column_name_02 = obj.optString("column_name_02");
        column_name_01 = obj.optString("column_name_01");
        column_name = obj.optString("column_name");
        agent_no = obj.optString("agent_no");
        // 折價
        coupon_value = obj.optInt("coupon_value");
        sql_query_column = obj.optString("sql_query_column");
        branch_code = obj.optString("branch_code");
        order_uid = obj.optString("order_uid");
        coupon_limit_branch_code = obj.optString("coupon_limit_branch_code");
        coupon_quick_select = obj.optString("coupon_quick_select");
        // should not use, taken care of by Server
        coupon_start_time = obj.optString("coupon_start_time");
        column_val = obj.optString("column_val");
        // should be "remit"
        coupon_type = obj.optString("coupon_type");
        coupon_end_time = obj.optString("coupon_end_time");
        order_value = obj.optInt("order_value");
        status = obj.optString("status");
    }
}
