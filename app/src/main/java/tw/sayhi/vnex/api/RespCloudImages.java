package tw.sayhi.vnex.api;

import org.json.JSONObject;

import tw.sayhi.shared.Utils;

public class RespCloudImages
{
    public boolean success = false;
    public int app_version = -1;
    public boolean force_update = false;
    public DataCloudImages old_data, new_data, data;
    public RespCloudImages(String resp)
    {
        if(Utils.isNull(resp))
            return;
        try {
            JSONObject jo = new JSONObject(resp);
            String string = jo.optString("android_version");
            if(!Utils.isNull(string)) {
                app_version = Utils.parseInt(string);
            }
            string = jo.optString("android_force_update");
            if(string.equals("true")) {
                force_update = true;
            }
            old_data = new DataCloudImages(jo.optJSONObject("old_data"));
            new_data = new DataCloudImages(jo.optJSONObject("new_data"));
            if (new_data.isValid) {
                data = new_data;
                success = true;
            } else if (old_data.isValid) {
                data = old_data;
                success = true;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
