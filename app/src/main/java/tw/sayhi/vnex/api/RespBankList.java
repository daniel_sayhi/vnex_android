package tw.sayhi.vnex.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RespBankList extends RespBase
{
    public final ArrayList<String> bankList = new ArrayList<>();

    public RespBankList(String resp) throws JSONException
    {
        super(resp);

        JSONArray ja = jo.optJSONArray(response_text);
        if (ja != null) {
            for (int i = 0; i < ja.length(); i++) {
                bankList.add((String)ja.get(i));
            }
        }
    }
}
