package tw.sayhi.vnex.api;

import org.json.JSONException;
import org.json.JSONObject;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.HttpURL;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.vnex.DebugOptions;

public class RespBase
{
    public static final int RESP_TIME_OUT = 0;
    public static final int RESP_SUCCESS = 1;
    public static final int RESP_FAIL = 2;

    public static final String STATUS_SUCCESS = "1";
    public static final String SUCCESS = "SUCCESS";
    public static final String response_text = "response_text";
    public String org_message;
    public String statusCode;
    public String message;
    public String response_time, device_id;
    public String exception_message;
    public JSONObject jo;
    public boolean time_out;

    public RespBase(String resp) throws JSONException
    {
        if(DebugOptions.bShowRespLog) {
            MyLog.i("resp=" + resp);
        }
        if(Utils.isNull(resp)) {
            org_message = "resp has empty";
            time_out = true;
            exception_message = HttpURL.sExceptionMessage;
            return;
        }
        org_message = resp;
        jo = new JSONObject(resp);
        statusCode = jo.optString("statusCode");
        message = jo.optString("message");
        response_time = jo.optString("response_time");
        device_id = jo.optString("device_id");
        time_out = false;
    }
    public int result()
    {
        if(Utils.isNull(statusCode)) {
            return RESP_TIME_OUT;
        }
        if(success()) {
            return RESP_SUCCESS;
        }
        return RESP_FAIL;
    }
    public boolean success()
    {
        return(statusCode != null && statusCode.equals(STATUS_SUCCESS));
    }
}
