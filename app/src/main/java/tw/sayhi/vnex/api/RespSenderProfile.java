package tw.sayhi.vnex.api;

import org.json.JSONException;
import org.json.JSONObject;

import tw.sayhi.shared.Utils;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.DebugOptions;

public class RespSenderProfile extends RespBase
{
    public String uuid, apid, status, promote_no, sender_arc, sender_name, sender_birthday, sender_arc_term;
    public String sender_tel, sender_mobile, sender_country, create_time;
    public long l_create_time;
    public String face_image, arc_image, arc_01_image;
    public boolean registered;
    public String password, profile_type;
    String transImageURL(String url)
    {
        int index = url.indexOf(Config.apid);
        if(index <= 0)
            return url;
        String substr = url.substring(index);
        if(!DebugOptions.bUseTestURL) {
            url = "https://nothing.index-group.com.tw/res/webFileStorage/euso/" + substr;
        } else {
            url = "http://systest.index-group.com.tw/res/webFileStorage/euso/" + substr;
        }
        return url;
    }
    public RespSenderProfile(String resp) throws JSONException
    {
        super(resp);

        if(jo == null) {
            return;
        }
        JSONObject obj = jo.optJSONObject(response_text);
        if (obj != null) {
            uuid = obj.optString("uuid");
            apid = obj.optString("apid");
            status = obj.optString("status");
            promote_no = obj.optString("promote_no");
            sender_arc = obj.optString("sender_arc");
            sender_name = obj.optString("sender_name");
            sender_birthday = obj.optString("sender_birthday");
            sender_arc_term = obj.optString("sender_arc_term");
            sender_tel = obj.optString("sender_tel");
            sender_mobile = obj.optString("sender_mobile");
            sender_country = obj.optString("sender_country");
            String value = obj.optString("registered");
            registered = (value != null && value.equals("1"));
            create_time = obj.optString("create_time");
            l_create_time = Utils.getTimeStamp(Utils.SDF_DASH, create_time);
            face_image = obj.optString("face_image");
            arc_image = obj.optString("arc_image");
            arc_01_image = obj.optString("arc_01_image");
            face_image = transImageURL(face_image);
            arc_image = transImageURL(arc_image);
            arc_01_image = transImageURL(arc_01_image);
            profile_type = obj.optString("profile_type");
        }
/*
        String r = "{\n" +
                "  \"statusCode\": \"1\",\n" +
                "  \"message\": \"SUCCESS\",\n" +
                "  \"response_time\": \"2017/11/15 14:54:40\",\n" +
                "  \"response_text\": {\n" +
                "    \"apid\": \"ROK01\",\n" +
                "    \"promote_no\": \"123\",\n" +
                "    \"status\": \"3\",\n" +
                "    \"uuid\": \"1234567890\",\n" +
                "    \"sender_name\": \"AAA\",\n" +
                "    \"sender_birthday\": \"1900/01/01\",\n" +
                "    \"sender_arc\": \"AA00000000\",\n" +
                "    \"sender_arc_term\": \"1900/01/01\",\n" +
                "    \"sender_tel\": \"0123456789\",\n" +
                "    \"sender_mobile\": \"0123456789\",\n" +
                "    \"sender_country\": \"IDN\",\n" +
                "    \"face_image\": \"``\",\n" +
                "    \"arc_image\": \"``\",\n" +
                "    \"arc_01_image\": \"``\"\n" +
                "  }\n" +
                "}";
*/
    }
}
