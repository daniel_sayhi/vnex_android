package tw.sayhi.vnex.api;

import android.app.Activity;
import android.content.ContentValues;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.HttpURL;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.DebugOptions;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.misc.DbSender;
import tw.sayhi.vnex.misc.MultipartUtility;
import tw.sayhi.vnex.misc.MultipartUtilityV3;
import tw.sayhi.vnex.misc.QueryApiService;

import static java.util.Arrays.asList;

public class EusoApi
{
    static final String EUSO_HOST = "https://euso.index-group.com.tw";
//    static final String EUSO_PRODUCTION_URL = "https://euso.index-group.com.tw/EUSO/api/services";
    static final String EUSO_PRODUCTION_URL = "https://nothing.index-group.com.tw/EUSO/api/services";
//    static final String EUSO_TEST_URL = "https://114.34.175.55/EUSO/api/services";
    static final String EUSO_TEST_URL = "https://220.133.81.91/EUSO/api/services";
    static String EUSO_URL = EUSO_PRODUCTION_URL;
    static String PRODUCTION_REMIT_URL = EUSO_PRODUCTION_URL + "/remit/";
    static String REMIT_URL;
    static String SENDER_PROFILE_URL;
    static String RED_POINT_URL;
    static String COUPON_URL;
//    static ArrayList<String> sHeaderBase = new ArrayList<>(asList("Content-type", "application/x-www-form-urlencoded"));
    static ArrayList<String> sHeaderBase = new ArrayList<>(asList(
            "Content-type", "application/x-www-form-urlencoded",
            "Accept-Encoding", "gzip"
    ));
    static ArrayList<String> sOrderHeader = new ArrayList<>(asList("Content-type", "multipart/form-data"));
    static final String DATA_STORE_URL = "https://sayhi-market-web.appspot.com/DataStore";;

    public static void init(Activity act, boolean tryout)
    {
        if(tryout) {
            useTestUrl(act);
        } else {
            useProductionUrl();
        }
    }
    public static void useProductionUrl()
    {
        EUSO_URL = EUSO_PRODUCTION_URL;
        REMIT_URL = EUSO_PRODUCTION_URL + "/remit/";
        SENDER_PROFILE_URL = EUSO_PRODUCTION_URL + "/senderProfile/";
        RED_POINT_URL = EUSO_PRODUCTION_URL + "/redPoint/";
        COUPON_URL = EUSO_PRODUCTION_URL + "/coupon/";
    }
    public static void useTestUrl(Activity act)
    {
        EUSO_URL = EUSO_TEST_URL;
        REMIT_URL = EUSO_TEST_URL + "/remit/";
        SENDER_PROFILE_URL = EUSO_TEST_URL + "/senderProfile/";
        RED_POINT_URL = EUSO_TEST_URL + "/redPoint/";
        COUPON_URL = EUSO_TEST_URL + "/coupon/";
        if(act != null) {
            Utils.postMessage(act, "Using TEST environment...");
        }
    }
    public static RespER queryER(String company, String apid)
            throws IOException, JSONException
    {
//        int t = MyLog.timeStart();

//        String body = HttpURL.encode("company", "index", "apid", "ROI01");
        String body = HttpURL.encode("company", company, "apid", apid);
        String resp = HttpURL.postHttps(REMIT_URL + "queryER.api", sHeaderBase, body, null);

//        MyLog.timeEnd(t, "queryER()");

        return new RespER(resp);
    }
    public static RespAddressList queryAddressList(String company, String apid)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid);

        String resp = HttpURL.postHttps(REMIT_URL + "queryAddressList.api", sHeaderBase, body, null);

        return new RespAddressList(resp);
    }
    public static RespBankList queryBankList(String company, String apid, String trans_type)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "trans_type", trans_type);

        String resp = HttpURL.postHttps(REMIT_URL + "queryBankList.api", sHeaderBase, body, null);

        return new RespBankList(resp);
    }
    public static RespBankFullList queryBankListWithInfo(String company, String apid)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid);

        String resp = HttpURL.postHttps(REMIT_URL + "queryBankListWithInfo.api", sHeaderBase, body, null);

        return new RespBankFullList(resp);
    }
    public static RespEvent queryEvent(String company, String apid)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid);

        String resp = HttpURL.postHttps(REMIT_URL + "queryEvent.api", sHeaderBase, body, null);

        return new RespEvent(resp);
    }
    public static RespServiceFee queryServiceFeeV2(String company, String apid, String trans_type, String amount, String bank_name)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "trans_type", trans_type,
                "amount", amount,
                "bank_name", bank_name);

        String resp = HttpURL.postHttps(REMIT_URL + "queryServiceFeeV2.api", sHeaderBase, body, null);

        return new RespServiceFee(resp);
    }
    public static RespRemitOrder createRemitOrder(ContentValues cv, String sign_image_file)
            throws IOException, JSONException
    {
//        MultipartUtilityV2 multipart = new MultipartUtilityV2(REMIT_URL + "createRemitOrder.api");
//        MultipartUtility multipart = new MultipartUtility(REMIT_URL + "createRemitOrder.api", "UTF-8");
        MultipartUtilityV3 multipart = new MultipartUtilityV3(REMIT_URL + "createRemitOrder.api", "UTF-8");

//        multipart.addHeaderField("User-Agent", "CodeJava");
//        multipart.addHeaderField("Test-Header", "Header-Value");

//        multipart.addFormField("description", "Cool Pictures");
//        multipart.addFormField("keywords", "Java,upload,Spring");

        multipart.addForms(cv);
//        multipart.addFilePart("face_image_file", new File(face_image_file));
//        multipart.addFilePart("arc_image_file", new File(arc_image_file));
//        multipart.addFilePart("arc_01_image_file", new File(arc_01_image_file));
        multipart.addFilePart("sign_image_file", new File(sign_image_file));

        String resp = multipart.finish();

        RespRemitOrder ro = new RespRemitOrder(resp);
        if(ro.statusCode.equals(RespBase.STATUS_SUCCESS)) {
            DbOrder.create(cv, PayInfo.commaNTD, ro);
            // save current euso_order in Pref
            Pref.set(DbOrder.EUSO_ORDER, ro.euso_order);
            Pref.set(DbOrder.UUID, ro.uuid);
            Pref.set(DbOrder.STATUS, ro.status);
            QueryApiService.setPref(QueryApiService.HAS_PENDING_ORDER, "1");
        }
        return ro;
    }
    public static RespBase allowRemitOrder(String company, String apid, String euso_order, String uuid)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "euso_order", euso_order,
                "uuid", uuid);

        String resp = HttpURL.postHttps(REMIT_URL + "allowRemitOrder.api", sHeaderBase, body, null);

        return new RespBase(resp);
    }

    public static RespQueryRemitOrder queryRemitOrder(String company, String apid, String euso_order, String uuid)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "euso_order", euso_order,
                "uuid", uuid);

        String resp = HttpURL.postHttps(REMIT_URL + "queryRemitOrder.api", sHeaderBase, body, null);

        return new RespQueryRemitOrder(resp);
    }
    public static RespBase uploadReceiptImg(String company, String apid, String euso_order, String uuid,
                                                 String receipt_image_file)
            throws IOException, JSONException
    {
        MultipartUtilityV3 multipart = new MultipartUtilityV3(REMIT_URL + "uploadReceiptImg.api", "UTF-8");

        multipart.addForms(
                "company", company, // "index"
                "apid", apid, // "ROI01"
                "euso_order", euso_order,
                "uuid", uuid);

        multipart.addFilePart("receipt_image_file", new File(receipt_image_file));

        String resp = multipart.finish();

        return new RespBase(resp);
    }
    public static RespSenderProfile queryRegister(String company, String apid, String sender_arc)
            throws IOException, JSONException
    {
        if(DebugOptions.bUseEEC) {
            company = "eec";
            apid = "ROE01";
        }
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "sender_arc", sender_arc);

//        MyLog.i("body=" + body);

//        String resp = "{\"statusCode\":\"1\",\"message\":\"SUCCESS\",\"response_time\":\"2018/11/27 11:54:14\",\"response_text\":{\"account\":\"\",\"apid\":\"ROK01\",\"arc_01_image\":\"/sender/AB/12/34/56/78/AB12345678_arc_01.jpg\",\"arc_image\":\"/sender/AB/12/34/56/78/AB12345678_arc.jpg\",\"ban\":\"\",\"branch_code\":\"\",\"company\":\"\",\"create_time\":\"2018/11/27 11:32:17\",\"device_id\":\"06f3dd75-e81f-4394-aa06-34706881d6d9\",\"face_image\":\"/sender/AB/12/34/56/78/AB12345678_face.jpg\",\"id\":\"38a52fbb2e000000c2c8\",\"lang\":\"\",\"last_access_ip\":\"\",\"last_access_time\":null,\"method\":\"\",\"modify_time\":null,\"note\":\"\",\"promote_no\":\"1\",\"promote_to\":\"\",\"red_point\":0,\"remit_company\":\"\",\"remit_country\":\"\",\"reverify\":\"\",\"sender_arc\":\"AB12345678\",\"sender_arc_term\":\"2020/01/01\",\"sender_birthday\":\"1990/12/31\",\"sender_country\":\"IDN\",\"sender_mobile\":\"0919550470\",\"sender_name\":\"DANIEL YAN\",\"sender_tel\":\"\",\"sign_image\":\"\",\"status\":\"3\",\"uuid\":\"186185777023764610548\"}}";
        String resp = HttpURL.postHttps(SENDER_PROFILE_URL + "queryRegister.api", sHeaderBase, body, null);

//        MyLog.i("resp = " + resp);

        return new RespSenderProfile(resp);
    }
    public static RespSenderProfile login(String company, String apid, String device_id, String sender_arc, String password)
            throws IOException, JSONException
    {
        if(DebugOptions.bUseEEC) {
            company = "eec";
            apid = "ROE01";
        }
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "device_id", device_id,
                "sender_arc", sender_arc,
                "password", password);

//        String resp = "{\"statusCode\":\"1\",\"message\":\"SUCCESS\",\"response_time\":\"2018/11/27 11:54:14\",\"response_text\":{\"account\":\"\",\"apid\":\"ROK01\",\"arc_01_image\":\"/sender/AB/12/34/56/78/AB12345678_arc_01.jpg\",\"arc_image\":\"/sender/AB/12/34/56/78/AB12345678_arc.jpg\",\"ban\":\"\",\"branch_code\":\"\",\"company\":\"\",\"create_time\":\"2018/11/27 11:32:17\",\"device_id\":\"06f3dd75-e81f-4394-aa06-34706881d6d9\",\"face_image\":\"/sender/AB/12/34/56/78/AB12345678_face.jpg\",\"id\":\"38a52fbb2e000000c2c8\",\"lang\":\"\",\"last_access_ip\":\"\",\"last_access_time\":null,\"method\":\"\",\"modify_time\":null,\"note\":\"\",\"promote_no\":\"1\",\"promote_to\":\"\",\"red_point\":0,\"remit_company\":\"\",\"remit_country\":\"\",\"reverify\":\"\",\"sender_arc\":\"AB12345678\",\"sender_arc_term\":\"2020/01/01\",\"sender_birthday\":\"1990/12/31\",\"sender_country\":\"IDN\",\"sender_mobile\":\"0919550470\",\"sender_name\":\"DANIEL YAN\",\"sender_tel\":\"\",\"sign_image\":\"\",\"status\":\"3\",\"uuid\":\"186185777023764610548\"}}";
        String resp = HttpURL.postHttps(SENDER_PROFILE_URL + "login.api", sHeaderBase, body, null);

        return new RespSenderProfile(resp);
    }
    public static RespRegister register(boolean isRegister,
                                        String company, String apid, String uuid, String device_id, String password,
                                        String sender_name, String sender_arc, String sender_arc_term,
                                        String sender_birthday, String sender_tel,
                                        String sender_mobile, String sender_country,
                                        String face_image_file, String arc_image_file, String arc_01_image_file,
                                        String promote_to, String review, String profile_type)
            throws IOException, JSONException
    {
        if(DebugOptions.bUseEEC) {
            company = "eec";
            apid = "ROE01";
        }
        String api = (isRegister ? "register.api" : "updateProfile.api");
        MultipartUtility multipart = new MultipartUtility(SENDER_PROFILE_URL + api,
                "UTF-8");

        multipart.addForms(
                "company", company, // "index"
                "apid", apid, // "ROI01"
                "uuid", uuid,
                "device_id", device_id, // "1234567890"
                "password", password, // "zh_TW"
                "sender_name", sender_name, // "AAA"
                "sender_arc", sender_arc, // "AA00000000"
                "sender_arc_term", sender_arc_term, // "``"
                "sender_birthday", sender_birthday, // "``"
                "sender_tel", sender_tel, // "0123456789"
                "sender_mobile", sender_mobile, // "0123456789"
                "sender_country", sender_country, // "IDN"
                "promote_to", promote_to,
                "review", review,
                "profile_type", profile_type
        );

        if(profile_type.equals("1")) {
            multipart.addFilePart("face_image_file", new File(face_image_file));
            multipart.addFilePart("arc_image_file", new File(arc_image_file));
            multipart.addFilePart("arc_01_image_file", new File(arc_01_image_file));
        }

        String resp = multipart.finish();
        HttpURL.log(resp);

        RespRegister ro = new RespRegister(resp);
        if(isRegister && ro.statusCode.equals(RespBase.STATUS_SUCCESS)) {
//            public static String createOrUpdate(String name, String arc_no, String expiry_date, String uuid,
//                    String phone, String birthday, String promote_no, String status)
            DbSender.create(isRegister, sender_name, sender_arc, password, sender_arc_term, ro.uuid,
                    sender_mobile, sender_birthday, ro.promote_no, promote_to, ro.status);
        }
        return ro;
    }
    public static RespSenderProfile queryStatus(String company, String apid, String device_id, String uuid,
                                                String sender_arc, String password)
            throws IOException, JSONException
    {
        if(DebugOptions.bUseEEC) {
            company = "eec";
            apid = "ROE01";
        }
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "device_id", device_id,
                "uuid", uuid,
                "sender_arc", sender_arc,
                "password", password);

//        MyLog.i("body=" + body);

        String resp = "{\"statusCode\":\"1\",\"message\":\"SUCCESS\",\"response_time\":\"2018/11/27 11:54:14\",\"response_text\":{\"account\":\"\",\"apid\":\"ROK01\",\"arc_01_image\":\"/sender/AB/12/34/56/78/AB12345678_arc_01.jpg\",\"arc_image\":\"/sender/AB/12/34/56/78/AB12345678_arc.jpg\",\"ban\":\"\",\"branch_code\":\"\",\"company\":\"\",\"create_time\":\"2018/11/27 11:32:17\",\"device_id\":\"06f3dd75-e81f-4394-aa06-34706881d6d9\",\"face_image\":\"/sender/AB/12/34/56/78/AB12345678_face.jpg\",\"id\":\"38a52fbb2e000000c2c8\",\"lang\":\"\",\"last_access_ip\":\"\",\"last_access_time\":null,\"method\":\"\",\"modify_time\":null,\"note\":\"\",\"promote_no\":\"1\",\"promote_to\":\"\",\"red_point\":0,\"remit_company\":\"\",\"remit_country\":\"\",\"reverify\":\"\",\"sender_arc\":\"AB12345678\",\"sender_arc_term\":\"2020/01/01\",\"sender_birthday\":\"1990/12/31\",\"sender_country\":\"IDN\",\"sender_mobile\":\"0919550470\",\"sender_name\":\"DANIEL YAN\",\"sender_tel\":\"\",\"sign_image\":\"\",\"status\":\"3\",\"uuid\":\"186185777023764610548\"}}";
        resp = HttpURL.postHttps(SENDER_PROFILE_URL + "queryStatus.api", sHeaderBase, body, null);

//        MyLog.i("resp = " + resp);

        return new RespSenderProfile(resp);
    }
    public static RespSenderProfile queryProfile(String company, String apid, String device_id, String uuid,
                                                 String sender_arc, String password)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "device_id", device_id,
                "uuid", uuid,
                "sender_arc", sender_arc,
                "password", password);

        String resp = "{\"statusCode\":\"1\",\"message\":\"SUCCESS\",\"response_time\":\"2018/11/27 11:54:14\",\"response_text\":{\"account\":\"\",\"apid\":\"ROK01\",\"arc_01_image\":\"/sender/AB/12/34/56/78/AB12345678_arc_01.jpg\",\"arc_image\":\"/sender/AB/12/34/56/78/AB12345678_arc.jpg\",\"ban\":\"\",\"branch_code\":\"\",\"company\":\"\",\"create_time\":\"2018/11/27 11:32:17\",\"device_id\":\"06f3dd75-e81f-4394-aa06-34706881d6d9\",\"face_image\":\"/sender/AB/12/34/56/78/AB12345678_face.jpg\",\"id\":\"38a52fbb2e000000c2c8\",\"lang\":\"\",\"last_access_ip\":\"\",\"last_access_time\":null,\"method\":\"\",\"modify_time\":null,\"note\":\"\",\"promote_no\":\"1\",\"promote_to\":\"\",\"red_point\":0,\"remit_company\":\"\",\"remit_country\":\"\",\"reverify\":\"\",\"sender_arc\":\"AB12345678\",\"sender_arc_term\":\"2020/01/01\",\"sender_birthday\":\"1990/12/31\",\"sender_country\":\"IDN\",\"sender_mobile\":\"0919550470\",\"sender_name\":\"DANIEL YAN\",\"sender_tel\":\"\",\"sign_image\":\"\",\"status\":\"3\",\"uuid\":\"186185777023764610548\"}}";
        resp = HttpURL.postHttps(SENDER_PROFILE_URL + "queryProfile.api", sHeaderBase, body, null);

        return new RespSenderProfile(resp);
    }
    public static RespBase queryAllRemitOrder(String company, String apid, String uuid)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "euso_order", "",
                "uuid", uuid);

        String resp = HttpURL.postHttps(REMIT_URL + "queryRemitOrder.api", sHeaderBase, body, null);

        return new RespBase(resp);
    }
    public static RespSenderProfile updatePassword(String company, String apid, String device_id, String uuid,
                                                   String sender_arc, String password)
            throws IOException, JSONException
    {
        if(DebugOptions.bUseEEC) {
            company = "eec";
            apid = "ROE01";
        }
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "device_id", device_id,
                "uuid", uuid,
                "sender_arc", sender_arc,
                "password", password);

        String resp = "{\"statusCode\":\"1\",\"message\":\"SUCCESS\",\"response_time\":\"2018/11/27 11:54:14\",\"response_text\":{\"account\":\"\",\"apid\":\"ROK01\",\"arc_01_image\":\"/sender/AB/12/34/56/78/AB12345678_arc_01.jpg\",\"arc_image\":\"/sender/AB/12/34/56/78/AB12345678_arc.jpg\",\"ban\":\"\",\"branch_code\":\"\",\"company\":\"\",\"create_time\":\"2018/11/27 11:32:17\",\"device_id\":\"06f3dd75-e81f-4394-aa06-34706881d6d9\",\"face_image\":\"/sender/AB/12/34/56/78/AB12345678_face.jpg\",\"id\":\"38a52fbb2e000000c2c8\",\"lang\":\"\",\"last_access_ip\":\"\",\"last_access_time\":null,\"method\":\"\",\"modify_time\":null,\"note\":\"\",\"promote_no\":\"1\",\"promote_to\":\"\",\"red_point\":0,\"remit_company\":\"\",\"remit_country\":\"\",\"reverify\":\"\",\"sender_arc\":\"AB12345678\",\"sender_arc_term\":\"2020/01/01\",\"sender_birthday\":\"1990/12/31\",\"sender_country\":\"IDN\",\"sender_mobile\":\"0919550470\",\"sender_name\":\"DANIEL YAN\",\"sender_tel\":\"\",\"sign_image\":\"\",\"status\":\"3\",\"uuid\":\"186185777023764610548\"}}";
        resp = HttpURL.postHttps(SENDER_PROFILE_URL + "updatePassword.api", sHeaderBase, body, null);

        return new RespSenderProfile(resp);
    }
    // Red Point
    public static RespResponseText queryRedPoint(String company, String apid, String device_id, String uuid,
                                                 String sender_arc)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "device_id", device_id,
                "uuid", uuid,
                "sender_arc", sender_arc);

        String resp = HttpURL.postHttps(RED_POINT_URL + "query.api", sHeaderBase, body, null);

//        MyLog.i("resp = " + resp);

        return new RespResponseText(resp);
    }
    public static RespCashHistory history(String company, String apid, String device_id, String uuid, String sender_arc)
            throws IOException, JSONException
    {
        if(DebugOptions.bUseEEC) {
            company = "eec";
            apid = "ROE01";
        }
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "device_id", device_id,
                "uuid", uuid,
                "sender_arc", sender_arc);

        String resp = HttpURL.postHttps(RED_POINT_URL + "history.api", sHeaderBase, body, null);

        return new RespCashHistory(resp);
    }
    public static RespResponseText adjustRedPoint(String company, String apid, String device_id, String uuid,
                                                  String sender_arc, String trans_point)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "device_id", device_id,
                "uuid", uuid,
                "sender_arc", sender_arc,
                "trans_type", "1",
                "trans_type_desc", "test",
                "trans_point", trans_point);

        String resp = "{\n" +
                "  \"statusCode\": \"1\",\n" +
                "  \"message\": \"SUCCESS\",\n" +
                "  \"response_time\": \"2017/11/15 14:54:40\",\n" +
                "  \"response_text\": {\n" +
                "    \"red_point\": 123\n" +
                "  }\n" +
                "}";
        resp = HttpURL.postHttps(RED_POINT_URL + "adjust.api", sHeaderBase, body, null);

        return new RespResponseText(resp);
    }
    public static String convertMessage(String message)
    {
        if(message.startsWith("Your ARC already reg")) {
            return "Thẻ cư trú đã đăng ký qua";
        }
        return message;
    }
    public static RespQueryExchangeCoupon queryExchangeCoupon(String company, String apid, String device_id, String uuid,
                                                      String coupon_type, String branch_code)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "device_id", device_id,
                "uuid", uuid,
                "coupon_type", coupon_type,
                "branch_code", branch_code);

        String resp = HttpURL.postHttps(COUPON_URL + "queryExchangeCoupon.api", sHeaderBase, body, null);

        return new RespQueryExchangeCoupon(resp, coupon_type);
    }
    public static RespExchangeCoupon exchangeCoupon(String company, String apid, String device_id, String uuid,
                                                              String coupon_type, String branch_code, String coupon_id,
                                                         int exchange_bonus_cost, String signature, String mote
                                                         )
            throws IOException, JSONException
    {
        String body = HttpURL.encode("company", company,
                "apid", apid,
                "device_id", device_id,
                "uuid", uuid,
                "coupon_type", coupon_type,
                "branch_code", branch_code,
                "coupon_id", coupon_id,
                "signature", signature,
                "exchange_bonus_cost", "" + exchange_bonus_cost,
                "mote", mote);

        String resp = HttpURL.postHttps(COUPON_URL + "exchangeCoupon.api", sHeaderBase, body, null);

        return new RespExchangeCoupon(resp);
    }
    public static RespExchangeCoupon exchangeCouponCV(ContentValues cv)
            throws IOException, JSONException
    {
        MultipartUtilityV3 multipart = new MultipartUtilityV3(
                COUPON_URL + "exchangeCoupon.api", "UTF-8");

        multipart.addForms(cv);

        String resp = multipart.finish();

        return new RespExchangeCoupon(resp);
    }
    public static void dataStore(String record, String content)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("record", record,
                "content", content);
        String resp = HttpURL.postHttps(DATA_STORE_URL, null, body, null);
        if(Utils.nonNull(resp)) {
        }
    }
}
