package tw.sayhi.vnex.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.Utils;

public class RespEvent extends RespBase
{
    String errCode, msg;
    public final ArrayList<DataEvent> eventList = new ArrayList<>();

    public RespEvent(String resp) throws JSONException
    {
        super(resp);

        if(Utils.isNull(resp))
            return;

        JSONObject obj_event = jo.optJSONObject(response_text);
        if(obj_event != null) {
            errCode = obj_event.optString("errCode");
            msg = obj_event.optString("msg");
            JSONArray coupons = obj_event.optJSONArray("coupons");
            if (coupons != null) {
                for (int i = 0; i < coupons.length(); i++) {
                    JSONObject coupon = coupons.getJSONObject(i);
                    DataEvent event = new DataEvent(coupon);
                    eventList.add(event);
                }
            }
        }
    }
}
