package tw.sayhi.vnex.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tw.sayhi.shared.Utils;

public class DataCloudImages {
    public boolean isValid = false;
    public String start_date, default_link;
    public DataImageLink[] banners;
    public DataCloudImages(JSONObject jo) throws JSONException
    {
        start_date = jo.optString("start_date");
        if(Utils.isNull(start_date))
            return;
        long start_time = Utils.getTimeStamp(start_date);
        long curr_time = System.currentTimeMillis();
        if(curr_time < start_time)
            return;
        isValid = true;
        // For now, it's a valid CloudImages
        default_link = jo.optString("default_link");
        JSONArray ja = jo.optJSONArray("banners");
        if (ja != null) {
            banners = new DataImageLink[ja.length()];
            for (int i = 0; i < ja.length(); i++) {
                banners[i] = new DataImageLink(ja.getJSONObject(i), default_link);
            }
        }
    }
}
