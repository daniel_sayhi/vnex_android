package tw.sayhi.vnex.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RespQueryExchangeCoupon extends RespBase
{
    public boolean hasCoupon = false;
    public int minCost = 10000;
    public ArrayList<DataExchangeCoupon> dataExchangeCoupons = new ArrayList<>();

    public RespQueryExchangeCoupon(String resp, String coupon_type) throws JSONException
    {
        super(resp);

        JSONObject jo_response = jo.optJSONObject(response_text);
        JSONArray ja = jo_response.optJSONArray("coupons");
        if (ja != null) {
            for (int i = 0; i < ja.length(); i++) {
                JSONObject obj = ja.optJSONObject(i);
                DataExchangeCoupon coupon = new DataExchangeCoupon(obj);
                coupon.coupon_type = coupon_type;
                if(coupon.isDateValid) {
                    dataExchangeCoupons.add(coupon);
                    hasCoupon = true;
                    if(coupon.exchange_bonus_cost < minCost) {
                        minCost = coupon.exchange_bonus_cost;
                    }
                }
            }
        }
    }
}
