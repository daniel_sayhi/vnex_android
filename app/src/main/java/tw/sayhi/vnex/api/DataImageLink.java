package tw.sayhi.vnex.api;

import org.json.JSONException;
import org.json.JSONObject;

import tw.sayhi.shared.Utils;

public class DataImageLink {
    public String image, link;
    public DataImageLink(JSONObject jo, String default_link) throws JSONException
    {
        image = jo.optString("image");
        link = jo.optString("link");
        if(Utils.isNull(link)) {
            link = default_link;
        }
    }
}
