package tw.sayhi.vnex.api;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RespServiceFee extends RespBase
{
    public long service_fee;
    public RespServiceFee(String resp) throws JSONException
    {
        super(resp);

        service_fee = 0;
        JSONObject obj = jo.optJSONObject(response_text);
        if (obj != null) {
            service_fee = obj.optLong("service_fee");
        }
    }
}
