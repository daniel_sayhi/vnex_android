package tw.sayhi.vnex.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tw.sayhi.shared.Utils;

public class RespQueryRemitOrder extends  RespBase
{
    public String uuid, euso_odrer, status, pay, pickup_code, trans_code, trans_remark;
    public String sender_arc;
    JSONObject ecPayOrderList;
    public String paymentType, barcode1, barcode2, barcode3, paymentNo, merchantTradeNo, expireDate, returnCode, returnTime;
    public String iBon, Family, Hilife;
    JSONObject obj = null;
    public RespQueryRemitOrder(String resp) throws JSONException
    {
        super(resp);

        if(Utils.isNull(resp))
            return;
        JSONArray ja = jo.optJSONArray(response_text);
        if (ja != null) {
            obj = ja.optJSONObject(0);
        }
//        JSONObject obj = jo.optJSONObject(response_text);
        if (obj != null) {
            uuid = obj.optString("uuid");
            euso_odrer = obj.optString("euso_order");
            status = obj.optString("status");
            pay = obj.optString("pay");
            pickup_code = obj.optString("pickup_code");
            sender_arc = obj.optString("sender_arc");
            trans_code = obj.optString("trans_code");
            trans_remark = obj.optString("trans_remark");
            ja = obj.optJSONArray("ecPayOrderList");
            if(ja != null) {
                ecPayOrderList = ja.optJSONObject(0);
                if (ecPayOrderList != null) {
                    paymentType = ecPayOrderList.optString("paymentType");
                    barcode1 = ecPayOrderList.optString("barcode1");
                    barcode2 = ecPayOrderList.optString("barcode2");
                    barcode3 = ecPayOrderList.optString("barcode3");
                    paymentNo = ecPayOrderList.optString("paymentNo");
                    merchantTradeNo = ecPayOrderList.optString("merchantTradeNo");
                    expireDate = ecPayOrderList.optString("expireDate");
                    returnCode = ecPayOrderList.optString("returnCode");
                    returnTime = ecPayOrderList.optString("returnTime");
                    JSONObject paymentURLs = ecPayOrderList.optJSONObject("paymentURLs");
                    iBon = paymentURLs.optString("iBon");
                    Family = paymentURLs.optString("Family");
                    Hilife = paymentURLs.optString("Hilife");
                }
            }
        }
    }
}
