package tw.sayhi.vnex.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RespAddressList extends RespBase
{
    JSONObject[] area;
    JSONArray[] province_names;
    public RespAddressList(String resp) throws JSONException
    {
        super(resp);

        JSONObject obj_area = jo.optJSONObject(response_text);
        area = new JSONObject[3];
        province_names = new JSONArray[3];
        if (obj_area != null) {
            area[0] = obj_area.optJSONObject("NORTH");
            area[1] = obj_area.optJSONObject("MIDDLE");
            area[2] = obj_area.optJSONObject("SOUTH");
            for(int i = 0; i < 3; i++) {
                province_names[i] = area[i].names();
            }
        }
    }
    public void getProvinces(int i_area, ArrayList<String> provinces)
    {
        provinces.clear();
        try {
            JSONArray ja = province_names[i_area];
            for (int i = 0; i < ja.length(); i++) {
                provinces.add(ja.getString(i));
            }
        } catch(JSONException e) {
        }
    }
    public void getCities(int i_area, int i_province, ArrayList<String> cities)
    {
        cities.clear();
        try {
            JSONObject obj_area = area[i_area];
            String province_name = province_names[i_area].getString(i_province);
            JSONArray ja_cities = obj_area.getJSONArray(province_name);
            for(int i = 0; i < ja_cities.length(); i++) {
                cities.add(ja_cities.getString(i));
            }
        } catch (JSONException e) {
        }
    }
}
