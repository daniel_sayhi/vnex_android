package tw.sayhi.vnex.api;

import android.app.Activity;

import org.json.JSONException;

import java.io.IOException;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.R;

public class DB2
{
    int sTrial = 1;
    public boolean bAllRemitOrder;
    public RespBase allRemitOrder;
    public boolean queryAllRemitOrder(Activity act, String uuid)
    {
        for(int i = 0; i < sTrial; i++) {
            bAllRemitOrder = queryAllRemitOrderCore(uuid);
            if(bAllRemitOrder) {
                return true;
            }
        }
        if(act != null) {
            Utils.postMessage(act, act.getString(R.string.network_timeout));
        }
        return false;
    }
    public boolean queryAllRemitOrderCore(String uuid)
    {
        boolean success = false;
        try {
            allRemitOrder = new RespQueryRemitOrder("");
            allRemitOrder = EusoApi.queryAllRemitOrder(Config.company, Config.apid, uuid);
            if(allRemitOrder.success()) {
                success = true;
//                DbPayee.update(allRemitOrder);
                MyLog.e("success");
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
}
