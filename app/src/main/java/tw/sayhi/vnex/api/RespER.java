package tw.sayhi.vnex.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.Utils;

public class RespER extends RespBase
{
    public final ArrayList<DataER> erList = new ArrayList<>();

    public RespER(String resp) throws JSONException
    {
        super(resp);

        if(Utils.isNull(resp))
            return;

        JSONArray ja = jo.optJSONArray(response_text);
        if (ja != null) {
            for (int i = 0; i < ja.length(); i++) {
                JSONObject obj = ja.optJSONObject(i);
                DataER er = new DataER(obj);
                erList.add(er);
            }
        }
    }
}
