package tw.sayhi.vnex.api;

import org.json.JSONObject;

public class DataBank
{
    public String bank_address, bank_name, bank_full_name, show_top;

    public DataBank(JSONObject obj)
    {
        bank_address = obj.optString("bank_address");
        bank_name = obj.optString("bank_name");
        bank_full_name = obj.optString("bank_full_name");
        show_top = obj.optString("show_top");
    }
}
