package tw.sayhi.vnex.api;

import org.json.JSONObject;

import tw.sayhi.shared.Utils;

public class DataExchangeCoupon {
    public String coupon_type;
    public String coupon_id, coupon_name, exchange_start_time, exchange_end_time, exchange_disabled, exchange_disabled_desc;
    public String exchange_coupon_code_show, exchange_feedback, exchange_image_url, signature, create_time;
    public int exchange_count, exchange_count_limit, exchange_bonus_cost;
    public boolean isDateValid = false;
    public DataExchangeCoupon(JSONObject obj) {
        coupon_id = obj.optString("coupon_id");
        coupon_name = obj.optString("coupon_name");
        exchange_count = obj.optInt("exchange_count");
        exchange_count_limit = obj.optInt("exchange_count_limit");
        exchange_bonus_cost = obj.optInt("exchange_bonus_cost");
        exchange_start_time = obj.optString("exchange_start_time");
        exchange_end_time = obj.optString("exchange_end_time");
        exchange_disabled = obj.optString("exchange_disabled"); // 0 or 1
        exchange_disabled_desc = obj.optString("exchange_disabled_desc");
        exchange_coupon_code_show = obj.optString("exchange_coupon_code_show"); // 0 or 1
        exchange_feedback = obj.optString("exchange_feedback"); // 0 or 1
        exchange_image_url = obj.optString("exchange_image_url");
        signature = obj.optString("signature");
        create_time = obj.optString("create_time");
        long start_time = Utils.getTimeStamp(Utils.SDF_SLASH, exchange_start_time);
        long end_time = Utils.getTimeStamp(Utils.SDF_SLASH, exchange_end_time);
        long curr_time = System.currentTimeMillis();
        if (curr_time >= start_time && curr_time <= end_time) {
            isDateValid = true;
        }
    }
}
