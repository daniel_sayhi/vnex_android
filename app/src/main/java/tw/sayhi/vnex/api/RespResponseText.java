package tw.sayhi.vnex.api;

import org.json.JSONException;
import org.json.JSONObject;

public class RespResponseText extends RespBase
{
    public long service_fee, red_point;
    public String device_id;
    public RespResponseText(String resp) throws JSONException
    {
        super(resp);

        service_fee = 0;
        red_point = 0;
        device_id = "";
        if(jo == null) {
            return;
        }
        JSONObject obj = jo.optJSONObject(response_text);
        if (obj != null) {
            service_fee = obj.optLong("service_fee");
            red_point = obj.optLong("red_point");
            device_id = obj.optString("device_id");
        }
    }
}
