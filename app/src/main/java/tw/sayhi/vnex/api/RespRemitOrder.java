package tw.sayhi.vnex.api;

import org.json.JSONException;
import org.json.JSONObject;

 public class RespRemitOrder extends RespBase
{
    public String uuid, euso_order, status;
    public boolean uploadReceipt;
    public RespRemitOrder(String resp) throws JSONException
    {
        super(resp);

        JSONObject obj = jo.optJSONObject(response_text);
        if (obj != null) {
            uuid = obj.optString("uuid");
            euso_order = obj.optString("euso_order");
            status = obj.optString("status");
            uploadReceipt = false;
        }
    }
}
