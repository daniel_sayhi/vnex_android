package tw.sayhi.vnex.api;

import org.json.JSONObject;

public class DataER
{
    public float exchange_rate;
    public String exchange_flag;

    public DataER(JSONObject obj)
    {
         exchange_rate = (float)obj.optDouble("exchange_rate");
         exchange_flag = obj.optString("exchange_flag");
    }
}
