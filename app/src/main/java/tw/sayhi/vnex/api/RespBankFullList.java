package tw.sayhi.vnex.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RespBankFullList extends RespBase
{
    public ArrayList<String> bankNames = new ArrayList<>();
    public Map<String, String> bankNameMap = new HashMap<>(101);

    public RespBankFullList(String resp) throws JSONException
    {
        super(resp);

        JSONArray ja = jo.optJSONArray(response_text);
        if (ja != null) {
            for (int i = 0; i < ja.length(); i++) {
                JSONObject obj = ja.optJSONObject(i);
                DataBank bank = new DataBank(obj);
                String full_name = bank.bank_name + ": " + bank.bank_full_name;
                bankNames.add(full_name);
                bankNameMap.put(full_name, bank.bank_name);
            }
        }
    }
}
