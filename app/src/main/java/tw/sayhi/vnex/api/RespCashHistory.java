package tw.sayhi.vnex.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RespCashHistory extends RespBase
{
    public final ArrayList<DataCashHistory> dataCashHistories = new ArrayList<>();

    public RespCashHistory(String resp) throws JSONException
    {
        super(resp);

        JSONArray ja = jo.optJSONArray(response_text);
        if (ja != null) {
            for (int i = 0; i < ja.length(); i++) {
                JSONObject obj = ja.optJSONObject(i);
                DataCashHistory red_point = new DataCashHistory(obj);
                dataCashHistories.add(0, red_point);
            }
        }
    }
}
