package tw.sayhi.vnex.api;

import org.json.JSONObject;

public class DataCashHistory
{
    public String trans_point, trans_type, trans_type_desc, trans_ref_info, create_time;
    public DataCashHistory(JSONObject obj)
    {
        trans_point = obj.optString("trans_point");
        trans_type = obj.optString("trans_type");
        trans_type_desc = obj.optString("trans_type_desc");
        trans_ref_info = obj.optString("trans_ref_info");
        create_time = obj.optString("create_time");
    }
}
