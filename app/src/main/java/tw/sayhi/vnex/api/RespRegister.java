package tw.sayhi.vnex.api;

import org.json.JSONException;
import org.json.JSONObject;

public class RespRegister extends RespBase
{
    public String uuid, apid, status, promote_no, sender_arc;
    public RespRegister(String resp) throws JSONException
    {
        super(resp);

        if(jo == null) {
            return;
        }
        JSONObject obj = jo.optJSONObject(response_text);
        if (obj != null) {
            uuid = obj.optString("uuid");
            apid = obj.optString("apid");
            status = obj.optString("status");
            promote_no = obj.optString("promote_no");
            sender_arc = obj.optString("sender_arc");
        }
    }
}
