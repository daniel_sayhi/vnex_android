package tw.sayhi.vnex;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;

import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import tw.sayhi.shared.Api;
import tw.sayhi.shared.AppCtx;
import tw.sayhi.shared.GlobalStatic;
import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.DevInfo;
import tw.sayhi.shared.util.ImageURL;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.activity.LoginActivity;
import tw.sayhi.vnex.activity.ReceiverActivity;
import tw.sayhi.vnex.activity.SenderActivity;
import tw.sayhi.vnex.api.EusoApi;
import tw.sayhi.vnex.misc.CloudImages;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.misc.DbRemit;
import tw.sayhi.vnex.misc.DbSender;
import tw.sayhi.vnex.misc.FuncTest;
import tw.sayhi.vnex.misc.Network;
import tw.sayhi.vnex.misc.QueryApiService;
import tw.sayhi.vnex.widget.ActionDialog;

import static tw.sayhi.shared.util.AppInfo.getVersionName;

public class PortalActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] items = {
            R.id.iv_home_news, R.id.iv_home_remit, R.id.iv_home_shopping, R.id.iv_home_cargo
    };
    public static CountDownLatch latchNetwork;
    public static CountDownLatch latchInitImageLoader = new CountDownLatch(1);
    ImageView ivLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            initGlobalStatic();
            Pref.init(getApplication());

            FuncTest.go(mActivity);

            super.onCreate(savedInstanceState);
            if(isFinishing()) return;
            setContentView(R.layout.portal__activity);

            init();

            if(!isServiceRunning(QueryApiService.class)) {
                Intent intent = new Intent(mActivity, QueryApiService.class);
                startService(intent);
            }

            buildViews();

            checkAppUpdate(); // todo

        } catch (Exception e) {
            DB.reportException("PortalActivity_onCreate", e);
        }
    }
    @Override
    protected void onLogout() {
        if(nonNull(mLoginArcNo)) {
            super.onLogout();
        } else {
            sMode = MODE_PORTAL;
            nextActivity(LoginActivity.class);
        }
    }
    void buildListeners(int id)
    {
        switch(id) {
            case R.id.iv_home_remit:
                if(DB.isLogin() && !DB.isForeignWorker()) {
                    actionDialog(R.string.app_name, R.string.tw_cannot_remit, new Runnable() {
                        @Override
                        public void run() {
                            Utils.openWebPage(mActivity, "https://www.eec-vnex.com.tw/dich-vu-chuyen-tien.php");
                        }
                    });
                    return;
                }
                nextActivity(MainActivity.class);
                break;
            case R.id.iv_home_news:
//                nextActivity(BarcodeCaptureActivity.class);
//                nextActivity(ScanQRCodeActivity.class);
                Utils.openWebPage(mActivity, "https://eec-vnex.com.tw/tin-tuc-khuyen-mai.php");
                break;
            case R.id.iv_home_shopping:
                Utils.openWebPage(mActivity, "https://eec-vnex.com.tw/portal_c1_cnt.php?owner_num=c1_74287&button_num=c1&folder_id=");
                break;
            case R.id.iv_home_cargo:
                Utils.openWebPage(mActivity, "https://eec-vnex.com.tw/cargo-van-chuyen-hang-khong.php");
                break;
        }
    }
    void buildViews()
    {

        detectNarrowScreen();

        findViewById(R.id.iv_back).setVisibility(View.GONE);
        findViewById(R.id.vi_margin).setVisibility(View.VISIBLE);
        ivLogout = findViewById(R.id.iv_logout);
        for (int item : items) {
            View view = findViewById(item);
            if(view == null)
                continue;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buildListeners(view.getId());
                }
            });
        }
    }
    boolean onResumeCalled = false;
    @Override
    protected void onResume()
    {
        super.onResume();

        if(!onResumeCalled) {
            buildTitleMenu();
            disableBack();
            if (!DB.isLogin()) {
                ivLogout.setImageDrawable(getResources().getDrawable(R.drawable.icon_login));
            }
            onResumeCalled = true;
        }
        if(!Utils.isNull(Pref.get(JUST_REGISTERED))) {
            Pref.set(JUST_REGISTERED, "");
            registerCompleteDialog();
        }
    }
    void afterNext()
    {
        String status = DB.getStatus();
        if (status.equals(DbSender.SENDER_SUCCESS)) {
            if(Utils.isDateEffective(DB.getExpiryDate())) {
                nextActivity(ReceiverActivity.class);
            } else {
                new ActionDialog(mActivity, -1, R.string.arc_is_expired,
                        R.drawable.warning, R.string.okay, -1) {
                    @Override
                    public void onOk() {
                        nextActivity(SenderActivity.class, ARC_EXPIRED, "1");
                    }
                };
            }
        } else if (status.equals(DbSender.SENDER_FAIL) ||
                status.equals(DbSender.SENDER_DELETE))
        {
            actionDialog(R.string.app_name, R.string.review_fail, new Runnable() {
                @Override
                public void run() {
                    nextActivity(SenderActivity.class);
                }
            });
        } else { // Sender under review
            warnDialog(R.string.app_name, R.string.register_succeed, -1);
        }
    }
    void initGlobalStatic()
    {
        GlobalStatic.appName = getResources().getString(R.string.app_name);
        GlobalStatic.appId = "vnex";
        GlobalStatic.apkName = GlobalStatic.appId + "-" + getVersionName(mActivity) + ".apk";
        GlobalStatic.isInitialized = true;
        GlobalStatic.isEmulator = Build.FINGERPRINT.contains("generic");

        GlobalStatic.drawableLogo = R.drawable.logo;
        GlobalStatic.colorPrimary = getResources().getColor(R.color.colorPrimary);
        GlobalStatic.GA_account = "UA-97706523-2";
    }
    void init()
    {
        int t = MyLog.timeStart();
        DebugOptions.init();
        if (DebugOptions.bUseTestURL) {
            Utils.postMessage(mActivity, "Using Tryout version...");
        }
        EusoApi.init(mActivity, DebugOptions.bUseTestURL);
        DbRemit.init(mActivity);
        // for ease of development
        Cursor c = DbOrder.readAll(null);
        if(DbRemit.isNull(c)) {
            Pref.set(DbOrder.EUSO_ORDER, "");
        }
        DbRemit.close(c);
        PayInfo.init();
        DevInfo.init(getApplication());
//        Locator.init(getApplication());
        Pref.setNextAdvRefreshTime(System.currentTimeMillis() + AppCtx.ADV_REFRESH_INTERVAL);
        Pref.setNextStoreRefreshTime(System.currentTimeMillis() + AppCtx.STORE_REFRESH_INTERVAL);

//        latchNetwork = new CountDownLatch(1);
//        Network.checkNetwork(MainActivity.this, latchNetwork);
        // Initialize ImageLoader with configuration.
        ImageURL.init(mActivity, latchInitImageLoader);
        CloudImages.loadImageDesc(mActivity, CloudImages.latchLoadImageDesc);
        new Thread() {
            @Override
            public void run() {
                String ts = Context.TELEPHONY_SERVICE;
                TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService(ts);
/*
                String imsi = "";
                try {
                    imsi =  mTelephonyMgr.getSubscriberId();
                } catch (Exception e) {

                }
                GlobalStatic.imsi = imsi;
                GlobalStatic.imei = mTelephonyMgr.getDeviceId();
*/
                GlobalStatic.android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                GlobalStatic.uuid = UUID.randomUUID().toString();
                GlobalStatic.sGoogleAdId = GlobalStatic.uuid;
                // try again, in afraid of timeout
//                GlobalStatic.sGoogleAdId = Api.getGoogleAdId(mActivity);
//                if(Utils.isNull(GlobalStatic.sGoogleAdId)) {
//                    GlobalStatic.sGoogleAdId = Api.getGoogleAdId(mActivity);
//                }
                for(int i = 0; i < 20; i++) {
                    if(isServiceRunning(QueryApiService.class)) {
                        buildServiceConnection();
                        break;
                    }
                    Utils.sleep(1000);
                }
                Network.chkUpdate(mActivity, null);
            }
        }.start();
//        MyLog.timeEnd(t, "init()");
    }
    void registerCompleteDialog()
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.register_complete__dialog);

        ViewGroup root = dialog.findViewById(R.id.root);
        scaleTextViewSize(root, 0.8f);
        TextView tvPromoteNo = dialog.findViewById(R.id.tv_promote_no);
        tvPromoteNo.setText(DB.getPromoteNo());
        TextView tvClose = dialog.findViewById(R.id.tv_close);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                actionDialog(getString(R.string.app_name), getString(R.string.register_succeed), R.drawable.info, null);
            }
        });
        TextView tvInviteFriend = dialog.findViewById(R.id.iv_invite_friend);
        tvInviteFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inviteFriend();
            }
        });
        dialog.show();
    }
    void detectNarrowScreen()
    {
        final ViewGroup root = findViewById(R.id.root);
        ViewTreeObserver vto = root.getViewTreeObserver();
        vto.addOnGlobalLayoutListener (new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                root.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                TextView tvTarget = findViewById(R.id.tv_test_narrow_screen);
                findViewById(R.id.ll_test_narrow_screen).setVisibility(View.GONE);
                int line_count = tvTarget.getLineCount();
                mIsNarrowScreen = (isNarrowScreen() || line_count > 1);
                if(mIsNarrowScreen) {
                    scaleImageSize(findViewById(R.id.ll_first_row), 0.8f);
                    scaleImageSize(findViewById(R.id.ll_second_row), 0.8f);
                    MyLog.e("mIsNarrowScreen is true.");
                }
            }
        });
    }
    void checkAppUpdate()
    {
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(mActivity);

// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            int available = appUpdateInfo.updateAvailability();
            boolean flexible = appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE);
            boolean immediate = appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE);
            if (available == UpdateAvailability.UPDATE_AVAILABLE && immediate) {
            }
        });
    }
}
