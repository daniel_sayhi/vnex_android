package tw.sayhi.vnex;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.Pref;

public class PayInfo
{
    public static final String RED_POINT_DOWNLOAD = "50";
    public static final String RED_POINT_SHARE = "10";
    public static final String RED_POINT_REMIT = "10";
    public static final String REGISTER_PASSWORD = "register_password";
    public static final String REFERENCE_PROMOTE_NO = "reference_promote_no";
    public static final String SEND_ORDER = "send_order";
    public static final String SENDER_SOURCE_OF_INCOME = "sender_source_of_income";
    public static final String SENDER_REMITTANCE_PURPOSE = "sender_remittance_purpose";
    public static final String RECIPINENT_RELATIONSHIP = "recipinent_relationship";

    public static Map<String, String> mMap = new HashMap<>();

    public static final String TRANS_TYPE = "trans_type";
    // Currency
    public static final String NTD = "NTD";
    public static final String VND = "VND";
    public static final String USD = "USD";
    public static float erVND, erUSD;
    // trans_type
    public static final String PIN = "PIN"; //密碼
    public static final String DTD = "DTD"; //送到家
    public static final String BTB = "BTB"; //帳號
    public static final String IDP = "IDP"; //ID取款
    public static String trans_type;
    // payee info
    public static final String PAYEE_NAME = "payee_name";
    public static String payee_name;
    public static final String PAYEE_PHONE = "payee_phone";
    public static String payee_phone;
    public static final String PAYEE_BIRTHDAY = "payee_birthday";
    public static final String PAYEE_ID = "payee_id";
    public static String payee_id;
    public static final String PAYEE_AREA = "payee_area";
    public static final String PAYEE_AREA_VI = "payee_area_vi";
    public static final String PAYEE_AREA_INDEX = "payee_area_index";
    public static final String PAYEE_PROVINCE = "payee_province";
    public static final String PAYEE_PROVINCE_INDEX = "payee_province_index";
    public static final String PAYEE_CITY = "payee_city";
    public static final String PAYEE_CITY_INDEX = "payee_city_index";
    public static String payee_area;
    public static final String PAYEE_ADDRESS = "payee_address";
    public static String payee_address;
    public static final String BANK_BRANCH = "bank_branch";
    public static final String BANK_ACCOUNT = "bank_account";
    public static String payee_account;
    public static final String PAY_ER_TYPE = "pay_er_type";
    public static final String EXCHANGE_RATE = "exchange_rate";
    public static final String REFERENCE_ER = "reference_er";
    public static String pay_er_type;
    public static final String BANK_NAME = "bank_name";
    public static final String BANK_FULL_NAME = "bank_full_name";
    public static String bank_name;
    public static final String PinBankName = "AGRIBANK";
    public static final String PinBankFullName = "Agribank: Ngân hàng Nông Nghiệp và Phát Triển Nông Thôn";
    // trustee
    public static final String TRUSTEE_NAME = "trustee_name";
    public static String trustee_name;
    public static final String TRUSTEE_PASSPORT_ID = "trustee_passport_id";
    public static String trustee_passport_id;
    public static final String TRUSTEE_PASSPORT_DUE = "trustee_passport_due";
    public static String trustee_passport_due;
    public static final String TRUSTEE_BIRTH_DAY = "trustee_birth_day";
    public static String trustee_birth_day;
    public static final String TRUSTEE_PHONE = "trustee_phone";
    public static String trustee_phone;
    public static final String TRUSTEE_ARC_PHOTO = "trustee_arc_photo";
    public static final String TRUSTEE_ARC_BACK_PHOTO = "trustee_arc_back_photo";
    public static final String TRUSTEE_SELF_PHOTO = "trustee_self_photo";
    public static final String TRUSTEE_SIGN = "trustee_sign";
    public static final String TRUSTEE_ARC_PHOTO_NORM = "trustee_arc_photo_norm";
    public static final String TRUSTEE_ARC_BACK_PHOTO_NORM = "trustee_arc_back_photo_norm";
    public static final String TRUSTEE_SELF_PHOTO_NORM = "trustee_self_photo_norm";
    public static final String TRUSTEE_SIGN_NORM = "trustee_sign_norm";
    public static final String RECEIPT_PHOTO_PATH = "receipt_photo_path";
    public static final String RECEIPT_PHOTO_PATH_NORM = "receipt_photo_path_norm";
    public static final String EUSO_TIMESTAMP = "euso_timestamp";
    public static final String TRUSTEE_NEED_REVIEW = "trustee_need_review";
    public static final String PROFILE_TYPE = "profile_type";
    public static final String COUPON_TYPE = "coupon_type";
    public static final String COUPON_GROCERY = "pos";
    public static final String COUPON_CARGO = "cargo";
    public static final String COUPON_GIFT = "gift";

    public static final float AMOUNT_LIMIT = 20000;
    public static final String AMOUNT = "amount";
    public static final String ER_TYPE = "er_type";
    public static float input_amount;
    public static String er_type;
    public static final String STRING_TOTAL_AMOUNT = "string_total_amount";
    public static final String STRING_NTD = "string_ntd";
    public static final String STRING_VND = "string_vnd";
    public static final String STRING_USD = "string_usd";
    public static float floatTotal, floatNTD, floatVND, floatUSD;
    public static String stringTotal, stringNTD, stringVND, stringUSD;
    public static String commaTotal, commaNTD, commaVND, commaUSD;

    public static final long OrderActiveTime = 6 * 60 * 60 * 1000; // 6 hours

    public static final String[] all_token = {
            PAYEE_NAME, PAYEE_PHONE, PAYEE_ID, PAYEE_AREA, PAYEE_ADDRESS, BANK_ACCOUNT,
            PAY_ER_TYPE, BANK_NAME,
            TRUSTEE_NAME, TRUSTEE_PASSPORT_ID, TRUSTEE_PASSPORT_DUE, TRUSTEE_BIRTH_DAY, TRUSTEE_PHONE,
    };
    public static void init()
    {
        setAmount("0", NTD);
        for(int i = 0; i < all_token.length; i++) {
            String str = Pref.get(all_token[i]);
            if(!Utils.isNull(str)) {
                mMap.put(all_token[i], str);
            }
        }
    }

    public static boolean setAmount(String _amount)
    {
        return setAmount(_amount, er_type);
    }
    public static boolean setAmount(String _amount, String _er_type)
    {
        String amount = _amount.replaceAll( ",", "" );
        input_amount = Utils.parseFloat(amount);
        er_type = _er_type;
        float amount_ntd = convertToNTD();
        if(er_type.equals(VND)) {
            amount_ntd = (float)Math.ceil(amount_ntd);
        }
        if(getTotalAmount(amount_ntd) > AMOUNT_LIMIT) {
//            return false;
        }
        floatNTD = amount_ntd;
        commaNTD = String.format(Locale.TAIWAN,"%,.0f", floatNTD);
        stringNTD = String.format(Locale.TAIWAN,"%.0f", floatNTD);
        floatVND = convertToVND();
        commaVND = String.format(Locale.TAIWAN,"%,.0f", floatVND);
        stringVND = String.format(Locale.TAIWAN,"%.0f", floatVND);
        floatUSD = convertToUSD();
        commaUSD = String.format(Locale.TAIWAN,"%,.0f", floatUSD);
        stringUSD = String.format(Locale.TAIWAN,"%.0f", floatUSD);
/*
        NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
        ((DecimalFormat)nf).applyPattern("###,###.##");
        commaUSD = nf.format(floatUSD);
*/
        return true;
    }
    public static float getTotalAmount(float amount_ntd)
    {
        return(amount_ntd + DB.getServiceFee() - DB.coupon_value);
    }
    public static void setTotalAmount()
    {
        floatTotal = getTotalAmount(PayInfo.floatNTD);
        commaTotal = String.format(Locale.TAIWAN, "%,.0f", floatTotal);
        stringTotal = String.format(Locale.TAIWAN, "%.0f", floatTotal);
    }
    public static float convertToNTD()
    {
        float f_amount = input_amount;
        switch(er_type) {
            case NTD:
                break;
            case VND:
                f_amount /= DB.erVND;
                break;
            case USD:
                f_amount *= DB.erUSD;
                break;
        }
        return f_amount;
    }
    public static float convertToVND()
    {
        float f_amount = input_amount;
        switch(er_type) {
            case NTD:
                f_amount *= DB.erVND;
                break;
            case VND:
                break;
            case USD:
                f_amount *= (DB.erVND * DB.erUSD);
                break;
        }
        return f_amount;
    }
    public static float convertToUSD()
    {
        float f_amount = input_amount;
        switch(er_type) {
            case NTD:
                f_amount /= DB.erUSD;
                break;
            case VND:
                f_amount /= (DB.erVND * DB.erUSD);
                break;
            case USD:
                break;
        }
        return f_amount;
    }
    public static void set(String key, String value)
    {
        if(Utils.isNull(value)) {
            return;
        }
        Pref.set(key, value);
    }
    public static String normVietText(String s)
    {
        String s_norm = Normalizer.normalize(s, Normalizer.Form.NFD);
        s_norm = s_norm.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        String s_up = s_norm.toUpperCase();
        // Currently, we have special characters:
        // Đ (-60, -112)
        // ₫ (-30, -126, -85)
        String s_remove = removeSpecialChar(s_up, (byte)-60, 2);
        s_remove = removeSpecialChar(s_remove, (byte)-30, 3);
        return s_remove;
    }
    public static String removeSpecialChar(String s, byte b, int size)
    {
        byte[] ba = s.getBytes();
        int count = 0;
        for(int i = 0; i < ba.length - 1; i++) {
            if(ba[i] == b)
                count++;
        }
        if(count == 0)
            return s;

        byte[] ba2 = new byte[ba.length - (size - 1) * count];
        int target = 0;
        int i = 0;
        while(i < ba.length - 1) {
            if(ba[i] == b) {
                ba2[target] = 'D';
                i += size;
            } else {
                ba2[target] = ba[i];
                i++;
            }
            target++;
        }
        if(i < ba.length) {
            ba2[target] = ba[i];
        }
        String s_remove = s;
        try {
            s_remove = new String(ba2, "UTF-8");
        } catch (Exception e) {

        }
        return s_remove;
    }
    public static String normAmount(String am)
    {
        float f_am = Utils.parseFloat(am);
        String norm = String.format(Locale.TAIWAN,"%,.0f", f_am );
        return norm;
    }
}
