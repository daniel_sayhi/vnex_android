package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.api.RespBase;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.misc.DbRemit;
import tw.sayhi.vnex.widget.BarCode;
import tw.sayhi.vnex.widget.ImageCapture;

public class SevenActivity extends BaseActivity
{
    Activity mActivity = this;
    String euso_order;
    ImageCapture imageCapture;
    String receiptPhotoPath;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.seven__activity);

        euso_order = getIntentValue(DbOrder.EUSO_ORDER);

        imageCapture = new ImageCapture(mActivity);

        buildViews();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode != RESULT_OK) {
            return;
        }
        ImageCapture.ImageDetails imageDetails;
        ImageView iv = (ImageView)findViewById(R.id.iv_receipt_photo);
        imageDetails = imageCapture.getImagePath(requestCode, RESULT_OK, data);
        Bitmap org_bitmap = imageDetails.getBitmap();
        if(org_bitmap == null) {
            return;
        }
        iv.setImageBitmap(org_bitmap);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(org_bitmap, Config.photo_width, Config.photo_height, true);
        receiptPhotoPath = ImageCapture.saveImage(mActivity, org_bitmap, Config.receipt_file_name);
        Pref.set(PayInfo.RECEIPT_PHOTO_PATH_NORM, ImageCapture.saveImage(mActivity, scaledBitmap, Config.receipt_file_name_norm));
    }
    void buildViews()
    {
        TextView tvMainHeader = (TextView)findViewById(R.id.tv_main_header);
        tvMainHeader.setText(R.string.super_store);
        TextView tvPay = (TextView)findViewById(R.id.tv_pay);
        tvPay.setText(R.string.pay_lower);

        Cursor c = DbOrder.read(euso_order);
        buildSevenBanner(c, false);

        ImageView iv = (ImageView)findViewById(R.id.iv_barcode1);
        TextView tv = (TextView)findViewById(R.id.tv_barcode1);
        String barcode1 = DbRemit.getString(c, DbOrder.BARCODE1);
        BarCode.buildBarCode(barcode1, iv, Config.barcode_width, Config.barcode_height);
        tv.setText(barcode1);
        iv = (ImageView)findViewById(R.id.iv_barcode2);
        tv = (TextView)findViewById(R.id.tv_barcode2);
        String barcode2 = DbRemit.getString(c, DbOrder.BARCODE2);
        BarCode.buildBarCode(barcode2, iv, Config.barcode_width, Config.barcode_height);
        tv.setText(barcode2);
        iv = (ImageView)findViewById(R.id.iv_barcode3);
        tv = (TextView)findViewById(R.id.tv_barcode3);
        String barcode3 = DbRemit.getString(c, DbOrder.BARCODE3);
        BarCode.buildBarCode(barcode3, iv, Config.barcode_width, Config.barcode_height);
        tv.setText(barcode3);

        findViewById(R.id.iv_receipt_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureImage(imageCapture, getString(R.string.receipt_photo));
            }
        });

        findViewById(R.id.tv_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSend();
            }
        });
    }
    void onSend()
    {
        if(Utils.isNull(receiptPhotoPath)) {
            warnDialog(R.string.receipt_photo, R.string.empty_field, -1);
            return;
        }
        final AlertDialog waitDialog = showWaitDialog(-1);
        new Thread() {
            @Override
            public void run() {
                int result = DB.uploadReceipt(mActivity);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!isFinishing()) {
                            waitDialog.cancel();
                        }
                    }
                });
                switch (result) {
                    case RespBase.RESP_TIME_OUT:
                        dialogWarn(R.string.main_item1, R.string.network_timeout);
                        break;
                    case RespBase.RESP_SUCCESS:
                        DbOrder.update(euso_order, DbOrder.UPLOAD_RECEIPT, "1");
                        if(isWorkingHour()) {
                            orderUnderReview(R.string.upload_receipt_message);
                        } else {
                            orderUnderReview(R.string.upload_receipt_message_off);
                        }
                        break;
                    case RespBase.RESP_FAIL:
                        Utils.dialogOK(mActivity, getString(R.string.order_error), DB.respRemitOrder.message, R.drawable.warning,
                                getString(R.string.back));
                        break;
                }
            }
        }.start();
    }
    boolean isWorkingHour()
    {
        Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date
        int curr_hour = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        if(curr_hour >= 9 && curr_hour < 21) {
            return true;
        }
        return false;
    }
}
