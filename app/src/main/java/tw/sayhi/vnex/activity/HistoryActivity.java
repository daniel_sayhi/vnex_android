package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ListView;

import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.MainActivity;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.adapter.HistoryAdapter;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.misc.DbRemit;
import tw.sayhi.vnex.misc.QueryApiService;

public class HistoryActivity extends BaseActivity
{
    Activity mActivity = this;
    LinearLayout llLogout;
    ListView mListView;
    HistoryAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.history__activity);

        buildViews();

        buildListView(true);

        startApi(R.string.app_name, R.string.loading);
        new Thread() {
            @Override
            public void run() {
                boolean success = QueryApiService.OnQueryAllRemitOrder(mActivity, DB.getUuid(), false);
                endApi();
                if(success) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            buildListView(false);
                        }
                    });
                }
            }
        }.start();
        /*
        apiService(QueryApiService.QUERY_ALL_REMIT_ORDER, true, new Runnable() {
            @Override
            public void run() {
                if(DB.bTransHistory) {
                    buildListView();
                } else {
                    new Thread() {
                        @Override
                        public void run() {
                            onApiFail(DB.allRemitOrder, null);
                        }
                    }.start();
                }
            }
        }, QueryApiService.NOTIFY_USER, QueryApiService.QUERY_TRANS_HISTORY); */
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        restartClass(MainActivity.class);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    void buildViews()
    {
        buildTitleMenu();
        mListView = findViewById(R.id.listview);
    }
    void buildListView(boolean first_time)
    {
        if(first_time) {
            addFooterView(mListView);
        }
        Cursor cursor = readHistory();
        mAdapter = new HistoryAdapter(mActivity, -1, cursor,
                new String[]{DbOrder.SENDER_NAME}, new int[]{R.id.tv_payee_name}, mActivity);

        mListView.setAdapter(mAdapter);
    }
    Cursor readHistory()
    {
        Cursor cursor = DbOrder.readBy(DbOrder.SENDER_ARC, BaseActivity.mLoginArcNo, DbOrder.CREATE_TIME + " DESC");
        int count = cursor.getCount();
        if(count > 1000) { //todo
            for(int i = 10; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                DbOrder.delete(DbRemit.getString(cursor, DbOrder.EUSO_ORDER));
            }
            cursor.close();
            cursor = DbOrder.readBy(DbOrder.SENDER_ARC, BaseActivity.mLoginArcNo, DbOrder.CREATE_TIME + " DESC");
        }
        return cursor;
    }
}
