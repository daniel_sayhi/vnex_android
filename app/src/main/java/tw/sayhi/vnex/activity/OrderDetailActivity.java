package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.DebugOptions;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.misc.DbRemit;
import tw.sayhi.vnex.misc.OrderData;
import tw.sayhi.vnex.misc.QueryApiService;

public class OrderDetailActivity extends BaseActivity
{
    Activity mActivity = this;
    String euso_order;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.order_detail__activity);

        euso_order = getIntentValue(DbOrder.EUSO_ORDER);

        buildViews();
    }
    int nClickTitle = 0;
    long last_time = 0;
    void buildViews()
    {
        buildTitleMenu();
        scaleTextViewSize(R.id.root, 0.8f);
        Cursor c = DbOrder.read(euso_order);
        OrderData os = new OrderData(c);
        String trans_type = DbRemit.getString(c, DbOrder.TRANS_TYPE);
//        TextView tvHeader = findViewById(R.id.tv_trans_type_header);
//        LinearLayout llOrderNumber = findViewById(R.id.ll_order_number);
        TextView tvPayWay = findViewById(R.id.tv_pay_way);
        switch (trans_type) {
            case PayInfo.DTD: //送到家
//                tvHeader.setText(R.string.remit_order_dtd_header);
//                llOrderNumber.setBackgroundColor(getResources().getColor(R.color.dtd_background));
                findViewById(R.id.ll_choice).setVisibility(View.GONE);
//                tvPayWay.setText(R.string.dtd_pay_way);
                tvPayWay.setText(R.string.remit_dtd);
                findViewById(R.id.ll_address).setVisibility(View.VISIBLE);
                break;
            case PayInfo.BTB: //帳號
//                tvHeader.setText(R.string.remit_order_btb_header);
//                llOrderNumber.setBackgroundColor(getResources().getColor(R.color.btb_background));
                findViewById(R.id.ll_payee_id).setVisibility(View.GONE);
//                tvPayWay.setText(R.string.btb_pay_way);
                tvPayWay.setText(R.string.remit_btb);
                break;
            case PayInfo.IDP: //ID取款
//                tvHeader.setText(R.string.remit_order_idp_header);
//                llOrderNumber.setBackgroundColor(getResources().getColor(R.color.idp_background));
//                tvPayWay.setText(R.string.idp_pay_way);
                tvPayWay.setText(R.string.remit_idp);
                break;
            case PayInfo.PIN: //密碼
//                tvHeader.setText(R.string.remit_order_pin_header);
//                llOrderNumber.setBackgroundColor(getResources().getColor(R.color.pin_background));
                findViewById(R.id.ll_payee_id).setVisibility(View.GONE);
//                tvPayWay.setText(R.string.pin_pay_way);
                tvPayWay.setText(R.string.remit_pin);
                findViewById(R.id.ll_withdrawal_password).setVisibility(View.VISIBLE);
                setTextView(R.id.tv_withdrawal_password, c, DbOrder.PICKUP_CODE);
                break;
        }
        setTextView(R.id.tv_order_number, c, DbOrder.EUSO_ORDER);
        // Payee Info
        setTextView(R.id.tv_payee_name, c, DbOrder.RECIPINENT_NAME);
        setTextView(R.id.tv_payee_id, c, DbOrder.RECIPINENT_PERSONAL_ID);
        setTextView(R.id.tv_payee_phone, c, DbOrder.RECIPINENT_TEL);
        setTextView(R.id.tv_bank_name, c, DbOrder.BANK_NAME);
        String payER = DbRemit.getString(c, DbOrder.EXCHANGE_RATE_FLAG);
        String pay_amount = PayInfo.normAmount(DbRemit.getString(c, DbOrder.REFERENCE_ER));
        setTextView(R.id.tv_pay_amount, pay_amount + " " + payER);
        // Trustee Info
        setTextView(R.id.tv_remit_name, c, DbOrder.SENDER_NAME);
        setTextView(R.id.tv_permit_id, c, DbOrder.SENDER_ARC);
        String sender_arc_term = DbRemit.getString(c, DbOrder.SENDER_ARC_TERM);
        String arc_term_norm = sender_arc_term.replaceAll("-", "/");
        setTextView(R.id.tv_permit_due, arc_term_norm);
        String sender_birthday = DbRemit.getString(c, DbOrder.SENDER_BIRTHDAY);
        String birthday_norm = sender_birthday.replaceAll("-", "/");
        setTextView(R.id.tv_birthday, birthday_norm);
        setTextView(R.id.tv_trustee_phone, c, DbOrder.SENDER_MOBILE);
        setTextView(R.id.tv_address, c, DbOrder.RECIPINENT_ADDRESS2);
        setTextView(R.id.tv_trade_date, Utils.getDateStringYyyyMmDd("" + os.l_create_time));
        String amount_nt = PayInfo.normAmount(DbRemit.getString(c, DbOrder.AMOUNT));
        setTextView(R.id.tv_amount_nt, amount_nt);
        String service_fee = DbRemit.getString(c, DbOrder.SERVICE_FEE);
        setTextView(R.id.tv_service_fee, service_fee);
        String discount_fee = DbRemit.getString(c, DbOrder.COUPON_VALUE);
        setTextView(R.id.tv_discount, "-" + discount_fee);
        String red_point_discount = DbRemit.getString(c, DbOrder.RED_POINT_DISCOUNT);
        if(nonNull(red_point_discount) && !red_point_discount.equals("0")) {
            red_point_discount = "-" + red_point_discount;
        }
        setTextView(R.id.tv_red_point_discount, red_point_discount);
        String total_amount = PayInfo.normAmount(DbRemit.getString(c, DbOrder.TOTAL_AMOUNT));
        setTextView(R.id.tv_total_amount, total_amount);
        ImageView ivLogo = findViewById(R.id.iv_vnex_logo);
        ivLogo.setClickable(true);
        ivLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long curr_time = System.currentTimeMillis();
                long diff_time = curr_time - last_time;
                last_time = curr_time;
                if(DebugOptions.bGeneral && diff_time < 1500) {
                    nClickTitle++;
                    if(nClickTitle >= 2) {
                        Pref.addOrderPaid(euso_order);
                        Utils.postMessage(mActivity, euso_order + " is paid.");
                    }
                } else {
                    nClickTitle = 0;
                }
            }
        });
    }
}
