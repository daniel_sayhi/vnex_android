package tw.sayhi.vnex.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import tw.sayhi.shared.util.ImageURL;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.api.DataExchangeCoupon;
import tw.sayhi.vnex.api.RespQueryExchangeCoupon;
import tw.sayhi.vnex.widget.ActionDialog;
import tw.sayhi.vnex.widget.BarCode;

public class ExchangeCouponActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] ids = {
            R.id.iv_coupon_1, R.id.iv_coupon_2, R.id.iv_coupon_3, R.id.iv_coupon_4, R.id.iv_coupon_5,
            R.id.iv_coupon_6, R.id.iv_coupon_7, R.id.iv_coupon_8, R.id.iv_coupon_9,
            R.id.ll_invite_friend
    };
    String coupon_type;
    RespQueryExchangeCoupon exchangeCoupon = null;
    List<DataExchangeCoupon> coupons;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.exchange_coupon__activity);

        coupon_type = getIntentValue(PayInfo.COUPON_TYPE);
        exchangeCoupon = DB.getExchangeCoupon(coupon_type);
        coupons = exchangeCoupon.dataExchangeCoupons;

        buildViews();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            buildListeners(view);
        }
    };
    @SuppressLint("NonConstantResourceId")
    void buildListeners(View view)
    {
        switch (view.getId()) {
            case R.id.ll_invite_friend:
                inviteFriend();
                break;
            default:
                onExchangeCoupon(view.getId());
                break;
        }
    }
    void buildViews()
    {
        ImageView ivPromoteNo = findViewById(R.id.iv_promote_no);
        BarCode.buildBarCode(DB.getPromoteNo(), ivPromoteNo, 1080, Config.barcode_height + 50);
        TextView tvPromoteNo = findViewById(R.id.tv_promote_no);
        tvPromoteNo.setText(DB.getPromoteNo());
        TextView tvRedPoint = findViewById(R.id.tv_cash_points);
        tvRedPoint.setText("" + DB.redPoint);
        if(isChinese) {
            setTextView(R.id.tv_header, "累計點數");
            setTextView(R.id.tv_invite_friend, "邀請好友累計點數");
            setTextView(R.id.tv_remain_points, "總點數");
            setTextView(R.id.tv_point, "點");
        }
        for(int i = 0; i < coupons.size(); i++) {
            DataExchangeCoupon coupon = coupons.get(i);
            if(DB.redPoint >= coupon.exchange_bonus_cost) {
                ImageView iv = findViewById(ids[i]);
                iv.setVisibility(View.VISIBLE);
                iv.setClickable(true);
                ImageURL.load(coupon.exchange_image_url, iv, mActivity);
            }
        }
        for(int id : ids) {
            findViewById(id).setOnClickListener(onClickListener);
        }
    }
    void onExchangeCoupon(int id)
    {
        // find the index first
        int index = -1;
        for(int i = 0 ; i < ids.length; i++) {
            if(ids[i] == id) {
                index = i;
                break;
            }
        }
        if(index < 0)
            return;
        final DataExchangeCoupon coupon = coupons.get(index);
        new ActionDialog(mActivity, R.string.cash_points_header, R.string.exchange_coupon_warning,
                R.drawable.warning, R.string.okay, R.string.cancel) {
            @Override
            public void onOk() {
                doExchangeCoupon(coupon);
            }
        };
    }
    void doExchangeCoupon(DataExchangeCoupon coupon)
    {
//        nextActivity(BarcodeCaptureActivity.class);
        /*
        startApi(R.string.app_name, R.string.loading);
        new Thread() {
            @Override
            public void run() {
                DB.exchangeCoupon(mActivity, coupon);
                endApi();
                if(!DB.bExchangeCoupon) {
                    warnAndFinish(mActivity, R.string.app_name, getString(R.string.network_timeout));
                    return;
                }
            }
        }.start();
         */
    }
    void setLocked(int id)
    {
        ImageView v = findViewById(id);
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);  //0 means grayscale
        ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
        v.setColorFilter(cf);
        v.setImageAlpha(128);   // 128 = 0.5
    }
    void  setUnlocked(ImageView v)
    {
        v.setColorFilter(null);
        v.setImageAlpha(255);
    }
}
