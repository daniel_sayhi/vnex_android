package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.BuildConfig;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.DebugOptions;
import tw.sayhi.vnex.MainActivity;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.PortalActivity;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.api.EusoApi;
import tw.sayhi.vnex.misc.DbSender;
import tw.sayhi.vnex.misc.Valid;
import tw.sayhi.vnex.widget.ActionDialog;

public class SenderTwActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] fields = { R.id.et_arc_no, R.id.et_password, R.id.et_name, R.id.et_mobile };
    int[] titles = { R.string.tw_arc_no, R.string.tw_password, R.string.tw_name, R.string.tw_mobile
    };
    int[] ids = { R.id.iv_password, R.id.tv_birthday, R.id.tv_next, R.id.tv_sign_up };
    EditText etArcNo;
    TextView tvBirthday;
    EditText etPassword;
    ImageView ivPassword;
    boolean viewPassword = false;
    TextView tvNext;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.sender_tw__activity);

        buildViews();

    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            buildListeners(view);
        }
    };
    void buildListeners(View view)
    {
        switch (view.getId()) {
            case R.id.iv_password:
                onPassword();
                break;
            case R.id.tv_birthday:
                datePickerDialog(tvBirthday, true, false);
                break;
            case R.id.tv_next:
                onNext();
                break;
            case R.id.tv_sign_up:
                if(BuildConfig.DEBUG || DebugOptions.bUseTestURL)
                    fillViews();
                break;
        }
    }
    void buildViews()
    {
//        adjustLayoutScale();
        etArcNo = findViewById(R.id.et_arc_no);
        etArcNo.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        etPassword = findViewById(R.id.et_password);
        etPassword.setTransformationMethod(new PasswordTransformationMethod());
        ivPassword = findViewById(R.id.iv_password);
        tvBirthday = findViewById(R.id.tv_birthday);
        tvNext = findViewById(R.id.tv_next);
        // build EditText listeners
        for(int id : ids) {
            findViewById(id).setOnClickListener(onClickListener);
        }
    }
    boolean verifyPassword()
    {
        String password = etPassword.getText().toString();
        if (!Valid.isPassword(password)) {
            warnDialog(R.string.tw_password, R.string.tw_format_error, R.id.et_password);
            return false;
        }
        return true;
    }
    boolean verifyData()
    {
        if(invalidLengthTw(fields, titles, 20)) {
            return false;
        }
        // validate format of AB12345678
        String arc_no = etArcNo.getText().toString();
        if(!Valid.isArcNo(arc_no) && !Valid.isNewArcNo(arc_no)) {
            warnDialog(R.string.tw_arc_no, R.string.tw_format_error, R.id.et_arc_no);
            return false;
        }
        if(!verifyPassword())
            return false;
        // validate date
        String birthday = tvBirthday.getText().toString();
        if(Utils.isNull(birthday)) {
            warnDialog(R.string.tw_birthday, R.string.tw_empty_field, -1);
            return false;
        }
        if(!Utils.isValidBirthday(birthday, 18)) {
            warnDialog(R.string.tw_birthday, R.string.tw_under_18, -1);
            return false;
        }
        String phone = getEditText(R.id.et_mobile);
        if(!Valid.isMobile(phone) && !Valid.isLocalPhone(phone)) {
            warnDialog(R.string.tw_mobile, R.string.tw_format_error, R.id.et_mobile);
            return false;
        }
        return true;
    }
    void onNext()
    {
        Utils.hideKeyboard(mActivity);
        if(!verifyData()) {
            return;
        }

        storeViews();

        PayInfo.set(PayInfo.TRUSTEE_NEED_REVIEW, "1");

        doRegister();

    }
    void storeViews()
    {
        storeEditView(PayInfo.TRUSTEE_PASSPORT_ID, R.id.et_arc_no);
        storeEditView(PayInfo.REGISTER_PASSWORD, R.id.et_password);
        storeEditView(PayInfo.TRUSTEE_NAME, R.id.et_name);
        PayInfo.set(PayInfo.TRUSTEE_PASSPORT_DUE, "2023/01/01");
        PayInfo.set(PayInfo.TRUSTEE_BIRTH_DAY, tvBirthday.getText().toString());
        storeEditView(PayInfo.TRUSTEE_PHONE, R.id.et_mobile);
        storeEditView(DbSender.PROMOTE_TO, R.id.et_promote_to);
        PayInfo.set(PayInfo.PROFILE_TYPE, "2");
        PayInfo.set(PayInfo.TRUSTEE_ARC_PHOTO, "");
        PayInfo.set(PayInfo.TRUSTEE_SELF_PHOTO, "");
        PayInfo.set(PayInfo.TRUSTEE_ARC_BACK_PHOTO, "");
    }
    // debugging purpose: quickly fill each field for testing
    void fillViews()
    {
        setEditText(DebugOptions.arc_no, R.id.et_arc_no);
        setEditText(DebugOptions.password, R.id.et_password);
        setEditText("Daniel", R.id.et_name);
        tvBirthday.setText("1990/12/31");
        setEditText("0919550470", R.id.et_mobile);
    }
    void onPassword()
    {
        if(viewPassword == true) {
            etPassword.setTransformationMethod(new PasswordTransformationMethod());
            ivPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_hide));
            viewPassword = false;
        } else {
            etPassword.setTransformationMethod(null);
            ivPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_show));
            viewPassword = true;
        }
    }
    void doRegister()
    {
        final String password = Pref.get(PayInfo.REGISTER_PASSWORD);
        startApi(R.string.app_name, R.string.sending);
        new Thread() {
            @Override
            public void run() {
                DB.register(mActivity, true);
                if (DB.bRegister) {
                    DB.clearSender();
                    DB.login(mActivity, Pref.get(PayInfo.TRUSTEE_PASSPORT_ID), password);
                    endApi();
                    actionDialog("加入會員", "您已註冊成功，成為會員。", -1, new Runnable() {
                        @Override
                        public void run() {
                            nextActivity(PortalActivity.class);
                        }
                    });
                    return;
                }
                endApi();
                // exception handling
                if (DB.respRegister != null) {
                    Pref.set(PayInfo.REGISTER_PASSWORD, "");
                    if(DB.respRegister.message.startsWith("Your ARC already reg")) {
                        String arc_no = etArcNo.getText().toString();
                        warnDialog(R.string.app_name, arc_no + "已註冊過。", -1);
                    } else {
                        warnDialog(R.string.app_name, DB.respRegister.message, -1);
                    }
                } else {
                    Pref.set(PayInfo.REGISTER_PASSWORD, "");
                    onApiFail(null, null);
                }
            }
        }.start();
    }
}