package tw.sayhi.vnex.activity;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.R;

public class WebViewActivity extends BaseActivity
{
    boolean isLandscape;
    WebView webView;
    LinearLayout llLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
/*
        isLandscape = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE);
        if(isLandscape) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN );
        }
*/

        setContentView(R.layout.webview__activity);

        buildViews();

        /*
            WebSettings
                Manages settings state for a WebView. When a WebView is first created, it obtains a
                set of default settings. These default settings will be returned from any getter
                call. A WebSettings object obtained from WebView.getSettings() is tied to the life
                of the WebView. If a WebView has been destroyed, any method call on WebSettings
                will throw an IllegalStateException.
        */
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        /*
            public abstract void setSupportZoom (boolean support)
                Sets whether the WebView should support zooming using its on-screen zoom controls
                and gestures. The particular zoom mechanisms that should be used can be set with
                setBuiltInZoomControls(boolean). This setting does not affect zooming performed
                using the zoomIn() and zoomOut() methods. The default is true.

            Parameters
                support : whether the WebView should support zoom

        */
        settings.setSupportZoom(true);
        settings.setDisplayZoomControls(true);
        settings.setBuiltInZoomControls(true);
//        webView.setInitialScale(1);
        settings.setUseWideViewPort(true);
        // Sets whether the WebView loads pages in overview mode, that is, zooms out the content to fit on screen by width.
        settings.setLoadWithOverviewMode(true);
/*
        // For API level below 18 (This method was deprecated in API level 18)
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);

        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
*/
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//                return true;
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                llLoading.setVisibility(View.VISIBLE);
                MyLog.i("onPageStarted() " + url);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                llLoading.setVisibility(View.GONE);
                trackEvent("WebView", Utils.trimURL(url));
                MyLog.i("onPageFinished() " + url);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                handler.cancel();
            }
        });

        if(mWebViewHTML != null) {
            String mime = "text/html";
            String encoding = "utf-8";
            webView.loadData(mWebViewHTML, mime, encoding);
        } else {
            webView.loadUrl(mWebViewURL);
//            webView.loadUrl("http://yes777.stargame.com.tw/act/yes777/rl77dcf5/index.html");
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId=item.getItemId();
        if(itemId==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void buildViews()
    {
        buildTitleMenu();
        webView = (WebView)findViewById(R.id.webview);
        llLoading = (LinearLayout)findViewById(R.id.ll_loading);
    }
    //Binds OS back button to webview
/*
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }
*/
}
