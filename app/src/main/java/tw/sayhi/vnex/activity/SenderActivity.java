package tw.sayhi.vnex.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.concurrent.CountDownLatch;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import androidx.core.app.ActivityCompat;
import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.ImageUtil;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.BuildConfig;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.DebugOptions;
import tw.sayhi.vnex.MainActivity;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.PortalActivity;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.api.EusoApi;
import tw.sayhi.vnex.api.RespSenderProfile;
import tw.sayhi.vnex.misc.DbSender;
import tw.sayhi.vnex.misc.Valid;
import tw.sayhi.vnex.widget.ActionDialog;
import tw.sayhi.vnex.widget.CapturePhoto;
import tw.sayhi.vnex.widget.ImageCapture;

public class SenderActivity extends BaseActivity
{
    Activity mActivity = this;
    CountDownLatch latchPermission = new CountDownLatch(1);
    int[] fields = { R.id.et_name, R.id.et_arc_no, R.id.et_mobile};
    int[] titles = { R.string.trustee_name, R.string.trustee_id, R.string.trustee_phone };
    int[] max_len = { 80, 20, 20 };
    int[] max_messages = { R.string.max_len_80, R.string.max_len_20, R.string.max_len_20 };
    int[] ids = { 
            R.id.iv_password, R.id.tv_expiry, R.id.tv_birthday,
            R.id.iv_arc_front, R.id.iv_selfie, R.id.iv_arc_back, R.id.tv_next };
    EditText etArcNo;
    TextView tvExpiry, tvBirthday;
    EditText etPassword, etOldPassword, etNewPassword, etConfirmPassword;
    ImageView ivPassword, ivOldPassword, ivNewPassword, ivConfirmPassword;
    boolean viewPassword = false;
    boolean viewOldPassword = false;
    boolean viewNewPassword = false;
    boolean viewConfirmPassword = false;
    TextView tvNext;
    ImageView ivArcFront, ivSelfie, ivArcBack;
    ImageCapture imageCapture;
    CapturePhoto capturePhoto;
    String arcPhotoPath, selfPhotoPath, arcBackPhotoPath;
    boolean arcPhotoChanged, selfPhotoChanged, arcBackPhotoChanged;
    int imageID = 0;
    boolean trusteeTheSame = true;
    boolean isRegister = false, isArcExpired;
    boolean isSenderSuccess = false;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.sender__activity);

        isRegister = intentContains(IS_REGISTER);
        isArcExpired = intentContains(ARC_EXPIRED); // todo

        imageCapture = new ImageCapture(mActivity);
        capturePhoto = new CapturePhoto(mActivity);

        buildViews();

        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, 999);
        if(!isRegister || isArcExpired) {
            startApi(R.string.app_name, R.string.loading);
            new Thread() {
                @Override
                public void run() {
                    DB.queryProfile(mActivity);
                    endApi();
                    Utils.waitFor(latchPermission);
                    if(DB.bQueryProfile) {
                        DbSender.update(DB.getSenderArc(), DbSender.STATUS, DB.querySenderProfile.status);
                        DB.setStatus(DB.querySenderProfile.status);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                initViews(DB.querySenderProfile);
                            }
                        });
                    } else {
                        finish();
                    }
                }
            }.start();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        latchPermission.countDown();
        for(int i = 0; i < permissions.length; i++) {
            if(grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                finish();
                return;
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        trusteeTheSame = false;
        Bitmap org_bitmap;
        if (requestCode == CapturePhoto.CAPTURE_PHOTO_CODE) {
            org_bitmap = imageCapture.getCaptureBitmap(Config.photo_width, Config.photo_height);
            if (org_bitmap == null) {
                warnDialog(R.string.camera_photo, R.string.format_error, -1);
                return;
            }
        } else {
            ImageCapture.ImageDetails imageDetails;
            imageDetails = imageCapture.getImagePath(requestCode, RESULT_OK, data);
            org_bitmap = imageDetails.getBitmap();
            if (org_bitmap == null) {
                warnDialog(R.string.gallery_photo, R.string.format_error, -1);
                return;
            }
            MyLog.i("image path=" + imageDetails.getPath());
        }
        MyLog.i("image width=" + org_bitmap.getWidth() + " height=" + org_bitmap.getHeight());
        saveImage(org_bitmap, imageID);
        switch (imageID) {
            case R.id.iv_arc_front:
                arcPhotoChanged = true;
                break;
            case R.id.iv_selfie:
                selfPhotoChanged = true;
                break;
            case R.id.iv_arc_back:
                arcBackPhotoChanged = true;
                break;
        }
    }
    void saveUrlImage(Bitmap bitmap, int image_id)
    {
        ImageView iv = findViewById(image_id);
        iv.setImageBitmap(bitmap);
        switch (image_id) {
            case R.id.iv_arc_front:
                arcPhotoPath = ImageCapture.saveImage(mActivity, bitmap, Config.arc_file_name_norm);
                Pref.set(PayInfo.TRUSTEE_ARC_PHOTO_NORM, arcPhotoPath);
                break;
            case R.id.iv_selfie:
                selfPhotoPath = ImageCapture.saveImage(mActivity, bitmap, Config.selfie_file_name_norm);
                Pref.set(PayInfo.TRUSTEE_SELF_PHOTO_NORM, selfPhotoPath);
                break;
            case R.id.iv_arc_back:
                arcBackPhotoPath = ImageCapture.saveImage(mActivity, bitmap, Config.arc_back_file_name_norm);
                Pref.set(PayInfo.TRUSTEE_ARC_BACK_PHOTO_NORM, arcBackPhotoPath);
                break;
        }
    }
    void saveImage(Bitmap org_bitmap, int image_id)
    {
        try {
            _saveImage(org_bitmap, image_id);
        } catch(Exception e) {
            DB.reportException("saveImage", e);
        }
    }
    void _saveImage(Bitmap org_bitmap, int image_id)
    {
        if(org_bitmap == null)
            return;
        int org_width = org_bitmap.getWidth();
        int org_height = org_bitmap.getHeight();
        int dst_width, dst_height;
        if(org_width >= org_height) {
            dst_width = Config.photo_width;
            dst_height = Config.photo_height;
        } else {
            dst_width = Config.photo_height;
            dst_height = Config.photo_width;
        }
        Bitmap scaled = ImageUtil.scalePreserveAspect(org_bitmap, dst_width, dst_height);
        Bitmap landscape = ImageUtil.landscape(scaled);
        ImageView iv = findViewById(image_id);
        iv.setImageBitmap(landscape);
        switch (image_id) {
            case R.id.iv_arc_front:
                arcPhotoPath = ImageCapture.saveImage(mActivity, scaled, Config.arc_file_name_norm);
                Pref.set(PayInfo.TRUSTEE_ARC_PHOTO_NORM, arcPhotoPath);
                break;
            case R.id.iv_selfie:
                selfPhotoPath = ImageCapture.saveImage(mActivity, scaled, Config.selfie_file_name_norm);
                Pref.set(PayInfo.TRUSTEE_SELF_PHOTO_NORM, selfPhotoPath);
                break;
            case R.id.iv_arc_back:
                arcBackPhotoPath = ImageCapture.saveImage(mActivity, scaled, Config.arc_back_file_name_norm);
                Pref.set(PayInfo.TRUSTEE_ARC_BACK_PHOTO_NORM, arcBackPhotoPath);
                break;
        }
    }
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            buildListeners(view);
        }
    };
    void buildListeners(View view)
    {
        switch (view.getId()) {
            case R.id.iv_password:
                onPassword();
                break;
            case R.id.tv_expiry:
                datePickerDialog(tvExpiry, false, true);
                break;
            case R.id.tv_birthday:
                if(!isSenderSuccess) {
                    datePickerDialog(tvBirthday, true, true);
                }
                break;
            case R.id.iv_arc_front:
                imageID = R.id.iv_arc_front;
                if(imageCapture.canPickImage())
                    captureImage(imageCapture, getString(R.string.arc_front));
                else
                    imageCapture.captureImage();
                break;
            case R.id.iv_selfie:
                imageID = R.id.iv_selfie;
                if(imageCapture.canPickImage())
                    useCamera(imageCapture);
                else
                    imageCapture.captureImage();
                break;
            case R.id.iv_arc_back:
                imageID = R.id.iv_arc_back;
                if(imageCapture.canPickImage())
                    captureImage(imageCapture, getString(R.string.arc_back));
                else
                    imageCapture.captureImage();
                break;
            case R.id.tv_next:
                onNext();
                break;
        }
    }
    void buildViews()
    {
        adjustLayoutScale();

        etArcNo = findViewById(R.id.et_arc_no);
        etArcNo.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        etPassword = findViewById(R.id.et_password);
        etPassword.setTransformationMethod(new PasswordTransformationMethod());
        if(!isRegister) {
            etPassword.setText(DB.getPassword());
        }
        ivPassword = findViewById(R.id.iv_password);
        tvExpiry = findViewById(R.id.tv_expiry);
        tvBirthday = findViewById(R.id.tv_birthday);
        tvNext = findViewById(R.id.tv_next);
        ivArcFront = findViewById(R.id.iv_arc_front);
        ivArcBack = findViewById(R.id.iv_arc_back);
        ivSelfie = findViewById(R.id.iv_selfie);
        // build EditText listeners
        for(int id : ids) {
            findViewById(id).setOnClickListener(onClickListener);
        }
        findViewById(R.id.tv_sign_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(BuildConfig.DEBUG)
                    fillViews();
            }
        });
    }
    boolean verifyPassword()
    {
        String password = etPassword.getText().toString();
        if(isRegister) {
            if (Utils.isNull(password)) {
                warnDialog(R.string.password_header, R.string.empty_field, R.id.et_password);
                return false;
            }
            if (!Valid.isPassword(password)) {
                warnDialog(R.string.password_header, R.string.password_format, R.id.et_password);
                return false;
            }
        }
        return true;
    }
    boolean verifyData()
    {
        if(invalidLength(fields, titles, max_len, max_messages)) {
            return false;
        }
        // validate format of AB12345678
        String arc_no = etArcNo.getText().toString();
        if(!Valid.isArcNo(arc_no) && !Valid.isNewArcNo(arc_no)) {
            warnDialog(R.string.arc_no_header, R.string.format_error, R.id.et_arc_no);
            return false;
        }
        if(!verifyPassword())
            return false;

        // validate date
        String expiry_date = tvExpiry.getText().toString();
        String birthday = tvBirthday.getText().toString();
        if(!Utils.isDateEffective(expiry_date)) {
            warnDialog(R.string.expiry_header, R.string.arc_is_expired, -1);
            return false;
        }
        if(!Utils.isValidBirthday(birthday, 18)) {
            warnDialog(R.string.birthday_header, R.string.under_18, -1);
            return false;
        }
        String phone = getEditText(R.id.et_mobile);
        if(!Valid.isMobile(phone) && !Valid.isLocalPhone(phone)) {
            warnDialog(R.string.trustee_phone, R.string.format_error, R.id.et_mobile);
            return false;
        }
        if(Utils.isNull(arcPhotoPath)) {
            warnDialog(R.string.arc_front_header, R.string.empty_field, -1);
            return false;
        }
        if(Utils.isNull(selfPhotoPath)) {
            warnDialog(R.string.selfie_header, R.string.empty_field, -1);
            return false;
        }
        if(Utils.isNull(arcBackPhotoPath)) {
            warnDialog(R.string.arc_back_header, R.string.empty_field, -1);
            return false;
        }
        return true;
    }
    boolean verifyPasswordChange()
    {
        String old_password = etOldPassword.getText().toString();
        String new_password = etNewPassword.getText().toString();
        String confirm_password = etConfirmPassword.getText().toString();
        if(Utils.isNull(old_password)) {
            warnDialog(R.string.old_password, R.string.empty_field, R.id.et_password);
            return false;
        }
        if(!old_password.equals(DB.getPassword())) {
            warnDialog(R.string.old_password, R.string.wrong_password, R.id.et_password);
            return false;
        }
        if(Utils.isNull(new_password)) {
            warnDialog(R.string.new_password, R.string.empty_field, R.id.et_new_password);
            return false;
        }
        if(!Valid.isPassword(new_password)) {
            warnDialog(R.string.new_password, R.string.password_format, R.id.et_new_password);
            return false;
        }
        if(Utils.isNull(confirm_password)) {
            warnDialog(R.string.confirm_password, R.string.empty_field, R.id.et_confirm_password);
            return false;
        }
        if(!new_password.equals(confirm_password)) {
            warnDialog(R.string.new_password, R.string.password_inconsistent, R.id.et_confirm_password);
            return false;
        }
        return true;
    }
    void onNext()
    {
        Utils.hideKeyboard(mActivity);
        if(!verifyData()) {
            return;
        }

        storeViews();

        DbSender.Data sender = DB.getSender();
        boolean mainDataEqual = false;
        boolean photosEqual = (!arcPhotoChanged && !arcBackPhotoChanged && !selfPhotoChanged);
        boolean allDataEqual = false;
        String new_arc_no = getNormViewText(R.id.et_arc_no);
        String password = Pref.get(PayInfo.REGISTER_PASSWORD);
        if(sender != null) {
            mainDataEqual = (
                    sender.name.equals(getNormViewText(R.id.et_name)) &&
                            sender.sender_arc.equals(new_arc_no) &&
                            sender.expiry_date.equals(getViewText(R.id.tv_expiry)) &&
                            sender.birthday.equals(getViewText(R.id.tv_birthday)));

            allDataEqual = (mainDataEqual && photosEqual && sender.phone.equals(getViewText(R.id.et_mobile)));
        }
        if(isRegister || !mainDataEqual || !photosEqual) {
            PayInfo.set(PayInfo.TRUSTEE_NEED_REVIEW, "1");
        } else {
            PayInfo.set(PayInfo.TRUSTEE_NEED_REVIEW, "0");
        }
        switch(DB.getStatus())
        {
            case DbSender.SENDER_SUCCESS:
                break;
            case DbSender.SENDER_FAIL:
                break;
            case DbSender.SENDER_EDITED:
            case DbSender.SENDER_REGISTERED:
                break;
            case DbSender.SENDER_DELETE:
                break;
            default:
                break;
        }
        if(isRegister) {
            doRegister(new_arc_no, password);
        } else if(!mainDataEqual && !(arcPhotoChanged && selfPhotoChanged && arcBackPhotoChanged)) {
            if(!arcPhotoChanged) {
                arcPhotoPath = "";
                ivArcFront.setImageDrawable(getResources().getDrawable(R.drawable.zzz__dummy));
            }
            if(!arcBackPhotoChanged) {
                arcBackPhotoPath = "";
                ivArcBack.setImageDrawable(getResources().getDrawable(R.drawable.zzz__dummy));
            }
            if(!selfPhotoChanged) {
                selfPhotoPath = "";
                ivSelfie.setImageDrawable(getResources().getDrawable(R.drawable.zzz__dummy));
            }
            warnDialog(R.string.empty, R.string.update_photo, -1);
        } else if(allDataEqual) {
            showSuccessDialog();
        } else {
            doSave(new_arc_no, password);
        }
    }
    void showSuccessDialog()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ActionDialog(mActivity, R.string.trustee_info, R.string.save_succeed,
                        -1, R.string.okay, -1) {
                    @Override
                    public void onOk() {
                        finish();
                    }
                };
            }
        });
    }
    void doSave(final String sender_arc, final String password)
    {
        final String needReview = Pref.get(PayInfo.TRUSTEE_NEED_REVIEW);
        startApi(R.string.app_name, R.string.sending);
        new Thread() {
            @Override
            public void run() {
                DB.register(mActivity, false);
                if(DB.bRegister) {
                    if(needReview.equals("1")) {
                        DB.clearSender();
                        DB.login(mActivity, sender_arc, password);
                        endApi();
                        actionDialog(R.string.empty, R.string.register_succeed, new Runnable() {
                            @Override
                            public void run() {
                                restartClass(MainActivity.class);
                            }
                        });
                    } else {
                        // When only phone is changed, we don't need review
                        endApi();
                        String phone = getViewText(R.id.et_mobile);
                        DbSender.update(sender_arc, DbSender.PHONE, phone);
                        DB.getSender().phone = phone;
                        showSuccessDialog();
                    }
                    return;
                }
                endApi();
                if (DB.respRegister != null) {
                    warnAndFinish(mActivity, R.string.app_name, DB.respRegister.message);
                } else {
                    onApiFail(null, null);
                }
            }
        }.start();
    }
    void initViews(RespSenderProfile sender)
    {
        TextView tvStatusWarning = findViewById(R.id.tv_status_warning);
        EditText etName = findViewById(R.id.et_name);
        switch(sender.status)
        {
            case DbSender.SENDER_SUCCESS:
//                tvStatusWarning.setVisibility(View.GONE);
                isSenderSuccess = true;
//                disableEditView(R.id.et_arc_no, R.id.iv_arc_no);
                ivPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_write));
                disableEditView(R.id.et_name, R.id.iv_name);
                findViewById(R.id.iv_birthday).setVisibility(View.VISIBLE);
                tvNext.setText(R.string.save);
                break;
            case DbSender.SENDER_FAIL:
//                tvStatusWarning.setVisibility(View.VISIBLE);
//                tvStatusWarning.setText(R.string.review_fail);
                etName.setTextColor(getResources().getColor(R.color.black));
                etName.setEnabled(true);
                etArcNo.setTextColor(colorUnchangeable);
                etArcNo.setFocusable(false);
                etArcNo.setClickable(true);
                etArcNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Utils.hideKeyboard(mActivity);
//                        actionDialog(-1, R.string.login_again, null);
                        new ActionDialog(mActivity, -1, R.string.login_again,
                                R.drawable.warning, R.string.exit, R.string.relogin) {
                            @Override
                            public void onCancel() {
                                DB.clearSender();
                                BaseActivity.mLoginArcNo = "";
                                sMode = MODE_PROFILE;
                                restartClass(LoginActivity.class);
                            }
                        };
                    }
                });
                tvNext.setText(R.string.save);
                break;
            case DbSender.SENDER_EDITED:
            case DbSender.SENDER_REGISTERED:
//                tvStatusWarning.setVisibility(View.VISIBLE);
//                tvStatusWarning.setText(R.string.register_succeed);
//                etName.setTextColor(getResources().getColor(R.color.black));
//                etArcNo.setTextColor(getResources().getColor(R.color.black));
                tvNext.setVisibility(View.INVISIBLE);
                ScrollView sv = findViewById(R.id.sv);
                enableViewGroup(sv, false);
                break;
            case DbSender.SENDER_DELETE:
                break;
        }
        setEditView(sender.sender_name, R.id.et_name);
        setEditView(sender.sender_arc, R.id.et_arc_no);
        tvExpiry.setText(sender.sender_arc_term);
        tvBirthday.setText(sender.sender_birthday);
        setEditView(sender.sender_mobile, R.id.et_mobile);
        if(isArcExpired) {
            Pref.set(PayInfo.TRUSTEE_ARC_PHOTO, "");
            Pref.set(PayInfo.TRUSTEE_SELF_PHOTO, "");
            Pref.set(PayInfo.TRUSTEE_ARC_BACK_PHOTO, "");
            tvExpiry.setText("");
        } else {
            ImageLoader.getInstance().loadImage(sender.face_image, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    saveUrlImage(loadedImage, R.id.iv_selfie);
                }
            });
            ImageLoader.getInstance().loadImage(sender.arc_image, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    saveUrlImage(loadedImage, R.id.iv_arc_front);
                }
            });
            ImageLoader.getInstance().loadImage(sender.arc_01_image, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    saveUrlImage(loadedImage, R.id.iv_arc_back);
                }
            });
            /*
            // Selfie
            ImageURL.load(sender.face_image, ivSelfie, mActivity, new Runnable() {
                @Override
                public void run() {
                    Bitmap bitmap = ((BitmapDrawable)ivSelfie.getDrawable()).getBitmap();
                    saveImage(bitmap, R.id.iv_selfie);
                }
            }, false);
            // Arc front
            ImageURL.load(sender.arc_image, ivArcFront, mActivity, new Runnable() {
                @Override
                public void run() {
                    Bitmap bitmap = ((BitmapDrawable)ivArcFront.getDrawable()).getBitmap();
                    saveImage(bitmap, R.id.iv_arc_front);
                }
            }, false);
            // Arc back
            ImageURL.load(sender.arc_01_image, ivArcBack, mActivity, new Runnable() {
                @Override
                public void run() {
                    Bitmap bitmap = ((BitmapDrawable)ivArcBack.getDrawable()).getBitmap();
                    saveImage(bitmap, R.id.iv_arc_back);
                }
            }, false); */
        }
    }
    void storeViews()
    {
        storeEditView(PayInfo.TRUSTEE_PASSPORT_ID, R.id.et_arc_no);
        storeEditView(PayInfo.REGISTER_PASSWORD, R.id.et_password);
        storeEditView(PayInfo.TRUSTEE_NAME, R.id.et_name);
        PayInfo.set(PayInfo.TRUSTEE_PASSPORT_DUE, tvExpiry.getText().toString());
        PayInfo.set(PayInfo.TRUSTEE_BIRTH_DAY, tvBirthday.getText().toString());
        storeEditView(PayInfo.TRUSTEE_PHONE, R.id.et_mobile);
        storeEditView(DbSender.PROMOTE_TO, R.id.et_promote_to);
        PayInfo.set(PayInfo.PROFILE_TYPE, "1");
        PayInfo.set(PayInfo.TRUSTEE_ARC_PHOTO, arcPhotoPath);
        PayInfo.set(PayInfo.TRUSTEE_SELF_PHOTO, selfPhotoPath);
        PayInfo.set(PayInfo.TRUSTEE_ARC_BACK_PHOTO, arcBackPhotoPath);
    }
    boolean arcExpired(String date)
    {
        long curr_time = System.currentTimeMillis();
        long due_time = Utils.getTimeStamp(date);
        if(due_time < (curr_time + 1000 * 60 * 60 * 24)) {
            return true;
        }
        return false;
    }
    // debugging purpose: quickly fill each field for testing
    void fillViews()
    {
        setEditText(DebugOptions.arc_no, R.id.et_arc_no);
        setEditText(DebugOptions.password, R.id.et_password);
        setEditText("Daniel", R.id.et_name);
        tvExpiry.setText("2030/01/01");
        tvBirthday.setText("1990/12/31");
        setEditText("0919550470", R.id.et_mobile);

        ivArcFront.setImageDrawable(getResources().getDrawable(R.drawable.logo));
        ivSelfie.setImageDrawable(getResources().getDrawable(R.drawable.logo));
        ivArcBack.setImageDrawable(getResources().getDrawable(R.drawable.logo));

        Bitmap org_bitmap = ((BitmapDrawable)ivArcFront.getDrawable()).getBitmap();
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(org_bitmap, Config.photo_width, Config.photo_height, true);

        arcPhotoPath = ImageCapture.saveImage(mActivity, org_bitmap, Config.arc_file_name);
        Pref.set(PayInfo.TRUSTEE_ARC_PHOTO_NORM, ImageCapture.saveImage(mActivity, scaledBitmap, Config.arc_file_name_norm));
        selfPhotoPath = ImageCapture.saveImage(mActivity, org_bitmap, Config.selfie_file_name);
        Pref.set(PayInfo.TRUSTEE_SELF_PHOTO_NORM, ImageCapture.saveImage(mActivity, scaledBitmap, Config.selfie_file_name_norm));
        arcBackPhotoPath = ImageCapture.saveImage(mActivity, org_bitmap, Config.arc_back_file_name);
        Pref.set(PayInfo.TRUSTEE_ARC_BACK_PHOTO_NORM, ImageCapture.saveImage(mActivity, scaledBitmap, Config.arc_back_file_name_norm));
    }
    void disableEditView(int id_edit, int id_image)
    {
        EditText et = findViewById(id_edit);
        et.setTextColor(colorUnchangeable);
        et.setEnabled(false);
        ImageView iv = findViewById(id_image);
        iv.setVisibility(View.VISIBLE);
    }
    void onPassword()
    {
        if(isRegister) {
            if(viewPassword == true) {
                etPassword.setTransformationMethod(new PasswordTransformationMethod());
                ivPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_hide));
                viewPassword = false;
            } else {
                etPassword.setTransformationMethod(null);
                ivPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_show));
                viewPassword = true;
            }
        } else {
            changePasswordDialog();
        }
    }
    void doRegister(final String sender_arc, final String password)
    {
        startApi(R.string.app_name, R.string.sending);
        new Thread() {
            @Override
            public void run() {
                DB.register(mActivity, true);
                if (DB.bRegister) {
                    MyLog.i("DB.promote_no = " + DB.respRegister.promote_no);
                    Pref.set(JUST_REGISTERED, "1");
                    DB.clearSender();
                    DB.login(mActivity, sender_arc, password);
                    endApi();
                    nextActivity(PortalActivity.class);
                    return;
                }
                endApi();
                if (DB.respRegister != null) {
                    Pref.set(PayInfo.REGISTER_PASSWORD, "");
                    String converted = EusoApi.convertMessage(DB.respRegister.message);
                    warnDialog(R.string.app_name, converted, -1);
                } else {
                    Pref.set(PayInfo.REGISTER_PASSWORD, "");
                    onApiFail(null, null);
                }
            }
        }.start();
    }
    void handlePasswordDialog(Dialog dialog, View view)
    {
        switch (view.getId()) {
            case R.id.iv_old_password:
                if(viewOldPassword == true) {
                    etOldPassword.setTransformationMethod(new PasswordTransformationMethod());
                    ivOldPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_hide));
                    viewOldPassword = false;
                } else {
                    etOldPassword.setTransformationMethod(null);
                    ivOldPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_show));
                    viewOldPassword = true;
                }
                break;
            case R.id.iv_new_password:
                if(viewNewPassword == true) {
                    etNewPassword.setTransformationMethod(new PasswordTransformationMethod());
                    ivNewPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_hide));
                    viewNewPassword = false;
                } else {
                    etNewPassword.setTransformationMethod(null);
                    ivNewPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_show));
                    viewNewPassword = true;
                }
                break;
            case R.id.iv_confirm_password:
                if(viewConfirmPassword == true) {
                    etConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
                    ivConfirmPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_hide));
                    viewConfirmPassword = false;
                } else {
                    etConfirmPassword.setTransformationMethod(null);
                    ivConfirmPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_show));
                    viewConfirmPassword = true;
                }
                break;
            case R.id.tv_close:
                dialog.dismiss();
                break;
            case R.id.tv_save:
                onChangePassword(dialog);
                break;
        }
    }
    void changePasswordDialog()
    {
        int buttons[] = {
                R.id.iv_old_password, R.id.iv_new_password, R.id.iv_confirm_password,
                R.id.tv_close, R.id.tv_save
        };
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.change_password__dialog);
        ViewGroup root = dialog.findViewById(R.id.root);
        scaleTextViewSize(root, 0.8f);
        ivOldPassword = dialog.findViewById(R.id.iv_old_password);
        ivNewPassword = dialog.findViewById(R.id.iv_new_password);
        ivConfirmPassword = dialog.findViewById(R.id.iv_confirm_password);
        etOldPassword = dialog.findViewById(R.id.et_old_password);
        etNewPassword = dialog.findViewById(R.id.et_new_password);
        etConfirmPassword = dialog.findViewById(R.id.et_confirm_password);
        etOldPassword.setTransformationMethod(new PasswordTransformationMethod());
        etNewPassword.setTransformationMethod(new PasswordTransformationMethod());
        etConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());

        for(int button : buttons) {
            final View view = dialog.findViewById(button);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handlePasswordDialog(dialog, view);
                }
            });
        }
        dialog.show();
    }
    void onChangePassword(final Dialog dialog)
    {
        if(!verifyPasswordChange())
            return;
        final String new_password = etNewPassword.getText().toString();
        startApi(R.string.app_name, R.string.sending);
        new Thread() {
            @Override
            public void run() {
                DB.updatePassword(null, new_password);
                endApi();
                if(DB.bUpdatePassword) {
                    DbSender.update(DB.getSenderArc(), DbSender.PASSWORD, new_password);
                    DB.getSender().password = new_password;
                    setEditText(new_password, R.id.et_password);
                    dialog.dismiss();
                    actionDialog(R.string.empty, R.string.password_changed, null);
                } else {
                    warnDialog(R.string.app_name, R.string.network_timeout, -1);
                }
            }
        }.start();
    }
    void adjustLayoutScale()
    {
        final ViewGroup root = findViewById(R.id.register_root);
        ViewTreeObserver vto = root.getViewTreeObserver();
        vto.addOnGlobalLayoutListener (new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                root.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                if(mIsNarrowScreen) {
                    float scale = 0.8f;
//                    scaleTextSize(root, scale);
                    scaleImageSize(R.id.iv_selfie, scale);
                    scaleImageSize(R.id.iv_arc_front, scale);
                    scaleImageSize(R.id.iv_arc_back, scale);
                }
            }
        });
    }
}
