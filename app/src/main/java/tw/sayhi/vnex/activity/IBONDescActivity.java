package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.R;

public class IBONDescActivity extends BaseActivity
{
    Activity mActivity = this;
//    int[] button_ids = { R.id.ll_seven, R.id.ll_family, R.id.ll_ok, R.id.ll_hilife };
    int[] button_ids = {  };
    ScrollView sv;
    ImageView ivDesc1, ivDesc2, ivDesc3;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.ibon_desc__activity);

        buildViews();
    }
    void buildViews()
    {
        buildTitleMenu();
        sv = (ScrollView)findViewById(R.id.sv);
        ivDesc1 = (ImageView)findViewById(R.id.iv_ibon_desc_1);
        ivDesc2 = (ImageView)findViewById(R.id.iv_ibon_desc_2);
        ivDesc3 = (ImageView)findViewById(R.id.iv_ibon_desc_3);
        for(int id : button_ids) {
            findViewById(id).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buildListeners(view.getId());
                }
            });
        }
//        buildListeners(R.id.ll_seven);
    }
    void buildListeners(int id)
    {
//        Utils.memStats(mActivity);
        clearButtons();
        LinearLayout ll = (LinearLayout)findViewById(id);
        ll.setBackgroundColor(getResources().getColor(R.color.deep_orange_500));
        sv.scrollTo(0, 0);
        int desc1, desc2, desc3;
        switch(id) {
            case R.id.ll_seven:
//                desc1 = R.drawable.ibon_seven_desc_1;
//                desc2 = R.drawable.ibon_seven_desc_2;
//                desc3 = R.drawable.ibon_seven_desc_3;
                break;
            case R.id.ll_family:
//                desc1 = R.drawable.ibon_family_desc_1;
//                desc2 = R.drawable.ibon_family_desc_2;
//                desc3 = R.drawable.ibon_family_desc_3;
                break;
            case R.id.ll_hilife:
//                desc1 = R.drawable.ibon_hilife_desc_1;
//                desc2 = R.drawable.ibon_hilife_desc_2;
//                desc3 = R.drawable.ibon_hilife_desc_3;
                break;
            case R.id.ll_ok:
                desc1 = R.drawable.ibon_ok_desc_1;
                desc2 = R.drawable.ibon_ok_desc_2;
                desc3 = R.drawable.ibon_ok_desc_3;
                break;
            default:
                desc1 = -1;
                desc2 = -1;
                desc3 = -1;
                break;
        }
//        ivDesc1.setImageDrawable(getResources().getDrawable(desc1));
//        ivDesc2.setImageDrawable(getResources().getDrawable(desc2));
//        ivDesc3.setImageDrawable(getResources().getDrawable(desc3));
    }
    void clearButtons()
    {
        for(int id : button_ids) {
            LinearLayout ll = (LinearLayout)findViewById(id);
            ll.setBackgroundColor(getResources().getColor(R.color.main_background));
        }
    }
}
