package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.MainActivity;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.widget.ActionDialog;

public class PayWayActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] items = { R.id.iv_pay_channel_vnex, R.id.iv_pay_channel_seven, R.id.tv_next};
    String euso_order;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.pay_way__activity);

        euso_order = getIntentValue(DbOrder.EUSO_ORDER);

        buildViews();

        if(intentContains(FROM_REMIT)) {
            new ActionDialog(mActivity, R.string.main_item1, R.string.more_transaction,
                    R.drawable.info, R.string.no, R.string.yes) {
                @Override
                public void onCancel() {
                    nextActivity(ExchangeRateActivity.class);
                }
            };
        }
    }
    void buildListeners(View v)
    {
        Class<?> cls = null;
        switch(v.getId()) {
            case R.id.iv_pay_channel_vnex:
                cls = PayVnexActivity.class;
                DbOrder.update(euso_order, DbOrder.PAY_CHANNEL, DbOrder.PAY_VNEX);
                break;
            case R.id.iv_pay_channel_seven:
                if(Config.payment_type.equals("CVS")) {
                    cls = PaySevenCVSActivity.class;
                } else {
                    cls = SevenActivity.class;
                }
                DbOrder.update(euso_order, DbOrder.PAY_CHANNEL, DbOrder.PAY_SEVEN);
                break;
            case R.id.tv_next:
                restartClass(MainActivity.class);
                return;
        }
        nextActivity(cls, DbOrder.EUSO_ORDER, euso_order);
    }
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            buildListeners(view);
        }
    };

    void buildViews()
    {
        setTreeObserver(R.id.tv_seven_header, 0.7f);
        buildTitleMenu();
        findViewById(R.id.iv_back).setVisibility(View.INVISIBLE);
        buildHeader("4", "Phương thức thanh toán");
        TextView tvNext = findViewById(R.id.tv_next);
        tvNext.setText(R.string.home);
        for (int item : items) {
            findViewById(item).setOnClickListener(onClickListener);
        }
    }
    @Override
    protected void onOrderPaid() {
        super.onOrderPaid();
        nextActivity(HistoryActivity.class);
    }
}
