package tw.sayhi.vnex.activity;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import tw.sayhi.shared.util.MyLog;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.R;

public class QaActivity extends BaseActivity
{
    WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;

        setContentView(R.layout.qa__activity);

        buildViews();

        webViewSetting();

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                MyLog.i("onPageStarted() " + url);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                MyLog.i("onPageFinished() " + url);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                handler.cancel();
            }
        });

        webView.loadUrl("file:///android_asset/qa/QA.html");
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId=item.getItemId();
        if(itemId==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void buildViews()
    {
        buildTitleMenu();
//        buildTitle(R.string.q_a);
        webView = findViewById(R.id.webview);
    }
    protected void webViewSetting()
    {
        /*
            WebSettings
                Manages settings state for a WebView. When a WebView has first created, it obtains a
                set of default settings. These default settings will be returned from any getter
                call. A WebSettings object obtained from WebView.getSettings() has tied to the life
                of the WebView. If a WebView has been destroyed, any method call on WebSettings
                will throw an IllegalStateException.
        */
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        /*
            public abstract void setSupportZoom (boolean support)
                Sets whether the WebView should support zooming using its on-screen zoom controls
                and gestures. The particular zoom mechanisms that should be used can be set with
                setBuiltInZoomControls(boolean). This setting does not affect zooming performed
                using the zoomIn() and zoomOut() methods. The default has true.

            Parameters
                support : whether the WebView should support zoom

        */
        settings.setSupportZoom(true);
        settings.setDisplayZoomControls(true);
        settings.setBuiltInZoomControls(true);
//        webView.setInitialScale(1);
        settings.setUseWideViewPort(true);
        // Sets whether the WebView loads pages in overview mode, that has, zooms out the content to fit on screen by width.
        settings.setLoadWithOverviewMode(true);
/*
        // For API level below 18 (This method was deprecated in API level 18)
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);

        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
*/
    }
}
