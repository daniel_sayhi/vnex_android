package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.BuildConfig;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.DebugOptions;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.adapter.BankFilter;
import tw.sayhi.vnex.misc.Valid;
import tw.sayhi.vnex.widget.ActionDialog;

public class ReceiverActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] buttons = {
            R.id.tv_payee_birthday,
        R.id.ll_payee_province, R.id.ll_payee_city, R.id.tv_bank_name, R.id.iv_cancel, R.id.tv_next,
        R.id.tv_minus, R.id.tv_plus
    };
    int[] DTD_fields = { R.id.et_payee_name, R.id.et_payee_phone, R.id.et_payee_id, R.id.et_payee_address};
    int[] DTD_titles = { R.string.payee_name, R.string.payee_phone, R.string.payee_id, R.string.payee_address };
    int[] DTD_max_len = { 80, 20, 20, 80 };
    int[] DTD_max_messages = { R.string.max_len_80, R.string.max_len_20, R.string.max_len_20, R.string.max_len_80 };
    int[] BTB_fields = { R.id.et_payee_name, R.id.et_payee_phone, R.id.et_bank_account,
            R.id.et_payee_address, R.id.et_bank_branch };
    int[] BTB_titles = { R.string.payee_name, R.string.payee_phone, R.string.viet_account, R.string.payee_address,
            R.string.viet_bank_branch };
    int[] BTB_max_len = { 80, 20, 20, 20, 80, 80 };
    int[] BTB_max_messages = { R.string.max_len_80, R.string.max_len_20, R.string.max_len_20,
            R.string.max_len_80, R.string.max_len_80 };
    int[] IDP_fields = { R.id.et_payee_name, R.id.et_payee_phone, R.id.et_payee_id,
            R.id.et_payee_address, R.id.et_bank_branch };
    int[] IDP_titles = { R.string.payee_name, R.string.payee_phone, R.string.payee_id,
            R.string.payee_address, R.string.viet_bank_branch };
    int[] IDP_max_len = { 80, 20, 20, 80, 80 };
    int[] IDP_max_messages = { R.string.max_len_80, R.string.max_len_20, R.string.max_len_20,
            R.string.max_len_80, R.string.max_len_80 };
    int[] PIN_fields = { R.id.et_payee_name, R.id.et_payee_phone, R.id.et_payee_address };
    int[] PIN_titles = { R.string.payee_name, R.string.payee_phone, R.string.payee_address };
    int[] PIN_max_len = { 80, 20, 80 };
    int[] PIN_max_messages = { R.string.max_len_80, R.string.max_len_20, R.string.max_len_80 };
//    RelativeLayout rlHeader;
    TextView tvPayeeBirthday;
    TextView tvHeader, tvServiceFee, tvTotalAmount;
    LinearLayout llPayeeArea;
    RadioGroup rgPayeeArea, rgPayCurrency;
    ArrayList<String> provinces = new ArrayList<>();
    ArrayList<String> cities = new ArrayList<>();
    EditText etBankAccount, etPayeeAddress;
    LinearLayout llChoice, llChoiceFilter;
    TextView tvChoiceHeader;
    ListView lvChoice;
    TextView tvPayeeProvince, tvPayeeCity, tvBankName;
    String mBankName = "";
    String mBankFullName = "";
    ArrayAdapter<String> arrayAdapter;
    BankFilter bankFilter = new BankFilter();
    EditText etInputAmount;
    boolean onResumeCalled = false;
    EditText etBankBranch;
    TextView tvMyPoints, tvUsePoints;
    ImageView ivPlus;
    String payCurrency;
    ImageView ivPayAmount;
    TextView tvPayAmount;
    boolean initViewsFinished = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isFinishing()) return;
        setContentView(R.layout.receiver__activity);

        DB.pointDiscount = 0;

        buildViews();

        initViews();

        doQuery(getBankName(), true);
    }
    void doQuery(final String _bank_name, final boolean init)
    {
        String default_bank_name = PayInfo.PinBankName;
        final String bank_name = (Utils.isNull(_bank_name)) ? default_bank_name : _bank_name;
        startApi(R.string.main_item1, R.string.please_wait);
        new Thread() {
            @Override
            public void run() {
                if(init) {
                    Utils.waitFor(DB.latchQueryEvent);
                    Utils.waitFor(DB.latchBankList);
                    Utils.waitFor(DB.latchAddressList);
                    if (!DB.bQueryEvent || !DB.bBankListWithInfo || !DB.bQueryAddressList) {
                        endApi();
                        warnNetwork(mActivity, R.string.main_item1);
                        return;
                    }
                }
                boolean queryServiceFee = DB.queryServiceFee(mActivity, PayInfo.stringNTD, bank_name);
                DB.queryRedPoint(mActivity);
                endApi();
                if(queryServiceFee && DB.bQueryRedPoint) {
                    runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvMyPoints.setText("" + DB.redPoint);
                                if(DB.redPoint >= 50) {
//                                    ivPlus.setImageDrawable(getResources().getDrawable(R.drawable.btn_plus_enabled));
                                }
                                setPayAmount();
                            }
                        });
                } else {
                    warnNetwork(mActivity, R.string.main_item1);
                    return;
                }
            }
        }.start();
    }
    void queryServiceFee()
    {
        startApi(R.string.main_item1, R.string.please_wait);
        new Thread() {
            @Override
            public void run() {
                boolean queryServiceFee = DB.queryServiceFee(mActivity, PayInfo.stringNTD, mBankName);
                endApi();
                if(queryServiceFee && DB.bQueryRedPoint) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setPayAmount();
                        }
                    });
                }
            }
        }.start();
    }
    @Override
    public void onResume()
    {
        super.onResume();
        onResumeCalled = true;
    }
    @Override
    public void onStop()
    {
        super.onStop();
        
        storeViews();
    }
    void buildListeners(View v)
    {
        switch(v.getId()) {
            case R.id.tv_payee_birthday:
                datePickerDialog(tvPayeeBirthday, true, true);
                return;
            case R.id.ll_payee_province:
                showProvinceList();
                return;
            case R.id.ll_payee_city:
                showCityList();
                return;
            case R.id.tv_bank_name:
                showBankList();
                return;
            case R.id.iv_cancel:
                llChoice.setVisibility(View.GONE);
                Utils.hideKeyboard(mActivity);
                return;
            case R.id.tv_next:
                onNext();
                return;
            case R.id.tv_minus:
                adjustPointDiscount(false);
                break;
            case R.id.tv_plus:
                adjustPointDiscount(true);
                break;
        }
    }
    View.OnClickListener onClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {
            buildListeners(v);
        }
    };
    boolean isTextChanging = false;
    int nClickTitle = 0;
    long last_time = 0;
    void buildViews()
    {
        try {
            _buildViews();
        } catch (Exception e) {
            DB.reportException("ReceiverActivity_buildViews", e);
        }
    }
    void _buildViews()
    {
        setTreeObserver(R.id.tv_amount_nt, 0.7f);
        buildTitleMenu();
        buildHeader("2", getString(R.string.remit_way));
        spanRedBold(R.string.max_amount_warn, R.id.tv_max_amount, 25, 8);
        for(int button : buttons) {
            View vi = findViewById(button);
            if (vi != null) {
                vi.setOnClickListener(onClickListener);
            }
        }
        // build PayWay specfics
//        rlHeader = (RelativeLayout)findViewById(R.id.rl_header);
        tvHeader = (TextView)findViewById(R.id.tv_header);
        tvPayeeBirthday = (TextView)findViewById(R.id.tv_payee_birthday);
        llPayeeArea = (LinearLayout)findViewById(R.id.ll_payee_area);
        rgPayeeArea = (RadioGroup) findViewById(R.id.rg_payee_area);
        etPayeeAddress = (EditText)findViewById(R.id.et_payee_address);
        etBankAccount = (EditText)findViewById(R.id.et_bank_account);
        llChoice = (LinearLayout)findViewById(R.id.ll_choice);
        tvChoiceHeader = (TextView)findViewById(R.id.tv_choice_header);
        llChoiceFilter = (LinearLayout)findViewById(R.id.ll_choice_filter);
        tvPayeeProvince = (TextView)findViewById(R.id.tv_payee_province);
        tvPayeeCity = (TextView)findViewById(R.id.tv_payee_city);
        tvBankName = (TextView)findViewById(R.id.tv_bank_name);
        etBankBranch = (EditText)findViewById(R.id.et_bank_branch);
        lvChoice = (ListView)findViewById(R.id.lv_choice);
        buildAddressList();
        switch(PayInfo.trans_type) {
            case PayInfo.DTD: //送到家
//                rlHeader.setBackgroundColor(getResources().getColor(R.color.dtd_background));
                tvHeader.setText(getString(R.string.remit_dtd_c));
                break;
            case PayInfo.BTB: //帳號
//                rlHeader.setBackgroundColor(getResources().getColor(R.color.btb_background));
                tvHeader.setText(getString(R.string.remit_btb_c));
                findViewById(R.id.et_payee_id).setVisibility(View.GONE);
                etBankAccount.setVisibility(View.VISIBLE);
                etBankBranch.setVisibility(View.VISIBLE);
                buildBankList();
                findViewById(R.id.rb_pay_us).setVisibility(View.GONE);
                break;
            case PayInfo.IDP: //ID取款
//                rlHeader.setBackgroundColor(getResources().getColor(R.color.idp_background));
                tvHeader.setText(getString(R.string.remit_idp_c));
                buildBankList();
                etBankBranch.setVisibility(View.VISIBLE);
                findViewById(R.id.rb_pay_us).setVisibility(View.GONE);
                break;
            case PayInfo.PIN: //密碼
//                rlHeader.setBackgroundColor(getResources().getColor(R.color.pin_background));
                tvHeader.setText(getString(R.string.remit_pin_c));
                findViewById(R.id.tv_fixed_bank_name).setVisibility(View.VISIBLE);
                findViewById(R.id.et_payee_id).setVisibility(View.GONE);
                findViewById(R.id.rb_pay_us).setVisibility(View.VISIBLE);
                ((RadioButton)findViewById(R.id.rb_pay_us)).setChecked(true);
                findViewById(R.id.rb_pay_vi).setVisibility(View.GONE);
                break;
        }
        if(rgPayeeArea != null) {
            rgPayeeArea.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int old_index = Pref.getInt(PayInfo.PAYEE_AREA_INDEX);
                    switch (checkedId) {
                        case R.id.rb_north:
                            Pref.set(PayInfo.PAYEE_AREA, getString(R.string.north));
                            Pref.setInt(PayInfo.PAYEE_AREA_INDEX, 0);
                            break;
                        case R.id.rb_middle:
                            Pref.set(PayInfo.PAYEE_AREA, getString(R.string.middle));
                            Pref.setInt(PayInfo.PAYEE_AREA_INDEX, 1);
                            break;
                        case R.id.rb_south:
                            Pref.set(PayInfo.PAYEE_AREA, getString(R.string.south));
                            Pref.setInt(PayInfo.PAYEE_AREA_INDEX, 2);
                            break;
                    }
                    int new_index = Pref.getInt(PayInfo.PAYEE_AREA_INDEX);
                    if(old_index != new_index) {
                        tvPayeeProvince.setText("");
                        Pref.setInt(PayInfo.PAYEE_PROVINCE_INDEX, -1);
                        tvPayeeCity.setText("");
                        Pref.setInt(PayInfo.PAYEE_CITY_INDEX, -1);
                    }
                }
            });
        }
        tvServiceFee = (TextView)findViewById(R.id.tv_service_fee);
        tvMyPoints = findViewById(R.id.tv_my_points);
        tvUsePoints = findViewById(R.id.tv_use_points);
        tvTotalAmount = (TextView)findViewById(R.id.tv_total_amount);
        etInputAmount = findViewById(R.id.et_input_amount);
        rgPayCurrency = (RadioGroup)findViewById(R.id.rg_pay_currency);
        payCurrency = ((RadioButton)findViewById(rgPayCurrency.getCheckedRadioButtonId())).getText().toString();

        rgPayCurrency.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                setPayAmount();
                setTargetAmount();
            }
        });
        ivPayAmount = findViewById(R.id.iv_pay_amount);
        tvPayAmount = findViewById(R.id.tv_pay_amount);

        etInputAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable in) {
                if(!initViewsFinished || isTextChanging) {
                    return;
                }
                isTextChanging = true;

                PayInfo.setAmount(in.toString(), PayInfo.NTD);
                in.replace(0, in.length(), PayInfo.commaNTD);
                if(onResumeCalled && PayInfo.trans_type.equals(PayInfo.PIN))
                    doQuery(mBankName, false);

                setPayAmount();
                setTargetAmount();

                isTextChanging = false;
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
        // for debugging purpose
        TextView tvHeader = findViewById(R.id.tv_header);
        tvHeader.setClickable(true);
        tvHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long curr_time = System.currentTimeMillis();
                long diff_time = curr_time - last_time;
                last_time = curr_time;
                if(BuildConfig.DEBUG || DebugOptions.bGeneral && diff_time < 1500) {
                    nClickTitle++;
                    if(nClickTitle >= 2) {
                        fillViews();
                    }
                } else {
                    nClickTitle = 0;
                }
            }
        });
    }
    void buildAddressList()
    {
        new Thread() {
            @Override
            public void run() {
                DB.queryAddressList(mActivity);
            }
        }.start();
    }
    void buildBankList()
    {
        View viBankName = findViewById(R.id.tv_bank_name);
        viBankName.setVisibility(View.VISIBLE);
        new Thread() {
            @Override
            public void run() {
                DB.queryBankListWithInfo(mActivity);
                if(DB.bankFullList != null && nonNull(mBankFullName) && tvBankName != null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvBankName.setText(mBankFullName);
                        }
                    });
                }
            }
        }.start();
        EditText etBankNameFilter = (EditText)findViewById(R.id.et_choice_filter);
        etBankNameFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable in) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(arrayAdapter != null) { // todo: consider when API failed
                    arrayAdapter.getFilter().filter(s);
                }
            }
        });
    }
    void showProvinceList()
    {
        if(Pref.getInt(PayInfo.PAYEE_AREA_INDEX) < 0) {
            warnDialog(R.string.payee_area, R.string.empty_field, -1);
            return;
        }
        if(DB.bQueryAddressList) {
            llChoice.setVisibility(View.VISIBLE);
            tvChoiceHeader.setText(R.string.payee_province);
            llChoiceFilter.setVisibility(View.GONE);
            DB.addressList.getProvinces(Pref.getInt(PayInfo.PAYEE_AREA_INDEX), provinces);
            arrayAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_single_choice, provinces);
            lvChoice.setAdapter(arrayAdapter);
            lvChoice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    if(position != Pref.getInt(PayInfo.PAYEE_PROVINCE_INDEX)) {
                        tvPayeeCity.setText("");
                        Pref.setInt(PayInfo.PAYEE_CITY_INDEX, -1);
                    }
                    String province = arrayAdapter.getItem(position);
                    Pref.set(PayInfo.PAYEE_PROVINCE, province);
                    Pref.setInt(PayInfo.PAYEE_PROVINCE_INDEX, position);
                    tvPayeeProvince.setText(province);
                    llChoice.setVisibility(View.GONE);
                }
            });
        }
    }
    void showCityList()
    {
        if(Pref.getInt(PayInfo.PAYEE_AREA_INDEX) < 0) {
            warnDialog(R.string.payee_area, R.string.empty_field, -1);
            return;
        }
        if(Pref.getInt(PayInfo.PAYEE_PROVINCE_INDEX) < 0) {
            warnDialog(R.string.payee_province, R.string.empty_field, -1);
            return;
        }
        if(DB.bQueryAddressList) {
            llChoice.setVisibility(View.VISIBLE);
            tvChoiceHeader.setText(R.string.payee_city);
            llChoiceFilter.setVisibility(View.GONE);
            DB.addressList.getCities(Pref.getInt(PayInfo.PAYEE_AREA_INDEX),
                    Pref.getInt(PayInfo.PAYEE_PROVINCE_INDEX), cities);
            arrayAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_single_choice, cities);
            lvChoice.setAdapter(arrayAdapter);
            lvChoice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    String city = arrayAdapter.getItem(position);
                    Pref.set(PayInfo.PAYEE_CITY, city);
                    Pref.setInt(PayInfo.PAYEE_CITY_INDEX, position);
                    tvPayeeCity.setText(city);
                    llChoice.setVisibility(View.GONE);
                }
            });
        }
    }
    void showBankList()
    {
        if(DB.bBankListWithInfo) {
            llChoice.setVisibility(View.VISIBLE);
            tvChoiceHeader.setText(R.string.viet_bank);
            llChoiceFilter.setVisibility(View.VISIBLE);
            arrayAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_single_choice,
                    DB.bankFullList.bankNames);
            bankFilter.setData(DB.bankFullList.bankNames, arrayAdapter);
            lvChoice.setAdapter(arrayAdapter);
            lvChoice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    mBankFullName = arrayAdapter.getItem(position);
                    mBankName = DB.bankFullList.bankNameMap.get(mBankFullName);
                    tvBankName.setText(mBankFullName);
                    llChoice.setVisibility(View.GONE);
                    Utils.hideKeyboard(mActivity);
                    queryServiceFee();
                }
            });
        }
    }
    void setPayAmount()
    {
        Long serviceFee = new Long(DB.serviceFee);
        tvServiceFee.setText(serviceFee.toString());
        TextView tvDiscountFee = findViewById(R.id.tv_discount_fee);
        tvDiscountFee.setText("-" + DB.coupon_value);
        PayInfo.setTotalAmount();
        tvTotalAmount.setText(PayInfo.commaTotal);
    }
    void setTargetAmount()
    {
        payCurrency = ((RadioButton)findViewById(rgPayCurrency.getCheckedRadioButtonId())).getText().toString();
        if(payCurrency.equals(PayInfo.USD)) {
            ivPayAmount.setImageResource(R.drawable.flag_us);
            tvPayAmount.setText(PayInfo.commaUSD);
        } else {
            ivPayAmount.setImageResource(R.drawable.flag_vn);
            tvPayAmount.setText(PayInfo.commaVND);
        }
    }
    String getPayeeArea()
    {
        String payee_area = "";
        String payee_area_vi = "";
        RadioButton rbPayeeArea = (RadioButton)findViewById(rgPayeeArea.getCheckedRadioButtonId());
        RadioButton rbNorth = (RadioButton)findViewById(R.id.rb_north);
        RadioButton rbMiddle = (RadioButton)findViewById(R.id.rb_middle);
        RadioButton rbSouth = (RadioButton)findViewById(R.id.rb_south);
        if(rbPayeeArea == rbNorth) {
            payee_area = "NORTH";
            payee_area_vi = getString(R.string.north);
        } else if(rbPayeeArea == rbMiddle) {
            payee_area = "MIDDLE";
            payee_area_vi = getString(R.string.middle);
        } else if(rbPayeeArea == rbSouth) {
            payee_area = "SOUTH";
            payee_area_vi = getString(R.string.south);
        }
        Pref.set(PayInfo.PAYEE_AREA_VI, payee_area_vi);
        return payee_area;
    }
    void onNext()
    {
        Utils.hideKeyboard(mActivity);
        // emptyness check
        switch(PayInfo.trans_type) {
            case PayInfo.DTD:
                if(invalidLength(DTD_fields, DTD_titles, DTD_max_len, DTD_max_messages)) {
                    return;
                }
                break;
            case PayInfo.BTB:
                if(invalidLength(BTB_fields, BTB_titles, BTB_max_len, BTB_max_messages)) {
                    return;
                }
                if(isBankNameEmpty()) {
                    return;
                }
                break;
            case PayInfo.IDP:
                if(invalidLength(IDP_fields, IDP_titles, IDP_max_len, IDP_max_messages)) {
                    return;
                }
                if(isBankNameEmpty()) {
                    return;
                }
                break;
            case PayInfo.PIN:
                if(invalidLength(PIN_fields, PIN_titles, PIN_max_len, PIN_max_messages)) {
                    return;
                }
                break;
        }
        String birthday = tvPayeeBirthday.getText().toString();
        if(!verifyDate(birthday, true)) {
            warnDialog(R.string.payee_birthday, R.string.format_error, -1);
            return;
        }
        if(!checkPayeeId()) {
            warnDialog(R.string.payee_id, R.string.format_error, R.id.et_payee_id);
            return;
        }
        String phone = getEditText(R.id.et_payee_phone);
        if(!Valid.isMobile(phone) && !Valid.isLocalPhone(phone)) {
            warnDialog(R.string.payee_phone, R.string.format_error, R.id.et_payee_phone);
            return;
        }
        if(Utils.isNull(getPayeeArea())) {
            warnDialog(R.string.payee_area, R.string.empty_field, -1);
            return;
        }
        if(Utils.isNull(tvPayeeProvince.getText())) {
            warnDialog(R.string.payee_province, R.string.empty_field, -1);
            return;
        }
        if(Utils.isNull(tvPayeeCity.getText())) {
            warnDialog(R.string.payee_city, R.string.empty_field, -1);
            return;
        }
        if(PayInfo.floatNTD < 1) {
            warnDialog(R.string.input_amount, R.string.empty_field, R.id.et_input_amount);
            return;
        }
        if(PayInfo.floatTotal >  PayInfo.AMOUNT_LIMIT) {
            warnDialog(R.string.main_item1_sub_title, R.string.input_amount_warn, R.id.et_input_amount);
            return;
        }
        startActivity(new Intent(mActivity, QuestionaryActivity.class));
    }
    void initViews()
    {
        String value;
        setPrefEditView(PayInfo.PAYEE_NAME, R.id.et_payee_name);
        setPrefEditView(PayInfo.PAYEE_PHONE, R.id.et_payee_phone);
        tvPayeeBirthday.setText(Pref.get(PayInfo.PAYEE_BIRTHDAY));
        setPrefEditView(PayInfo.PAYEE_ID, R.id.et_payee_id);
        value = Pref.get(PayInfo.PAYEE_AREA);
        int id = -1;
        if(value.equals("NORTH")) {
            id = R.id.rb_north;
        } else if(value.equals("MIDDLE")) {
            id = R.id.rb_middle;
        } else if(value.equals("SOUTH")) {
            id = R.id.rb_south;
        }
        if(id > 0) {
            RadioButton rb = findViewById(id);
            rb.setChecked(true);
            tvPayeeProvince.setText(Pref.get(PayInfo.PAYEE_PROVINCE));
            tvPayeeCity.setText(Pref.get(PayInfo.PAYEE_CITY));
        }
        setPrefEditView(PayInfo.PAYEE_ADDRESS, R.id.et_payee_address);
        setPrefEditView(PayInfo.BANK_ACCOUNT, R.id.et_bank_account);
        value = Pref.get(PayInfo.STRING_NTD);
        PayInfo.setAmount(value, PayInfo.NTD);
        etInputAmount.setText(PayInfo.commaNTD);
        value = Pref.get(PayInfo.PAY_ER_TYPE);
        if(PayInfo.trans_type.equals(PayInfo.DTD)) {
            if(value.equals(PayInfo.USD)) {
                ((RadioButton)findViewById(R.id.rb_pay_us)).setChecked(true);
            } else {
                ((RadioButton)findViewById(R.id.rb_pay_vi)).setChecked(true);
            }
        }
        setTargetAmount();
        mBankName = Pref.get(PayInfo.BANK_NAME);
        mBankFullName = Pref.get(PayInfo.BANK_FULL_NAME);
        if(nonNull(mBankFullName)) {
            tvBankName.setText(mBankFullName);
        }
        setPrefEditView(PayInfo.BANK_BRANCH, R.id.et_bank_branch);
        setPayAmount();
        setTargetAmount();
        initViewsFinished = true;
    }
    void storeViews()
    {
        storeEditView(PayInfo.PAYEE_NAME, R.id.et_payee_name);
        storeEditView(PayInfo.PAYEE_PHONE, R.id.et_payee_phone);
        Pref.set(PayInfo.PAYEE_BIRTHDAY, tvPayeeBirthday.getText().toString());
        storeEditView(PayInfo.PAYEE_ID, R.id.et_payee_id);
        String payee_area = getPayeeArea();
        if(!Utils.isNull(payee_area)) {
            Pref.set(PayInfo.PAYEE_AREA, payee_area);
        }
        Pref.set(PayInfo.PAYEE_PROVINCE, tvPayeeProvince.getText().toString());
        Pref.set(PayInfo.PAYEE_CITY, tvPayeeCity.getText().toString());
        storeEditView(PayInfo.PAYEE_ADDRESS, R.id.et_payee_address);
        storeEditView(PayInfo.BANK_ACCOUNT, R.id.et_bank_account);
        storeEditView(PayInfo.BANK_BRANCH, R.id.et_bank_branch);
        if(!isFixedBankName() && !Utils.isNull(mBankName)) {
            Pref.set(PayInfo.BANK_NAME, mBankName);
            Pref.set(PayInfo.BANK_FULL_NAME, mBankFullName);
        } else {
            Pref.set(PayInfo.BANK_NAME, PayInfo.PinBankName);
        }
        Pref.set(PayInfo.STRING_NTD, PayInfo.stringNTD);
        Pref.set(PayInfo.STRING_USD, PayInfo.stringUSD);
        Pref.set(PayInfo.STRING_VND, PayInfo.stringVND);
        String pay_er_type =
                ((RadioButton)findViewById(rgPayCurrency.getCheckedRadioButtonId()))
                        .getText().toString();
        Pref.set(PayInfo.PAY_ER_TYPE, pay_er_type);
        if(pay_er_type.equals(PayInfo.VND)) {
            Pref.set(PayInfo.EXCHANGE_RATE, "" + DB.erVND);
            Pref.set(PayInfo.REFERENCE_ER, PayInfo.stringVND);
        } else {
            Pref.set(PayInfo.EXCHANGE_RATE, "" + DB.erUSD);
            Pref.set(PayInfo.REFERENCE_ER, PayInfo.stringUSD);
        }
        // Delete Globals which shouldn't exist
        switch(PayInfo.trans_type) {
            case PayInfo.DTD: //送到家
//                Pref.set(PayInfo.PAYEE_ID, "");
                Pref.set(PayInfo.BANK_NAME, "");
                Pref.set(PayInfo.BANK_ACCOUNT, "");
//                Pref.set(PayInfo.PAYEE_AREA, "");
//                Pref.set(PayInfo.PAYEE_PROVINCE, "");
//                Pref.set(PayInfo.PAYEE_CITY, "");
//                Pref.set(PayInfo.PAYEE_ADDRESS, "");
                break;
            case PayInfo.BTB: //帳號
//                Pref.set(PayInfo.PAYEE_ID, "");
//                Pref.set(PayInfo.BANK_NAME, "");
//                Pref.set(PayInfo.BANK_ACCOUNT, "");
//                Pref.set(PayInfo.PAYEE_AREA, "");
//                Pref.set(PayInfo.PAYEE_PROVINCE, "");
//                Pref.set(PayInfo.PAYEE_CITY, "");
//                Pref.set(PayInfo.PAYEE_ADDRESS, "");
                break;
            case PayInfo.IDP: //ID取款
//                Pref.set(PayInfo.PAYEE_ID, "");
//                Pref.set(PayInfo.BANK_NAME, "");
                Pref.set(PayInfo.BANK_ACCOUNT, "");
//                Pref.set(PayInfo.PAYEE_AREA, "");
//                Pref.set(PayInfo.PAYEE_PROVINCE, "");
//                Pref.set(PayInfo.PAYEE_CITY, "");
//                Pref.set(PayInfo.PAYEE_ADDRESS, "");
                break;
            case PayInfo.PIN: //密碼
                Pref.set(PayInfo.PAYEE_ID, "");
//                Pref.set(PayInfo.BANK_NAME, "");
                Pref.set(PayInfo.BANK_ACCOUNT, "");
//                Pref.set(PayInfo.PAYEE_AREA, "");
//                Pref.set(PayInfo.PAYEE_PROVINCE, "");
//                Pref.set(PayInfo.PAYEE_CITY, "");
//                Pref.set(PayInfo.PAYEE_ADDRESS, "");
                break;
        }
    }
    boolean isBankNameEmpty()
    {
        if(!Utils.isNull(mBankName)) {
            return false;
        }
        new ActionDialog(this, R.string.viet_bank, R.string.empty_field,
                R.drawable.warning, R.string.back, -1) {
            @Override
            public void onOk() {
                showBankList();
            }
        };
        return true;
    }
    String findBankFullName(String bank_name)
    {
        ArrayList<String> bankList = DB.bankFullList.bankNames;
        for(int i = 0; i < bankList.size(); i++) {
            String fullName = bankList.get(i);
            if(fullName.startsWith(bank_name)) {
                return fullName;
            }
        }
        return bank_name;
    }
    boolean checkPayeeId()
    {
        TextView tv = (TextView)findViewById(R.id.et_payee_id);
        if(tv.getVisibility() == View.VISIBLE) {
            String payee_id = tv.getText().toString();
            if(payee_id.length() < 9 || payee_id.length() > 12) {
                return false;
            }
        }
        return true;
    }
    public static boolean isFixedBankName()
    {
        String type = PayInfo.trans_type;
        return (type.equals(PayInfo.PIN));
    }
    public static String getBankName()
    {
        String bank_name;
        switch(PayInfo.trans_type) {
            case PayInfo.PIN:
                bank_name = PayInfo.PinBankName;
                break;
            default:
                bank_name = Pref.get(PayInfo.BANK_NAME);
                break;
        }
        return bank_name;
    }
    void fillViews()
    {
        setEditView("KEVIN", R.id.et_payee_name);
        setEditView("0919550470", R.id.et_payee_phone);
        tvPayeeBirthday.setText("1987/12/31");
        setEditView("0123456789", R.id.et_payee_id);
        RadioButton rbArea = findViewById(R.id.rb_north);
        rbArea.setChecked(true);
        tvPayeeProvince.setText("BAC GIANG");
        tvPayeeCity.setText("BAC GIANG");
        setEditView("Taipei", R.id.et_payee_address);
        setEditView("1234567890", R.id.et_bank_account);
        setEditView("bank branch", R.id.et_bank_branch);
    }
    void adjustPointDiscount(boolean increase)
    {
        if(DB.redPoint <= 0)
            return;
        long adjust = (increase ? 5 : -5);
        long newPointDiscount = DB.pointDiscount + adjust;
        long total_discount = newPointDiscount + DB.coupon_value;
        if(newPointDiscount < 0 || newPointDiscount > DB.redPoint || total_discount > DB.serviceFee)
            return;
        /*
        if(newPointDiscount == 0)
            ivMinus.setImageDrawable(getResources().getDrawable(R.drawable.btn_minus));
        else
            ivMinus.setImageDrawable(getResources().getDrawable(R.drawable.btn_minus_enabled));
        if(newPointDiscount == 100 || newPointDiscount == DB.redPoint)
            ivPlus.setImageDrawable(getResources().getDrawable(R.drawable.btn_plus));
        else
            ivPlus.setImageDrawable(getResources().getDrawable(R.drawable.btn_plus_enabled));
         */

        DB.pointDiscount = newPointDiscount;
        tvMyPoints.setText("" + (DB.redPoint - DB.pointDiscount));
        tvUsePoints.setText("" + DB.pointDiscount);
        setPayAmount();
    }
}
