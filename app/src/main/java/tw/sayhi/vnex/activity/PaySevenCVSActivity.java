package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.misc.DbRemit;
import tw.sayhi.vnex.widget.BarCode;

public class PaySevenCVSActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] items = { R.id.iv_pay_711, R.id.iv_pay_hilife, R.id.iv_pay_familymart };
    String euso_order;
    String url_711, url_hilife, url_familymart;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.pay_seven_cvs__activity);

        euso_order = getIntentValue(DbOrder.EUSO_ORDER);

        buildViews();

    }
    void buildListeners(View v)
    {
        switch(v.getId()) {
            case R.id.iv_pay_711:
//                Utils.openWebPage(mActivity, url_711);
                mWebViewURL = url_711;
                nextActivity(WebViewActivity.class);
                break;
            case R.id.iv_pay_hilife:
//                Utils.openWebPage(mActivity, url_hilife);
                mWebViewURL = url_hilife;
                nextActivity(WebViewActivity.class);
                break;
            case R.id.iv_pay_familymart:
//                Utils.openWebPage(mActivity, url_familymart);
                mWebViewURL = url_familymart;
                nextActivity(WebViewActivity.class);
                break;
        }
    }
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            buildListeners(view);
        }
    };
    void buildViews()
    {
        buildTitleMenu();
        buildHeader("4", "Thanh toán tại cửa hàng tiện lợi");
        //        scaleViewTextSize(R.id.sv, 0.8f);
        setOrderSummary(euso_order);

        int start = getString(R.string.pay_seven_duration).indexOf('6');
        spanRedBold(R.string.pay_seven_duration, R.id.tv_pay_duration, start, 7);

        TextView tvPayPlace = findViewById(R.id.tv_pay_place);
        tvPayPlace.setText("Thanh toán tại cửa hàng tiện lợi");

        ImageView iv = findViewById(R.id.iv_payment_code);
        TextView tv = findViewById(R.id.tv_payment_code);
        Cursor c = DbOrder.read(euso_order);
        url_711 = DbRemit.getString(c, DbRemit.IBON);
        url_hilife = DbRemit.getString(c, DbRemit.HILIFE);
        url_familymart = DbRemit.getString(c, DbRemit.FAMILYMART);
        String payment_no = DbRemit.getString(c, DbOrder.PAYMENT_NO);
        BarCode.buildBarCode(payment_no, iv, 1080, Config.barcode_height + 50);
        tv.setText(payment_no);

        for (int item : items) {
            findViewById(item).setOnClickListener(onClickListener);
        }
        findViewById(R.id.tv_ibon_desc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start(mActivity, IBONDescActivity.class, getString(R.string.back), getString(R.string.ibon_desc));
            }
        });
    }
    @Override
    protected void onOrderPaid() {
        super.onOrderPaid();
        nextActivity(HistoryActivity.class);
    }
}
