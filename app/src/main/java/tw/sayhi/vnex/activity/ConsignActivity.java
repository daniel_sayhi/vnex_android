package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

//import rapid.decoder.BitmapDecoder;
import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.ImageUtil;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.api.RespBase;
import tw.sayhi.vnex.api.RespQueryRemitOrder;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.misc.DbSender;
import tw.sayhi.vnex.widget.ActionDialog;

public class ConsignActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] items = { R.id.tv_send};
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.consign__activity);

        buildViews();
    }
    void buildListeners(View v)
    {
        switch(v.getId()) {
            case R.id.tv_send:
                onSend();
                break;
        }
    }
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            buildListeners(view);
        }
    };

    void buildViews()
    {
        buildTitleMenu();
        for (int item : items) {
            View view = findViewById(item);
            if (view != null) {
                view.setOnClickListener(onClickListener);
            }
        }
        setConsigneeName(R.id.tv_consignor_zh, R.string.consignor_zh, 4);
        setConsigneeName(R.id.tv_consignor_en, R.string.consignor_en, 14);
        // set payee data
        setTextView(R.id.tv_payee_name, Pref.get(PayInfo.PAYEE_NAME));
        setTextView(R.id.tv_payee_birthday, Pref.get(PayInfo.PAYEE_BIRTHDAY));
        setTextView(R.id.tv_payee_phone, Pref.get(PayInfo.PAYEE_PHONE));
        setTextView(R.id.tv_payee_id, Pref.get(PayInfo.PAYEE_ID));
        setTextView(R.id.tv_bank_name, Pref.get(PayInfo.BANK_NAME));
        setTextView(R.id.tv_bank_account, Pref.get(PayInfo.BANK_ACCOUNT));
        setTextView(R.id.tv_payee_address, getPayeeAddress());
        // set trustee data
        DbSender.Data sender = DB.getSender();
        setTextView(R.id.tv_trustee_name, sender.name);
        setTextView(R.id.tv_trustee_birthday, sender.birthday);
        setTextView(R.id.tv_trustee_phone, sender.phone);
        setTextView(R.id.tv_total_amount, PayInfo.commaNTD);
        setTextView(R.id.tv_arc_no, sender.sender_arc);
        setTextView(R.id.tv_expiry, sender.expiry_date);
        // set others data
        setTextView(R.id.tv_source, Pref.get("source"));
        setTextView(R.id.tv_purpose, Pref.get("purpose"));
        setTextView(R.id.tv_relation, Pref.get("relation"));
//        setTextView(R.id.tv_kyc_others, Pref.get("kyc_others"));
        setTextView(R.id.tv_kyc_others, getString(R.string.normal));
        // set current date
        setTextView(R.id.tv_date, Utils.SDF_YYYY_MM_DD.format(System.currentTimeMillis()));
        // set consignor
        setTextView(R.id.tv_consignor, sender.name);
//        BitmapDecoder.from(Pref.get(PayInfo.TRUSTEE_SIGN_NORM)).into(findViewById(R.id.iv_sign));
        ImageUtil.loadImageFile(Pref.get(PayInfo.TRUSTEE_SIGN_NORM), (ImageView)findViewById(R.id.iv_sign));
    }
    void setConsigneeName(int tv_id, int str_id, int start)
    {
        TextView tvConsignee = findViewById(tv_id);
        String name = DB.getSenderName();
        String full_text = String.format(getString(str_id), name);
        SpannableString spanString = new SpannableString(full_text);
        spanString.setSpan(new UnderlineSpan(), start, start + name.length(), 0);
        spanString.setSpan(new StyleSpan(Typeface.BOLD), start, start + name.length(), 0);
        tvConsignee.setText(spanString);
    }
    void onSend()
    {
        startApi(R.string.main_item1, R.string.please_wait);
        new Thread() {
            @Override
            public void run() {
                DB.queryER(mActivity);
                endApi();
                boolean er_changed = false;
                String er_type = Pref.get(PayInfo.PAY_ER_TYPE);
                if(DB.bQueryER) {
                    if (er_type.equals(PayInfo.VND) && DB.erVND != PayInfo.erVND) {
                        er_changed = true;
                    } else if (er_type.equals(PayInfo.USD) && DB.erUSD != PayInfo.erUSD)
                        er_changed = true;
                }
                if(er_changed) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new ActionDialog(mActivity, R.string.main_item1, R.string.er_changed,
                                    R.drawable.warning, R.string.okay, -1) {
                                @Override
                                public void onOk() {
                                    restartClass(ExchangeRateActivity.class);
                                }
                            };
                        }
                    });
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onSendCore();
                    }
                });
            }
        }.start();
    }
    void onSendCore()
    {
        Pref.set(PayInfo.EUSO_TIMESTAMP, "" + System.currentTimeMillis());

        startApi(R.string.main_item1, R.string.sending);
        new Thread() {
            @Override
            public void run() {
                int result = DB.createRemitOrder(mActivity);
                if(result == RespBase.RESP_SUCCESS) {
                    String euso_order = Pref.get(DbOrder.EUSO_ORDER);
                    String uuid = Pref.get(DbOrder.UUID);
                    RespQueryRemitOrder query = DB.queryRemitOrder(null, euso_order, uuid);
                    if(query != null) {
                        DbOrder.update(query);
                    }
                }
                endApi();
                switch (result) {
                    case RespBase.RESP_TIME_OUT:
                        dialogWarn(R.string.main_item1, R.string.network_timeout);
                        break;
                    case RespBase.RESP_SUCCESS:
                        nextActivity(PayWayActivity.class, DbOrder.EUSO_ORDER, Pref.get(DbOrder.EUSO_ORDER),
                                FROM_REMIT, "1");
                        break;
                    case RespBase.RESP_FAIL:
                        Utils.dialogOK(mActivity, getString(R.string.order_error), DB.respRemitOrder.message, R.drawable.warning,
                                getString(R.string.back));
                        break;
                }
            }
        }.start();
    }
    String getPayeeAddress()
    {
        String area = Pref.get(PayInfo.PAYEE_AREA);
        String area_vi = Pref.get(PayInfo.PAYEE_AREA_VI);
        String province = Pref.get(PayInfo.PAYEE_PROVINCE);
        String city = Pref.get(PayInfo.PAYEE_CITY);
        String address = Pref.get(PayInfo.PAYEE_ADDRESS);
        if(Utils.isNull(area)) {
            return "";
        }
        if(Utils.isNull(address)) {
            return area_vi;
        }
        return address + ", " + city + ", " + ", " + province + ", " + area_vi;
    }
}
