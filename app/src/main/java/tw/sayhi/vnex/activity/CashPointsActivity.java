package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.adapter.CashHistoryAdapter;

public class CashPointsActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] ids = { R.id.iv_invite_friend, R.id.tv_cash_history, R.id.tv_cash_get_bonus };
    TextView tvCashHistory, tvCashGetBonus;
    LinearLayout llCashHistory;
    ScrollView svCashGetBonus;
    ListView lvCashHistory;
    CashHistoryAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.cash_points__activity);

        buildViews();

        buildDB();
    }
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            buildListeners(view);
        }
    };
    void buildListeners(View view)
    {
        switch (view.getId()) {
            case R.id.iv_invite_friend:
                inviteFriend();
                break;
            case R.id.tv_cash_history:
                llCashHistory.setVisibility(View.VISIBLE);
                svCashGetBonus.setVisibility(View.GONE);
                tvCashHistory.setTextColor(getResources().getColor(R.color.white));
                tvCashHistory.setBackgroundColor(getResources().getColor(R.color.bg_deep_cyan));
                tvCashGetBonus.setTextColor(getResources().getColor(R.color.black));
                tvCashGetBonus.setBackgroundColor(getResources().getColor(R.color.bg_cyan));
                break;
            case R.id.tv_cash_get_bonus:
                llCashHistory.setVisibility(View.GONE);
                svCashGetBonus.setVisibility(View.VISIBLE);
                tvCashHistory.setTextColor(getResources().getColor(R.color.black));
                tvCashHistory.setBackgroundColor(getResources().getColor(R.color.bg_cyan));
                tvCashGetBonus.setTextColor(getResources().getColor(R.color.white));
                tvCashGetBonus.setBackgroundColor(getResources().getColor(R.color.bg_deep_cyan));
                break;
        }
    }
    void buildViews()
    {
        buildTitleMenu();
        setTreeObserver(R.id.tv_cash_get_bonus, 0.8f);
        TextView tvShareCode = findViewById(R.id.tv_share_code);
        tvShareCode.setText(DB.getPromoteNo());
        llCashHistory = findViewById(R.id.ll_cash_history);
        svCashGetBonus = findViewById(R.id.sv_cash_get_bonus);
        tvCashHistory = findViewById(R.id.tv_cash_history);
        tvCashGetBonus = findViewById(R.id.tv_cash_get_bonus);
        lvCashHistory = findViewById(R.id.lv_cash_history);
//        addFooterView(lvCashHistory);
        for(int id : ids) {
            findViewById(id).setOnClickListener(onClickListener);
        }
    }
    void buildDB()
    {
        startApi(R.string.app_name, R.string.loading);
        new Thread() {
            @Override
            public void run() {
                DB.queryRedPoint(mActivity);
                DB.history(mActivity);
                // only after endApi() that users could proceed
                endApi();
                if(!DB.bQueryRedPoint || !DB.bRedPointHistory) {
                    warnAndFinish(mActivity, R.string.app_name, getString(R.string.network_timeout));
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        buildFields();
                    }
                });
            }
        }.start();
    }
    void buildFields()
    {
        TextView tvRedPoint = findViewById(R.id.tv_cash_points);
        tvRedPoint.setText("" + DB.redPoint);
        mAdapter = new CashHistoryAdapter(mActivity, DB.respCashHistory.dataCashHistories, mActivity);
        lvCashHistory.setAdapter(mAdapter);
    }
}
