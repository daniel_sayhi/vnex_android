package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.widget.BarCode;

public class PayVnexActivity extends BaseActivity
{
    Activity mActivity = this;
    String euso_order;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.pay_vnex__activity);

        euso_order = getIntentValue(DbOrder.EUSO_ORDER);

        buildViews();
    }
    void buildViews()
    {
        buildTitleMenu();
        buildHeader("4", "Thanh toán tại chi nhánh VNEX");
//        scaleViewTextSize(R.id.root, 0.8f);

        setOrderSummary(euso_order);

        int start = getString(R.string.pay_vnex_duration).indexOf('6');
        spanRedBold(R.string.pay_vnex_duration, R.id.tv_pay_duration, start, 7);

        TextView tvPayPlace = findViewById(R.id.tv_pay_place);
        tvPayPlace.setText("Thanh toán tại chi nhánh VNEX");
        Cursor c = DbOrder.read(euso_order);
//        buildVnexBanner(c,false);

        TextView tvPaymentCode = (TextView)findViewById(R.id.tv_payment_code);
        ImageView ivPaymentCode = (ImageView)findViewById(R.id.iv_payment_code);
        tvPaymentCode.setText(euso_order);
        BarCode.buildBarCode(euso_order, ivPaymentCode, Config.barcode_width, Config.barcode_height + 50);
    }
    @Override
    protected void onOrderPaid() {
        super.onOrderPaid();
        nextActivity(HistoryActivity.class);
    }
}
