package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import tw.sayhi.shared.AppCtx;
import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.DevInfo;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.shared.widget.ItemImageText;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;

public class InfoActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] items = { R.id.ll_hotline, R.id.iv_line, R.id.iv_fb, R.id.iv_vnexbranch, R.id.iv_qa };
    ImageView ivLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.info__activity);

        buildViews();

    }
    void buildListeners(View v)
    {
        String url;
        Intent intent;
        switch(v.getId()) {
            case R.id.iv_line: // LINE
//                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("line://home/public/main?id=vnexchuyentien"));
//                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://line.me/ti/p/vnexchuyentien"));
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://line.me/ti/p/YVrnFI6Lbl"));
                startActivity(intent);
/*
                line = "jp.naver.line.android";
                intent = getPackageManager().getLaunchIntentForPackage(line);
                if (intent != null) {
                    // We found the activity now start the activity
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    // Bring user to the market or let them choose an app?
                    intent = new Intent(Intent.ACTION_VIEW);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setData(Uri.parse("market://details?id=" + line));
                    startActivity(intent);
                }
*/
                break;
            case R.id.iv_fb: // FACEBOOK
//                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/techmarks"));
//                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/284173107077"));
//                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/100024708330819"));
//                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://messaging/100024708330819"));
//                startActivity(intent);
                url = "https://www.facebook.com/VNEX-1496980310570124/";
//                url = "https://www.facebook.com/tien.chuyen.311";
                Utils.openWebPage(mActivity, url);
                return;
            case R.id.ll_hotline: // HOTLINE
                startActivity(new Intent(
                        Intent.ACTION_DIAL, Uri.parse("tel:0982473800")));
                return;
            case R.id.iv_vnexbranch: // EEC
                nextActivity(StoreActivity.class);
/*
                url = "http://www.eec-elite.com/portal_d625.php?owner_num=d625_18302&button_num=d625";
                Utils.openWebPage(mActivity, url);
*/
                return;
            case R.id.iv_qa:
                nextActivity(QaActivity.class);
                return;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Utils.isNull(mLoginArcNo)) {
            ivLogout.setImageDrawable(getResources().getDrawable(R.drawable.icon_login));
        }
    }

    @Override
    protected void onLogout() {
        if(nonNull(mLoginArcNo)) {
            super.onLogout();
        } else {
            sMode = MODE_PORTAL;
            nextActivity(LoginActivity.class);
        }
    }
    void buildViews()
    {
        ivLogout = findViewById(R.id.iv_logout);
        TextView tvVersion = findViewById(R.id.tv_version);
        tvVersion.setText(DevInfo.getVersionName());
        for (int item : items) {
            View view = findViewById(item);
            if(view != null) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        buildListeners(v);
                    }
                });
            }
        }
    }
}
