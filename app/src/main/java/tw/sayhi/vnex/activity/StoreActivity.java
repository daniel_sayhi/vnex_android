package tw.sayhi.vnex.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.widget.ListView;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import tw.sayhi.shared.AppCtx;
import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.adapter.StoreArrayAdapter;
import tw.sayhi.vnex.misc.EECStore;

public class StoreActivity extends BaseActivity
{
    Activity mActivity = this;
    CountDownLatch latchPermission = new CountDownLatch(1);
    CountDownLatch latchLocation = new CountDownLatch(1);
    Location mLocation;
    static boolean promptLocationSettings = false;
    ListView mListView;
    StoreArrayAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.store__activity);

        buildViews();

        mLocation = null;

        requestPermission();

        new Thread() {
            @Override
            public void run() {
                Utils.waitFor(latchPermission);
                Utils.waitFor(latchLocation, 3 * 1000);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        buildListView();
                    }
                });
            }
        }.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isFinishing()) {
            return;
        }
    }

    void buildViews()
    {
        buildTitleMenu();
        mListView = findViewById(R.id.lv_service);
        addFooterView(mListView);
    }
    void buildListView()
    {
        // Setup Adapter
        EECStore eecStore = new EECStore();
        eecStore.init(mLocation);
        mAdapter = new StoreArrayAdapter(mActivity, eecStore.mArray, mActivity, mLocation);
        mListView.setAdapter(mAdapter);
        if(false) { // report location
            for(int i = 0; i < eecStore.mArray.size(); i++) {
                EECStore.Store store = eecStore.mArray.get(i);
                Address location = getLocationFromAddress(mActivity, store.address);
                if (location != null) {
                    MyLog.i( "" + location.getLatitude() + ", " + location.getLongitude() + " " + store.address);
                } else {
                    MyLog.e("No Location for " + store.address);
                }
            }
        }
    }
    public Address getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null || address.size() == 0) {
                return null;
            }

            return address.get(0);

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return null;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        latchPermission.countDown();
        latchLocation.countDown();
        if((ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)) {
            mLocation = AppCtx.getLocation(mActivity);
            if(mLocation == null) {
                postPromptLocationSettings();
            }
        }
    }
    void postPromptLocationSettings()
    {
        if(!promptLocationSettings) {
            promptLocationSettings = true;
        }

    }
    void requestPermission() {
        if(isLocationGranted(mActivity)) {
            onRequestPermissionsResult(-1, null, null);
        } else {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    999);
        }
    }
}
