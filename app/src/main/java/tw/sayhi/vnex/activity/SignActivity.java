package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kyanogen.signatureview.SignatureView;

import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.widget.ImageCapture;

public class SignActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] ids = { R.id.ll_agree, R.id.tv_resign, R.id.tv_next};
    CheckBox cbAgree;
    SignatureView svSign;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.sign__activity);

        buildViews();
    }
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            buildListeners(view);
        }
    };
    void buildListeners(View view)
    {
        boolean checked;
        switch (view.getId()) {
            case R.id.ll_agree:
                checked = cbAgree.isChecked();
                cbAgree.setChecked(!checked);
            case R.id.tv_resign:
                svSign.clearCanvas();
                break;
            case R.id.tv_next:
                onNext();
                break;
        }
    }
    void buildViews()
    {
        setTreeObserver(R.id.tv_sign_header, 0.7f);
        buildTitleMenu();
        buildHeader("3", "Ký tên");
        setRedStar(R.id.tv_sign_desc, R.string.sign_desc_2);
        cbAgree = (CheckBox)findViewById(R.id.cb_agree);
        svSign = (SignatureView)findViewById(R.id.sv_sign);
        for(int id : ids) {
            findViewById(id).setOnClickListener(onClickListener);
        }
    }
    void onNext() {
        if (!cbAgree.isChecked()) {
            warnDialog(R.string.sign_desc_3, R.string.empty_field, -1);
            return;
        }
        if (svSign.isBitmapEmpty()) {
            warnDialog(R.string.sign, R.string.empty_field, -1);
            return;
        }
        Bitmap bitmap = Bitmap.createScaledBitmap(svSign.getSignatureBitmap(),
                Config.sign_width, Config.sign_height, true);

        String path = ImageCapture.saveImage(mActivity, bitmap, Config.sign_file_name_norm);
        Pref.set(PayInfo.TRUSTEE_SIGN_NORM, path);

        nextActivity(ConsignActivity.class);
    }
}
