package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;

public class QuestionaryActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] items = { R.id.tv_next};
    int idSource = -1, idPurpose = -1, idRelation = -1, idOthers = -1;
    String codeSource, codePurpose, codeRelation, codeOthers;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.questionary__activity);

        buildViews();
    }
    void buildListeners(View v)
    {
        switch(v.getId()) {
            case R.id.tv_next:
                onNext();
                break;
        }
    }
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            buildListeners(view);
        }
    };
    RadioGroup.OnCheckedChangeListener rgListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                // for source of income
                case R.id.rb_salary:
                    idSource = R.string.salary;
                    codeSource = "001";
                    break;
                case R.id.rb_bonus:
                    idSource = R.string.bonus;
                    codeSource = "002";
                    break;
                case R.id.rb_source_others:
                    idSource = R.string.source_others;
                    codeSource = "999";
                    break;
                // for purpose of remittance
                case R.id.rb_savings:
                    idPurpose = R.string.savings;
                    codePurpose = "001";
                    break;
                case R.id.rb_house:
                    idPurpose = R.string.house_expenses;
                    codePurpose = "002";
                    break;
                case R.id.rb_medical:
                    idPurpose = R.string.medical_expenses;
                    codePurpose = "003";
                    break;
                case R.id.rb_purpose_others:
                    idPurpose = R.string.purpose_others;
                    codePurpose = "999";
                    break;
                // for relationship to recipient
                case R.id.rb_owner:
                    idRelation = R.string.owner_account;
                    codeRelation = "001";
                    break;
                case R.id.rb_family:
                    idRelation = R.string.family;
                    codeRelation = "002";
                    break;
                case R.id.rb_friend:
                    idRelation = R.string.friend;
                    codeRelation = "003";
                    break;
                case R.id.rb_relation_others:
                    idRelation = R.string.relationship_others;
                    codeRelation = "999";
                    break;
                // for Others
/*
                case R.id.rb_normal:
                    idOthers = R.string.normal;
                    codeOthers = "001";
                    break;
                case R.id.rb_abnormal:
                    idOthers = R.string.abnormal;
                    codeOthers = "002";
                    break;
*/
            }
        }
    };
    void buildViews()
    {
        buildTitleMenu();
        RadioGroup rgSource = findViewById(R.id.rg_source);
        rgSource.setOnCheckedChangeListener(rgListener);
        RadioGroup rgPurpose = findViewById(R.id.rg_purpose);
        rgPurpose.setOnCheckedChangeListener(rgListener);
        RadioGroup rgRelation = findViewById(R.id.rg_relation);
        rgRelation.setOnCheckedChangeListener(rgListener);
//        RadioGroup rgOthers = findViewById(R.id.rg_kyc_others);
//        rgOthers.setOnCheckedChangeListener(rgListener);
        for(int item : items) {
            View view = findViewById(item);
            if(view != null) {
                view.setOnClickListener(onClickListener);
            }
        }
    }
    void onNext()
    {
        if(idSource < 0 || idPurpose < 0 || idRelation < 0) {
            warnDialog(-1, R.string.complete_sections, -1);
            return;
        }
/*
        if(idSource < 0) {
            warnDialog(R.string.source_of_income, R.string.empty_field, -1);
            return;
        }
        if(idPurpose < 0) {
            warnDialog(R.string.purpose_of_remittance, R.string.empty_field, -1);
            return;
        }
        if(idRelation < 0) {
            warnDialog(R.string.relationship_to_recipient, R.string.empty_field, -1);
            return;
        }
        if(idOthers < 0) {
            warnDialog(R.string.kyc_others, R.string.empty_field, -1);
            return;
        }
*/
        Pref.set("source", getString(idSource));
        Pref.set("purpose", getString(idPurpose));
        Pref.set("relation", getString(idRelation));
//        Pref.set("kyc_others", getString(idOthers));
        Pref.set(PayInfo.SENDER_SOURCE_OF_INCOME, codeSource);
        Pref.set(PayInfo.SENDER_REMITTANCE_PURPOSE, codePurpose);
        Pref.set(PayInfo.RECIPINENT_RELATIONSHIP, codeRelation);

        nextActivity(SignActivity.class);
    }
}
