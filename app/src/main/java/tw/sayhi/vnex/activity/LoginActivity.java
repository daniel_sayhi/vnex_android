package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.BuildConfig;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.DebugOptions;
import tw.sayhi.vnex.MainActivity;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.misc.QueryApiService;
import tw.sayhi.vnex.misc.Valid;

public class LoginActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] items = { R.id.iv_show_password, R.id.tv_sign_in, R.id.tv_register, R.id.tv_forget_password };
    EditText etArcNo, etPassword;
    boolean showPassword = false;
    ImageView ivShowPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.login__activity);

        buildViews();

        startQueryService();
    }
    void buildListeners(int id)
    {
        switch(id) {
            case R.id.tv_sign_in:
                onLogin();
                break;
            case R.id.tv_register:
                onChooseIdentity();
//                nextActivity(SenderActivity.class, IS_REGISTER, "1");
                break;
            case R.id.iv_show_password:
                if(showPassword == true) {
                    etPassword.setTransformationMethod(new PasswordTransformationMethod());
                    ivShowPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_hide));
                    showPassword = false;
                } else {
                    etPassword.setTransformationMethod(null);
                    ivShowPassword.setImageDrawable(getResources().getDrawable(R.drawable.ic_password_show));
                    showPassword = true;
                }
                break;
            case R.id.tv_forget_password:
//                if(isChinese) {
//                    warnDialog(R.string.empty, R.string.forget_password_note_tw, -1);
//                } else {
                    warnDialog(R.string.empty, R.string.forget_password_note, -1);
//                }
                break;
        }
    }
    void buildViews()
    {
        if(mIsNarrowScreen) {
            findViewById(R.id.vi_wide_screen_margin).setVisibility(View.GONE);
        }
        etArcNo = findViewById(R.id.et_arc_no);
        etArcNo.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        etPassword = findViewById(R.id.et_password);
        etPassword.setTransformationMethod(new PasswordTransformationMethod());
        ivShowPassword = findViewById(R.id.iv_show_password);
        if(isChinese) {
            etArcNo.setHint("居留證號碼");
            etPassword.setHint("密碼");
            setTextView(R.id.tv_sign_in, "成員登入");
            setTextView(R.id.tv_register, "新註冊");
            setTextView(R.id.tv_forget_password, "忘記密碼");
            setTextView(R.id.tv_note, "您尚未登記會員，請註冊");
        }
        for (int item : items) {
            View view = findViewById(item);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buildListeners(view.getId());
                }
            });
        }
        findViewById(R.id.iv_login_arc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(BuildConfig.DEBUG || DebugOptions.bUseTestURL) {
                    etArcNo.setText(DebugOptions.arc_no);
                    etPassword.setText(DebugOptions.password);
                }
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    void onLogin()
    {
        final String sender_arc = etArcNo.getText().toString();
        if(Utils.isNull(sender_arc)) {
            warnDialog(R.string.arc_no_hint2, R.string.empty_field, R.id.et_arc_no);
            return;
        }
        if(!Valid.isArcNo(sender_arc) && !Valid.isNewArcNo(sender_arc)) {
            warnDialog(R.string.arc_no_hint2, R.string.format_error, R.id.et_arc_no);
            return;
        }
        final String password = etPassword.getText().toString();
        if(Utils.isNull(password)) {
            warnDialog(R.string.password_hint2, R.string.empty_field, R.id.et_password);
            return;
        }
        if(!Valid.isPassword(password)) {
            warnDialog(R.string.password_hint2, R.string.password_format, R.id.et_password);
            return;
        }
        startApi(R.string.app_name, R.string.please_wait);
        new Thread() {
            @Override
            public void run() {
                DB.login(mActivity, sender_arc, password);
                if(!DB.bLoginFinished) {
                    endApi();
                    warnDialog(R.string.login, R.string.network_timeout, -1);
                } else if(DB.bLoginSuccess) {
                    endApi();
                    if(DB.isForeignWorker()) {
                        switch (sMode) {
                            case MODE_PORTAL:
                                nextActivity(MainActivity.class);
                                break;
                            case MODE_REMIT:
                                nextActivity(RemitWayActivity.class);
                                break;
                            case MODE_HISTORY:
                                nextActivity(HistoryActivity.class);
                                break;
                            case MODE_PROFILE:
                                nextActivity(SenderActivity.class);
                                break;
                            case MODE_CASH_POINTS:
                                nextActivity(CashPointsNewActivity.class);
                                break;
                        }
                    } else {
                        nextActivity(CashPointsNewActivity.class);
                    }
                    finish();
                } else {
                    DB.queryRegister(mActivity, sender_arc);
                    endApi();
                    if(DB.bQueryRegister) {
                        if(DB.respQueryRegister.registered) {
                            warnDialog(R.string.member_sign_in, R.string.login_fail, -1);
                        } else {
                            actionDialog(R.string.member_sign_in, R.string.not_registered, new Runnable() {
                                @Override
                                public void run() {
                                    Pref.set(PayInfo.TRUSTEE_PASSPORT_ID, sender_arc);
                                    nextActivity(SenderActivity.class, IS_REGISTER, "1");
                                }
                            });
                        }
                    } else {
                        onApiFail(DB.respQueryRegister, null);
                    }
                }
            }
        }.start();
    }
    void onChooseIdentity()
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.choose_identity__dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.findViewById(R.id.tv_foreign_worker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVi();
                dialog.dismiss();
                onForeignWorker();
            }
        });
        dialog.findViewById(R.id.tv_taiwanese).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTw();
                dialog.dismiss();
                onTaiwanese();
            }
        });
        dialog.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    void onForeignWorker()
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.foreign_worker__dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if(mIsNarrowScreen) {
            scaleTextSize(dialog.findViewById(R.id.root), 0.9f);
        }
        dialog.findViewById(R.id.tv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onChooseIdentity();
            }
        });
        dialog.findViewById(R.id.tv_okay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVi();
                nextActivity(SenderActivity.class, IS_REGISTER, "1");
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    void onTaiwanese()
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.taiwanese__dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if(mIsNarrowScreen) {
            scaleTextSize(dialog.findViewById(R.id.root), 0.8f);
        }
        dialog.findViewById(R.id.tv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onChooseIdentity();
            }
        });
        dialog.findViewById(R.id.tv_okay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVi();
                dialog.dismiss();
                nextActivity(SenderTwActivity.class);
            }
        });
        dialog.show();
    }
    void startQueryService()
    {
        new Thread() {
            @Override
            public void run() {
                if(!isServiceRunning(QueryApiService.class)) {
                    Intent intent = new Intent(mActivity, QueryApiService.class);
                    startService(intent);
                }
                for(int i = 0; i < 20; i++) {
                    if(isServiceRunning(QueryApiService.class)) {
                        buildServiceConnection();
                        break;
                    }
                    Utils.sleep(1000);
                }
            }
        }.start();
    }
}
