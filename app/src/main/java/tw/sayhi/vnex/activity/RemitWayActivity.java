package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import tw.sayhi.shared.Utils;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.misc.DbSender;
import tw.sayhi.vnex.misc.QueryApiService;
import tw.sayhi.vnex.widget.ActionDialog;

public class RemitWayActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] items = { R.id.item1, R.id.item2, R.id.item3, R.id.item4 };
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.remit_way__activity);

        buildViews();
    }
    void buildListeners(int id)
    {
        switch(id) {
            case R.id.item1: // 密碼
                PayInfo.trans_type = PayInfo.PIN;
                break;
            case R.id.item2: // 帳號
                PayInfo.trans_type = PayInfo.BTB;
                break;
            case R.id.item3: // 送到家
                PayInfo.trans_type = PayInfo.DTD;
                break;
            case R.id.item4: // ID
                PayInfo.trans_type = PayInfo.IDP;
                break;
        }
        startApi(R.string.app_name, R.string.loading);
        new Thread() {
            @Override
            public void run() {
                QueryApiService.onQueryStatus(mActivity, DB.getSenderArc(), false);
                endApi();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        doNext();
                    }
                });
            }
        }.start();
        /*
        if(!DB.needQueryStatus()) {
            doNext();
        } else {
            apiService(QueryApiService.QUERY_STATUS, true, new Runnable() {
                @Override
                public void run() {
                    doNext();
                }
            });
        } */
//        startActivity(new Intent(mActivity, PayWayActivity.class));
    }
    void buildViews()
    {
        buildTitleMenu();
        buildHeader("2", getString(R.string.remit_way));
        spanRedBold(R.string.remit_desc_1, R.id.tv_remit_way_1, 2, 7);
        spanRedBold(R.string.remit_desc_2, R.id.tv_remit_way_2, 2, 13);
        spanRedBold(R.string.remit_desc_3, R.id.tv_remit_way_3, 2, 13);
        spanRedBold(R.string.remit_desc_4, R.id.tv_remit_way_4, 2, 15);
        for (int item : items) {
            View view = findViewById(item);
            if(view == null)
                continue;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buildListeners(view.getId());
                }
            });
        }
    }
    void doNext()
    {
        String status = DB.getStatus();
        if (status.equals(DbSender.SENDER_SUCCESS)) {
            if(Utils.isDateEffective(DB.getExpiryDate())) {
                nextActivity(ReceiverActivity.class);
            } else {
                new ActionDialog(mActivity, -1, R.string.arc_is_expired,
                        R.drawable.warning, R.string.okay, -1) {
                    @Override
                    public void onOk() {
                        nextActivity(SenderActivity.class, ARC_EXPIRED, "1");
                    }
                };
            }
        } else if (status.equals(DbSender.SENDER_FAIL) ||
                status.equals(DbSender.SENDER_DELETE))
        {
            actionDialog(R.string.app_name, R.string.review_fail, new Runnable() {
                @Override
                public void run() {
                    nextActivity(SenderActivity.class);
                }
            });
        } else { // Sender under review
            warnDialog(R.string.app_name, R.string.register_succeed, -1);
        }
    }
}
