package tw.sayhi.vnex.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;

import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.MainActivity;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.R;

public class ExchangeRateActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] items = {
            R.id.tv_retype, R.id.tv_next
    };
    ImageView ivLogout;
    EditText etZh, etVi, etUs;
    boolean isTextChanging = false;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.exchange_rate__activity);

        buildViews();

        startApi(R.string.main_item1, R.string.please_wait);
        new Thread() {
            @Override
            public void run() {
                DB.queryER(mActivity);
                DB.queryEvent(null);
                endApi();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!DB.bQueryER || !DB.bQueryEvent) {
                            warnNetwork(mActivity, R.string.main_item1);
                        } else {
                            buildER();
                            initEditText();
                        }
                    }
                });
                DB.queryBankListWithInfo(null);
                DB.queryAddressList(null);
            }
        }.start();
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if(Utils.isNull(mLoginArcNo)) {
            ivLogout.setImageDrawable(getResources().getDrawable(R.drawable.icon_login));
        }
    }
    @Override
    protected void onLogout() {
        if(nonNull(mLoginArcNo)) {
            super.onLogout();
        } else {
            sMode = MODE_PORTAL;
            nextActivity(LoginActivity.class);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        restartClass(MainActivity.class);
    }

    void initEditText()
    {
        String string_ntd = Pref.get(PayInfo.STRING_NTD);

        if(Utils.isNull(string_ntd)) {
            return;
        }
        PayInfo.setAmount(string_ntd, PayInfo.NTD);
        isTextChanging = true;
        etZh.setText(PayInfo.commaNTD);
        etVi.setText(PayInfo.commaVND);
        etUs.setText(PayInfo.commaUSD);
        isTextChanging = false;
    }
    @Override
    public void onStop()
    {
        super.onStop();

        Pref.set(PayInfo.STRING_NTD, PayInfo.stringNTD);
    }
    void buildListeners(int id)
    {
        switch (id) {
            case R.id.tv_retype:
                onRetype();
                break;
            case R.id.tv_next:
                onNext();
                break;
        }
    }
    void buildViews()
    {
        buildHeader("1", "Tính thử");
        ivLogout = findViewById(R.id.iv_logout);
        for (int item : items) {
            View view = findViewById(item);
            if(view == null)
                continue;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buildListeners(view.getId());
                }
            });
        }
        etZh = findViewById(R.id.et_zh);
        etVi = findViewById(R.id.et_vi);
        etUs = findViewById(R.id.et_us);
        etZh.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable in) {
                if(isTextChanging) {
                    return;
                }
                isTextChanging = true;

                if(!PayInfo.setAmount(in.toString(), PayInfo.NTD)) {
//                    warnMaxRemit();
                }
                in.replace(0, in.length(), PayInfo.commaNTD);
                etVi.setText(PayInfo.commaVND);
                etUs.setText(PayInfo.commaUSD);

                isTextChanging = false;
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
                if(mTextChanging) {
                    return;
                }
                long dollar = Utils.parseLong(s);
                mTextChanging = true;
                PayInfo.setPayAmount(s.toString(), DB.NTD);
//                DecimalFormat formatter = new DecimalFormat("###,###,###,###.##");
//                etVi.setText(formatter.format(DB.erVND * dollar));
                etVi.setText(PayInfo.commaVND);
                etUs.setText(PayInfo.commaUSD);
                mTextChanging = false;
*/
            }
        });
        etVi.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable in) {
                if(isTextChanging) {
                    return;
                }
                isTextChanging = true;

                if(!PayInfo.setAmount(in.toString(), PayInfo.VND)) {
//                    warnMaxRemit();
                }
                in.replace(0, in.length(), PayInfo.commaVND);
                etZh.setText(PayInfo.commaNTD);
//                etVi.setText(PayInfo.commaVND);
                etUs.setText(PayInfo.commaUSD);

                isTextChanging = false;
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
                if(mTextChanging) {
                    return;
                }
                long dollar = Utils.parseLong(s);
                mTextChanging = true;
                PayInfo.setPayAmount(s.toString(), DB.VND);
                etZh.setText(PayInfo.commaNTD);
                etUs.setText(PayInfo.commaUSD);
                mTextChanging = false;
*/
            }
        });
        etUs.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable in) {
                if(isTextChanging) {
                    return;
                }
                isTextChanging = true;

                if(!PayInfo.setAmount(in.toString(), PayInfo.USD)) {
//                    warnMaxRemit();
                }
                in.replace(0, in.length(), PayInfo.commaUSD);
                etZh.setText(PayInfo.commaNTD);
                etVi.setText(PayInfo.commaVND);

                isTextChanging = false;
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
/*
                if(mTextChanging) {
                    return;
                }
                long dollar = Utils.parseLong(s);
                mTextChanging = true;
                PayInfo.setPayAmount(s.toString(), DB.USD);
                etZh.setText(PayInfo.commaNTD);
                etVi.setText(PayInfo.commaUSD);
                mTextChanging = false;
*/
            }
        });
    }
    void buildER()
    {
        PayInfo.erVND = DB.erVND;
        PayInfo.erUSD = DB.erUSD;
        TextView tvViER = findViewById(R.id.tv_er_vnd);
        DecimalFormat df = new DecimalFormat("0.#");
        tvViER.setText(df.format(DB.erVND));
        TextView tvUsER = findViewById(R.id.tv_er_usd);
        String usER = String.format("%.2f", DB.erUSD);
        tvUsER.setText("" + DB.erUSD);
    }
    void onRetype()
    {
        isTextChanging = true;
        etZh.setText("");
        etVi.setText("");
        etUs.setText("");
        isTextChanging = false;
    }
    void onNext()
    {
        String ntd = etZh.getText().toString();
        if(isNull(ntd)) {
            warnDialog(R.string.remit_amount, R.string.empty_field, R.id.et_zh);
            return;
        }
        if(PayInfo.floatNTD > PayInfo.AMOUNT_LIMIT) {
            warnDialog(R.string.main_item1_sub_title, R.string.input_amount_warn, R.id.et_zh);
            return;
        }
        if(isLogin()) {
            start(mActivity, RemitWayActivity.class, "", "");
        } else {
            start(mActivity, LoginActivity.class, getString(R.string.back), getString(R.string.main_item1));
        }
    }
}
