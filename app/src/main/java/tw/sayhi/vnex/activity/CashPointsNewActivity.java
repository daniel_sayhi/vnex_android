package tw.sayhi.vnex.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.Config;
import tw.sayhi.vnex.DB;
import tw.sayhi.vnex.PayInfo;
import tw.sayhi.vnex.PortalActivity;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.adapter.CashHistoryAdapter;
import tw.sayhi.vnex.widget.BarCode;

public class CashPointsNewActivity extends BaseActivity
{
    Activity mActivity = this;
    int[] ids = { R.id.ll_invite_friend, R.id.tv_cash_history, R.id.tv_cash_get_bonus,
            R.id.iv_grocery, R.id.iv_cargo, R.id.iv_gift
    };
    TextView tvCashHistory, tvCashGetBonus;
    LinearLayout llCashHistory, llCashGetBonus;
    ListView lvCashHistory;
    CashHistoryAdapter mAdapter;
    ScrollView sv;
    ImageView ivGrocery, ivCargo, ivGift;
    boolean bExchangeCoupon = false;
    boolean isGroceryAvail = true, isCargoAvail = true, isGiftAvail = true;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isFinishing()) return;
        setContentView(R.layout.cash_points_new__activity);

        buildViews();

        buildDB();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(!DB.isForeignWorker())
            restartClass(PortalActivity.class);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            buildListeners(view);
        }
    };
    @SuppressLint("NonConstantResourceId")
    void buildListeners(View view)
    {
        switch (view.getId()) {
            case R.id.ll_invite_friend:
                inviteFriend();
                break;
            case R.id.tv_cash_history:
                llCashHistory.setVisibility(View.VISIBLE);
                llCashGetBonus.setVisibility(View.GONE);
                tvCashHistory.setTextColor(getResources().getColor(R.color.white));
                tvCashHistory.setBackgroundColor(getResources().getColor(R.color.bg_deep_cyan));
                tvCashGetBonus.setTextColor(getResources().getColor(R.color.black));
                tvCashGetBonus.setBackgroundColor(getResources().getColor(R.color.bg_cyan));
                break;
            case R.id.tv_cash_get_bonus:
                llCashHistory.setVisibility(View.GONE);
                if(DB.isForeignWorker())
                    llCashGetBonus.setVisibility(View.VISIBLE);
                tvCashHistory.setTextColor(getResources().getColor(R.color.black));
                tvCashHistory.setBackgroundColor(getResources().getColor(R.color.bg_cyan));
                tvCashGetBonus.setTextColor(getResources().getColor(R.color.white));
                tvCashGetBonus.setBackgroundColor(getResources().getColor(R.color.bg_deep_cyan));
                break;
                /*
            case R.id.iv_grocery:
                if(!isGroceryAvail) {
                    warnDialog(R.string.cash_points_header, R.string.cash_points_not_enough, -1);
                    return;
                }
                nextActivity(ExchangeCouponActivity.class, PayInfo.COUPON_TYPE, PayInfo.COUPON_GROCERY);
                break;
            case R.id.iv_cargo:
                if(!isCargoAvail) {
                    warnDialog(R.string.cash_points_header, R.string.cash_points_not_enough, -1);
                    return;
                }
                nextActivity(ExchangeCouponActivity.class, PayInfo.COUPON_TYPE, PayInfo.COUPON_CARGO);
                break;
            case R.id.iv_gift:
                if(!isGiftAvail) {
                    warnDialog(R.string.cash_points_header, R.string.cash_points_not_enough, -1);
                    return;
                }
                nextActivity(ExchangeCouponActivity.class, PayInfo.COUPON_TYPE, PayInfo.COUPON_GIFT);
                break;
                 */
        }
    }
    void buildViews()
    {
        /*
        if( mIsNarrowScreen) {
//            setTreeObserver(R.id.tv_cash_get_bonus, 0.8f);
            scaleTextSize(R.id.root, 0.8f);
        } */

        ImageView ivPromoteNo = findViewById(R.id.iv_promote_no);
        BarCode.buildBarCode(DB.getPromoteNo(), ivPromoteNo, 1080, Config.barcode_height + 50);
        TextView tvPromoteNo = findViewById(R.id.tv_promote_no);
        tvPromoteNo.setText(DB.getPromoteNo());
        sv = findViewById(R.id.sv);
        llCashHistory = findViewById(R.id.ll_cash_history);
        llCashGetBonus = findViewById(R.id.ll_cash_get_bonus);
        tvCashHistory = findViewById(R.id.tv_cash_history);
        tvCashGetBonus = findViewById(R.id.tv_cash_get_bonus);
        lvCashHistory = findViewById(R.id.lv_cash_history);
        ivGrocery = findViewById(R.id.iv_grocery);
        ivCargo = findViewById(R.id.iv_cargo);
        ivGift = findViewById(R.id.iv_gift);
//        addFooterView(lvCashHistory);
        if(isChinese) {
            setTextView(R.id.tv_header, "累計點數");
            setTextView(R.id.tv_invite_friend, "邀請好友累計點數");
            setTextView(R.id.tv_remain_points, "總點數");
            setTextView(R.id.tv_point, "點");
            setTextView(R.id.tv_cash_history, "點數紀錄");
            setTextView(R.id.tv_cash_get_bonus, "點數計算方式");
            setTextView(R.id.tv_date, "日期");
            setTextView(R.id.tv_content, "內容");
            setTextView(R.id.tv_adjust_points, "點數");
            setTextView(R.id.tv_points_1, "點");
            setTextView(R.id.tv_points_2, "點");
            setTextView(R.id.tv_points_3, "點");
            setTextView(R.id.tv_desc_download, "下載並完成登記");
            setTextView(R.id.tv_desc_share, "寄分享碼給朋友，朋友下載並完成第一筆交易");
            setTextView(R.id.tv_desc_remit, "每完成一筆匯款交易");
        }
        for(int id : ids) {
            findViewById(id).setOnClickListener(onClickListener);
        }
    }
    void buildDB()
    {
        startApi(R.string.app_name, R.string.loading);
        new Thread() {
            @Override
            public void run() {
                DB.queryRedPoint(mActivity);
                boolean bCoupon = true;
                if(bExchangeCoupon) {
                    DB.queryExchangeCoupon(mActivity, PayInfo.COUPON_GROCERY);
                    bCoupon = bCoupon && DB.bQueryExchangeCoupon;
                    DB.queryExchangeCoupon(mActivity, PayInfo.COUPON_CARGO);
                    bCoupon = bCoupon && DB.bQueryExchangeCoupon;
                    DB.queryExchangeCoupon(mActivity, PayInfo.COUPON_GIFT);
                    bCoupon = bCoupon && DB.bQueryExchangeCoupon;
                }
                DB.history(mActivity);
                // only after endApi() that users could proceed
                endApi();
                if(!DB.bQueryRedPoint || !DB.bRedPointHistory || !bCoupon) {
                    warnAndFinish(mActivity, R.string.app_name, getString(R.string.network_timeout));
                    return;
                }
                runOnUiThread(() -> buildFields());
            }
        }.start();
    }
    void buildFields()
    {
        TextView tvRedPoint = findViewById(R.id.tv_cash_points);
        tvRedPoint.setText("" + DB.redPoint);
        mAdapter = new CashHistoryAdapter(mActivity, DB.respCashHistory.dataCashHistories, mActivity);
        lvCashHistory.setAdapter(mAdapter);
//        sv.fullScroll(ScrollView.FOCUS_DOWN);
        sv.smoothScrollTo(0,0);
//        Utils.setListViewHeightBasedOnChildren(lvCashHistory);
        if(bExchangeCoupon) {
            if (!DB.groceryExchangeCoupon.hasCoupon || DB.redPoint < DB.groceryExchangeCoupon.minCost) {
                ivGrocery.setImageDrawable(getResources().getDrawable(R.drawable.points_coupon_grocery_inactive));
                isGroceryAvail = false;
                //            ivGrocery.setClickable(false);
            }
            if (!DB.cargoExchangeCoupon.hasCoupon || DB.redPoint < DB.cargoExchangeCoupon.minCost) {
                ivCargo.setImageDrawable(getResources().getDrawable(R.drawable.points_coupon_seacargo_inactive));
                isCargoAvail = false;
                //            ivCargo.setClickable(false);
            }
            if (!DB.giftExchangeCoupon.hasCoupon || DB.redPoint < DB.giftExchangeCoupon.minCost) {
                ivGift.setImageDrawable(getResources().getDrawable(R.drawable.points_coupon_gift_inactive));
                isGiftAvail = false;
                //            ivGift.setClickable(false);
            }
        }
    }
}
