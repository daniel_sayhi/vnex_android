package tw.sayhi.vnex;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
/*
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
*/
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import tw.sayhi.shared.GlobalStatic;
import tw.sayhi.shared.AppCtx;
import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.activity.CashPointsNewActivity;
import tw.sayhi.vnex.activity.InfoActivity;
import tw.sayhi.vnex.activity.LoginActivity;
import tw.sayhi.vnex.adapter.BannerAdapter;
import tw.sayhi.vnex.api.RespBase;
import tw.sayhi.vnex.misc.CloudImages;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.misc.DbRemit;
import tw.sayhi.vnex.misc.DbSender;
import tw.sayhi.vnex.misc.QueryApiService;
import tw.sayhi.vnex.widget.ActionDialog;
import tw.sayhi.vnex.widget.BarCode;
import tw.sayhi.vnex.widget.ImageCapture;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;
import static tw.sayhi.vnex.PayInfo.normVietText;

public class BaseActivity extends AppCompatActivity
{
    public static Class<?> mClass;
    public static boolean isInBackground = true;
    public static boolean mIsNarrowScreen = false;
    // Constants for Activity display
    public static final String PREV_PAGE = "prev_page";
    public static final String TITLE = "title";
    public static final String ADV_ID = "adv_id";
    public final static String URL = "URL";
    public final static String HTML = "HTML";
    public static String mWebViewURL = null, mWebViewHTML = null;
    //
    public static final int MODE_PORTAL = 0;
    public static final int MODE_REMIT = 1;
    public static final int MODE_PROFILE = 2;
    public static final int MODE_HISTORY = 3;
    public static final int MODE_CASH_POINTS = 4;
    public static int sMode;
    // Constants for Profile/Remit order
    public static final String IS_REGISTER = "is_register";
    public static final String JUST_REGISTERED = "just_registered";
    public static final String ARC_EXPIRED = "arc_expired";
    public static final String FROM_REMIT = "from_remit";

    public static String mLoginArcNo = "";
    public static String mShareCode = "";

    protected int colorUnchangeable;
//    private Tracker mTracker;
    protected String mCurrentLocale;
    protected String mTitle;
    protected LinearLayout llLoading = null;
    protected ProgressDialog progressDialog;
    public static long mLastQueryTime = 0;
    protected Timer queryTimer;
    protected TimerTask queryTimerTask;
    public static ServiceConnection mServiceConnection;
    public static Messenger mMessenger;
    public static Bundle mMessageBundle;
    public static String[] months_vi = {
            "Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu",
            "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"
    };
    public static String[] months_tw = {
            "一月", "二月", "三月", "四月", "五月", "六月",
            "七月", "八月", "九月", "十月", "十一月", "十二月"
    };
    public static boolean firstEntry = true;
    protected ViewPager mViewPager;
    protected BannerAdapter mBannerAdapter;
    protected static int mBannerIndex = 0;
    protected Timer mTimerSlides;
    protected TimerTask mTimerTaskSlides;
    protected int mnPagerInterval = 1000 * 7; // 7 seconds
    public static int id_back = R.string.back;
    public static boolean isChinese = false;
    protected boolean isTextScaled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // for Google Analytics
        AppCtx application = (AppCtx) getApplication();
//        mTracker = application.getDefaultTracker();

        super.onCreate(savedInstanceState);

        /*
        Class<?> enclosingClass = getClass().getEnclosingClass();
        if (enclosingClass != null) {
            MyLog.i(enclosingClass.getName());
        } else {
            MyLog.i(getClass().getName());
        } */

        if(!GlobalStatic.isInitialized) {
            startActivity(new Intent(this, PortalActivity.class));
            finish();
            return;
        }
        // for Locale
//        mCurrentLocale = Pref.getLanguage("vi");
        mCurrentLocale = "vi";
        setLocale(this, mCurrentLocale);
        if(BuildConfig.DEBUG) {
            Utils.postMessage(this, this.getClass().getSimpleName());
        }
        colorUnchangeable = getResources().getColor(R.color.grey);
    }
    protected boolean mTextChanging = false;
    protected TextWatcher vietTextWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable in) {
            if(mTextChanging) {
                return;
            }
            mTextChanging = true;

            in.replace(0, in.length(), normVietText(in.toString()));

            mTextChanging = false;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}
    };
    protected void buildTitle(int id_title)
    {
        TextView tvTitle = findViewById(R.id.tv_title);
        if(tvTitle != null) {
            tvTitle.setText(id_title);
        }
    }
    protected void buildBack()
    {
        // for R.id.iv_back
        ImageView ivBack = findViewById(R.id.iv_back);
        if(ivBack != null) {
            ivBack.setVisibility(View.VISIBLE);
            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }
    protected void disableBack()
    {
        // for R.id.iv_back
        ImageView ivBack = findViewById(R.id.iv_back);
        if(ivBack != null) {
            ivBack.setVisibility(View.GONE);
        }
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev)
    {
        if(llLoading != null) {
            return true; // disable all touch events for this activity
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isInBackground = true;
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        mWebViewURL = null;
        mWebViewHTML = null;
        if(queryTimer != null) {
            queryTimer.cancel();
            queryTimer = null;
        }
        if(mTimerSlides != null) {
            mTimerSlides.cancel();
        }
    }
    @Override
    public void setContentView(int layoutResID)
    {
        super.setContentView(layoutResID);
    }
    public void trackScreenName(String name)
    {
//        mTracker.setScreenName(name);
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
    public void trackScreenName(Bundle bundle)
    {
        if(bundle.containsKey("title")) {
            trackScreenName(bundle.getString("title"));
        }
    }
    public void trackScreenName(TextView tv)
    {
        if(tv != null) {
            trackScreenName(tv.getText().toString());
        }
    }
    public void trackEvent(String cate, String action)
    {
//        mTracker.send(new HitBuilders.EventBuilder()
//                .setCategory(cate)
//                .setAction(action)
//                .build());

    }
    public void addFooterView(ListView lv)
    {
        LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View footerView =  inflater.inflate(R.layout.list_view_footer, null, false);
        lv.addFooterView(footerView,null,false);
    }
    public String getIntentValue(String key)
    {
        String value = getIntent().getStringExtra(key);
        if(value == null)
            value = "";
        return value;
    }
    protected boolean intentContains(String key) {
        return(!Utils.isNull(getIntentValue(key)));
    }
    public static void start(Activity context, Class<?> cls, String prev_page, String title)
    {
        Intent intent = new Intent();
        intent.putExtra(PREV_PAGE, prev_page);
        intent.putExtra(TITLE, title);
        if(mWebViewURL != null)
            intent.putExtra(URL, mWebViewURL);
        if(mWebViewHTML != null)
            intent.putExtra(HTML, mWebViewHTML);

        intent.setClass(context, cls);
        context.startActivity(intent);
    }
    protected void scaleTextViewSize(int id, final float scale)
    {
        final ViewGroup root = findViewById(id);
        scaleTextViewSize(root, scale);
    }
    public static void scaleTextViewSize(final ViewGroup root, final float scale)
    {
        ViewTreeObserver vto = root.getViewTreeObserver();
        vto.addOnGlobalLayoutListener (new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                root.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                scaleTextSize(root, scale);
            }
        });
    }
    protected void adjustBannerHeight(ViewGroup vg, double ratio) {
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        ViewGroup.LayoutParams params = vg.getLayoutParams();
        params.height = (int)(width * ratio);
        vg.setLayoutParams(params);
    }
    protected boolean isNarrowScreen()
    {
        return isNarrowScreen(this);
    }
    public static boolean isNarrowScreen(Activity act)
    {
        Display display = act.getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        return (width <= 960);
    }
    protected String getTrimString(int id)
    {
        String str = getString(id).replace("\n", "");
        return str;
    }
    public static void setLocale(Activity act, String languageToLoad) {
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        Resources resources = act.getBaseContext().getResources();
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }
    protected String getEditText(int id)
    {
        EditText et = (EditText)findViewById(id);
        if(et == null) {
            return "";
        }
        String org = et.getText().toString();
        return normVietText(org);
    }
    protected void dialogWarn(final int title_id, final int msg_id)
    {
        Utils.dialogOK(this, title_id, msg_id, R.drawable.warning, R.string.okay);
    }
    protected void warnNetwork(final Activity act, final int title_id)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ActionDialog(act, title_id, R.string.network_timeout,
                        R.drawable.warning, R.string.okay, -1) {
                    @Override
                    public void onOk() {
                        finish();
                    }
                };
            }
        });
    }
    protected void spanRedBold(int str_id, int tv_id, int start, int len)
    {
        SpannableString spanString = new SpannableString(getString(str_id));
        spanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red_highlight)), start, start + len, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanString.setSpan(new StyleSpan(Typeface.BOLD), start, start + len, 0);
        TextView tv = findViewById(tv_id);
        tv.setText(spanString);
    }
    protected void setRedStar(int tv_id, int str_id)
    {
        TextView tv;
        tv = (TextView)findViewById(tv_id);
        Spannable span = new SpannableString("* " + getString(str_id));
        span.setSpan(new RelativeSizeSpan(2f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new ForegroundColorSpan(Color.RED), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new StyleSpan(Typeface.BOLD), 0, 1, 0);
        tv.setText(span);
    }
    protected boolean storeEditView(String key, int et_id)
    {
        EditText et = (EditText)findViewById(et_id);
        if(et == null) {
            return false;
        }
        String value = et.getText().toString();
        if(Utils.isNull(value)) {
            return false;
        }
        Pref.set(key, value);
        return true;
    }
    protected boolean setPrefEditView(String key, int et_id)
    {
        String value = Pref.get(key);
        return setEditView(value, et_id);
    }
    protected boolean setEditView(String value, int et_id)
    {
        EditText et = findViewById(et_id);
        if(Utils.isNull(value) || et == null) {
            return false;
        }
        et.setText(value);
        return true;
    }
    protected boolean setEditViewHint(int et_id, String hint)
    {
        EditText et = findViewById(et_id);
        if(Utils.isNull(hint) || et == null) {
            return false;
        }
        et.setHint(hint);
        return true;
    }
    protected boolean isEqual(String key, int et_id)
    {
        EditText et = (EditText)findViewById(et_id);
        if(et == null) {
            return false;
        }
        String et_value = et.getText().toString();
        if(Utils.isNull(et_value)) {
            return false;
        }
        return et_value.equals(Pref.get(key));
    }
    protected boolean setTextView(String key, int et_id)
    {
        String value = Pref.get(key);
        TextView tv = (TextView)findViewById(et_id);
        if(Utils.isNull(value) || tv == null) {
            return false;
        }
        tv.setText(value);
        return true;
    }
    protected void actionDialog(final int title_id, final int msg_id, final Runnable ok_runnable)
    {
        final Activity act = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ActionDialog(act, title_id, msg_id,
                        R.drawable.warning, R.string.okay, -1) {
                    @Override
                    public void onOk() {
                        if(ok_runnable != null) {
                            ok_runnable.run();
                        }
                    }
                };
            }
        });

    }
    protected void actionDialog(final String title, final String msg, final int icon_id, final Runnable ok_runnable)
    {
        final Activity act = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ActionDialog(act, title, msg, icon_id, R.string.okay, -1) {
                    @Override
                    public void onOk() {
                        if(ok_runnable != null)
                            ok_runnable.run();
                    }
                };
            }
        });

    }
    protected void warnAndFinish(final Activity act, final int title_id, final String message)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ActionDialog(act, act.getString(title_id), message, R.drawable.warning, R.string.okay, -1) {
                    @Override
                    public void onOk() {
                        finish();
                    }
                };
            }
        });
    }
    protected boolean warnDialog(final int title_id, final String msg, final int et_id) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                warnDialogCore(title_id, msg, et_id);
            }
        });
        return true;
    }
    protected boolean warnDialog(final int title_id, final int msg_id, final int et_id) {
        return warnDialog(title_id, getString(msg_id), et_id);
    }
    protected void warnDialogCore(int title_id, String msg, int et_id) {
        Utils._assertUIThread(true, "warnDialog() needs UIThread()");
        final EditText et = (EditText)findViewById(et_id);
        String title = "";
        if(title_id > 0) {
            title = getString(title_id);
        }
        if(et != null) {
            new ActionDialog(this, title, msg,
                    R.drawable.warning, id_back, -1) {
                @Override
                public void onOk() {
                    et.postDelayed(new Runnable() {
                        public void run() {
                            et.requestFocusFromTouch();
                            Utils.showSoftInput(BaseActivity.this, et);
                        }
                    }, 300);
                }
            };
        } else {
            Utils.dialogOK(this, title, msg, R.drawable.warning, getString(id_back));
        }
    }
/*
    protected void okayDialog(int id_layout) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(id_layout);

        TextView tvOkay = dialog.findViewById(R.id.tv_okay);
        tvOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
*/
    protected boolean invalidLength(int[] fields, int[] titles, int[] max_len, int[] max_message)
    {
        Utils._assert(fields.length == titles.length, "length should be equal");
        for(int i = 0; i < fields.length; i++) {
            EditText et = (EditText)findViewById(fields[i]);
            String str = et.getText().toString();
            if(Utils.isNull(str)) {
                warnDialog(titles[i], R.string.empty_field, fields[i]);
                return true;
            }
            if(str.length() > max_len[i]) {
                warnDialog(titles[i], max_message[i], fields[i]);
                return true;
            }
        }
        return false;
    }
    protected boolean invalidLengthTw(int[] fields, int[] titles, int max_len)
    {
        Utils._assert(fields.length == titles.length, "length should be equal");
        for(int i = 0; i < fields.length; i++) {
            EditText et = (EditText)findViewById(fields[i]);
            String str = et.getText().toString();
            if(Utils.isNull(str)) {
                warnDialog(titles[i], R.string.tw_empty_field, fields[i]);
                return true;
            }
            if(str.length() > max_len) {
                warnDialog(titles[i], R.string.tw_exceed_max_len, fields[i]);
                return true;
            }
        }
        return false;
    }
    protected void restartApp() {
        Intent intent = new Intent(this, PortalActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(intent);
    }
    public String savePngFile(Bitmap myBitmap)
    {
        File dir;
        dir = Environment.getDataDirectory();
        dir = Environment.getDownloadCacheDirectory();
        dir = Environment.getExternalStorageDirectory();
//        String dir = Environment.getExternalStorageDirectory() + "/" + GlobalStatic.appId;
        String file_name = dir + "/" + System.currentTimeMillis() + ".jpg";
        FileOutputStream out = null;
        try {
            File cache = getCacheDir();
            File.createTempFile("new.jpg", null, cache);
            if(!dir.exists()) {
                dir.mkdir();
            }
            out = new FileOutputStream(file_name);
            myBitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp has your Bitmap instance
            // PNG has a lossless format, the compression factor (100) has ignored
            if (out != null) {
                out.close();
            }
            return file_name;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    protected void setEditText(final String text, final int et_id)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                EditText et = findViewById(et_id);
                if(et == null) {
                    return;
                }
                et.setText(text);
            }
        });
    }
    protected void setTextView(int tv_id, String text)
    {
        TextView tv = findViewById(tv_id);
        if(tv == null) {
            return;
        }
        tv.setText(text);
    }
    protected void setTextView(Dialog dialog, int tv_id, String text)
    {
        TextView tv = dialog.findViewById(tv_id);
        if(tv == null) {
            return;
        }
        tv.setText(text);
    }
    protected void setTextView(int tv_id, Cursor c, String db_token)
    {
        TextView tv = findViewById(tv_id);
        if(tv == null) {
            return;
        }
        tv.setText(DbRemit.getString(c, db_token));
    }
/*
    protected void buildVnexBanner(Cursor c, boolean showPayButton)
    {
        setTextView(R.id.tv_vnex_payee_name, c, DbOrder.RECIPINENT_NAME);
        String total_amount = PayInfo.normAmount(DbRemit.getString(c, DbOrder.TOTAL_AMOUNT));
        setTextView(R.id.tv_vnex_amount, total_amount);
        if(showPayButton) {
            findViewById(R.id.bt_pay_vnex).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.bt_pay_vnex).setVisibility(View.GONE);
        }
    }
*/
    protected void buildSevenBanner(Cursor c, boolean showPayButton)
    {
        setTextView(R.id.tv_seven_payee_name, c, DbOrder.RECIPINENT_NAME);
        String total_amount = PayInfo.normAmount(DbRemit.getString(c, DbOrder.TOTAL_AMOUNT));
        setTextView(R.id.tv_seven_amount, total_amount);
        if(showPayButton) {
            findViewById(R.id.bt_pay_seven).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.bt_pay_seven).setVisibility(View.GONE);
        }
    }
    public void captureImage(final ImageCapture imageCapture, String title)
    {

        final CharSequence[] items = {
                getString(R.string.camera_photo),
                getString(R.string.gallery_photo),
                getString(R.string.cancel)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch(item) {
                    case 0:
                        imageCapture.captureImage();
                        break;
                    case 1:
                        imageCapture.pickImage();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        builder.show();
    }
    public void useCamera(final ImageCapture imageCapture)
    {

        final CharSequence[] items = {
                getString(R.string.camera_photo),
                getString(R.string.cancel)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch(item) {
                    case 0:
                        imageCapture.captureImage();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        builder.show();
    }
    protected void startLoading()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                llLoading = (LinearLayout)findViewById(R.id.ll_loading);
                llLoading.setVisibility(View.VISIBLE);
            }
        });
    }
    protected void endLoading()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(llLoading != null) {
                    llLoading.setVisibility(View.GONE);
                }
                llLoading = null;
            }
        });
    }
    protected void orderUnderReview(int _msg_id)
    {
        final Activity act = this;
        final int msg_id = (_msg_id < 0) ? R.string.under_review : _msg_id;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ActionDialog(act, R.string.main_item1, msg_id, R.drawable.info,
                        R.string.okay, -1) {
                    @Override
                    public void onOk() {
                        super.onOk();
                        restartApp();
                    }
                };
            }
        });
    }
    public static void enableViewGroup(ViewGroup viewGroup, boolean enabled)
    {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            view.setFocusable(enabled);
            if (view instanceof ViewGroup) {
                enableViewGroup((ViewGroup) view, enabled);
            }
        }
    }
    protected AlertDialog showWaitDialog(int msg_id)
    {
        Utils._assertUIThread(true, "showWaitDialog()");
        final View dlgView = View.inflate(this, R.layout.dialog_wait, null);
        if(msg_id > 0) {
            TextView tvRegistering = (TextView)dlgView.findViewById(R.id.tv_sending);
            tvRegistering.setText(getString(msg_id));
        }

        AlertDialog.Builder db = new AlertDialog.Builder(this);
        db.setView(dlgView);

        AlertDialog dlg = db.create();
        dlg.show();

        return dlg;
    }
    protected void inviteFriend()
    {
        String invite_title = "Xin chào bạn,\n";
        String invite_body = "\n" +
                "Vui lòng tải ứng dụng VNEX App để dễ dàng và tiện lợi hơn khi chuyển tiền về Việt Nam.\n" +
                "\n" +
                "Mã chia sẻ của tôi: " + DB.getPromoteNo() + "\n" +
                "https://play.google.com/store/apps/details?id=tw.sayhi.vnex\n";
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
//        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.invite_title));
        intent.putExtra(Intent.EXTRA_SUBJECT, invite_title);
//        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.invite_body));
        intent.putExtra(Intent.EXTRA_TEXT, invite_body);
        startActivity(Intent.createChooser(intent, getString(R.string.share_to)));
    }
    public boolean permissionAllowed(String permission)
    {
        boolean b = (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED);
        return b;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        for(int i = 0; i < permissions.length; i++) {
            if(grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                finish();
                return;
            }
        }
    }
    public boolean requestPermission(String permission) {
        boolean granted = permissionAllowed(permission);
        if(!granted) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE
            }, 999);
            // true – if the app has requested this permission previously,
            // and the user denied it without selecting the ‘Never ask me again’ checkbox
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
            }
        }
        return granted;
    }
    protected ProgressDialog showProgressDialog(int title_id, int msg_id) {
        Utils._assertUIThread(true, "showProgressDialog()");
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle(getString(title_id)); // Setting Title
        dialog.setMessage(getString(msg_id)); // Setting Message
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        if(!isFinishing()) {
            dialog.show(); // Display Progress Dialog
        }
        return dialog;
    }
    protected void startApi(final int title_id, final int msg_id)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = showProgressDialog(title_id, msg_id);
            }
        });
    }
    protected void endApi()
    {
        Utils._assert(progressDialog != null, "Should call startLoading() first!!!");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }
    public void nextActivity(Class <?> cls, String... params)
    {
        Utils._assert(params.length % 2 == 0, "Even number of strings required in nextActivity().");
        Intent intent = new Intent(this, cls);
        for(int i = 0; i < params.length; i += 2) {
            intent.putExtra(params[i], params[i + 1]);
        }
        startActivity(intent);
    }
    protected void restartClass(Class<?> cls)
    {
        finishAffinity();
        nextActivity(cls);
    }
    protected void onApiFail(RespBase resp, Runnable runnable)
    {
        warnDialog(R.string.app_name, R.string.network_problem, -1);
        /*
        if(resp.time_out) {
            Utils.waitFor(DB.latchInternet, 2000);
            int msg_id;
            if(DB.bInternet) {
                msg_id = R.string.system_busy;
            } else {
                msg_id = R.string.internet_na;
            }
            warnDialog(R.string.app_name_capital, msg_id, -1);
        } else {
            if(runnable != null) {
                runOnUiThread(runnable);
            } else {
                warnDialog(R.string.app_name_capital, R.string.system_busy, -1);
            }
        } */
    }
    protected boolean isLogin()
    {
        if(!Utils.isNull(mLoginArcNo)) {
            DB.prepareSender(mLoginArcNo);
            return true;
        }
        return false;
    }
    protected boolean isLocationGranted(Activity act)
    {
        boolean granted = (ContextCompat.checkSelfPermission(act, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
        return granted;
    }
    public static int CHANNEL_ID = 1;
    public static void createNotificationApi26(Context context, String title, String message,
                                               PendingIntent pendingIntent) {
        String NOTIFICATION_CHANNEL_ID = "123";
        String NOTIFICATION_CHANNEL_NAME = "VNEX Channel";
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);

            notificationManager.notify((int) (System.currentTimeMillis() / 1000), mBuilder.build());
            /*
            StatusBarNotification[] notifies = notificationManager.getActiveNotifications();
            if (notifies != null && notifies.length > 0) {
                MyLog.e("Notify: " + message);
            } else {
                MyLog.e("Notify: empty.");
            } */
        }
    }
    /*
    public static void notifyQueryProfileApi26(Context context, String title, String message,
                                          Class<?> cls, String sender_arc)
    {

        int NOTIFICATION_ID = 234;
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String CHANNEL_ID = "my_channel_01";
            CharSequence name = "my_channel";
            String Description = "This is my channel";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message);

        Intent resultIntent = new Intent(context, PortalActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(PortalActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    } */
    public static void createNotification(Context context, String title, String message, PendingIntent pendingIntent)
    {

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationApi26(context, title, message, pendingIntent);
            return;
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(CHANNEL_ID, mBuilder.build());

        /*
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Notification Channel";
            String description = "Review status or order paid.";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("CHANNEL ID", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            notificationManager.notify(CHANNEL_ID, mBuilder.build());
        } else {
// notificationId is a unique int for each notification that you must define
        } */
/* todo
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManagerCompat.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, title, importance);
            channel.setDescription(description);
            // Register the channel with the system
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.createNotificationChannel(channel);
        } */
    }
    protected void cancelNotification()
    {
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }
    boolean onResumeCalled = false;
    @Override
    protected void onResume() {
        super.onResume();

        mClass = getClass();

        isInBackground = false;

        if(!onResumeCalled) {
            buildBack();
            if(mIsNarrowScreen) { // mIsNarrowScreen is set at PortalActivity
                scaleTextSize(R.id.root, 0.8f);
            }
            onResumeCalled = true;
        }
        if(DB.isLogin())
            buildQueryTimer();

        if(mViewPager != null) {
//            mBannerAdapter.notifyDataSetChanged(); todo
            updateBanner();
            buildTimer();
        }
    }
    protected void onOrderStatusChanged() {
        cancelNotification();
    }
    protected void onQueryOrderStatus()
    {
        String euso_order = QueryApiService.getOrderStatusChanged();
        if(Utils.isNull(euso_order))
            return;
        String message = QueryApiService.getPref(euso_order + QueryApiService.NOTIFICATION_MESSAGE);
        actionDialog(getString(R.string.app_name), message, R.drawable.info, new Runnable() {
            @Override
            public void run() {
                onOrderStatusChanged();
            }
        });
    }
    protected void onProfileVerified() {
        cancelNotification();
    }
    protected void onProfileFailed() {
        cancelNotification();
    }
    protected void onQueryReviewStatus()
    {
        if(!DB.isLogin())
            return;
        String sender_arc = DB.getSenderArc();
        String status = QueryApiService.getPref(sender_arc + DbSender.STATUS);
        if(Utils.isNull(status)) {
            return;
        }
        QueryApiService.setPref(sender_arc + DbSender.STATUS, "");
        String title = "Arc No. " + sender_arc;
        switch(status) {
            case DbSender.SENDER_SUCCESS:
                actionDialog(title, getString(R.string.notify_profile_verified), R.drawable.info, new Runnable() {
                    @Override
                    public void run() {
                        onProfileVerified();
                    }
                });
                break;
            case DbSender.SENDER_FAIL:
                actionDialog(title, getString(R.string.review_fail), R.drawable.warning, new Runnable() {
                    @Override
                    public void run() {
                        onProfileFailed();
                    }
                });
                break;
            default:
                break;
        }
    }
    protected void buildQueryTimer() {
        if(queryTimer != null)
            queryTimer.cancel();
        queryTimer = new Timer();
        queryTimerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onQueryReviewStatus();
                        onQueryOrderStatus();
                    }
                });
            }
        };
        queryTimer.scheduleAtFixedRate(queryTimerTask, 0, 3 * 1000);
    }
    /*
    protected void buildQueryTimer() {
        if(queryTimer != null)
            queryTimer.cancel();
        queryTimer = new Timer();
        queryTimerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onQueryReviewStatus();
                        onQueryOrderStatus();
                    }
                });
            }
        };
        queryTimer.scheduleAtFixedRate(queryTimerTask, 0, 3 * 1000);
    } */
    /*
    protected void doQueryApi()
    {
        if(!isLogin())
            return;
        long curr_time = System.currentTimeMillis();
        if(curr_time - mLastQueryTime < QueryApiService.QUERY_INTERVAL)
            return;
        mLastQueryTime = System.currentTimeMillis();
        int what = QueryApiService.QUERY_STATUS_AND_ALL_ORDER;
        apiService(what, false,
                new Runnable() {
                    @Override
                    public void run() {
                        onQueryProfile();
                        onQueryOrder();
                    }
                }, QueryApiService.NOTIFY_USER);
    } */
    protected void apiService(final int what, final boolean showProgress, final Runnable runnable,
                              final String... params)
    {
        if(mMessenger == null || !DB.isLogin())
            return;
        if(showProgress)
            startApi(R.string.app_name, R.string.please_wait);
        new Thread() {
            @Override
            public void run() {
                try {
                    Message message = new Message();
                    message.what = what;
                    Bundle bundle = new Bundle();
                    bundle.putString(DbSender.ARC_NO, DB.getSenderArc());
                    bundle.putString(DbSender.UUID, DB.getUuid());
                    for(int i = 0; i < params.length; i++) {
                        bundle.putString(params[i], "1");
                    }
                    message.setData(bundle);
                    message.replyTo = new Messenger(new Handler(getMainLooper()){
                        @Override
                        public void handleMessage(Message msg) {
                            if(showProgress)
                                endApi();
                            mMessageBundle = msg.getData();
                            if(runnable != null) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        runnable.run();
                                    }
                                });
                            }
                        }
                    });
                    mMessenger.send(message);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
    protected void onOrderPaid() {}
    protected void buildServiceConnection()
    {
        if(mMessenger != null)
            return;
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mMessenger = new Messenger(service);
                MyLog.e("onServiceConnected()");
            }
            @Override
            public void onServiceDisconnected(ComponentName name) {
                mMessenger = null;
            }
        };
        Intent intent = new Intent();
        intent.setAction("service.query.api");
        intent.setPackage("tw.sayhi.vnex");
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }
    protected boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    protected String getNormViewText(int id)
    {
        String text = getViewText(id);
        return PayInfo.normVietText(text);
    }
    protected String getViewText(int id)
    {
        View vi = findViewById(id);
        if(vi == null) {
            return "";
        }
        String text = "";
        if(vi instanceof EditText) {
            text = ((EditText)vi).getText().toString();
        } else if(vi instanceof  TextView) {
            text = ((TextView)vi).getText().toString();
        }
        return text;
    }
    protected void onLogout()
    {
        new ActionDialog(this, R.string.app_name, R.string.logout_popup,
                R.drawable.warning, R.string.okay, R.string.cancel) {
            @Override
            public void onOk() {
                onLogoutSucceed();
            }
        };
    }
    protected void onLogoutSucceed()
    {
        new ActionDialog(this, -1, R.string.logout_succeed,
                -1, R.string.okay, -1) {
            @Override
            public void onOk() {
                DB.clearSender();
                mLoginArcNo = "";
                QueryApiService.logout();
                restartClass(PortalActivity.class);
            }
        };
    }
    // Date Format: YYYY/MM/DD
    protected boolean verifyDate(String date, boolean verifyCurrYear)
    {
        int[] nums = new int[3];
        if(!Utils.getYearMonthDay(date, nums))
            return false;
        int curr_year = Calendar.getInstance().get(Calendar.YEAR);
        if(verifyCurrYear && (nums[0] < 1900 || nums[0] >= curr_year))
            return false;
        if(nums[1] < 1 || nums[1] > 12)
            return false;
        if(nums[2] < 1 || nums[2] > 31)
            return false;
        return true;
    }
    protected int getFullHeight(ViewGroup layout)
    {
        int specWidth = View.MeasureSpec.makeMeasureSpec(0 /* any */, View.MeasureSpec.UNSPECIFIED);
        int specHeight = View.MeasureSpec.makeMeasureSpec(0 /* any */, View.MeasureSpec.UNSPECIFIED);


        layout.measure(specWidth,specHeight);
        int totalHeight = 0;//layout.getMeasuredHeight();
        int numberOfChildren = layout.getChildCount();
        for(int i = 0;i<numberOfChildren;i++) {
            View child = layout.getChildAt(i);
            if(child instanceof ViewGroup) {
                totalHeight+=getFullHeight((ViewGroup)child);
            }else {
                int desiredWidth = View.MeasureSpec.makeMeasureSpec(layout.getWidth(),
                        View.MeasureSpec.AT_MOST);
                child.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight+=child.getMeasuredHeight();
            }

        }
        return totalHeight;
    }
    protected void setAllTextSize(ViewGroup v, float size) {
        for (int i = 0; i < v.getChildCount(); i++) {
            Object child = v.getChildAt(i);
            if (child instanceof TextView) {
                TextView tv = (TextView) child;
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
            } else if(child instanceof ViewGroup) {
                setAllTextSize((ViewGroup)child, size);
            }
        }
    }
    protected void scaleTextSize(int id_vg, float scale)
    {
        if(isTextScaled) {
            return;
        }
        ViewGroup vg = findViewById(id_vg);
        if(vg != null) {
            scaleTextSize(vg, scale);
            isTextScaled = true;
        }
    }
    public static void scaleTextSize(ViewGroup v, float scale)
    {
        for (int i = 0; i < v.getChildCount(); i++) {
            Object child = v.getChildAt(i);
            if (child instanceof TextView) {
                TextView tv = (TextView) child;
                float textSize = tv.getTextSize();
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize * scale);
            } else if(child instanceof ViewGroup) {
                scaleTextSize((ViewGroup)child, scale);
            }
        }
    }
    protected void setAllImageSize(ViewGroup v, int width, int height) {
        for (int i = 0; i < v.getChildCount(); i++) {
            Object child = v.getChildAt(i);
            if (child instanceof ImageView) {
                ImageView iv = (ImageView) child;
                // getTextSize() will return size in terms of pixels
                ViewGroup.LayoutParams params = iv.getLayoutParams();
                params.width = width;
                params.height = height;
                iv.setLayoutParams(params);
            } else if(child instanceof ViewGroup) {
                setAllImageSize((ViewGroup)child, width, height);
            }
        }
    }
    protected void scaleImageSize(ViewGroup v, float scale) {
        for (int i = 0; i < v.getChildCount(); i++) {
            Object child = v.getChildAt(i);
            if (child instanceof ImageView) {
                scaleImageSize((ImageView) child, scale);
            } else if(child instanceof ViewGroup) {
                scaleImageSize((ViewGroup)child, scale);
            }
        }
    }
    protected void scaleImageSize(int iv_id, float scale)
    {
        View iv = findViewById(iv_id);
        scaleImageSize(iv, scale);
    }
    protected void scaleImageSize(View iv, float scale)
    {
        // getTextSize() will return size in terms of pixels
        ViewGroup.LayoutParams params = iv.getLayoutParams();
//        MyLog.i("width=" + iv.getWidth() + " height=" + iv.getHeight());
        params.width = (int)(scale * iv.getWidth());
        params.height = (int)(scale * iv.getHeight());
        iv.setLayoutParams(params);
    }
    protected boolean isNull(String str)
    {
        return Utils.isNull(str);
    }
    protected boolean nonNull(String str)
    {
        return !Utils.isNull(str);
    }
    protected void datePickerDialog(final TextView tvDate, boolean isBirthday, boolean isViet)
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.date_picker__dialog);

        LinearLayout llRoot = dialog.findViewById(R.id.root);
        final DatePicker datePicker;
        if(mIsNarrowScreen) {
            datePicker = dialog.findViewById(R.id.dp_date_small);
            datePicker.setVisibility(View.VISIBLE);
            dialog.findViewById(R.id.dp_date_medium).setVisibility(View.GONE);
        } else {
            datePicker = dialog.findViewById(R.id.dp_date_medium);
        }
        if(!isViet) {
            TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
            tvCancel.setText("取消");
        }
        String[] months = isViet ? months_vi : months_tw;
        initDatePicker(datePicker, isBirthday, months);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvOkay = dialog.findViewById(R.id.tv_okay);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth() + 1;
                int year = datePicker.getYear();
                String format = "%04d/%02d/%02d";
                String date = String.format(format, year, month, day);
                tvDate.setText(date);
                dialog.dismiss();
            }
        });
        dialog.show();
        llRoot.invalidate();
    }
    protected void setDividerColor(NumberPicker picker, int color)
    {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
    protected void setDatePickerColor(ViewGroup v, int color) {
        for (int i = 0; i < v.getChildCount(); i++) {
            Object child = v.getChildAt(i);
            if (child instanceof NumberPicker) {
                NumberPicker np = (NumberPicker)child;
                setDividerColor(np, color);
            } else if(child instanceof ViewGroup) {
                setDatePickerColor((ViewGroup)child, color);
            }
        }
    }
    protected NumberPicker getNumberPicker(ViewGroup v, int max_value) {
        for (int i = 0; i < v.getChildCount(); i++) {
            Object child = v.getChildAt(i);
            if (child instanceof NumberPicker) {
                NumberPicker np = (NumberPicker)child;
                if(np.getMaxValue() <= max_value)
                    return np;
            } else if(child instanceof ViewGroup) {
                return getNumberPicker((ViewGroup)child, max_value);
            }
        }
        return null;
    }
    protected void initDatePicker(DatePicker dpBirthday, boolean isBirthday, final String[] months)
    {
        setDatePickerColor(dpBirthday, getResources().getColor(R.color.colorAccent));
        final NumberPicker npMonth = getNumberPicker(dpBirthday, 12);
        npMonth.setMinValue(0);
        npMonth.setMaxValue(11);
        npMonth.setDisplayedValues(months);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        if(isBirthday) {
            year -= 18;
            month = 7;
            day = 15;
        }
        dpBirthday.init(2000, 1, 1,
                new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                        npMonth.setDisplayedValues(months);
                    }
                });
        dpBirthday.updateDate(year, month, day);
    }
    protected void startFacebook()
    {
        Utils.openWebPage(this, "https://m.me/VNEX.Taiwan");
    }
    protected void buildTitleMenu()
    {
        ImageView ivLogout = findViewById(R.id.iv_logout);
        ImageView ivMenu  = findViewById(R.id.iv_menu);
        ivLogout.setVisibility(View.VISIBLE);
        ivMenu.setVisibility(View.VISIBLE);
        ivLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onLogout();
                }
            });
        final Activity act = this;
        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMore(act);
            }
        });
    }
    protected void buildHeader(String index, String title)
    {
        TextView tvIndex = findViewById(R.id.tv_header_index);
        tvIndex.setText(index);
        TextView tvTitle = findViewById(R.id.tv_header_title);
        tvTitle.setText(title);
    }
    protected void setOrderSummary(String euso_order)
    {
        Cursor c = DbOrder.read(euso_order);
        String create_time = DbRemit.getString(c, DbOrder.CREATE_TIME);
        long l_create_time = Utils.getTimeStamp(Utils.SDF_DASH, create_time);
        setTextView(R.id.tv_date, Utils.SDF_YYYY_MM_DD.format(l_create_time));
        String payment_no = DbRemit.getString(c, DbOrder.PAYMENT_NO);
        setTextView(R.id.tv_payment_no, euso_order);
        setTextView(R.id.tv_sender_name, DB.getSenderName());
        setTextView(R.id.tv_payee_name, c, DbOrder.RECIPINENT_NAME);
        String trans_type = DbRemit.getString(c, DbOrder.TRANS_TYPE);
        int remit_way = -1;
        switch (trans_type) {
            case PayInfo.DTD: //送到家
                remit_way = R.string.remit_dtd;
                break;
            case PayInfo.BTB: //帳號
                remit_way = R.string.remit_btb;
                break;
            case PayInfo.IDP: //ID取款
                remit_way = R.string.remit_idp;
                break;
            case PayInfo.PIN: //密碼
                remit_way = R.string.remit_pin;
                break;
        }
        setTextView(R.id.tv_remit_way, getString(remit_way));
        String amount_nt = PayInfo.normAmount(DbRemit.getString(c, DbOrder.AMOUNT));
        setTextView(R.id.tv_remit_amount, amount_nt);
        setTextView(R.id.tv_service_fee, c, DbOrder.SERVICE_FEE);
        String discount_fee = DbRemit.getString(c, DbOrder.COUPON_VALUE);
        setTextView(R.id.tv_discount, "-" + discount_fee);
        String red_point_discount = DbRemit.getString(c, DbOrder.RED_POINT_DISCOUNT);
        if(Utils.isNull(red_point_discount) || red_point_discount.equals("0")) {
            red_point_discount = "0";
        } else {
            red_point_discount = "-" + red_point_discount;
        }
        setTextView(R.id.tv_red_point_discount, red_point_discount);
        String total_amount = PayInfo.normAmount(DbRemit.getString(c, DbOrder.TOTAL_AMOUNT));
        setTextView(R.id.tv_total_amount, total_amount);

        String pay = DbRemit.getString(c, DbOrder.PAY);
        if(pay.equals("1")) {
            setTextView(R.id.tv_status, getString(R.string.success));
        } else {
            long expire_time = l_create_time + PayInfo.OrderActiveTime;
            String expire_str = Utils.SDF_0.format(expire_time);
            String expire_desc;
            if (mCurrentLocale.equals("vi")) {
                expire_desc = getString(R.string.not_paid) + "(Trước " + expire_str + ")";

            } else {
                expire_desc = getString(R.string.not_paid) + "(" + expire_str + "前)";
            }
            setTextView(R.id.tv_status, expire_desc);
        }
    }
    ViewPager.SimpleOnPageChangeListener mPageChangeListener =  new ViewPager.SimpleOnPageChangeListener() {
        /*
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//            MyLog.i("onPageScrolled() " + position);
        }
        */
        @Override
        public void onPageSelected(int position) {
//            MyLog.i("page " + position);
            mBannerIndex = position;
        }
/*
        @Override
        public void onPageScrollStateChanged(int state) {
            MyLog.i("onPageScrollStateChanged() " + state);
        }
*/
    };
    void updateBanner() {
//        MyLog.i("updateBanner index=" + mBannerIndex);
        mViewPager.setCurrentItem(mBannerIndex);
        mBannerIndex++;
        if(mBannerIndex >= CloudImages.cloudImages.data.banners.length) {
            mBannerIndex = 0;
        }
    }
    void buildTimer() {
        if(mTimerSlides != null)
            mTimerSlides.cancel();
        mTimerSlides = new Timer();
        mTimerTaskSlides = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateBanner();
                    }
                });
            }
        };
        mTimerSlides.scheduleAtFixedRate(mTimerTaskSlides, mnPagerInterval, mnPagerInterval);
    }
    protected void buildViewPager()
    {
        new Thread() {
            @Override
            public void run() {
                int times = 1;
                while(times++ < 120) {
                    if (CloudImages.latchLoadImageDesc.getCount() < 1) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                buildViewPagerUI();
                            }
                        });
                        break;
                    }
                    Utils.sleep(1000);
                }
            }
        }.start();
    }
    protected void buildViewPagerUI() {
        try {
            mViewPager = findViewById(R.id.view_pager);
            mBannerAdapter = new BannerAdapter(this, CloudImages.cloudImages.data.banners);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mViewPager.setAdapter(mBannerAdapter);
                    mViewPager.addOnPageChangeListener(mPageChangeListener);
                    buildTimer();
                }
            });
        } catch(Exception e) {
            DB.reportException("buildViewPagerUI", e);
        }
    }
    protected void setTreeObserver(final int id_ref, final float scale)
    {
        final ViewGroup vgRoot = findViewById(R.id.root);
        ViewTreeObserver vto = vgRoot.getViewTreeObserver();
        vto.addOnGlobalLayoutListener (new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                vgRoot.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                TextView tvTarget = findViewById(id_ref);
                int line_count = tvTarget.getLineCount();
                if(isNarrowScreen() || line_count > 1) { // 1 line = 71
                    scaleTextSize(vgRoot, scale);
                }
            }
        });
    }
    int[] menu_ids = {
            R.id.ll_points, R.id.ll_barcode, R.id.ll_info, R.id.ll_facebook, R.id.ll_privacy
    };
    public static void startUrl(Activity act, String url)
    {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            act.startActivity(intent);
        } catch(ActivityNotFoundException e) {
            Utils.postMessage(act, "請安裝網路流覽器");
        }
    }
    protected void onMore(Activity act)
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.more__dialog);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        if(mIsNarrowScreen) {
            scaleTextSize(dialog.findViewById(R.id.root), 0.8f);
        }
//        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        wlp.x = 10;
        wlp.y = 100;
        window.setAttributes(wlp);

        if(isChinese) {
            setTextView(dialog, R.id.tv_points, "點數");
            setTextView(dialog, R.id.tv_barcode, "會員編號");
            setTextView(dialog, R.id.tv_info, "資料");
            setTextView(dialog, R.id.tv_facebook, "協助");
            setTextView(dialog, R.id.tv_privacy, "隱私權政策");
        }
        for(int id : menu_ids) {
            View vi = dialog.findViewById(id);
            vi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.ll_points:
                            onMenuPoints();
                            break;
                        case R.id.ll_barcode:
                            onMenuBarcode();
                            break;
                        case R.id.ll_info:
                            onMenuInfo();
                            break;
                        case R.id.ll_facebook:
                            onMenuFacebook();
                            break;
                        case R.id.ll_privacy:
                            startUrl(act, "https://www.eec-vnex.com.tw/chinh-sach-quyen-rieng-tu.php");
                            /*
                            isChinese = !isChinese;
                            if(isChinese) {
                                setTw();
                            } else {
                                setVi();
                            } */
                            break;
                    }
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }
    protected void onMenuPoints()
    {
        if(!DB.isLogin()) {
            nextActivity(LoginActivity.class);
        } else {
            nextActivity(CashPointsNewActivity.class);
        }
    }
    protected void onMenuInfo()
    {
        nextActivity(InfoActivity.class);
    }
    protected void onMenuFacebook()
    {
        startFacebook();
    }
    protected void setVi()
    {
        id_back = R.string.back;
    }
    protected void setTw()
    {
        id_back = R.string.tw_back;
    }
    protected void onMenuBarcode()
    {
        if(!DB.isLogin()) {
            nextActivity(LoginActivity.class);
            return;
        }
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.barcode__dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView ivPromoteNo = dialog.findViewById(R.id.iv_promote_no);
        BarCode.buildBarCode(DB.getPromoteNo(), ivPromoteNo, 1080, Config.barcode_height + 50);
        TextView tvPromoteNo = dialog.findViewById(R.id.tv_promote_no);
        tvPromoteNo.setText(DB.getPromoteNo());

        if(isChinese) {
            setTextView(dialog, R.id.tv_barcode_title, "您的會員編號");
            setTextView(dialog, R.id.tv_barcode_note, "請提供此條碼給工作人員");
        }

        dialog.findViewById(R.id.tv_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    } /*
    protected Bitmap encodeAsBitmap(String str, int width, int height) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, width, height, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }

        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
        return bitmap;
    } */
    protected void generateQrCode(ImageView iv, String str, int width, int height)
    { /*
        try {
            Bitmap bitmap = encodeAsBitmap(str, width, height);
            iv.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        } */
    }
    protected void debugDialog(final Activity act)
    {
        final CharSequence[] items = {
                "審核通過",
                "審核失敗",
                "訂單已付款",
                "重新註冊",
                "修改資料",
                "刪除資料",
                "交易成功",
                "交易失敗",
                "訂單過期"
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("選擇執行模式");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                Cursor c = DbOrder.readBy(DbOrder.UUID, DB.getUuid(), "");
                String euso_order = "";
                if(!DbRemit.isNull(c)) {
                    c.moveToFirst();
                    euso_order = DbRemit.getString(c, DbOrder.EUSO_ORDER);
                    DbRemit.close(c);
                }
                switch(item) {
                    case 0:
                        QueryApiService.setFakeStatus(DbSender.SENDER_SUCCESS);
                        Utils.postMessage(act, DB.getSenderArc() + " is SUCCESS.");
                        break;
                    case 1:
                        QueryApiService.setFakeStatus(DbSender.SENDER_FAIL);
                        Utils.postMessage(act, DB.getSenderArc() + " is FAIL.");
                        break;
                    case 2:
                        QueryApiService.setPref(euso_order, DbOrder.ORDER_PAID);
                        Utils.postMessage(act, euso_order + " is ORDER_PAID.");
                        break;
                    case 3:
                        QueryApiService.setFakeStatus(DbSender.SENDER_REGISTERED);
                        Utils.postMessage(act, DB.getSenderArc() + " is REGISTER.");
                        break;
                    case 4:
                        QueryApiService.setFakeStatus(DbSender.SENDER_EDITED);
                        Utils.postMessage(act, DB.getSenderArc() + " is EDITED.");
                        break;
                    case 5:
                        QueryApiService.setFakeStatus(DbSender.SENDER_DELETE);
                        Utils.postMessage(act, DB.getSenderArc() + " is DELETE.");
                        break;
                    case 6:
                        QueryApiService.setPref(euso_order, DbOrder.TRANS_SUCCESS);
                        Utils.postMessage(act, euso_order + " is TRANS_SUCCESS.");
                        break;
                    case 7:
                        QueryApiService.setPref(euso_order, DbOrder.TRANS_FAIL);
                        Utils.postMessage(act, euso_order + " is TRANS_FAIL.");
                        break;
                    case 8:
                        QueryApiService.setPref(euso_order, DbOrder.ORDER_EXPIRED);
                        Utils.postMessage(act, euso_order + " is ORDER_EXPIRED.");
                        break;
                }
            }
        });
        builder.show();
    }
}
