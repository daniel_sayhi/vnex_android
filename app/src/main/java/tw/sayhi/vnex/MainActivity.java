package tw.sayhi.vnex;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.Calendar;
import java.util.concurrent.CountDownLatch;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import tw.sayhi.vnex.activity.CashPointsNewActivity;
import tw.sayhi.vnex.activity.HistoryActivity;
import tw.sayhi.vnex.activity.ExchangeRateActivity;
import tw.sayhi.vnex.activity.LoginActivity;
import tw.sayhi.vnex.activity.PayWayActivity;
import tw.sayhi.vnex.misc.CloudImages;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.shared.Utils;
import tw.sayhi.vnex.activity.OrderDetailActivity;
import tw.sayhi.vnex.activity.SenderActivity;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.misc.DbRemit;
import tw.sayhi.vnex.misc.Network;
import tw.sayhi.vnex.misc.OrderData;
import tw.sayhi.vnex.widget.ActionDialog;

public class MainActivity extends BaseActivity
{
    Activity mActivity = this;
    private static final String TAG = MainActivity.class.getSimpleName();
    int[] items = {
            R.id.tv_pay_seven,
            R.id.item1, R.id.item2, R.id.item3, R.id.item4
    };
    int[] iv_items = { R.id.iv_item1, R.id.iv_item2, R.id.iv_item3, R.id.iv_item4 };

    String banner_euso_order;
    boolean needUpdate = false;

    public static boolean isStopped = false;
    int officeTimerInterval = 5 * 60 * 1000; // 3 minutes
    int offOfficeTimerInterval = 15 * 60 * 1000; // 10 minutes
    int currTimerInterval;
    boolean isWideScreen = false;
    ViewGroup rlRoot;
    LinearLayout llBanner, llPayBanner;
    ImageView ivLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        MyLog.timeStart(TAG);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main__activity);

//        Utils.postMessage(null, "");

        if(BuildConfig.DEBUG) {
            Utils.postMessage(mActivity, "LOGIN_ARC_NO = " + mLoginArcNo);
        } else if(DebugOptions.bUseTestURL) {
            Utils.postMessage(mActivity, "Using VNEX test environment...");
        } else if(DebugOptions.isEnabled()) {
            Utils.postMessage(mActivity, "Debug version is turned on.");
        }
        if(BuildConfig.DEBUG) {
            officeTimerInterval = 30 * 1000;
            offOfficeTimerInterval = 30 * 1000;
        }

        trackScreenName(mTitle);

        buildViews();

        if(isOfficeHour()) {
            currTimerInterval = officeTimerInterval;
        } else {
            currTimerInterval = offOfficeTimerInterval;
        }
        CountDownLatch loadLatch;
        if(firstEntry) {
            firstEntry = false;
            loadLatch = CloudImages.latchLoadImageDesc;
        } else {
            loadLatch = new CountDownLatch(1);
            CloudImages.loadImageDesc(mActivity, loadLatch);
        }
        checkAppUpdate(loadLatch);
    }
    boolean firstResume = true;
    @Override
    protected void onResume()
    {
        super.onResume();
        isStopped = false;
        if(Utils.isNull(mLoginArcNo)) {
            ivLogout.setImageDrawable(getResources().getDrawable(R.drawable.icon_login));
        }
        buildBanner();
        if(!firstResume && CloudImages.latchLoadImageDesc.getCount() < 1 && needUpdate && CloudImages.cloudImages.force_update) {
            Network.updateApp(mActivity, true, null);
        }
        firstResume = false;
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        isStopped = true;
        mWebViewURL = null;
        mWebViewHTML = null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        restartClass(PortalActivity.class);
    }

    @Override
    protected void onLogout() {
        if(nonNull(mLoginArcNo)) {
            super.onLogout();
        } else {
            sMode = MODE_PORTAL;
            nextActivity(LoginActivity.class);
        }
    }
    void buildListeners(int id) {
        Class<?> cls = MainActivity.class;
        String prev_page = getString(R.string.main_page);
        int title_id = 0;
        Intent intent;
        switch (id) {
            case R.id.item1: // 代收薪資結匯
                title_id = R.string.main_item1;
                cls = ExchangeRateActivity.class;
                break;
            case R.id.item2: // 歷史紀錄
                title_id = R.string.main_item2;
                if(isLogin()) {
                    cls = HistoryActivity.class;
                } else {
                    cls = LoginActivity.class;
                    sMode = MODE_HISTORY;
                }
                break;
            case R.id.item3: // 個人資訊
                title_id = R.string.main_item3_n;
                if(isLogin()) {
                    nextActivity(SenderActivity.class);
                    return;
                } else {
                    cls = LoginActivity.class;
                    sMode = MODE_PROFILE;
                }
                break;
            case R.id.item4: // 好康
/*
                Utils.openWebPage(MainActivity.this,
                        "http://www.eec-elite.com/portal_b1.php?owner_num=b1_16434&button_num=b1");
*/
                title_id = R.string.main_item4;
                if(isLogin()) {
                    nextActivity(CashPointsNewActivity.class);
                    return;
                } else {
                    cls = LoginActivity.class;
                    sMode = MODE_CASH_POINTS;
                }
                break;
                /*
            case R.id.item5: // 分享好友
//                Utils.openWebPage(MainActivity.this,
//                        "http://www.eec-elite.com/index.php?website_language_id=12");
                title_id = R.string.main_item5;
                inviteFriend();
                return; */
            case R.id.tv_pay_seven:
                nextActivity(PayWayActivity.class, DbOrder.EUSO_ORDER, banner_euso_order);
                return;
        }
        String title = getString(title_id).replace("\n", "");
        start(MainActivity.this, cls, prev_page, title);
    }
    void buildViews()
    {
        if(DB.isLogin()) {
            buildTitleMenu();
        }
        rlRoot = findViewById(R.id.root);
        ivLogout = findViewById(R.id.iv_logout);
        llBanner = findViewById(R.id.ll_banner);
        llPayBanner = findViewById(R.id.ll_pay_banner);
        /*
        if(mIsNarrowScreen) {
            ViewGroup llIcons = findViewById(R.id.ll_icons);
            scaleTextSize(llIcons, 0.8f);
        } */
        buildViewPager();
//        adjustBannerHeight();
        setTreeObserver();
        for (int item : items) {
            View view = findViewById(item);
            if(view == null)
                continue;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buildListeners(view.getId());
                }
            });
        }
        findViewById(R.id.iv_vnex_logo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(BuildConfig.DEBUG) {
                    debugDialog(mActivity);
                }
            }
        });
    }
    void showPromoBanner()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                llPayBanner.setVisibility(View.GONE);
                llBanner.setVisibility(View.VISIBLE);
            }
        });
    }
    void buildBanner()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                buildBannerUI();
            }
        });
    }
    void buildBannerUI()
    {
        // readAll in ascending order, old first
//        MyLog.i("buildBanner()");
        showPromoBanner();
        if(Utils.isNull(mLoginArcNo))
            return;
        Cursor c = DbOrder.readBy(DbOrder.ACTIVE, "1", DbOrder.CREATE_TIME);
        if(DbRemit.isNull(c)) {
            DbRemit.close(c);
            return;
        }
        // find the oldest order with status=1 and pay=0
        int which = -1;
        OrderData os = new OrderData();
        for(int i = 0; i < c.getCount(); i++) {
            c.moveToPosition(i);
            os.setData(c);
            if(os.sender_arc.equals(mLoginArcNo) &&
                    os.status.equals(DbOrder.ORDER_SUCCESS) && os.pay.equals("0") && os.upload_receipt.equals("0")) {
                which = i;
                break;
            }
        }
        if(which < 0) {
            DbRemit.close(c);
            return;
        }
        c.moveToPosition(which);
        banner_euso_order = os.euso_order;
        if(os.notified.equals(DbOrder.NOTIFY_PENDING)) {
            remitOrderAllowed(os.euso_order);
            DbRemit.close(c);
            return;
        }
        llPayBanner.setVisibility(View.VISIBLE);
        DbRemit.close(c);
    }
/*
    void buildTimer(final int delay) {
        new Thread() {
            @Override
            public void run() {
                if(delay > 0) {
                    Utils.sleep(delay);
                }
                while(true) {
                    queryRemitOrder();
                    if (isOfficeHour()) {
                        currTimerInterval = officeTimerInterval;
                    } else {
                        currTimerInterval = offOfficeTimerInterval;
                    }
                    Utils.sleep(currTimerInterval);
                }
            }
        }.start();
    }
*/
/*
    void queryRemitOrder()
    {
        String curr_euso = Pref.get(DbOrder.EUSO_ORDER);
        final Cursor c = DbOrder.readColumn(DbOrder.ACTIVE, "1", DbOrder.CREATE_TIME);
        if (DbRemit.isNull(c)) {
            MyLog.i("no active order");
            DbRemit.close(c);
            showPromoBanner();
            return;
        }
        MyLog.i("active order = " + c.getCount());
        final OrderStatus os = new OrderStatus();
        for(int i = 0; i < c.getCount(); i++) {
            c.moveToPosition(i);
            os.setData(c);
            if(os.pay.equals("1")) {
                continue;
            }
            long curr_time = System.currentTimeMillis();
            if(curr_time - os.l_create_time > PayInfo.OrderActiveTime) { // 6 hours
                DbOrder.update(os.euso_order, DbOrder.STATUS, DbOrder.ORDER_EXPIRED);
                DbOrder.update(os.euso_order, DbOrder.ACTIVE, "0");
                if(os.euso_order.equals(curr_euso)) {
                    Pref.set(DbOrder.STATUS, DbOrder.ORDER_FAIL);
                }
                remitOrderDialog(R.string.arc_expired, R.drawable.warning, R.string.description,
                        RemitOrderActivity.class, os.euso_order);
                DbRemit.close(c);
                return;
            }
            RespQueryRemitOrder query = DB.queryRemitOrder(mActivity, os);
            if(query != null && query.success() && !Utils.isNull(query.status) && !Utils.isNull(query.pay)) {
                DbOrder.update(query);
                if(!query.status.equals(os.status) || !query.pay.equals(os.pay)) {
                    DbOrder.update(os.euso_order, DbOrder.NOTIFIED, DbOrder.NOTIFY_PENDING);
                }
                MyLog.i("queryRemitOrder() status=" + query.status + " pay=" + query.pay);
            }
        }
        for(int i = 0; i < c.getCount(); i++) {
            c.moveToPosition(i);
            os.setData(c);
            if(os.active.equals("0") || os.notified.equals(DbOrder.NOTIFY_NONE)) {
                continue;
            }
            if(os.euso_order.equals(curr_euso)) {
                Pref.set(DbOrder.STATUS, os.status);
            }
            switch(os.status) {
                case DbOrder.ORDER_FAIL:
                    DbOrder.update(os.euso_order, DbOrder.ACTIVE, "0");
                    remitOrderDialog(R.string.order_error, R.drawable.warning, R.string.description,
                            RemitOrderActivity.class, os.euso_order);
                    break;
                case DbOrder.ORDER_SUCCESS:
                    if(os.pay.equals("0")) {
                        remitOrderAllowed(os.euso_order);
                        sendAllowNotification(getString(R.string.main_item1), getString(R.string.remit_allowed));
                    } else {
                        remitOrderPaid(os.euso_order);
                    }
                    break;
            }
            break;
        }
        DbRemit.close(c);
    }
*/
/*
    void forceAllowRemitOrder()
    {
        new Thread() {
            @Override
            public void run() {
                if(DB.allowRemitOrder(mActivity, Pref.get(DbOrder.EUSO_ORDER), Pref.get(DbOrder.UUID))) {
                    Pref.set(DbOrder.STATUS, DbOrder.ORDER_SUCCESS);
                    queryRemitOrder();
                }
            }
        }.start();
    }
*/
    void remitOrderDialog(final int msg_id, final int icon_id, final int button_id,
                                        final Class<?> cls, final String euso_order)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isFinishing()) {
                    return;
                }
                DbOrder.update(euso_order, DbOrder.NOTIFIED, DbOrder.NOTIFY_NONE);
                new ActionDialog(mActivity, R.string.main_item1, msg_id, icon_id, button_id, -1) {
                    @Override
                    public void onOk() {
                        super.onOk();
                        nextActivity(cls, DbOrder.EUSO_ORDER, euso_order);
                    }
                };
            }
        });
    }
    void sendAllowNotification(String textTitle, String textContent)
    {
// Create an explicit intent for an Activity in your app
        Intent intent = new Intent(this, PayWayActivity.class);
        intent.putExtra(DbOrder.EUSO_ORDER, Pref.get(DbOrder.EUSO_ORDER));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(textTitle)
                .setContentText(textContent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

// notificationId has a unique int for each notification that you must define
        notificationManager.notify(1, mBuilder.build());

    }
    void remitOrderAllowed(String euso_order)
    {
//        Pref.set(DbOrder.SENDER_INFO_MODIFY,"0");
        Pref.set(DbOrder.USER_REVIEWED_FLAG, "1");
        remitOrderDialog(R.string.remit_allowed, R.drawable.info, R.string.okay,
                PayWayActivity.class, euso_order);
    }
    void remitOrderPaid(String euso_order)
    {
        DbOrder.update(euso_order, DbOrder.PAY, "1");
        buildBanner();
        sendAllowNotification(getString(R.string.main_item1), getString(R.string.receipt_review_okay));
        remitOrderDialog(R.string.receipt_review_okay, R.drawable.info, R.string.description,
                OrderDetailActivity.class, euso_order);
    }
    boolean isOfficeHour()
    {
        Calendar currDate = Calendar.getInstance();
        int currHour = currDate.get(Calendar.HOUR_OF_DAY);
        if(currHour >= 9 && currHour < 21) {
            return true;
        }
        return false;
    }
    @Override
    protected void onOrderPaid() {
        super.onOrderPaid();
        buildBanner();
    }
    void adjustLayoutHeight()
    {
        int row1Height = findViewById(R.id.ll_row1).getHeight();
        int row2Height = findViewById(R.id.ll_row2).getHeight();
        if(row1Height - row2Height < 4)
            return;

        ImageView ivItem1 = findViewById(R.id.iv_item1);
        int ivHeight = ivItem1.getHeight();
        ivHeight -= (row1Height - row2Height) / 2;
        if(ivHeight < 20)
            return;
        for(int item : iv_items) {
            ImageView iv = findViewById(item);
            iv.requestLayout();
            ViewGroup.LayoutParams params = iv.getLayoutParams();
            params.width = ivHeight;
            params.height = ivHeight;
            iv.setLayoutParams(params);
        }
/*
        Display display = getWindowManager().getDefaultDisplay();
        int screenHeight = display.getHeight();
        Rect rect = new Rect();
        getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int statusBarHeight = rect.top;
        int displayHeight = screenHeight - statusBarHeight;
*/
    }
    void setTreeObserver()
    {
        final LinearLayout llBanner = findViewById(R.id.ll_banner);
        ViewTreeObserver vto = rlRoot.getViewTreeObserver();
        vto.addOnGlobalLayoutListener (new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rlRoot.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                adjustBannerHeight(llBanner, 0.958f);
            }
        });
    }
    void checkAppUpdate(final CountDownLatch latch)
    {
        new Thread() {
            @Override
            public void run() {
                Utils.waitFor(latch);
                needUpdate = Network.needUpdateApp(mActivity, CloudImages.cloudImages.app_version);
                if(needUpdate) {
                    Network.updateApp(mActivity, CloudImages.cloudImages.force_update, null);
                }
            }
        }.start();
    }
}