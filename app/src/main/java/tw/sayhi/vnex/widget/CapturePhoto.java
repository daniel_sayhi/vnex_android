package tw.sayhi.vnex.widget;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import androidx.core.content.FileProvider;

public class CapturePhoto
{
    public static final int CAPTURE_PHOTO_CODE = 777;
    public static final String CAPTURE_PHOTO_TEMP_FILE = "CAPTURE_PHOTO_TEMP_FILE";
    Activity mActivity;
    File mPhotoFile;

    public CapturePhoto(Activity activity)
    {
        mActivity = activity;
    }
    public void startCamera()
    {
        mPhotoFile = createTempImageFile();
        if (mPhotoFile != null) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if ((intent.resolveActivity(mActivity.getPackageManager()) != null)) {
                Uri photoURI = FileProvider.getUriForFile(mActivity,
                        mActivity.getPackageName() + ".profileimage.fileprovider", mPhotoFile);

                // Samsung Galaxy S3 Fix
                List<ResolveInfo> resInfoList = mActivity.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    mActivity.grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }

                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                mActivity.startActivityForResult(intent, CAPTURE_PHOTO_CODE);
            }
        }
    }
    File createTempImageFile()
    {
        try {
            return File.createTempFile(
                    CAPTURE_PHOTO_TEMP_FILE,
                    ".jpg",
                    mActivity.getExternalFilesDir("images")
            );
        } catch (IOException e) {
            return null;
        }
    }
    public void saveProfilePicture()
    {
        final File imageFile = new File(mActivity.getExternalFilesDir("images"), "my_profile_picture.jpg");;
        if (imageFile.exists()) {
            imageFile.delete();
        }

        String orgPhotoPath = mPhotoFile.getAbsolutePath();
        try {
            Bitmap bitmapOrg = createOriginalBitmap();
            bitmapOrg = rotateImage(orgPhotoPath, bitmapOrg);

            final Bitmap finalBitmap = resizeBitmap(bitmapOrg);
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(imageFile);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            } catch (final Exception e) {
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (final IOException e) {
                }
            }
        } catch (final Exception e) {
            return;
        }
        try {
            mPhotoFile.delete();
        } catch (final Exception e) {
        }
    }
    public Bitmap createOriginalBitmap()
    {
        String imagePath = mPhotoFile.getAbsolutePath();
        Bitmap bitmapOrg;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            bitmapOrg = BitmapFactory.decodeFile(imagePath);
        } else {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inDither = true;
            bitmapOrg = BitmapFactory.decodeFile(imagePath, options);
        }
        return bitmapOrg;
    }
    public Bitmap createScaledBitmap(int width, int height)
    {
        String imagePath = mPhotoFile.getAbsolutePath();
        Bitmap bitmapScaled;
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(imagePath, bmOptions);

//        bmOptions.inSampleSize = calculateInSampleSize(bmOptions, width, height);
        bmOptions.inSampleSize = calcSampleSize(bmOptions, width, height);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPurgeable = true;
        bitmapScaled = BitmapFactory.decodeFile(imagePath, bmOptions);

        return bitmapScaled;
    }
    public static Bitmap rotateImage(final String imagePath, Bitmap source) throws IOException
    {
        final ExifInterface ei = new ExifInterface(imagePath);
        final int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                source = rotateImageByAngle(source, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                source = rotateImageByAngle(source, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                source = rotateImageByAngle(source, 270);
                break;
        }
        return source;
    }
    public static Bitmap rotateImageByAngle(final Bitmap source, final float angle)
    {
        final Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    public static Bitmap resizeBitmap(Bitmap source)
    {
        final int heightOrg = source.getHeight();
        final int heightNew = 800;
        if (heightNew < heightOrg) {
            final int widthOrg = source.getWidth();
            final int widthNew = (heightNew * widthOrg) / heightOrg;

            final Matrix matrix = new Matrix();
            matrix.postScale(((float) widthNew) / widthOrg, ((float) heightNew) / heightOrg);
            source = Bitmap.createBitmap(source, 0, 0, widthOrg, heightOrg, matrix, false);
        }
        return source;
    }
    // reqWidth is larger than reqHeight
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        int width, height;
        if(options.outWidth >= options.outHeight) {
            width = options.outWidth;
            height = options.outHeight;
        } else {
            width = options.outHeight;
            height = options.outWidth;
        }
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
    public static int calcSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        int width, height;
        if(options.outWidth >= options.outHeight) {
            width = options.outWidth;
            height = options.outHeight;
        } else {
            width = options.outHeight;
            height = options.outWidth;
        }
        double width_ratio = (double)width/(double)reqWidth;
        double height_ratio = (double)height/(double)reqHeight;
        int sample_size;
        if(width_ratio >= height_ratio) {
            sample_size = (int)height_ratio;
        } else {
            sample_size = (int)width_ratio;
        }
        if(sample_size <= 0) {
            sample_size = 1;
        }
        return sample_size;
    }
}
