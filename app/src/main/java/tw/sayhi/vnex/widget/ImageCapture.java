package tw.sayhi.vnex.widget;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import tw.sayhi.shared.util.MyLog;
import tw.sayhi.vnex.DebugOptions;

public class ImageCapture
{
    public static String TAG = "ImageCapture";
    Activity mActivity;
    Uri mImageCaptureUri;
    CapturePhoto mCapturePhoto;

    public static final int CAMERA_CODE = 64;
    public static final int GALLERY_CODE = 74;
    public static final int CROPPING_CODE = 84;
    private final static int REQUEST_PERMISSION_REQ_CODE = 704;

    public ImageCapture(Activity activity) {
        mActivity = activity;
        mCapturePhoto = new CapturePhoto(mActivity);
    }

    private void LogToast(String message) {

        try {
            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, message);
    }
    private void Log(String message) {

        Log.d(TAG, message);
    }

    /**
     * This function return captured image path
     *
     * @param requestCode on mActivity result requestCode
     * @param resultCode on mActivity result resultCode
     * @param intent on mActivity result intent
     * @return ImageDetails values
     */
    public ImageDetails getImagePath(int requestCode, int resultCode, Intent intent) {

        ImageDetails imageDetails = new ImageDetails();

        if(resultCode == Activity.RESULT_OK) {

            if(requestCode == CAMERA_CODE) {
                imageDetails.setUri(mImageCaptureUri);
                imageDetails.setPath(mImageCaptureUri.getPath());
                Bitmap bitmap = getBitmap(mActivity, mImageCaptureUri);
                imageDetails.setBitmap(bitmap);
                imageDetails.setFile(new File(mImageCaptureUri.getPath()));

            } else if(requestCode == GALLERY_CODE) {

                Uri uri = intent.getData();

                imageDetails.setUri(uri);
                Bitmap bitmap = getBitmap(mActivity, uri);
                imageDetails.setBitmap(bitmap);
                imageDetails.setFile(new File(uri.getPath()));
                imageDetails.setPath(uri.getPath());

            }
        } else {
            LogToast("user cancelled.");
        }

        return imageDetails;
    }

    /**
     * Capture image using camera <br/>
     * REQUEST_CODE = ImageCapture.CAMERA_CODE
     */
    public void captureImage() {

        if(mCapturePhoto != null) {
            mCapturePhoto.startCamera();
            return;
        }
        if(mActivity != null) {

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp1.jpg");
            mImageCaptureUri = Uri.fromFile(f);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            mActivity.startActivityForResult(intent, CAMERA_CODE);

/*
            File photoFile = new File(android.os.Environment.getExternalStorageDirectory(), "temp1.jpg");
            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            mImageCaptureUri = FileProvider.getUriForFile(
                    mActivity,
                    mActivity.getPackageName() + ".provider",
                    photoFile);
            takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            mActivity.startActivityForResult(takePhotoIntent, CAMERA_CODE);
*/
        } else {

            LogToast("Activity not assigned");
        }
    }

    /**
     * pick image from gallery
     */
    public boolean canPickImage() {

        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if(i.resolveActivity(mActivity.getPackageManager()) != null) {
            return true;
        }
        return false;
    }
    public void pickImage() {

        // This will cause SecurityException
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        mActivity.startActivityForResult(i, GALLERY_CODE);
    }
    public Bitmap getCaptureBitmap()
    {
        return mCapturePhoto.createOriginalBitmap();
    }
    public Bitmap getCaptureBitmap(int width, int height)
    {
        return mCapturePhoto.createScaledBitmap(width, height);
    }
    /**
     * get path, bitmap, file and uri from image details object
     */
    public class ImageDetails {

        String path="";
        Bitmap bitmap=null;
        File file=null;
        Uri uri=null;

        public Uri getUri() {
            return uri;
        }

        public void setUri(Uri uri) {
            this.uri = uri;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public Bitmap getBitmap() {
            return bitmap;
        }

        public void setBitmap(Bitmap bitmap) {
            this.bitmap = bitmap;
        }

        public File getFile() {
            return file;
        }

        public void setFile(File file) {
            this.file = file;
        }
    }

    public static Bitmap getBitmap(Activity act, Uri uri) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(act.getContentResolver(), uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
    public static String saveImage(Activity act, Bitmap bitmap, String file_name)
    {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if(file_name.contains(".jpg")) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        } else if(file_name.contains(".png")) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        } else {
            return "";
        }
        try {
            File files;
            if( DebugOptions.bExternalDirs) {
                files = Environment.getExternalStorageDirectory();
            } else {
                files = act.getFilesDir();
            }
            File file = new File(files, file_name);
            String path_name = file.getAbsolutePath();
            FileOutputStream fos = new FileOutputStream(file);
            byte[] byte_array = bytes.toByteArray();
            fos.write(byte_array);
            long len = file.length();
            fos.close();

/*
            boolean success = file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(act,
                    new String[]{file.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
*/
//            MyLog.i("File Saved::--->" + path_name);
            return path_name;
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }
}