package tw.sayhi.vnex.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;

import tw.sayhi.shared.Utils;

public class ActionDialog extends Dialog
{
    public ActionDialog(Activity act, int title_id, int msg_id, int icon_id, int ok_id, int cancel_id)
    {
        super(act);
        String title = "";
        if(title_id > 0) {
            title = act.getString(title_id);
        }
        construct(act, title, act.getString(msg_id), icon_id, ok_id, cancel_id);
    }
    public ActionDialog(Activity act, String title, String msg, int icon_id, int ok_id, int cancel_id)
    {
        super(act);
        construct(act, title, msg, icon_id, ok_id, cancel_id);
    }
    public void construct(Activity act, String title, String msg, int icon_id, int ok_id, int cancel_id)
    {
        if(act.isFinishing()) {
            return;
        }
        title = title.replaceAll("\\*", "");
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        if(icon_id > 0) {
            builder.setIcon(icon_id);
        }
        if(!Utils.isNull(title))
            builder.setTitle(title);
        builder.setMessage(msg);

        builder.setPositiveButton(act.getString(ok_id), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onOk();
            }
        });
        if(cancel_id > 0) {
            builder.setNegativeButton(act.getString(cancel_id), new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onCancel();
                }
            });
        }
        Dialog noticeDialog = builder.create();
        noticeDialog.setCancelable(false);
        noticeDialog.setCanceledOnTouchOutside(false);
        noticeDialog.show();
    }
    public void onOk() {}
    public void onCancel() {}
}
