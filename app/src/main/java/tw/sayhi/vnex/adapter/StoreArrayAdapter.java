package tw.sayhi.vnex.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import tw.sayhi.shared.Utils;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.misc.EECStore;

public class StoreArrayAdapter extends ArrayAdapter<EECStore.Store>
{
    private static final int layoutId = R.layout.store__item;
    private final String TAG = StoreArrayAdapter.class.getSimpleName();
    private Activity mActivity;
    Location mLocation;

    public StoreArrayAdapter(Context context, ArrayList<EECStore.Store> storeArray, Activity act, Location loc) {
        super(context, 0, storeArray);
        mActivity = act;
        mLocation = loc;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent)
    {
        // Get the data item for this position
        final EECStore.Store data = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(layoutId, parent, false);
        }
        TextView tvName = v.findViewById(R.id.tv_name);
        TextView tvPhone = v.findViewById(R.id.tv_phone);
        TextView tvDistance = v.findViewById(R.id.tv_distance);
        TextView tvAddress = v.findViewById(R.id.tv_address);
        TextView tvAddressIn = v.findViewById(R.id.tv_address_in);

        tvName.setText(data.name + " ");
        // phone
        tvPhone.setText(data.phone);
        Linkify.addLinks(tvPhone, Patterns.PHONE,"tel:", Linkify.sPhoneNumberMatchFilter,
                Linkify.sPhoneNumberTransformFilter);
        tvPhone.setMovementMethod(LinkMovementMethod.getInstance());
        // distance
        if(mLocation != null) {
            Location location = new Location("");
            location.setLatitude(data.latitude);
            location.setLongitude(data.longitude);
            float distance = mLocation.distanceTo(location);
            String str_dist;
            if(distance < 1000) {
                str_dist = String.format("%.0fm", distance);
            } else {
                str_dist = String.format("%.1fkm", distance / 1000.0f);
            }
            tvDistance.setText(str_dist);
        }
        // address
        tvAddress.setText("Địa chỉ: " + data.address);
        if(!Utils.isNull(data.address_in)) {
            tvAddressIn.setText(data.address_in);
        } else {
            tvAddressIn.setVisibility(View.GONE);
        }
        TextView tvLocationFront = v.findViewById(R.id.tv_location_front);
        tvLocationFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.link));
                mActivity.startActivity(intent);
            }
        });
        return v;
    }
    void intentToMap(String lat,String lng){
        Uri gmmIntentUri = Uri.parse("geo:0,0?q="+lat+","+lng);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        mActivity.startActivity(mapIntent);
    }
}
