package tw.sayhi.vnex.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.ImageURL;
import tw.sayhi.vnex.BaseActivity;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.api.DataImageLink;

public class BannerAdapter extends PagerAdapter {
    Activity mActivity;
    DataImageLink[] mBanners;

    public BannerAdapter(Activity activity, DataImageLink[] banners) {
        mActivity = activity;
        mBanners = banners;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.banner__fragment, collection, false);
        collection.addView(layout);
        ImageView ivSlides = layout.findViewById(R.id.iv_slides);

        ImageURL.load(mBanners[position].image, ivSlides, mActivity, null, true);
//        ivSlides.setImageDrawable(mActivity.getResources().getDrawable(mBannerUrls[position]));
        ivSlides.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.openWebPage(mActivity, mBanners[position].link);
            }
        });
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    boolean doNotifyDataSetChangedOnce = true;
    @Override
    public int getCount() {
        if (doNotifyDataSetChangedOnce) {
            doNotifyDataSetChangedOnce = false;
            notifyDataSetChanged();
        }
        return mBanners.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}
