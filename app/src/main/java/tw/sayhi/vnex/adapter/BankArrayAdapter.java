package tw.sayhi.vnex.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import tw.sayhi.shared.util.MyLog;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.api.DataBank;

public class BankArrayAdapter extends ArrayAdapter<DataBank>
{
    private static final int layoutId = R.layout.bank__item;
    private final String TAG = BankArrayAdapter.class.getSimpleName();
    String mSelection;

    public BankArrayAdapter(Context context, ArrayList<DataBank> storeArray, String selection) {
        super(context, 0, storeArray);
        mSelection = selection;
    }
    public void setSelection(String selection)
    {
        mSelection = selection;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        DataBank bank = getItem(position);
        // Check if an existing view has being reused, otherwise inflate the view
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(layoutId, parent, false);
        }
        //view.setTag(position);
        try {
            TextView tvBankName = (TextView)view.findViewById(R.id.tv_bank_name);
            TextView tvBankFullName = (TextView)view.findViewById(R.id.tv_bank_full_name);
            RadioButton rbSelection = (RadioButton)view.findViewById(R.id.rb_selection);

            tvBankName.setText(bank.bank_name);
            tvBankFullName.setText(bank.bank_full_name);

            if(bank.bank_name.equals(mSelection)) {
                rbSelection.setChecked(true);
                rbSelection.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            MyLog.e(TAG, "Failed to bindView: " + e.toString());
        }
        return view;
    }
}
