package tw.sayhi.vnex.adapter;

        import android.app.Activity;
        import android.content.Context;
        import android.content.Intent;
        import android.database.Cursor;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.SimpleCursorAdapter;
        import android.widget.TextView;

        import java.text.SimpleDateFormat;
        import java.util.Locale;

        import tw.sayhi.shared.Utils;
        import tw.sayhi.vnex.BaseActivity;
        import tw.sayhi.vnex.PayInfo;
        import tw.sayhi.vnex.R;
        import tw.sayhi.vnex.activity.ExchangeRateActivity;
        import tw.sayhi.vnex.activity.PayWayActivity;
        import tw.sayhi.vnex.activity.OrderDetailActivity;
        import tw.sayhi.vnex.misc.DbOrder;
        import tw.sayhi.vnex.misc.DbRemit;
        import tw.sayhi.vnex.misc.OrderData;

public class HistoryAdapter extends SimpleCursorAdapter
{
    private static final String TAG = HistoryAdapter.class.getSimpleName();

    private static final SimpleDateFormat DATE_FMT_0 =
            new SimpleDateFormat("MM/dd", Locale.TAIWAN);
    private static final SimpleDateFormat DATE_FMT_1 =
            new SimpleDateFormat("MM/dd HH:mm", Locale.TAIWAN);

    public static int mLayout;
    private Activity mActivity;

    @SuppressWarnings("deprecation")
    public HistoryAdapter(Context ctx, int layout, Cursor c, String[] from,
                          int[] to, Activity act)
    {
        super(ctx, layout, c, from, to);
        mActivity = act;
        mLayout = layout;
        if(mLayout < 0) {
            if(BaseActivity.isNarrowScreen(mActivity)) {
                mLayout = R.layout.history__item_small;
            } else {
                mLayout = R.layout.history__item;
            }
        }
    }

    private static String getReadableTime(long date) {
        String d = DATE_FMT_1.format(date);
        String today = DATE_FMT_0.format(new java.util.Date());
        if (d.startsWith(today)) {
            d = d.substring(6);
        } else {
            d = d.substring(0, 5);
        }

        return d;
    }
    public static View createTopView(Context ctx)
    {
        View topView = View.inflate(ctx, mLayout, null);
        topView.setBackgroundColor(0xffffff99);
        return topView;
    }

    static int count = 0;
    public void setItemContent(View v, final Cursor c)
    {
        TextView tv;
        final OrderData os = new OrderData(c);
        tv = v.findViewById(R.id.tv_payee_name);
        tv.setText(DbRemit.getString(c, DbOrder.RECIPINENT_NAME));
        tv = v.findViewById(R.id.tv_date);
        tv.setText(Utils.getDateStringYyyyMmDd("" + os.l_create_time));
        tv = v.findViewById(R.id.tv_remit_number);
        tv.setText(os.euso_order);
        tv = v.findViewById(R.id.tv_remit_way);
        String trans_type = DbRemit.getString(c, DbOrder.TRANS_TYPE);
        switch (trans_type) {
            case PayInfo.DTD: //送到家
//                tv.setText(R.string.deliver_to_home);
                tv.setText(R.string.remit_dtd);
                break;
            case PayInfo.BTB: //帳號
//                tv.setText(R.string.viet_account);
                tv.setText(R.string.remit_btb);
                break;
            case PayInfo.IDP: //ID取款
//                tv.setText(R.string.id_withdrawal);
                tv.setText(R.string.remit_idp);
                break;
            case PayInfo.PIN: //密碼
//                tv.setText(R.string.password_capital);
                tv.setText(R.string.remit_pin);
                break;
        }
        tv = v.findViewById(R.id.tv_amount);
        float total_amount = Utils.parseFloat(DbRemit.getString(c, DbOrder.TOTAL_AMOUNT));
        tv.setText(String.format(Locale.TAIWAN,"%,.0f", total_amount) + " NTD");
        tv = v.findViewById(R.id.tv_status);
        TextView tvPay = v.findViewById(R.id.tv_pay);
        tvPay.setVisibility(View.INVISIBLE);
        int pos = c.getPosition();
        if(false && pos < 100) {
            switch (pos) {
                case 0:
                    os.status = DbOrder.ORDER_FAIL;
                    break;
                case 1:
                    os.status = DbOrder.ORDER_SUCCESS;
                    break;
                case 2:
                    os.status = DbOrder.ORDER_REVIEW;
                    break;
                case 3:
                    os.status = DbOrder.ORDER_PAID;
                    break;
                case 4:
                    os.status = DbOrder.TRANS_SUCCESS;
                    break;
                case 5:
                    os.status = DbOrder.TRANS_FAIL;
                    break;
                default:
                    os.status = DbOrder.ORDER_EXPIRED;
                    break;
            }
        }
        switch (os.status) {
            case DbOrder.ORDER_FAIL:
                tv.setText(R.string.order_error);
                break;
            case DbOrder.ORDER_SUCCESS:
                tv.setText(R.string.not_paid);
                tvPay.setVisibility(View.VISIBLE);
                tvPay.setText(R.string.pay);
                tvPay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mActivity, PayWayActivity.class);
                        intent.putExtra(DbOrder.EUSO_ORDER, os.euso_order);
                        mActivity.startActivity(intent);
                        mActivity.finish();
                    }
                });
                break;
            case DbOrder.ORDER_REVIEW:
                tv.setText(R.string.in_processing);
                break;
            case DbOrder.ORDER_PAID:
//                tv.setText(R.string.success);
                tv.setText(R.string.order_paid);
                break;
            case DbOrder.TRANS_SUCCESS:
                tv.setText(R.string.trans_succeed);
                break;
            case DbOrder.TRANS_FAIL:
                tv.setText(R.string.trans_fail);
                break;
            case DbOrder.ORDER_EXPIRED:
                tv.setText(R.string.arc_expired);
                break;
            default:
                tv.setText(R.string.unknown);
                break;
        }
        v.findViewById(R.id.tv_remit_again).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DbOrder.setPayeeGlobal(mActivity, "", os.euso_order);
                Intent intent = new Intent(mActivity, ExchangeRateActivity.class);
                mActivity.startActivity(intent);
            }
        });
        v.findViewById(R.id.tv_detail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, OrderDetailActivity.class);
                intent.putExtra(DbOrder.EUSO_ORDER, os.euso_order);
                mActivity.startActivity(intent);
            }
        });
    }
    @Override
    public void bindView(View v, Context ctx, Cursor c)
    {
        setItemContent(v, c);
    }

    @Override
    public View newView(Context ctx, Cursor c, ViewGroup parent)
    {
        LayoutInflater vi = (LayoutInflater) ctx.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        return vi.inflate(mLayout, parent, false);
    }
}
