package tw.sayhi.vnex.adapter;

import android.widget.ArrayAdapter;
import android.widget.Filter;

import java.util.ArrayList;

public class BankFilter extends Filter
{
    ArrayList<String> data;
    ArrayAdapter<String> adapter;

    public void setData(ArrayList<String> _data, ArrayAdapter<String> _adapter) {
        data = _data;
        adapter = _adapter;
    }
    @Override
    protected FilterResults performFiltering(CharSequence constraint)
    {
        constraint = constraint.toString().toLowerCase();

        FilterResults newFilterResults = new FilterResults();

        if (constraint != null && constraint.length() > 0) {


            ArrayList<String> auxData = new ArrayList<>();

            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toLowerCase().contains(constraint))
                    auxData.add(data.get(i));
            }

            newFilterResults.count = auxData.size();
            newFilterResults.values = auxData;
        } else {

            newFilterResults.count = data.size();
            newFilterResults.values = data;
        }

        return newFilterResults;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        ArrayList<String> resultData = (ArrayList<String>) results.values;

        adapter.clear();
        for(int i = 0; i < resultData.size(); i++) {
            adapter.add(resultData.get(i));
        }
        adapter.notifyDataSetChanged();
    }
}