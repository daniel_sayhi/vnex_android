package tw.sayhi.vnex.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import tw.sayhi.shared.Utils;
import tw.sayhi.vnex.R;
import tw.sayhi.vnex.api.DataCashHistory;

public class CashHistoryAdapter extends ArrayAdapter<DataCashHistory>
{
    private static final int layoutId = R.layout.cash_history__item;
    private final String TAG = CashHistoryAdapter.class.getSimpleName();
    private Activity mActivity;

    public CashHistoryAdapter(Context context, ArrayList<DataCashHistory> storeArray, Activity act) {
        super(context, 0, storeArray);
        mActivity = act;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent)
    {
        // Get the data item for this position
        final DataCashHistory data = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(layoutId, parent, false);
        }
        TextView tvDate = v.findViewById(R.id.tv_date);
        TextView tvSource = v.findViewById(R.id.tv_source);
        TextView tvPoints = v.findViewById(R.id.tv_points);

        tvDate.setText(Utils.getDate(data.create_time));
        tvSource.setText(data.trans_type_desc);
        tvPoints.setText(data.trans_point);
        return v;
    }
}
