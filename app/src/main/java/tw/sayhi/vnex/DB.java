package tw.sayhi.vnex;

import android.app.Activity;
import android.content.ContentValues;
import android.os.Build;

import org.json.JSONException;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import androidx.annotation.RequiresApi;
import tw.sayhi.shared.GlobalStatic;
import tw.sayhi.shared.Utils;
import tw.sayhi.shared.util.MyLog;
import tw.sayhi.shared.util.Pref;
import tw.sayhi.vnex.activity.ReceiverActivity;
import tw.sayhi.vnex.api.DataER;
import tw.sayhi.vnex.api.DataEvent;
import tw.sayhi.vnex.api.DataExchangeCoupon;
import tw.sayhi.vnex.api.EusoApi;
import tw.sayhi.vnex.api.RespAddressList;
import tw.sayhi.vnex.api.RespBankFullList;
import tw.sayhi.vnex.api.RespBase;
import tw.sayhi.vnex.api.RespER;
import tw.sayhi.vnex.api.RespEvent;
import tw.sayhi.vnex.api.RespExchangeCoupon;
import tw.sayhi.vnex.api.RespQueryExchangeCoupon;
import tw.sayhi.vnex.api.RespQueryRemitOrder;
import tw.sayhi.vnex.api.RespCashHistory;
import tw.sayhi.vnex.api.RespRegister;
import tw.sayhi.vnex.api.RespRemitOrder;
import tw.sayhi.vnex.api.RespResponseText;
import tw.sayhi.vnex.api.RespSenderProfile;
import tw.sayhi.vnex.api.RespServiceFee;
import tw.sayhi.vnex.misc.DbOrder;
import tw.sayhi.vnex.misc.DbSender;
import tw.sayhi.vnex.misc.Network;
import tw.sayhi.vnex.misc.QueryApiService;

public class DB
{
    public static final int ONE_SECOND = 1 * 1000;
    public static final int _10_SECONDS = 10 * 1000;
    public static final int _20_SECONDS = 20 * 1000;
    public static final int _30_SECONDS = 30 * 1000;
    public static final String senderCountry = "VMN";

    public static int sTrial = 1;
    public static int sSafeTrial = 3;
    public static int sMustTrial = 15;
    public static CountDownLatch latchInternet;
    public static boolean bInternet;
    public static void checkInternet()
    {
        bInternet = false;
        latchInternet = new CountDownLatch(1);
        new Thread() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void run() {
                bInternet = Network.isInternetAvailable();
                latchInternet.countDown();
            }
        }.start();
    }
    public static boolean bQueryER = false;
    public static float erVND = 0, erUSD = 0;
    public static void queryER(Activity act)
    {
        for(int i = 0; i < sSafeTrial; i++) {
            if(queryERCore(act)) {
                bQueryER = true;
                break;
            }
        }
    }
    public static boolean queryERCore(Activity act)
    {
        boolean success = false;
        try {
            RespER r = EusoApi.queryER(Config.company, Config.apid);
            if(r.success()) {
                for(int i = 0; i < r.erList.size(); i++) {
                    DataER er = r.erList.get(i);
                    if(er.exchange_flag.equals(PayInfo.VND)) {
                        erVND = er.exchange_rate;
                    } else if(er.exchange_flag.equals(PayInfo.USD)) {
                        erUSD = er.exchange_rate;
                    }
                }
            }
            success = true;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
/*
    public static boolean bBankList = false;
    public static ArrayList<String> bankList;
    public static void queryBankList(Activity act)
    {
        for(int i = 0; i < sRemitTrial; i++) {
            if(queryBankListCore(act)) {
                bBankList = true;
                break;
            }
        }
        if(!bBankList) {
            Utils.postMessage(act, act.getString(R.string.network_timeout));
        }
    }
    public static boolean queryBankListCore(Activity act)
    {
        boolean success = false;
        try {
            RespBankList r = EusoApi.queryBankList(Config.company, Config.apid, PayInfo.BTB);
            if(r.statusCode.equals(RespBase.STATUS_SUCCESS)) {
                bankList = r.bankList;
            }
            success = true;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
*/
    public static CountDownLatch latchAddressList = new CountDownLatch(1);
    public static boolean bQueryAddressList = false;
    public static RespAddressList addressList;
    public static void queryAddressList(Activity act)
    {
        if(bQueryAddressList) {
            return;
        }
        if(latchAddressList.getCount() < 1) {
            latchAddressList = new CountDownLatch(1);
        }
        for(int i = 0; i < sSafeTrial; i++) {
            if(queryAddressListCore(act)) {
                bQueryAddressList = true;
                break;
            }
        }
        latchAddressList.countDown();
        if(!bQueryAddressList && act != null) {
            Utils.postMessage(act, act.getString(R.string.network_timeout));
        }
    }
    public static boolean queryAddressListCore(Activity act)
    {
        boolean success = false;
        try {
            addressList = EusoApi.queryAddressList(Config.company, Config.apid);
            success = addressList.success();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static CountDownLatch latchBankList = new CountDownLatch(1);
    public static boolean bBankListWithInfo = false;
    public static RespBankFullList bankFullList;
    public static void queryBankListWithInfo(Activity act)
    {
        if(bBankListWithInfo) {
            return;
        }
        if(latchBankList.getCount() < 1) {
            latchBankList = new CountDownLatch(1);
        }
        for(int i = 0; i < sSafeTrial; i++) {
            if(queryBankListWithInfoCore(act)) {
                bBankListWithInfo = true;
                break;
            }
        }
        latchBankList.countDown();
        if(!bBankListWithInfo && act != null) {
            Utils.postMessage(act, act.getString(R.string.network_timeout));
        }
    }
    public static boolean queryBankListWithInfoCore(Activity act)
    {
        boolean success = false;
        try {
            bankFullList = EusoApi.queryBankListWithInfo(Config.company, Config.apid);
            success = bankFullList.success();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static CountDownLatch latchServiceFee;
    public static boolean bServiceFee;
    public static long serviceFee;
    public static long pointDiscount = 0;
    public static long getServiceFee()
    {
        return (serviceFee - pointDiscount);
    }
    public static boolean queryServiceFee(Activity act, String amount, String bank_name)
    {
        latchServiceFee = new CountDownLatch(1);
        bServiceFee = false;
        serviceFee = -1000;
        for(int i = 0; i < sSafeTrial; i++) {
            if(queryServiceFeeCore(act, amount, bank_name)) {
                bServiceFee = true;
                break;
            }
        }
        latchServiceFee.countDown();
        return bServiceFee;
    }
    public static boolean queryServiceFeeCore(Activity act, String amount, String bank_name)
    {
        boolean success = false;
        try {
            RespServiceFee r = EusoApi.queryServiceFeeV2(Config.company, Config.apid, PayInfo.trans_type, amount, bank_name);
            if(r.success()) {
                serviceFee = r.service_fee;
                success = true;
                if(DebugOptions.bShowQuery) {
                    Utils.postMessage(act, "serviceFee=" + serviceFee);
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static CountDownLatch latchCreateOrder = new CountDownLatch(1);
    public static boolean bCreateRemitOrder;
    public static RespRemitOrder respRemitOrder;
    public static int createRemitOrder(Activity act)
    {
        int result = RespBase.RESP_TIME_OUT;
        latchCreateOrder = new CountDownLatch(1);
        for(int i = 0; i < sSafeTrial; i++) {
            result = createRemitOrderCore(act);
            if(result != RespBase.RESP_TIME_OUT) {
                break;
            }
        }
        bCreateRemitOrder = (result == RespBase.RESP_SUCCESS);
        latchCreateOrder.countDown();
        return result;
    }
    public static String getValue(String key)
    {
        String value = Pref.get(key);
        return PayInfo.normVietText(value);
    }
    public static int createRemitOrderCore(Activity act)
    {
        int result = RespBase.RESP_TIME_OUT;
        boolean success = false;
        String sender_info_modify = "1";
/*
        String sender_tel = "", sender_mobile = "";
        String phone = Pref.get(PayInfo.TRUSTEE_PHONE);
        if(Valid.isMobile(phone)) {
            sender_mobile = phone;
        } else {
            sender_tel = phone;
        }
*/
        String addr1 = Pref.get(PayInfo.PAYEE_AREA);
        if(!Utils.isNull(Pref.get(PayInfo.PAYEE_PROVINCE))) {
            addr1 += ("|||" + Pref.get(PayInfo.PAYEE_PROVINCE));
        }
        if(!Utils.isNull(Pref.get(PayInfo.PAYEE_CITY))) {
            addr1 += ("|||" + Pref.get(PayInfo.PAYEE_CITY));
        }
        String bank_name = ReceiverActivity.getBankName();
        DbSender.Data user = DB.sender;
        ContentValues cv = new ContentValues();
        try {
            cv.put("company", Config.company);
            cv.put("apid", Config.apid);
            cv.put("uuid", DB.getUuid());
            cv.put("device_id", GlobalStatic.sGoogleAdId);
            cv.put("lang", Config.lang);
            cv.put("sender_name", user.name);
            cv.put("sender_arc", user.sender_arc);
            cv.put("sender_arc_term", user.expiry_date);
            cv.put("sender_birthday", user.birthday);
            cv.put("sender_tel", "");
            cv.put("sender_mobile", user.phone);
            cv.put("sender_country", Config.sender_country);
            cv.put("recipinent_name", getValue(PayInfo.PAYEE_NAME));
            cv.put("recipinent_personal_id", getValue(PayInfo.PAYEE_ID));
            cv.put("recipinent_tel", getValue(PayInfo.PAYEE_PHONE));
            cv.put("recipinent_address1", addr1);
            cv.put("recipinent_address2", getValue(PayInfo.PAYEE_ADDRESS));
            cv.put("recipinent_birthday_note", Pref.get(PayInfo.PAYEE_BIRTHDAY));
            cv.put("bank_name", PayInfo.normVietText(bank_name));
            cv.put("bank_branch", getValue(PayInfo.BANK_BRANCH));
            cv.put("bank_account", getValue(PayInfo.BANK_ACCOUNT));
            cv.put("discount_fee", "0");
            cv.put("red_point_discount", "" + pointDiscount);
            cv.put("service_fee", "" + serviceFee);
            cv.put("amount", PayInfo.stringNTD);
            cv.put("total_amount", PayInfo.stringTotal);
            cv.put("exchange_rate_flag", Pref.get(PayInfo.PAY_ER_TYPE));
            cv.put("exchange_rate", Pref.get(PayInfo.EXCHANGE_RATE));
            cv.put("referenceER", Pref.get(PayInfo.REFERENCE_ER));
            cv.put("trans_type", PayInfo.trans_type);
            cv.put("note", "");
            cv.put("coupon_code", coupon_code);
            cv.put("coupon_value", "" + coupon_value);
            cv.put("payment_type", Config.payment_type);
            cv.put("sender_info_modify", sender_info_modify);
            cv.put("euso_timestamp", Pref.get(PayInfo.EUSO_TIMESTAMP));
            cv.put("sender_source_of_income", Pref.get(PayInfo.SENDER_SOURCE_OF_INCOME));
            cv.put("sender_source_of_income_other", "其他");
            cv.put("sender_remittance_purpose", Pref.get(PayInfo.SENDER_REMITTANCE_PURPOSE));
            cv.put("sender_remittance_purpose_other", "其他");
            cv.put("recipinent_relationship", Pref.get(PayInfo.RECIPINENT_RELATIONSHIP));
            cv.put("recipinent_relationship_other", "其他");

            respRemitOrder = EusoApi.createRemitOrder(cv, Pref.get(PayInfo.TRUSTEE_SIGN_NORM));
            result = respRemitOrder.result();
            if(DebugOptions.bShowQuery && result == RespBase.RESP_FAIL) {
                Utils.postMessage(act, "Failed:" + respRemitOrder.message);
            }
            if(respRemitOrder.success()) {
                QueryApiService.setPref(DbOrder.EUSO_ORDER, respRemitOrder.euso_order);
                QueryApiService.setPref(QueryApiService.HAS_PENDING_ORDER, "1");
                MyLog.i("success");
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
    public static int uploadReceipt(Activity act)
    {
        int result = RespBase.RESP_TIME_OUT;
        for(int i = 0; i < sSafeTrial; i++) {
            result = uploadReceiptCore(act);
            if(result != RespBase.RESP_TIME_OUT) {
                break;
            }
        }
        return result;
    }
    public static int uploadReceiptCore(Activity act)
    {
        int result = RespBase.RESP_TIME_OUT;
        try {
            RespBase r = EusoApi.uploadReceiptImg(Config.company, Config.apid, Pref.get(DbOrder.EUSO_ORDER),
                    Pref.get(DbOrder.UUID), Pref.get(PayInfo.RECEIPT_PHOTO_PATH_NORM));
            result = r.result();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
    public static boolean allowRemitOrder(Activity act, String euso_order, String uuid)
    {
        boolean success = false;
        for(int i = 0; i < sSafeTrial; i++) {
            if(allowRemitOrderCore(act, euso_order, uuid)) {
                success = true;
                break;
            }
        }
        if(!success) {
            Utils.postMessage(act, act.getString(R.string.network_timeout));
        }
        return success;
    }
    public static boolean allowRemitOrderCore(Activity act, String euso_order, String uuid)
    {
        boolean success = false;
        serviceFee = 0;
        try {
            RespBase r = EusoApi.allowRemitOrder(Config.company, Config.apid, euso_order, uuid);
            success = r.success();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static RespQueryRemitOrder queryRemitOrder(Activity act, String euso_order, String uuid)
    {
        RespQueryRemitOrder queryRemitOrder;
        for(int i = 0; i < sSafeTrial; i++) {
            queryRemitOrder = queryRemitOrderCore(euso_order, uuid);
            if(queryRemitOrder != null && queryRemitOrder.success()) {
                return queryRemitOrder;
            }
        }
        if(act != null) {
            Utils.postMessage(act, act.getString(R.string.network_timeout));
        }
        return null;
    }
    public static RespQueryRemitOrder queryRemitOrderCore(String euso_order, String uuid)
    {
        RespQueryRemitOrder queryRemitOrder = null;
        try {
            queryRemitOrder = EusoApi.queryRemitOrder(Config.company, Config.apid, euso_order, uuid);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return queryRemitOrder;
    }
    public static CountDownLatch latchQueryEvent;
    public static boolean bQueryEvent;
    public static int coupon_value;
    public static String coupon_code;
    public static boolean queryEvent(Activity act)
    {
        latchQueryEvent = new CountDownLatch(1);
        bQueryEvent = false;
        coupon_value = 0;
        coupon_code = "";
        for(int i = 0; i < sSafeTrial; i++) {
            if(queryEventCore(act)) {
                bQueryEvent = true;
                break;
            }
        }
        latchQueryEvent.countDown();
        return bQueryEvent;
    }
    public static boolean queryEventCore(Activity act)
    {
        boolean success = false;
        try {
            RespEvent r = EusoApi.queryEvent(Config.company, Config.apid);
            if(r.success()) {
                success = true;
                for(int i = 0; i < r.eventList.size(); i++) {
                    DataEvent event = r.eventList.get(i);
                    coupon_value = event.coupon_value;
                    coupon_code = event.coupon_code;
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static boolean bQueryRegister;
    public static RespSenderProfile respQueryRegister;
    public static void queryRegister(Activity act, String sender_arc)
    {
        bQueryRegister = false;
        for(int i = 0; i < sSafeTrial; i++) {
            if(queryRegisterCore(act, sender_arc)) {
                bQueryRegister = true;
                break;
            }
        }
    }
    static boolean queryRegisterCore(Activity act, String sender_arc)
    {
        boolean success = false;
        try {
            respQueryRegister = new RespSenderProfile("");
            respQueryRegister = EusoApi.queryRegister(Config.company, Config.apid, sender_arc);
            if(!Utils.isNull(respQueryRegister.statusCode)) {
                success = true;
                MyLog.i("success registered = " + respQueryRegister.registered);
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    private static DbSender.Data sender;
    public static DbSender.Data getSender()
    {
        return sender;
    }
    public static void prepareSender(String sender_arc)
    {
        sender = DbSender.read(DbSender.ARC_NO, sender_arc);
    }
    public static void clearSender()
    {
        sender = null;
    }
    public static boolean isLogin()
    {
        return(!Utils.isNull(getSenderArc()));
    }
    public static boolean isForeignWorker()
    {
        if(sender != null)
            return !sender.profile_type.equals("2");
        return false;
    }
    public static String getSenderArc()
    {
        if(sender != null && sender.sender_arc != null)
            return sender.sender_arc;
        return "";
    }
    public static String getSenderName()
    {
        if(sender != null && sender.name != null)
            return sender.name;
        return "";
    }
    public static String getExpiryDate()
    {
        if(sender != null && sender.sender_arc != null)
            return sender.expiry_date;
        return "";
    }
    public static String getPassword()
    {
        if(sender != null && sender.password != null)
            return sender.password;
        return "";
    }
    public static String getStatus()
    {
        if(sender != null && sender.sender_arc != null)
            return sender.status;
        return "";
    }
    public static boolean needQueryStatus()
    {
        if(sender == null)
            return false;
        if(sender.status.equals(DbSender.SENDER_REGISTERED) || sender.status.equals(DbSender.SENDER_EDITED))
            return true;
        return false;
    }
    public static void setStatus(String status)
    {
        if(sender != null && sender.sender_arc != null)
            sender.status = status;
    }
    public static String getUuid()
    {
        if(sender != null && sender.uuid != null)
            return sender.uuid;
        return "";
    }
    public static String getPromoteNo()
    {
        if(sender != null && sender.promote_no != null)
            return sender.promote_no;
        return "";
    }
    public static boolean bLoginSuccess;
    public static boolean bLoginFinished;
    public static void login(Activity act, String sender_arc, String password)
    {
        bLoginFinished = false;
        bLoginSuccess = false;
        for(int i = 0; i < 1; i++) {
            if(loginCore(act, sender_arc, password)) {
                bLoginFinished = true;
                break;
            }
        }
    }
    static boolean loginCore(Activity act, String sender_arc, String password)
    {
        boolean login_finished = false;
        try {
            RespSenderProfile senderProfile = EusoApi.login(Config.company, Config.apid, GlobalStatic.sGoogleAdId,
                    sender_arc, password);
            login_finished = Utils.nonNull(senderProfile.statusCode);
            if(senderProfile.success()) {
                bLoginSuccess = true;
                String fake_status = QueryApiService.getFakeStatus();
                if(Utils.nonNull(fake_status)) {
                    senderProfile.status = fake_status;
                }

                sender = DbSender.update(senderProfile, password);
                BaseActivity.mLoginArcNo = sender_arc;
                DbOrder.setPayeeGlobal(act, sender_arc, "");
                QueryApiService.login(DB.getSenderArc(), DB.getUuid());
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return login_finished;
    }
    public static CountDownLatch latchRegister = new CountDownLatch(1);
    public static boolean bRegister;
    public static RespRegister respRegister;
    public static int register(Activity act, boolean first_time)
    {
        int result = RespBase.RESP_TIME_OUT;
        latchRegister = new CountDownLatch(1);
        for(int i = 0; i < sTrial; i++) {
            result = registerCore(act, first_time);
            if(result != RespBase.RESP_TIME_OUT) {
                break;
            }
        }
        bRegister = (result == RespBase.RESP_SUCCESS);
        latchRegister.countDown();
        return result;
    }
    static int registerCore(Activity act, boolean first_time)
    {
        int result = RespBase.RESP_TIME_OUT;
        try {
            respRegister = EusoApi.register(
                    first_time,
                    Config.company, // 公司代碼
                    Config.apid, // APP 代碼
                    getUuid(),
                    GlobalStatic.sGoogleAdId, /* device_id */
                    Pref.get(PayInfo.REGISTER_PASSWORD), /* password, preserve lower-case letters */
                    getValue(PayInfo.TRUSTEE_NAME), /* sender_name */
                    getValue(PayInfo.TRUSTEE_PASSPORT_ID), /* sender_arc */
                    Pref.get(PayInfo.TRUSTEE_PASSPORT_DUE), /* sender_arc_term */
                    Pref.get(PayInfo.TRUSTEE_BIRTH_DAY), /* sender_birthday */
                    "", /* sender_tel */
                    Pref.get(PayInfo.TRUSTEE_PHONE), /* sender_mobile */
                    Config.sender_country, /* sender_country IDN | PHL | VNM */
                    Pref.get(PayInfo.TRUSTEE_SELF_PHOTO_NORM), /* face_image_file */
                    Pref.get(PayInfo.TRUSTEE_ARC_PHOTO_NORM), /* arc_image_file */
                    Pref.get(PayInfo.TRUSTEE_ARC_BACK_PHOTO_NORM), /* arc_01_image_file */
                    Pref.get(DbSender.PROMOTE_TO), /* promote_no */
                    Pref.get(PayInfo.TRUSTEE_NEED_REVIEW), /* review */
                    Pref.get(PayInfo.PROFILE_TYPE) /* profile_type */
            );
            result = respRegister.result();
            if(DebugOptions.bShowQuery && result == RespBase.RESP_FAIL) {
                Utils.postMessage(act, "Failed:" + respRegister.message);
            }
            if(respRegister.success()) {
                MyLog.i("success");
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
    public static CountDownLatch latchQueryStatus;
    public static boolean bQueryStatus;
    public static RespSenderProfile querySenderStatus;
    public static void queryStatus(Activity act)
    {
        queryStatus(act, sender.sender_arc, sender.uuid, sender.password);
    }
    public static void queryStatus(Activity act, String sender_arc, String uuid, String password)
    {
        bQueryStatus = false;
        latchQueryStatus = new CountDownLatch(1);
        for(int i = 0; i < sTrial; i++) {
            if(queryStatusCore(act, sender_arc, uuid, password)) {
                bQueryStatus = true;
                break;
            } else if(latchQueryStatus.getCount() <= 0) {
                break;
            }
        }
        if(latchQueryStatus.getCount() > 0 && act != null) {
            Utils.postMessage(act, act.getString(R.string.network_timeout));
        }
    }
    static boolean queryStatusCore(Activity act, String sender_arc, String uuid, String password)
    {
        boolean success = false;
        try {
            querySenderStatus = new RespSenderProfile("");
            querySenderStatus = EusoApi.queryStatus(Config.company, Config.apid, UUID.randomUUID().toString(),
                    uuid, sender_arc, password);
            latchQueryStatus.countDown();
            success = querySenderStatus.success();
            if(querySenderStatus.success()) {
                String fake_status = QueryApiService.getFakeStatus();
                if(Utils.nonNull(fake_status)) {
                    querySenderStatus.status = fake_status;
                }
                MyLog.i("success status = " + querySenderStatus.status);
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static CountDownLatch latchQueryProfile;
    public static boolean bQueryProfile;
    public static RespSenderProfile querySenderProfile;
    public static void queryProfile(Activity act)
    {
        try {
            queryProfile(act, sender.sender_arc, sender.uuid, sender.password);
        } catch(Exception e) {
            DB.reportException("queryProfile", e);
        }
    }
    public static void queryProfile(Activity act, String sender_arc, String uuid, String password)
    {
        bQueryProfile = false;
        latchQueryProfile = new CountDownLatch(1);
        for(int i = 0; i < sTrial; i++) {
            if(queryProfileCore(act, sender_arc, uuid, password)) {
                bQueryProfile = true;
                break;
            } else if(latchQueryProfile.getCount() <= 0) {
                break;
            }
        }
        if(latchQueryProfile.getCount() > 0 && act != null) {
            Utils.postMessage(act, act.getString(R.string.network_timeout));
        }
    }
    static boolean queryProfileCore(Activity act, String sender_arc, String uuid, String password)
    {
        boolean success = false;
        try {
            querySenderProfile = EusoApi.queryProfile(Config.company, Config.apid, GlobalStatic.sGoogleAdId,
                    uuid, sender_arc, password);
            latchQueryProfile.countDown();
            success = querySenderProfile.success();
            if(querySenderProfile.success()) {
                String fake_status = QueryApiService.getFakeStatus();
                if(Utils.nonNull(fake_status)) {
                    querySenderProfile.status = fake_status;
                }
                DbSender.update(sender_arc, DbSender.STATUS, DB.querySenderProfile.status);
                DB.setStatus(DB.querySenderProfile.status);
                MyLog.i("success status = " + querySenderProfile.status);
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static boolean bAllRemitOrder = false;
    public static RespBase allRemitOrder;
    public static boolean bTransHistory;
    public static boolean queryAllRemitOrder(Activity act, String uuid)
    {
        for(int i = 0; i < sTrial; i++) {
            bAllRemitOrder = queryAllRemitOrderCore(uuid);
            if(bAllRemitOrder) {
                return true;
            }
        }
        if(act != null) {
            Utils.postMessage(act, act.getString(R.string.network_timeout));
        }
        return false;
    }
    static boolean queryAllRemitOrderCore(String uuid)
    {
        boolean success = false;
        try {
            allRemitOrder = new RespQueryRemitOrder("");
            allRemitOrder = EusoApi.queryAllRemitOrder(Config.company, Config.apid, uuid);
            if(allRemitOrder.success()) {
                success = true;
                MyLog.e("success");
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static CountDownLatch latchUpdatePassword;
    public static boolean bUpdatePassword;
    public static void updatePassword(Activity act, String password)
    {
        bUpdatePassword = false;
        latchUpdatePassword = new CountDownLatch(1);
        for(int i = 0; i < sTrial; i++) {
            if(updatePasswordCore(act, password)) {
                bUpdatePassword = true;
                break;
            } else if(latchUpdatePassword.getCount() <= 0) {
                break;
            }
        }
        if(latchUpdatePassword.getCount() > 0 && act != null) {
            Utils.postMessage(act, act.getString(R.string.network_timeout));
        }
    }
    static boolean updatePasswordCore(Activity act, String password)
    {
        boolean success = false;
        try {
            RespSenderProfile updatePassword = EusoApi.updatePassword(Config.company, Config.apid, GlobalStatic.sGoogleAdId,
                    sender.uuid, sender.sender_arc, password);
            latchUpdatePassword.countDown();
            success = updatePassword.success();
            if(updatePassword.success()) {
                MyLog.i("success");
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static CountDownLatch latchQueryRedPoint;
    public static boolean bQueryRedPoint;
    public static long redPoint;
    public static void queryRedPoint(Activity act)
    {
        bQueryRedPoint = false;
        redPoint = 0;
        latchQueryRedPoint = new CountDownLatch(1);
        for(int i = 0; i < sTrial; i++) {
            if(queryRedPointCore(act)) {
                bQueryRedPoint = true;
                break;
            } else if(latchQueryRedPoint.getCount() <= 0) {
                break;
            }
        }
        if(latchQueryRedPoint.getCount() > 0 && act != null) {
            Utils.postMessage(act, act.getString(R.string.network_timeout));
        }
    }
    static boolean queryRedPointCore(Activity act)
    {
        boolean success = false;
        try {
            RespResponseText queryRedPoint = EusoApi.queryRedPoint(Config.company, Config.apid, GlobalStatic.sGoogleAdId,
                    sender.uuid, sender.sender_arc);
            latchQueryRedPoint.countDown();
            success = queryRedPoint.success();
            redPoint = queryRedPoint.red_point;
            if(queryRedPoint.success()) {
                MyLog.i("success redPoint = " + redPoint);
            } else {
                MyLog.e("fail");
            }
        } catch (Exception e) {
            reportException("queryRedPointCore", e);
        }
        return success;
    }
    public static boolean bRedPointHistory;
    public static RespCashHistory respCashHistory;
    public static void history(Activity act)
    {
        bRedPointHistory = false;
        for(int i = 0; i < sTrial; i++) {
            if(historyCore(act)) {
                bRedPointHistory = true;
                break;
            }
        }
    }
    public static boolean historyCore(Activity act)
    {
        boolean success = false;
        try {
            respCashHistory = EusoApi.history(Config.company, Config.apid, GlobalStatic.sGoogleAdId,
                    sender.uuid, sender.sender_arc);
            success = respCashHistory.success();
            if(respCashHistory.success()) {
                MyLog.i("success");
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static boolean bAdjustRedPoint;
    public static RespResponseText respAdjustRedPoint;
    public static void adjustRedPoint(Activity act, String sender_arc, String uuid, String trans_point)
    {
        bAdjustRedPoint = false;
        redPoint = 0;
        for(int i = 0; i < sSafeTrial; i++) {
            if(adjustRedPointCore(act, sender_arc, uuid, trans_point)) {
                bAdjustRedPoint = true;
                break;
            }
        }
    }
    static boolean adjustRedPointCore(Activity act, String sender_arc, String uuid, String trans_point)
    {
        boolean success = false;
        try {
            respAdjustRedPoint = EusoApi.adjustRedPoint(Config.company, Config.apid, GlobalStatic.sGoogleAdId,
                    uuid, sender_arc, trans_point);
            success = respAdjustRedPoint.success();
            if(respAdjustRedPoint.success()) {
                MyLog.i("success trans_point = " + trans_point);
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static boolean bQueryExchangeCoupon;
    public static RespQueryExchangeCoupon groceryExchangeCoupon, cargoExchangeCoupon, giftExchangeCoupon;
    public static RespQueryExchangeCoupon getExchangeCoupon(String coupon_type)
    {
        switch (coupon_type) {
            case PayInfo.COUPON_GROCERY:
                return groceryExchangeCoupon;
            case PayInfo.COUPON_CARGO:
                return cargoExchangeCoupon;
            case PayInfo.COUPON_GIFT:
                return giftExchangeCoupon;
        }
        return null;
    }
    public static void queryExchangeCoupon(Activity act, String coupon_type)
    {
        bQueryExchangeCoupon = false;
        for(int i = 0; i < sTrial; i++) {
            if(queryExchangeCouponCore(act, coupon_type)) {
                bQueryExchangeCoupon = true;
                break;
            }
        }
    }
    public static boolean queryExchangeCouponCore(Activity act, String coupon_type)
    {
        boolean success = false;
        try {
            RespQueryExchangeCoupon respExchangeCoupon = EusoApi.queryExchangeCoupon(Config.company, Config.apid,
                    GlobalStatic.sGoogleAdId, sender.uuid, coupon_type, "AAA");
            success = respExchangeCoupon.success();
            if(respExchangeCoupon.success()) {
                switch (coupon_type) {
                    case PayInfo.COUPON_GROCERY:
                        groceryExchangeCoupon = respExchangeCoupon;
                        break;
                    case PayInfo.COUPON_CARGO:
                        cargoExchangeCoupon = respExchangeCoupon;
                        break;
                    case PayInfo.COUPON_GIFT:
                        giftExchangeCoupon = respExchangeCoupon;
                        break;
                }
                MyLog.i("success");
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static boolean bExchangeCoupon;
    public static RespExchangeCoupon exchangeCoupon;
    public static void exchangeCoupon(Activity act, DataExchangeCoupon coupon)
    {
        bExchangeCoupon = false;
        for(int i = 0; i < sTrial; i++) {
            if(exchangeCouponCore(act, coupon)) {
                bExchangeCoupon = true;
                break;
            }
        }
    }
    public static boolean exchangeCouponCore(Activity act, DataExchangeCoupon coupon)
    {
        boolean success = false;
        try {
            RespExchangeCoupon respExchangeCoupon = EusoApi.exchangeCoupon(Config.company, Config.apid,
                    GlobalStatic.sGoogleAdId, sender.uuid, coupon.coupon_type, "AAA", coupon.coupon_id,
                    coupon.exchange_bonus_cost, coupon.signature, "");
            success = respExchangeCoupon.success();
            if(success) {
                MyLog.i("success");
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static boolean exchangeCouponCoreCV(Activity act, DataExchangeCoupon coupon)
    {
        boolean success = false;
        ContentValues cv = new ContentValues();
        cv.put("company", Config.company);
        cv.put("apid", Config.apid);
        cv.put("device_id", GlobalStatic.sGoogleAdId);
        cv.put("uuid", sender.uuid);
        cv.put("coupon_type", coupon.coupon_type);
        cv.put("branch_code", "AAA");
        cv.put("coupon_id", coupon.coupon_id);
        cv.put("exchange_bonus_cost", coupon.exchange_bonus_cost);
        cv.put("signature", coupon.signature);
        cv.put("mote", "mote");
        try {
            RespExchangeCoupon respExchangeCoupon = EusoApi.exchangeCouponCV(cv);
            success = respExchangeCoupon.success();
            if(success) {
                MyLog.i("success");
            } else {
                MyLog.e("fail");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
    public static String getStackStrace(Exception e)
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String stack_trace = sw.toString();
        return stack_trace;
    }
    public static void reportException(String record, String content)
    {
        new Thread() {
            @Override
            public void run() {
                try {
                    EusoApi.dataStore("vnex_" + record, GlobalStatic.apkName + "\n" + content);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
    public static void reportException(String record, Exception e)
    {
        String stack_trace = getStackStrace(e);
        reportException(record, stack_trace);
    }
    public static void reportException(String record, String message, Exception e)
    {
        String stack_trace = getStackStrace(e);
        reportException(record, message + "\n\n" + stack_trace);
    }
}
