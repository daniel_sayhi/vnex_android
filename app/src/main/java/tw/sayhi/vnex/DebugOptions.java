package tw.sayhi.vnex;

public class DebugOptions
{
    public static boolean bGeneral, bUseTestURL, bUseEEC, bShowQuery, bShowRespLog, bExternalDirs, bSkipQuery;
    public static String arc_no, password;
    public static void init()
    {
        boolean bUseProductionURL = true;
//        arc_no = "ND12345678";
//        password = "12345678";
        if(bUseProductionURL) {
            switch (2) {
                case 1:
                    arc_no = "ND12345678";
                    password = "12345678";
                    break;
                case 2:
                    arc_no = "HC12345679"; // Expired
                    password = "123456";
                    break;
            }
//            arc_no = "BD00444384";
//            password = "19710429";
            bGeneral = false;
            bUseTestURL = false;
        } else {
            switch (5) {
                case 1:
                    arc_no = "AD12345678"; // SUCCESS
                    password = "123456";
                    break;
                case 2:
                    arc_no = "BD12345678"; // FAIL
                    password = "123456";
                    break;
                case 3:
                    arc_no = "CD12345678"; // UNDER REVIEW
                    password = "123456";
                    break;
                case 4:
                    arc_no = "AB11223344"; // Under Review
//            arc_no = "AC11223344"; // Under Review
//            arc_no = "AA09195504"; // Under Review
//            arc_no = "AB09195504"; // New account
                    password = "1234";
                    break;
                case 5:
                    arc_no = "B919550470"; // NEW
                    password = "1234";
                    break;
            }
            bGeneral = true;
            bUseTestURL = true;
        }
        bUseEEC = false;
        bShowQuery = false;
        bShowRespLog = false;
        bExternalDirs = false;
        bSkipQuery = false;
    }
    public static boolean isEnabled()
    {
        return(bGeneral || bUseTestURL || bUseEEC || bShowQuery || bShowRespLog || bExternalDirs || bSkipQuery);
    }
}
