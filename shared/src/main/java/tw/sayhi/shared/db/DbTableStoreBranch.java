package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataStoreBranch;


public class DbTableStoreBranch
{
    public static final String C_ADDRESS = "address";
    public static final String C_CODE = "code";
    public static final String C_DESC = "desc";
    public static final String C_DISTANCE = "distance";
    public static final String C_ID = "id";
    public static final String C_LAT = "lat";
    public static final String C_LNG = "lng";
    public static final String C_NAME = "name";
    public static final String C_PHONE = "phone";
    public static final String C_PRIMARY_KEY = "_id";
    public static final String C_STORE_ID = "store_id";
    public static final String C_TITLE = "title";

    private static final String[] COLUMNS =
            new String[]
            {
                C_ADDRESS,
                C_CODE,
                C_DESC,
                C_DISTANCE,
                C_ID,
                C_LAT,
                C_LNG,
                C_NAME,
                C_PHONE,
                C_PRIMARY_KEY,
                C_STORE_ID,
                C_TITLE
            };

    public static final String TABLE_NAME = "store_branch";


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_PRIMARY_KEY + " TEXT PRIMARY KEY, " +
                C_ADDRESS + " TEXT, " +
                C_CODE + " TEXT, " +
                C_DESC + " TEXT, " +
                C_DISTANCE + " INTEGER, " +
                C_ID + " TEXT, " +
                C_LAT + " TEXT, " +
                C_LNG + " TEXT, " +
                C_NAME + " TEXT, " +
                C_PHONE + " TEXT, " +
                C_STORE_ID + " TEXT, " +
                C_TITLE + " TEXT)");
    }

    public static void delete(String storeId)
    {
        synchronized (DbHelper.class) {
            DbHelper.sDb.delete(TABLE_NAME, C_STORE_ID + "=?",
                    new String[] {storeId});
        }
    }

    public static Cursor read(String storeId)
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, C_STORE_ID + "=?",
                new String[] {storeId}, null, null, C_DISTANCE);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }

    public static void update(ArrayList<DataStoreBranch> branchList)
    {
        synchronized (DbHelper.class) {
            for (DataStoreBranch b : branchList) {
                ContentValues cv = new ContentValues();
                cv.put(C_ADDRESS, b.address);
                cv.put(C_CODE, b.code);
                cv.put(C_DESC, b.desc);
                cv.put(C_ID, b.id);
                cv.put(C_LAT, b.lat);
                cv.put(C_LNG, b.lng);
                cv.put(C_NAME, b.name);
                cv.put(C_PHONE, b.phone);
                cv.put(C_PRIMARY_KEY, b.storeId + "_" + b.id);
                cv.put(C_STORE_ID, b.storeId);
                cv.put(C_TITLE, b.title);

                int tmp = DbHelper.sDb.update(TABLE_NAME, cv,
                        C_PRIMARY_KEY + "=?",
                        new String[] {b.storeId + "_" + b.id});
                if (tmp == 0) {
                    DbHelper.sDb.insert(TABLE_NAME, null, cv);
                }
            }
        }
    }

    public static void updateDistance(int lat, int lng)
    {
        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.execSQL("UPDATE " + TABLE_NAME + " SET " +
                C_DISTANCE + " = (" + C_LAT + " - " + lat + ") * (" +
                C_LAT + " - " + lat + ") + (" + C_LNG + " - " + lng +
                ") * (" + C_LNG + " - " + lng + ")");

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
}
