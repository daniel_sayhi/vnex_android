package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataFriend;

public class DbTableFriend
{
    public static final String C_CODE = "code";
    public static final String C_N = "n";
    public static final String C_NAME = "name";
    public static final String C_P = "_id";
    public static final String C_S = "m";


    private static final String[] COLUMNS = new String[] {
            C_CODE,
            C_N,
            C_NAME,
            C_P,
            C_S
    };

    public static final String TABLE_NAME = "friend";

//    private static final String TAG = DbTableFriend.class.getSimpleName();


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_CODE + " TEXT, " +
                C_N + " TEXT, " +
                C_NAME + " TEXT, " +
                C_P + " TEXT PRIMARY KEY, " +
                C_S + " TEXT)");
    }

    public static void delete()
    {
        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.delete(TABLE_NAME, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static Cursor read()
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, null, null, null,
                null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }
    public static void update(ArrayList<DataFriend> friends)
    {
        DbHelper.sDb.beginTransaction();

        for (DataFriend f : friends) {
            ContentValues cv = new ContentValues();
            cv.put(C_CODE, f.code);
            cv.put(C_N, f.n);
            cv.put(C_NAME, f.name);
            cv.put(C_P, f.p);
            cv.put(C_S, f.s);

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_P + "=?",
                    new String[] {f.p});
            if (tmp == 0) {
                DbHelper.sDb.insert(TABLE_NAME, null, cv);
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
}
