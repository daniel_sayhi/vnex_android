package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import tw.sayhi.shared.result.ResultCateAreaList;


public class DbTableStoreCate {

    public static final String C_CODE = "_id";
    public static final String C_IMG_URL = "img_url";
    public static final String C_NAME = "name";

    private static final String[] COLUMNS =
            new String[]
            {
                C_CODE,
                C_IMG_URL,
                C_NAME
            };

    private static final String TABLE_NAME = "store_cate";

    private static final String TAG = DbTableStoreCate.class.getSimpleName();


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_CODE + " TEXT PRIMARY KEY, " +
                C_IMG_URL + " TEXT, " +
                C_NAME + " TEXT)");
    }

    public static void delete()
    {
        synchronized (DbHelper.class) {
            DbHelper.sDb.delete(TABLE_NAME, null, null);
        }
    }

    public static Cursor read(String[] codes)
    {
        synchronized (DbHelper.class) {
            if (codes == null) {
                codes = new String[] {"%"};
            }


            String selection = "";
            for (String c : codes) {
                selection += "(" + C_CODE + " LIKE '" + c + "') OR ";
            }
            selection = selection.substring(0, selection.length() - 4);
            Log.d(TAG, selection);


            return DbHelper.sDb.query(TABLE_NAME, COLUMNS, selection, null,
                    null, null, null);
        }
    }

    public static void update(ArrayList<ResultCateAreaList.Item> cateList)
    {
        synchronized (DbHelper.class) {
            for (ResultCateAreaList.Item a : cateList) {
                ContentValues cv = new ContentValues();
                cv.put(C_CODE, a.code);
                cv.put(C_IMG_URL, a.imgUrl);
                cv.put(C_NAME, a.name);

                int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_CODE + "=?",
                        new String[] {a.code});
                if (tmp == 0) {
                    DbHelper.sDb.insert(TABLE_NAME, null, cv);
                }
            }
        }
    }
}
