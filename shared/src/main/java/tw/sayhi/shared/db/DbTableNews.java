package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataNews;


public class DbTableNews
{
    public static final String C_CODE = "code";
    public static final String C_MESSAGE = "message";
    public static final String C_NAME = "name";
    public static final String C_NEWS_ID = "_id";
    public static final String C_PERIOD = "period";
    public static final String C_PRICE = "price";
    public static final String C_PRICE_DESC = "price_desc";
    public static final String C_SUBJECT = "subject";
    public static final String C_SUBSCRIBE = "subscribe";

    private static final String[] COLUMNS = new String[] {
            C_CODE,
            C_MESSAGE,
            C_NAME,
            C_NEWS_ID,
            C_PERIOD,
            C_PRICE,
            C_PRICE_DESC,
            C_SUBJECT,
            C_SUBSCRIBE
    };

    public static final String TABLE_NAME = "news";

//    private static final String TAG = DbTableNews.class.getSimpleName();


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_CODE + " TEXT, " +
                C_MESSAGE + " TEXT, " +
                C_NAME + " TEXT, " +
                C_NEWS_ID + " TEXT PRIMARY KEY, " +
                C_PERIOD + " TEXT, " +
                C_PRICE + " TEXT, " +
                C_PRICE_DESC + " TEXT, " +
                C_SUBJECT + " TEXT, " +
                C_SUBSCRIBE + " TEXT)");
    }

    public static void delete()
    {
        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.delete(TABLE_NAME, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static Cursor read()
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, null, null, null,
                null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }

    public static void update(ArrayList<DataNews> newsList)
    {
        DbHelper.sDb.beginTransaction();

        for (DataNews d : newsList) {
            ContentValues cv = new ContentValues();
            cv.put(C_CODE, d.code);
            cv.put(C_MESSAGE, d.message);
            cv.put(C_NAME, d.name);
            cv.put(C_NEWS_ID, d.newsId);
            cv.put(C_PERIOD, d.period);
            cv.put(C_PRICE, d.price);
            cv.put(C_PRICE_DESC, d.priceDesc);
            cv.put(C_SUBJECT, d.subject);
            cv.put(C_SUBSCRIBE, d.subscribe);

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_NEWS_ID + "=?",
                    new String[] {d.newsId});
            if (tmp == 0) {
                DbHelper.sDb.insert(TABLE_NAME, null, cv);
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static void update(String newsId, boolean subscribe)
    {
        DbHelper.sDb.beginTransaction();

        ContentValues cv = new ContentValues();
        if (subscribe) {
            cv.put(C_SUBSCRIBE, "1");
        } else {
            cv.put(C_SUBSCRIBE, "0");
        }
        DbHelper.sDb.update(TABLE_NAME, cv, C_NEWS_ID + "=?",
                new String[]{newsId});

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
}
