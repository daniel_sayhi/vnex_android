package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import tw.sayhi.shared.result.DataStore;

public class DbTableStore
{
    public static final String C_ADV_COUNT = "adv_count";
    public static final String C_AREA = "area";
    public static final String C_AREA_CODES = "area_codes";
    public static final String C_CATEGORY = "category";
    public static final String C_CATEGORY_CODE = "category_code";
    public static final String C_CLICK_COUNT = "click_count";
    public static final String C_DEAL_DESC = "deal_desc";
    public static final String C_DESC = "desc";
    public static final String C_DISTANCE = "distance";
    public static final String C_EXPIRY = "expiry";
    public static final String C_IMAGE_URL = "image_url";
    public static final String C_LATITUDE = "latitude";
    public static final String C_LINK_URL = "link_url";
    public static final String C_LONGITUDE = "longitude";
    public static final String C_MAIN_ADDRESS = "main_address";
    public static final String C_MAIN_PHONE = "main_phone";
    public static final String C_NICKNAME = "nickname";
    public static final String C_STORE_ID = "_id";
    public static final String C_STORE_URL = "store_url";
    public static final String C_TITLE = "title";
    public static final String C_UPDATED = "updated";

    private static final String[] COLUMNS = new String[] {
            C_ADV_COUNT,
            C_AREA,
            C_AREA_CODES,
            C_CATEGORY,
            C_CATEGORY_CODE,
            C_CLICK_COUNT,
            C_DEAL_DESC,
            C_DESC,
            C_DISTANCE,
            C_EXPIRY,
            C_IMAGE_URL,
            C_LATITUDE,
            C_LINK_URL,
            C_LONGITUDE,
            C_MAIN_ADDRESS,
            C_MAIN_PHONE,
            C_NICKNAME,
            C_STORE_ID,
            C_STORE_URL,
            C_TITLE,
            C_UPDATED
    };

    public static final String TABLE_NAME = "store";

    private static final String TAG = DbTableStore.class.getSimpleName();

    public static Set mStoreIdMap = new HashSet(101);

    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_STORE_ID + " TEXT PRIMARY KEY, " +
                C_ADV_COUNT + " TEXT, " +
                C_AREA + " TEXT, " +
                C_AREA_CODES + " TEXT, " +
                C_CATEGORY + " TEXT, " +
                C_CATEGORY_CODE + " TEXT, " +
                C_CLICK_COUNT + " TEXT, " +
                C_DEAL_DESC + " TEXT, " +
                C_DESC + " TEXT, " +
                C_DISTANCE + " INTEGER, " +
                C_EXPIRY + " TEXT, " +
                C_IMAGE_URL + " TEXT, " +
                C_LATITUDE + " TEXT, " +
                C_LINK_URL + " TEXT, " +
                C_LONGITUDE + " TEXT, " +
                C_MAIN_ADDRESS + " TEXT, " +
                C_MAIN_PHONE + " TEXT, " +
                C_NICKNAME + " TEXT, " +
                C_STORE_URL + " TEXT, " +
                C_TITLE + " TEXT, " +
                C_UPDATED + " TEXT)");
    }

    public static void delete()
    {
        DbHelper.sDb.beginTransaction();

        //
        DbHelper.sDb.delete(TABLE_NAME, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static Cursor read(String areaFilter, String cateFilter)
    {
        DbHelper.sDb.beginTransaction();

        //
        String orderBy;
        String selection;
        if (areaFilter.equals("$5")) {
            orderBy = C_DISTANCE;
            selection = C_CATEGORY_CODE + " LIKE '" + cateFilter + "'";
        } else {
            orderBy = C_UPDATED + " DESC";
            selection = "(" + C_CATEGORY_CODE + " LIKE '" + cateFilter +
                    "') " + "AND (" + C_AREA_CODES + " LIKE '" +
                    areaFilter + "')";
        }
//        MyLog.d(TAG, selection);

        //
        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, selection, null,
                null, null, orderBy);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        return c;
    }

    public static Cursor read(String storeId)
    {

        DbHelper.sDb.beginTransaction();

        //
        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, C_STORE_ID + "=?",
                new String[] {storeId}, null, null, C_UPDATED + " DESC");

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        return c;
    }

    public static Cursor read(ArrayList<String> storeIds)
    {
        if(storeIds.size() == 0)
            return null;

        DbHelper.sDb.beginTransaction();

        //
        String[] storeIds2 = new String[storeIds.size()];
        for (int i = 0; i < storeIds2.length; i++) {
            storeIds2[i] = storeIds.get(i);
        }

        //
        Cursor c = read(storeIds2);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        return c;
    }

    public static Cursor read(String[] storeIds)
    {
        DbHelper.sDb.beginTransaction();

        //
        String selection = "";
        for (String id : storeIds) {
            selection += C_STORE_ID + "=? OR ";
        }
        selection = selection.substring(0, selection.length() - 4);

        //
        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, selection, storeIds,
                null, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        return c;
    }

    public static Cursor readMainSlides()
    {
        DbHelper.sDb.beginTransaction();

        String selection = C_CATEGORY_CODE + " LIKE '9906%'";
        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, selection, null,
                null, null, C_UPDATED + " DESC LIMIT 6");

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        return c;
    }

    public static void update(ArrayList<DataStore> storeList)
    {
        mStoreIdMap.clear();

        DbHelper.sDb.beginTransaction();

        for (DataStore a : storeList) {
            mStoreIdMap.add(a.storeId);
            ContentValues cv = new ContentValues();
            cv.put(C_ADV_COUNT, a.advCount);
            cv.put(C_AREA, a.area);
            cv.put(C_AREA_CODES, a.areaCodes);
            cv.put(C_CATEGORY, a.category);
            cv.put(C_CATEGORY_CODE, a.categoryCode);
            cv.put(C_CLICK_COUNT, a.clickCount);
            cv.put(C_DEAL_DESC, a.dealDesc);
            cv.put(C_DESC, a.desc);
            cv.put(C_EXPIRY, a.expiry);
            cv.put(C_IMAGE_URL, a.imageUrl);
            cv.put(C_LATITUDE, a.latitude);
            cv.put(C_LINK_URL, a.linkUrl);
            cv.put(C_LONGITUDE, a.longitude);
            cv.put(C_MAIN_ADDRESS, a.mainAddress);
            cv.put(C_MAIN_PHONE, a.mainPhone);
            cv.put(C_NICKNAME, a.nickName);
            cv.put(C_STORE_ID, a.storeId);
            cv.put(C_STORE_URL, a.storeUrl);
            cv.put(C_TITLE, a.title);
            cv.put(C_UPDATED, a.updated);
            cv.put(C_DISTANCE, 99999999); // make it a big number

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_STORE_ID + "=?",
                    new String[] {a.storeId});
            if (tmp == 0) {
                DbHelper.sDb.insert(TABLE_NAME, null, cv);
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
    public static boolean contains(String storeId)
    {
        return (mStoreIdMap.contains(storeId));
    }
}
