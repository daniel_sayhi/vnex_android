package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import tw.sayhi.shared.result.DataStoreLocation;
import tw.sayhi.shared.util.MyLog;


public class DbTableStoreLocation
{
    public static final String C_CODE = "code";
    public static final String C_DISTANCE = "distance";
    public static final String C_ID = "id";
    public static final String C_LAT = "lat";
    public static final String C_LNG = "lng";
    public static final String C_NAME = "name";
    public static final String C_PRIMARY_KEY = "_id";
    public static final String C_STORE_ID = "store_id";

    private static final String[] COLUMNS =
            new String[]
                    {
                            C_CODE,
                            C_DISTANCE,
                            C_ID,
                            C_LAT,
                            C_LNG,
                            C_NAME,
                            C_PRIMARY_KEY,
                            C_STORE_ID
                    };

    static final String TABLE_NAME = "store_location";

    private static final String TAG = DbTableStoreLocation.class.getSimpleName();

    public static void setUseHash(boolean bUseHash) {
        DbTableStoreLocation.bUseHash = bUseHash;
    }

    /* Daniel Yan : May 26, 2016
                   The primary usage of store locations has used for sorting, according to the distance between the user
                   and the store location. We don't actually need a SQL database for it.
                   Since ArrayList<DataStoreLocation> has already created, we could just use it. It avoids the slow
                   process of inserting records into SQL database.
                 */
    private static boolean bUseHash = false;
    private static ArrayList<DataStoreLocation> mStoreLocation;

    static void _create(SQLiteDatabase db)
    {
        if(bUseHash == true)
            return;

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_PRIMARY_KEY + " INTEGER PRIMARY KEY, " +
                C_CODE + " TEXT, " +
                C_DISTANCE + " INTEGER, " +
                C_ID + " TEXT, " +
                C_LAT + " TEXT, " +
                C_LNG + " TEXT, " +
                C_NAME + " TEXT, " +
                C_STORE_ID + " TEXT)");
    }

    public static void delete()
    {
        if(bUseHash == true)
            return;

        synchronized (DbHelper.class) {
            DbHelper.sDb.delete(TABLE_NAME, null, null);
        }
    }

    public static void update(ArrayList<DataStoreLocation> locationList)
    {

        if(bUseHash == true) {
            mStoreLocation = locationList;
            return;
        }
        DbHelper.sDb.beginTransaction();

        for (DataStoreLocation dsl : locationList) {
            String storeId = dsl.storeId;
            String branchId = dsl.id;
            long id = -1;
            while (branchId.length() > 0) {
                try {
                    id = Long.parseLong(storeId + branchId);
                    break;
                } catch (NumberFormatException e) {
                    branchId = branchId.substring(1, branchId.length());
                }
            }

            ContentValues cv = new ContentValues();
            cv.put(C_CODE, dsl.code);
            cv.put(C_ID, dsl.id);
            cv.put(C_LAT, dsl.lat);
            cv.put(C_LNG, dsl.lng);
            cv.put(C_NAME, dsl.name);
            cv.put(C_PRIMARY_KEY, id);
            cv.put(C_STORE_ID, dsl.storeId);

            // Daniel Yan
            if (false) {
                DbHelper.sDb.insertWithOnConflict(TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
            } else {
                int tmp = DbHelper.sDb.update(TABLE_NAME, cv,
                        C_PRIMARY_KEY + "=" + id, null);
                if (tmp == 0) {
                    DbHelper.sDb.insert(TABLE_NAME, null, cv);
                }
            }
        }
        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

    }

    public static void updateDistance(int lat, int lng)
    {
        if(bUseHash) {
            updateDistanceWithHash(lat, lng);
            return;
        }

        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.execSQL("UPDATE " + TABLE_NAME + " SET " +
                C_DISTANCE + " = (" + C_LAT + " - " + lat + ") * (" +
                C_LAT + " - " + lat + ") + (" + C_LNG + " - " + lng +
                ") * (" + C_LNG + " - " + lng + ")");

        Cursor c = DbHelper.sDb.rawQuery("SELECT " + C_STORE_ID + ", " +
                "MIN(" + C_DISTANCE + ") AS minDistance " + "FROM " +
                TABLE_NAME + " GROUP BY " + C_STORE_ID, null);

        c.moveToFirst();
        while (true) {
            ContentValues cv = new ContentValues();
            cv.put(DbTableStore.C_DISTANCE,
                    Long.parseLong(c.getString(c.getColumnIndex(
                            "minDistance"))));

            DbHelper.sDb.update(DbTableStore.TABLE_NAME, cv,
                    DbTableStore.C_STORE_ID + "=" +
                            c.getString(c.getColumnIndex(C_STORE_ID)),
                    null);

            if (c.isLast()) {
                c.close();
                break;
            } else {
                c.moveToNext();
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
    public static void updateDistanceWithHash(int lat, int lng)
    {
        MyLog.timeStart(TAG);
        //findLatLngMismatch();
        Map<String, Long> hashMap = new HashMap<String, Long>(1009); // some prime number
        for(DataStoreLocation dsl : mStoreLocation) {
            Long nLat = Long.parseLong(dsl.lat);
            Long nLng = Long.parseLong(dsl.lng);
            if(nLat == 0 && nLng == 0)
                continue;
            double dLatDiff = (nLat - lat);
            double dLngDiff = (nLng - lng);
            double dNewDist = Math.sqrt(dLatDiff * dLatDiff + dLngDiff * dLngDiff) + 0.5;
            Long nNewDist = (new Double(dNewDist)).longValue();
            Long nDist = nNewDist;
            if(hashMap.containsKey(dsl.storeId)) {
                nDist = hashMap.get(dsl.storeId);
                if(nNewDist < nDist)
                    nDist = nNewDist;
            }
            hashMap.put(dsl.storeId, nDist);
        }
        MyLog.timePrint("loop mStoreLocation");
        DbHelper.sDb.beginTransaction();
        Set<String> keys = hashMap.keySet();
        for (String storeId : keys) {
            ContentValues cv = new ContentValues();
            cv.put(DbTableStore.C_DISTANCE, hashMap.get(storeId));

            DbHelper.sDb.update(DbTableStore.TABLE_NAME, cv,
                    DbTableStore.C_STORE_ID + "=" + storeId,
                    null);
        }
        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        MyLog.timeEnd("updateDistanceWithHash()");
    }
    public static void findLatLngMismatch()
    {
        Iterator iter = DbTableStore.mStoreIdMap.iterator();
        int nMismatch = 1;
        while (iter.hasNext()) {
            String storeId = (String)iter.next();
            Cursor c = DbTableStore.read(storeId);
            c.moveToFirst();
            final String title = c.getString(c.getColumnIndex(
                    DbTableStore.C_TITLE));
            final String desc = c.getString(c.getColumnIndex(
                    DbTableStore.C_DEAL_DESC));
            final String imgUrl = c.getString(c.getColumnIndex(
                    DbTableStore.C_IMAGE_URL));
            final String dist = c.getString(c.getColumnIndex(
                    DbTableStore.C_DISTANCE));
            final String lat = c.getString(c.getColumnIndex(
                    DbTableStore.C_LATITUDE));
            final String lng = c.getString(c.getColumnIndex(
                    DbTableStore.C_LONGITUDE));
            Long nLat = Long.parseLong(lat);
            Long nLng = Long.parseLong(lng);
            boolean found = false;
            for(DataStoreLocation dsl : mStoreLocation) {
                Long lLat = Long.parseLong(dsl.lat);
                Long lLng = Long.parseLong(dsl.lng);
                if(dsl.storeId.equals(storeId) && Math.abs(nLat - lLat) < 100 &&
                        Math.abs(nLng - lLng) < 100) {
                    found = true;
                    break;
                }
            }
            if(!found) {
                print("store " + nMismatch + " = " + title);
                nMismatch++;
                print("storeId = " + storeId);
                print("lat = " + lat + " lng = " + lng);
                print("branches:");
                for(DataStoreLocation dsl : mStoreLocation) {
                    if(dsl.storeId.equals(storeId))
                        print("lat = " + dsl.lat + " lng = " + dsl.lng);
                }
            }
        }
    }
    private static void print(String s) {
        MyLog.i("LatLng", s);
    }
    /*
    public void fromString(String in)
    {
        String[] arr = in.split("");
    }

    private static void writeStoreLocation(Activity act,ArrayList<DataStoreLocation> locationList)
    {
        FileOutputStream fos;
        try {
            fos = act.openFileOutput(mStoreLocationFile, Context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            for (DataStoreLocation dsl : locationList) {
                String out = String.format("%s:%s:%s\n", dsl.storeId,
                        dsl.lat.isEmpty() ? "0" : dsl.lat,
                        dsl.lng.isEmpty() ? "0" : dsl.lng);
                osw.write(out);
            }
            osw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void updateDistanceWithHash(Activity act, int lat, int lng)
    {
        MyLog.timeStart(TAG);
        //findLatLngMismatch();
        Map<String, Long> hashMap = new HashMap<String, Long>(1009); // some prime number
        try {
            InputStream inputStream = act.openFileInput(mStoreLocationFile);

            if ( inputStream != null ) {

                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String readString = "";

                while ( (readString = bufferedReader.readLine()) != null ) {
                    String[] arr = readString.split(":");
                    //parseLocation();
                }
                inputStream.close();
            }
        }
        catch (FileNotFoundException e) {
            MyLog.e(TAG, "File not found: " + e.toString());
        } catch (IOException e) {
            MyLog.e(TAG, "Can not read file: " + e.toString());
        }

        for(DataStoreLocation dsl : mStoreLocation) {
            Long nLat = Long.parseLong(dsl.lat);
            Long nLng = Long.parseLong(dsl.lng);
            if(nLat == 0 && nLng == 0)
                continue;
            double dLatDiff = (nLat - lat);
            double dLngDiff = (nLng - lng);
            double dNewDist = Math.sqrt(dLatDiff * dLatDiff + dLngDiff * dLngDiff) + 0.5;
            Long nNewDist = (new Double(dNewDist)).longValue();
            Long nDist = nNewDist;
            if(hashMap.containsKey(dsl.storeId)) {
                nDist = hashMap.get(dsl.storeId);
                if(nNewDist < nDist)
                    nDist = nNewDist;
            }
            hashMap.put(dsl.storeId, nDist);
        }
        MyLog.timePrint("loop mStoreLocation");
        DbHelper.sDb.beginTransaction();
        Set<String> keys = hashMap.keySet();
        for (String storeId : keys) {
            ContentValues cv = new ContentValues();
            cv.put(DbTableStore.C_DISTANCE, hashMap.get(storeId));

            DbHelper.sDb.update(DbTableStore.TABLE_NAME, cv,
                    DbTableStore.C_STORE_ID + "=" + storeId,
                    null);
        }
        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        MyLog.timeEnd("updateDistanceWithHash()");
    }
    */
}
