package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;

import com.nostra13.universalimageloader.utils.L;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import tw.sayhi.shared.result.DataAdv;

public class DbTableAdv extends DbAdvBase
{
    public static String [] getColumnNames() { return COLUMNS; }

    private static final String TABLE_NAME = "adv";

    private static final String TAG = DbTableAdv.class.getSimpleName();

    public static final int MAX_POSITION = 15;

    private static boolean mHasPromo = false;
    public static boolean hasPromo() { return mHasPromo; }
    public static void setPromo(boolean promo) { mHasPromo = promo; }

    static void _create(SQLiteDatabase db)
    {
       db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + mTableString);
    }

    public static void delete()
    {
        synchronized (DbHelper.class) {
            DbHelper.sDb.delete(TABLE_NAME, null, null);
        }
    }

    public static Cursor read()
    {
        DbHelper.sDb.beginTransaction();

        //
        Cursor c = DbHelper.sDb.query(
                TABLE_NAME, COLUMNS, null, null, null, null, C_POSITION);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        return c;
    }

    public static Cursor readAll(String sortBy)
    {
        DbHelper.sDb.beginTransaction();

/*
        Cursor c = DbHelper.sDb.query(
                TABLE_NAME, COLUMNS, null, null, null, null, sortBy);
*/
        Cursor c = DbHelper.sDb.rawQuery("SELECT * FROM " + TABLE_NAME + " ORDER BY " + sortBy + " ASC", null);
        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        return c;
    }

    public static Cursor readCate(String cate)
    {
        synchronized (DbHelper.class) {
            return DbHelper.sDb.query(TABLE_NAME, COLUMNS, C_CATEGORY + "=?",
                    new String[]{cate}, null, null, C_DISTANCE);
        }
    }

    public static Cursor read(String advId)
    {
        synchronized (DbHelper.class) {
            return DbHelper.sDb.query(TABLE_NAME, COLUMNS, C_ADV_ID + "=?",
                    new String[]{advId}, null, null, null);
        }
    }

    public static Cursor read(ArrayList<String> advIds)
    {
        synchronized (DbHelper.class) {
            //
            String[] advIds2 = new String[advIds.size()];
            for (int i = 0; i < advIds2.length; i++) {
                advIds2[i] = advIds.get(i);
            }

            return read(advIds2);
        }
    }

    public static Cursor read(String[] advIds) {
        synchronized (DbHelper.class) {
            String selection = "";
            for (String id : advIds) {
                selection += C_ADV_ID + "=? OR ";
            }
            selection = selection.substring(0, selection.length() - 4);

            return DbHelper.sDb.query(TABLE_NAME, COLUMNS, selection, advIds,
                    null, null, C_POSITION);
        }
    }

    public static Cursor read(
            ArrayList<String> areaCodes, ArrayList<String> areaCodesX,
            ArrayList<String> cateCodes, ArrayList<String> cateCodesX)
    {
        DbHelper.sDb.beginTransaction();

        if (areaCodes == null) {
            areaCodes = new ArrayList<>();
            areaCodes.add("%");
        }
        if (areaCodesX == null) {
            areaCodesX = new ArrayList<>();
            areaCodesX.add("42674");
        }
        if (cateCodes == null) {
            cateCodes = new ArrayList<>();
            cateCodes.add("%");
        }
        if (cateCodesX == null) {
            cateCodesX = new ArrayList<>();
            cateCodesX.add("42674");
        }

        //
        String selection = "(";
        for (String c : areaCodes) {
            selection += "(" + C_AREA_CODE + " LIKE '" + c + "') OR ";
        }
        if (areaCodes.size() > 0) {
            selection = selection.substring(0, selection.length() - 3) + "AND ";
        }
        for (String c : areaCodesX) {
            selection += "(" + C_AREA_CODE + " NOT LIKE '" + c + "') AND ";
        }
        if (areaCodesX.size() > 0) {
            selection = selection.substring(0, selection.length() - 5) + ") AND (";
        }

        for (String c : cateCodes) {
            selection += "(" + C_CATE_CODE + " LIKE '" + c + "') OR ";
        }
        if (cateCodes.size() > 0) {
            selection = selection.substring(0, selection.length() - 3) + "AND ";
        }
        for (String c : cateCodesX) {
            selection += "(" + C_CATE_CODE + " NOT LIKE '" + c + "') AND ";
        }
        if (cateCodesX.size() > 0) {
            selection = selection.substring(0, selection.length() - 5) + ")";
        }
//        MyLog.d(TAG, selection);

        //
        Cursor c = DbHelper.sDb.query(
                TABLE_NAME, COLUMNS, selection, null, null, null, C_POSITION);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        return c;
    }

    public static Cursor readSortByDist(
            ArrayList<String> cateCodes, ArrayList<String> cateCodesX)
    {
        DbHelper.sDb.beginTransaction();

        if (cateCodes == null) {
            cateCodes = new ArrayList<>();
            cateCodes.add("%");
        }
        if (cateCodesX == null) {
            cateCodesX = new ArrayList<>();
            cateCodesX.add("42674");
        }

        //
        String selection = "";
        for (String c : cateCodes) {
            selection += "(" + C_CATE_CODE + " LIKE '" + c + "') OR ";
        }
        selection = selection.substring(0, selection.length() - 3) + "AND ";
        for (String c : cateCodesX) {
            selection += "(" + C_CATE_CODE + " NOT LIKE '" + c + "') AND ";
        }
        selection = selection.substring(0, selection.length() - 5);
//        MyLog.d(TAG, selection);

        //
        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, selection, null,
                null, null, C_DISTANCE);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        return c;
    }

    public static Cursor readSlides()
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, C_ON_SLIDES + "=?",
                new String[] {"1"}, null, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }

    public static Cursor readTop()
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, C_ON_TOP + "=?",
                new String[] {"true"}, null, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }

    public static void update(DataAdv a)
    {
        DbHelper.sDb.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(C_ADV_ID, a.advId);
        cv.put(C_AREA, a.area);
        cv.put(C_AREA_CODE, a.areaCode);
        cv.put(C_AREA_PATH, a.areaPath);
        cv.put(C_ADDRESS, a.address);
        cv.put(C_BODY, a.body);
        cv.put(C_CATE_CODE, a.cateCode);
        cv.put(C_CATEGORY, a.category);
        cv.put(C_CATEGORY_PATH, a.categoryPath);
        cv.put(C_CLICK_COUNT, a.clickCount);
        cv.put(C_CODE, a.code);
        cv.put(C_CREATOR_NAME, a.creatorName);
        cv.put(C_DISPLAY_PRICE, a.displayPrice);
        cv.put(C_EXT_KEY, a.extKey);
        cv.put(C_IMG_URL, a.imgUrl);
        cv.put(C_LABEL, a.label);
        cv.put(C_LATITUDE, a.latitude);
        cv.put(C_LIST_PRICE, a.listPrice);
        cv.put(C_LONGITUDE, a.longitude);
        cv.put(C_NAME, a.name);
        cv.put(C_ON_TOP, a.onTop);
        cv.put(C_PHONE, a.phone);
        cv.put(C_START_TIME, a.startTime);
        cv.put(C_STOP_TIME, a.stopTime);
        cv.put(C_SUBJECT, a.subject);
        cv.put(C_TAGS, a.tags);
        cv.put(C_TIME, a.time);
        cv.put(C_TITLE, a.title);
        cv.put(C_URL, a.url);
        cv.put(C_VIEW_COUNT, a.viewCount);
        cv.put(C_WATCH_COUNT, a.watchCount);

        int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_ADV_ID + "=?",
                new String[] {a.advId});
        if (tmp == 0) {
            DbHelper.sDb.insert(TABLE_NAME, null, cv);
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
    static int nOnTop = 0;
    public static void update(ArrayList<DataAdv> advList, Map<String, String> cateToCode)
    {
        DbHelper.sDb.beginTransaction();

        int i = 0;
        for (DataAdv a : advList) {
            i++;
            ContentValues cv = new ContentValues();
            cv.put(C_ADV_ID, a.advId);
            cv.put(C_AREA, a.area);
            cv.put(C_AREA_CODE, a.areaCode);
            if(cateToCode != null)
                cv.put(C_CATE_CODE, cateToCode.get(a.category));
            else
                cv.put(C_CATE_CODE, a.cateCode);
            cv.put(C_CATEGORY, a.category);
            cv.put(C_CLICK_COUNT, a.clickCount);
            cv.put(C_CODE, a.code);
            long dist;
/*
            if(a.cateCode.equals("1101"))
                dist = Long.parseLong("1" + a.time);
            else
                dist = Long.parseLong(a.time);
*/
            if(a.cateCode.equals("1101"))
                dist = i - 10000;
            else
                dist = i;
            cv.put(C_DISTANCE, dist);
            cv.put(C_DISPLAY_PRICE, a.displayPrice);
            cv.put(C_EXT_KEY, a.extKey);
            cv.put(C_IMG_URL, a.imgUrl);
            cv.put(C_LABEL, a.label);
            cv.put(C_LATITUDE, a.latitude);
            cv.put(C_LIST_PRICE, a.listPrice);
            cv.put(C_LONGITUDE, a.longitude);
            cv.put(C_NAME, a.name);
            cv.put(C_ON_SLIDES, "0");
            if(false && nOnTop < 3) {
                cv.put(C_ON_TOP, "true");
                nOnTop++;
            } else
                cv.put(C_ON_TOP, a.onTop);
            cv.put(C_POSITION, a.position);
            cv.put(C_SUBJECT, a.subject);
            cv.put(C_TIME, a.time);
            cv.put(C_VIEW_COUNT, a.viewCount);

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_ADV_ID + "=?",
                    new String[] {a.advId});
            if (tmp == 0) {
                DbHelper.sDb.insert(TABLE_NAME, null, cv);
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    /**
     * Fucking important: Call this after <i>all</i> Adv data have been stored
     * in DB!
     *
     * @param advList Adv list returned from <code>/c2/advPromo</code>
     */
    public static void updatePromo(ArrayList<DataAdv> advList)
    {
        DbHelper.sDb.beginTransaction();

        for (DataAdv a : advList) {
            ContentValues cv = new ContentValues();
            cv.put(C_ADV_ID, a.advId);
            if (a.position.equals("0")) {
                cv.put(C_ON_SLIDES, "1");
            }
            if (!a.position.equals("0")) {
                cv.put(C_POSITION, a.position);
            }

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_ADV_ID + "=?",
                    new String[] {a.advId});
            if (tmp == 0) {
                throw new RuntimeException("No such Adv in DB yet: " +
                        "\n>  advId = " + a.advId +
                        "\n>  subject = " + a.subject);
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static void updateDistance(int lat, int lng)
    {
        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.execSQL("UPDATE " + TABLE_NAME + " SET " +
                C_DISTANCE + " = (" + C_LATITUDE + " - " + lat + ") * (" +
                C_LATITUDE + " - " + lat + ") + (" + C_LONGITUDE + " - " + lng +
                ") * (" + C_LONGITUDE + " - " + lng + ")");

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
    public static int getPosition(Cursor c)
    {
        String sPos = c.getString(c.getColumnIndex(DbTableAdv.C_POSITION));
        if(sPos == null || sPos.length() == 0)
            return -1;
        int nPos = -1;
        try {
            nPos = Integer.parseInt(sPos);
        } catch (NumberFormatException e) {
            nPos = -1;
        }
        return nPos;
    }
}
