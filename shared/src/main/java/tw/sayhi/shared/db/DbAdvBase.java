package tw.sayhi.shared.db;

/**
 * Created by VAIO on 2016/8/21.
 */
public class DbAdvBase {
    public static final String C_ADV_ID = "_id";
    public static final String C_AREA = "area";
    public static final String C_AREA_CODE = "area_code";
    public static final String C_AREA_PATH = "area_path"; //
    public static final String C_ADDRESS = "address"; //
    public static final String C_BODY = "body"; //
    public static final String C_CATE_CODE = "cate_code";
    public static final String C_CATEGORY = "category";
    public static final String C_CATEGORY_PATH = "category_path"; //
    public static final String C_CLICK_COUNT = "click_count";
    public static final String C_CODE = "code";
    public static final String C_CREATOR_NAME = "creator_name"; //
    public static final String C_DISPLAY_PRICE = "display_price";
    public static final String C_DISTANCE = "distance";
    public static final String C_EXT_KEY = "ext_key";
    public static final String C_IMG_URL = "img_url";
    public static final String C_LABEL = "label";
    public static final String C_LATITUDE = "latitude";
    public static final String C_LIST_PRICE = "list_price";
    public static final String C_LONGITUDE = "longitude";
    public static final String C_NAME = "name";
    public static final String C_ON_SLIDES = "on_slides";
    public static final String C_ON_TOP = "on_top";
    public static final String C_PHONE = "phone";
    public static final String C_POSITION = "position";
    public static final String C_START_TIME = "start_time"; //
    public static final String C_STOP_TIME = "stop_time"; //
    public static final String C_SUBJECT = "subject";
    public static final String C_TAGS = "tags"; //
    public static final String C_TIME = "time";
    public static final String C_TITLE = "title"; //
    public static final String C_URL = "url"; //
    public static final String C_VIEW_COUNT = "view_count";
    public static final String C_WATCH_COUNT = "watch_count"; //
    public static final String C_ORDER = "sort_order";

    public static final String[] COLUMNS = new String[] {
            C_ADV_ID,
            C_AREA,
            C_AREA_CODE,
            C_AREA_PATH,
            C_ADDRESS,
            C_BODY,
            C_CATE_CODE,
            C_CATEGORY,
            C_CATEGORY_PATH,
            C_CLICK_COUNT,
            C_CODE,
            C_CREATOR_NAME,
            C_DISPLAY_PRICE,
            C_DISTANCE,
            C_EXT_KEY,
            C_IMG_URL,
            C_LABEL,
            C_LATITUDE,
            C_LIST_PRICE,
            C_LONGITUDE,
            C_NAME,
            C_ON_SLIDES,
            C_ON_TOP,
            C_PHONE,
            C_POSITION,
            C_START_TIME,
            C_STOP_TIME,
            C_SUBJECT,
            C_TAGS,
            C_TIME,
            C_TITLE,
            C_URL,
            C_VIEW_COUNT,
            C_WATCH_COUNT,
            C_ORDER
    };
    public static final String mTableString = " (" +
            C_ADV_ID + " TEXT PRIMARY KEY, " +
            C_AREA + " TEXT, " +
            C_AREA_CODE + " TEXT, " +
            C_AREA_PATH + " TEXT, " +
            C_ADDRESS + " TEXT, " +
            C_BODY + " TEXT, " +
            C_CATE_CODE + " TEXT, " +
            C_CATEGORY + " TEXT, " +
            C_CATEGORY_PATH + " TEXT, " +
            C_CLICK_COUNT + " TEXT, " +
            C_CODE + " TEXT, " +
            C_CREATOR_NAME + " TEXT, " +
            C_DISPLAY_PRICE + " TEXT, " +
            C_DISTANCE + " INTEGER, " +
            C_EXT_KEY + " TEXT, " +
            C_IMG_URL + " TEXT, " +
            C_LABEL + " TEXT, " +
            C_LATITUDE + " TEXT, " +
            C_LIST_PRICE + " TEXT, " +
            C_LONGITUDE + " TEXT, " +
            C_NAME + " TEXT, " +
            C_ON_SLIDES + " TEXT, " +
            C_ON_TOP + " TEXT, " +
            C_PHONE + " TEXT, " +
            C_POSITION + " TEXT, " +
            C_START_TIME + " TEXT, " +
            C_STOP_TIME + " TEXT, " +
            C_SUBJECT + " TEXT, " +
            C_TAGS + " TEXT, " +
            C_TIME + " TEXT, " +
            C_TITLE + " TEXT, " +
            C_URL + " TEXT, " +
            C_VIEW_COUNT + " TEXT, " +
            C_WATCH_COUNT + " TEXT, " +
            C_ORDER + " INTEGER)";
}
