package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataHotTag;

public class DbTableHotTags
{
    public static final String C_CODE = "code";
    public static final String C_LATITUDE = "latitude";
    public static final String C_LONGITUDE = "longitude";
    public static final String C_NAME = "name";
    public static final String C_PRI_KEY = "_id";
    public static final String C_REF_COUNT = "ref_count";
    public static final String C_TAG = "tag";


    private static final String[] COLUMNS = new String[] {
            C_CODE,
            C_LATITUDE,
            C_LONGITUDE,
            C_NAME,
            C_PRI_KEY,
            C_REF_COUNT,
            C_TAG
    };

    public static final String TABLE_NAME = "hot_tags";

//    private static final String TAG = DbTableHotTags.class.getSimpleName();


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_CODE + " TEXT, " +
                C_LATITUDE + " TEXT, " +
                C_LONGITUDE + " TEXT, " +
                C_NAME + " TEXT, " +
                C_PRI_KEY + " TEXT PRIMARY KEY, " +
                C_REF_COUNT + " TEXT, " +
                C_TAG + " TEXT)");
    }

    public static void delete()
    {
        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.delete(TABLE_NAME, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static Cursor read()
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, null, null, null,
                null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }
    public static void update(ArrayList<DataHotTag> tags)
    {
        DbHelper.sDb.beginTransaction();

        for (int i = 0; i < tags.size(); i++) {
            DataHotTag tag = tags.get(i);

            ContentValues cv = new ContentValues();
            cv.put(C_CODE, tag.code);
            cv.put(C_LATITUDE, tag.latitude);
            cv.put(C_LONGITUDE, tag.longitude);
            cv.put(C_NAME, tag.name);
            cv.put(C_PRI_KEY, "" + i);
            cv.put(C_REF_COUNT, tag.refCount);
            cv.put(C_TAG, tag.tag);

            DbHelper.sDb.insert(TABLE_NAME, null, cv);
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
}
