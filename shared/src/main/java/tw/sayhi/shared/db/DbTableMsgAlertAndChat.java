package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataMsgOthers;

public class DbTableMsgAlertAndChat
{
    public static final String C_CONTENT_ID = "content_id";
    public static final String C_DATE = "date";
    public static final String C_MESSAGE = "message";
    public static final String C_MSG_ID = "msgid";
    public static final String C_NEW = "new";
    public static final String C_NOTIF = "notif";
    public static final String C_PHONE = "phone";
    public static final String C_PRIMARY_KEY = "_id";
    public static final String C_RECEIVERS = "receivers";
    public static final String C_TYPE = "type";

    private static final String[] COLUMNS = new String[] {
            C_CONTENT_ID,
            C_DATE,
            C_MESSAGE,
            C_MSG_ID,
            C_NEW,
            C_NOTIF,
            C_PHONE,
            C_PRIMARY_KEY,
            C_RECEIVERS,
            C_TYPE
    };

    public static final String TABLE_NAME = "msg_others";

//    private static final String TAG = DbTableNews.class.getSimpleName();


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_CONTENT_ID + " TEXT, " +
                C_DATE + " TEXT, " +
                C_MESSAGE + " TEXT, " +
                C_MSG_ID + " TEXT, " +
                C_NEW + " TEXT, " +
                C_NOTIF + " TEXT, " +
                C_PHONE + " TEXT, " +
                C_PRIMARY_KEY + " TEXT PRIMARY KEY, " +
                C_RECEIVERS + " TEXT, " +
                C_TYPE + " TEXT)");
    }

    public static void delete()
    {
        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.delete(TABLE_NAME, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static Cursor readAlert(String phone)
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS,
                C_PHONE + "=?", new String[] {phone},
                null, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }

    public static Cursor readChat(String convId)
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS,
                C_MSG_ID + "=?", new String[] {convId},
                null, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }
    public static void update(ArrayList<DataMsgOthers> msgList)
    {
        DbHelper.sDb.beginTransaction();

        for (DataMsgOthers d : msgList) {
            ContentValues cv = new ContentValues();
            cv.put(C_CONTENT_ID, d.contentId);
            cv.put(C_DATE, d.date);
            cv.put(C_MESSAGE, d.message);
            cv.put(C_MSG_ID, d.msgid);
            cv.put(C_NEW, d.neww);
            cv.put(C_NOTIF, d.notif);
            cv.put(C_PHONE, d.phone);
            cv.put(C_PRIMARY_KEY, d.msgid + d.contentId);
            cv.put(C_RECEIVERS, d.receivers);
            cv.put(C_TYPE, d.type);

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_PRIMARY_KEY + "=?",
                    new String[] {d.msgid + d.contentId});
            if (tmp == 0) {
                DbHelper.sDb.insert(TABLE_NAME, null, cv);
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
}
