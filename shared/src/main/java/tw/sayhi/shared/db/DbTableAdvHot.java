package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataAdv;

public class DbTableAdvHot extends DbAdvBase
{
    private static final String TABLE_NAME = "adv_hot";

    private static final String TAG = DbTableAdvHot.class.getSimpleName();


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + mTableString);
    }

    public static void delete()
    {
        synchronized (DbHelper.class) {
            DbHelper.sDb.delete(TABLE_NAME, null, null);
        }
    }

    public static Cursor read()
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, null, null, null,
                null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        return c;
    }

    public static void update(ArrayList<DataAdv> advList)
    {
        DbHelper.sDb.beginTransaction();

        for (DataAdv a : advList) {
            ContentValues cv = new ContentValues();
            cv.put(C_ADV_ID, a.advId);
            cv.put(C_AREA, a.area);
            cv.put(C_AREA_CODE, a.areaCode);
            cv.put(C_CATE_CODE, a.cateCode);
            cv.put(C_CATEGORY, a.category);
            cv.put(C_CLICK_COUNT, a.clickCount);
            cv.put(C_CODE, a.code);
            cv.put(C_DISPLAY_PRICE, a.displayPrice);
            cv.put(C_EXT_KEY, a.extKey);
            cv.put(C_IMG_URL, a.imgUrl);
            cv.put(C_LABEL, a.label);
            cv.put(C_LATITUDE, a.latitude);
            cv.put(C_LIST_PRICE, a.listPrice);
            cv.put(C_LONGITUDE, a.longitude);
            cv.put(C_NAME, a.name);
            cv.put(C_ON_TOP, a.onTop);
            cv.put(C_SUBJECT, a.subject);
            cv.put(C_TIME, a.time);
            cv.put(C_VIEW_COUNT, a.viewCount);

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_ADV_ID + "=?",
                    new String[] {a.advId});
            if (tmp == 0) {
                DbHelper.sDb.insert(TABLE_NAME, null, cv);
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
}
