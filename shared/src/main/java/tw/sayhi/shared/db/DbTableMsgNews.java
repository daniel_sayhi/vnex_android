package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataMsgNews;

public class DbTableMsgNews
{
    public static final String C_CODE = "code";
    public static final String C_CONTENT_ID = "content_id";
    public static final String C_DATE = "date";
    public static final String C_MESSAGE = "message";
    public static final String C_NAME = "name";
    public static final String C_NEWS_ID = "news_id";
    public static final String C_NOTIF = "notif";
    public static final String C_PRIMARY_KEY = "_id";
    public static final String C_SUBJECT = "subject";

    private static final String[] COLUMNS = new String[] {
            C_CONTENT_ID,
            C_DATE,
            C_MESSAGE,
            C_NAME,
            C_NEWS_ID,
            C_NOTIF,
            C_PRIMARY_KEY,
            C_SUBJECT
    };

    public static final String TABLE_NAME = "msg_news";

//    private static final String TAG = DbTableNews.class.getSimpleName();


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_CONTENT_ID + " TEXT, " +
                C_DATE + " TEXT, " +
                C_MESSAGE + " TEXT, " +
                C_NAME + " TEXT, " +
                C_NEWS_ID + " TEXT, " +
                C_NOTIF + " TEXT, " +
                C_PRIMARY_KEY + " TEXT PRIMARY KEY, " +
                C_SUBJECT + " TEXT)");
    }

    public static void delete()
    {
        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.delete(TABLE_NAME, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static Cursor read(String newsId)
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS,
                C_NEWS_ID + "=?", new String[] {newsId},
                null, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }
    public static void update(ArrayList<DataMsgNews> msgList)
    {
        DbHelper.sDb.beginTransaction();

        for (DataMsgNews d : msgList) {
            ContentValues cv = new ContentValues();
            cv.put(C_CONTENT_ID, d.contentId);
            cv.put(C_DATE, d.date);
            cv.put(C_MESSAGE, d.message);
            cv.put(C_NAME, d.name);
            cv.put(C_NEWS_ID, d.newsId);
            cv.put(C_NOTIF, d.notif);
            cv.put(C_PRIMARY_KEY, d.newsId + d.contentId);
            cv.put(C_SUBJECT, d.subject);

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_PRIMARY_KEY + "=?",
                    new String[] {d.newsId + d.contentId});
            if (tmp == 0) {
                DbHelper.sDb.insert(TABLE_NAME, null, cv);
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
}
