package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataServiceBranch;
import tw.sayhi.shared.Utils;

/* History:
    01/15, 2017, Daniel Yan
    1. Use C_CODE as primary key, instead of C_STORE_ID which has empty.
    2. Change C_LAT, C_LNG, C_DISTANCE to REAL
    3. Change the interface of updateDistance() to double.
    4. update() has too slow. Enclosed it in a transaction.
 */
/*
        "code": "4556547383623680",
        "name": "木柵木新加盟門市",
        "address": "台北市文山區木新路三段167號  ",
        "city": "台北市",
        "direct": false,
        "hours": "週一 ~ 週日 11:00~22:00",
        "phone": "02-29386010",
        "longitude": 1215183,
        "latitude": 250322
        */
public class DbTableServiceBranch
{
    public static final String C_ADDRESS = "address";
    public static final String C_CODE = "code";
    public static final String C_CITY = "city";
    public static final String C_DESC = "desc";
    public static final String C_DIRECT = "direct";
    public static final String C_DISTANCE = "distance";
    public static final String C_HOURS = "hours";
    public static final String C_ID = "id";
    public static final String C_LAT = "latitude";
    public static final String C_LNG = "longitude";
    public static final String C_NAME = "name";
    public static final String C_PHONE = "phone";
    public static final String C_PRIMARY_KEY = "_id";
    public static final String C_STORE_ID = "store_id";
    public static final String C_TITLE = "title";

    public static class Data
    {
        public final String address;
        public final String code;
        public final String city;
        public final String direct;
        public final String hours;
        public final double latitude;
        public final double longitude;
        public final String name;
        public final String phone;

        public Data(Cursor c)
        {
            address = DbHelper.getString(c, C_ADDRESS);
            code = DbHelper.getString(c, C_CODE);
            city = DbHelper.getString(c, C_CITY);
            direct = DbHelper.getString(c, C_DIRECT);
            hours = DbHelper.getString(c, C_HOURS);
            latitude = DbHelper.getDouble(c, C_LAT);
            longitude = DbHelper.getDouble(c, C_LNG);
            name = DbHelper.getString(c, C_NAME);
            phone = DbHelper.getString(c, C_PHONE);
        }
    }

    private static final String[] COLUMNS =
            new String[]
            {
                C_ADDRESS,
                    C_CITY,
                C_CODE,
                C_DESC,
                    C_DIRECT,
                C_DISTANCE,
                    C_HOURS,
                C_ID,
                C_LAT,
                C_LNG,
                C_NAME,
                C_PHONE,
                C_PRIMARY_KEY,
                C_STORE_ID,
                C_TITLE
            };

    public static final String TABLE_NAME = "service_branch";


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_PRIMARY_KEY + " TEXT PRIMARY KEY, " +
                C_ADDRESS + " TEXT, " +
                C_CITY + " TEXT, " +
                C_CODE + " TEXT, " +
                C_DESC + " TEXT, " +
                C_DIRECT + " TEXT, " +
                C_DISTANCE + " REAL, " +
                C_HOURS + " TEXT, " +
                C_ID + " TEXT, " +
                C_LAT + " REAL, " +
                C_LNG + " REAL, " +
                C_NAME + " TEXT, " +
                C_PHONE + " TEXT, " +
                C_STORE_ID + " TEXT, " +
                C_TITLE + " TEXT)");
    }

    public static void delete()
    {
        synchronized (DbHelper.class) {
            DbHelper.sDb.delete(TABLE_NAME, null, null);
        }
    }

    public static void delete(String code)
    {
        synchronized (DbHelper.class) {
            DbHelper.sDb.delete(TABLE_NAME, C_CODE + "=?",
                    new String[] {code});
        }
    }

    public static Cursor read()
    {
        DbHelper.sDb.beginTransaction();

        //
        Cursor c = DbHelper.sDb.query(
                TABLE_NAME, COLUMNS, null, null, null, null, C_DISTANCE);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
        return c;
    }
    // key can be C_CODE, C_CITY, C_NAME ...
    public static Cursor read(String key, String value)
    {
        if(Utils.isNull(value))
            return read();

        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, key + "=?",
                new String[] {value}, null, null, C_DISTANCE);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }
    public static void update(ArrayList<DataServiceBranch> branchList)
    {
        DbHelper.sDb.beginTransaction();

        for (DataServiceBranch b : branchList) {
            ContentValues cv = new ContentValues();
            cv.put(C_ADDRESS, b.address);
            cv.put(C_CITY, b.city);
            cv.put(C_CODE, b.code);
            cv.put(C_DESC, b.desc);
            cv.put(C_DIRECT, b.direct);
            cv.put(C_DISTANCE, 0);
            cv.put(C_HOURS, b.hours);
            cv.put(C_ID, b.id);
            double lat = Double.valueOf(b.lat) / 10000.0;
            double lng = Double.valueOf(b.lng) / 10000.0;
            cv.put(C_LAT, lat);
            cv.put(C_LNG, lng);
            cv.put(C_NAME, b.name);
            cv.put(C_PHONE, b.phone);
            cv.put(C_PRIMARY_KEY, b.code);
            cv.put(C_STORE_ID, b.storeId);
            cv.put(C_TITLE, b.title);

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv,
                    C_PRIMARY_KEY + "=?",
                    new String[] {b.code});
            if (tmp == 0) {
                DbHelper.sDb.insert(TABLE_NAME, null, cv);
            }
        }
        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static void updateDistance(double lat, double lng)
    {
        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.execSQL("UPDATE " + TABLE_NAME + " SET " +
                C_DISTANCE + " = (" + C_LAT + " - " + lat + ") * (" +
                C_LAT + " - " + lat + ") + (" + C_LNG + " - " + lng +
                ") * (" + C_LNG + " - " + lng + ")");

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
}
