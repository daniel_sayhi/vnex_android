package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataConvItem;
import tw.sayhi.shared.util.Pref;

public class DbTableConv
{
    public static final String C_CONTENT_ID = "content_id";
    public static final String C_DATE = "date";
    public static final String C_MESSAGE = "message";
    public static final String C_MSG_ID = "_id";
    public static final String C_NEW = "new";
    public static final String C_NOTIF = "notif";
    public static final String C_PHONE = "phone";
    public static final String C_RECEIVERS = "receivers";
    public static final String C_TYPE = "type";
    public static final int TYPE_ALERT = 0;
    public static final int TYPE_BUSINESS = 1;
    public static final int TYPE_CHAT_GRP = 2;
    public static final int TYPE_CHAT_MUL = 3;
    public static final int TYPE_NEWS = 4;


    private static final String[] COLUMNS = new String[] {
            C_CONTENT_ID,
            C_DATE,
            C_MESSAGE,
            C_MSG_ID,
            C_NEW,
            C_NOTIF,
            C_PHONE,
            C_RECEIVERS,
            C_TYPE
    };

    public static final String TABLE_NAME = "conv";

//    private static final String TAG = DbTableConv.class.getSimpleName();


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_CONTENT_ID + " TEXT, " +
                C_DATE + " TEXT, " +
                C_MESSAGE + " TEXT, " +
                C_MSG_ID + " TEXT PRIMARY KEY, " +
                C_NEW + " TEXT, " +
                C_NOTIF + " TEXT, " +
                C_PHONE + " TEXT, " +
                C_RECEIVERS + " TEXT, " +
                C_TYPE + " TEXT)");
    }

    public static void delete()
    {
        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.delete(TABLE_NAME, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static String getConvTitle(Cursor c)
    {
        String ph = c.getString(c.getColumnIndex(DbTableConv.C_PHONE));
        String recvs = c.getString(c.getColumnIndex(DbTableConv.C_RECEIVERS));

        int type = getConvType(c);
        if (type == TYPE_ALERT) {
            return ph; // E.g., 北北基
        } else if (type == TYPE_BUSINESS) {
            return ph; // E.g., 廣信數位股份有限公司
        } else if (type == TYPE_CHAT_GRP) {
            // tmp = "ph_0 ph_1 ph_me ph_2 "
            String tmp = recvs + " " + ph + " ";
            // tmp = "ph_0 ph_1 ph_2 "
            tmp = tmp.replace(Pref.getMsisdn("") + " ", "");
            // tmp = "ph_0 ph_1 ph_2"
            tmp = tmp.trim();
            // Return "ph_0、ph_1、ph_2"
            return tmp.replace(' ', '、');
        } else if (type == TYPE_CHAT_MUL) {
            String[] recvArr = recvs.split(" ");
            return recvArr[0].equals(Pref.getMsisdn("")) ? ph : recvArr[0];
        } else if (type == TYPE_NEWS) {
            return ph;
        } else {
            throw new RuntimeException("Impossible: " + type);
        }
    }

    public static int getConvType(Cursor c)
    {
        String recvs = c.getString(c.getColumnIndex(DbTableConv.C_RECEIVERS));
        String type = c.getString(c.getColumnIndex(DbTableConv.C_TYPE));

        if (type.equals("News")) {
            return TYPE_NEWS;
        } else if (type.equals("AdvAlert")) {
            return TYPE_ALERT;
        } else if (type.equals("Business")) {
            return TYPE_BUSINESS;
        } else if (type.startsWith("Normal")) {
            String[] recvArr = recvs.split(" ");
            if (recvArr.length == 1) { // One-to-one chatting
                return TYPE_CHAT_MUL;
            } else if (recvArr.length > 1) { // Group chatting
                return TYPE_CHAT_GRP;
            } else {
                throw new RuntimeException("Impossible: " + recvArr.length);
            }
        } else {
            throw new RuntimeException("Impossible: " + type);
        }
    }

    public static Cursor read()
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, null, null, null,
                null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }
    public static void update(ArrayList<DataConvItem> msgList)
    {
        DbHelper.sDb.beginTransaction();

        for (DataConvItem d : msgList) {
            ContentValues cv = new ContentValues();
            cv.put(C_CONTENT_ID, d.contentId);
            cv.put(C_DATE, d.date);
            cv.put(C_MESSAGE, d.message);
            cv.put(C_MSG_ID, d.msgid);
            cv.put(C_NEW, d.neww);
            cv.put(C_NOTIF, d.notif);
            cv.put(C_PHONE, d.phone);
            cv.put(C_RECEIVERS, d.receivers);
            cv.put(C_TYPE, d.type);

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_CONTENT_ID + "=?",
                    new String[] {d.contentId});
            if (tmp == 0) {
                DbHelper.sDb.insert(TABLE_NAME, null, cv);
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
}
