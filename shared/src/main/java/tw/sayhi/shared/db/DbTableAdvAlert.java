package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataAdvAlert;

public class DbTableAdvAlert
{
    public static final String C_AREA_CODE = "_id";
    public static final String C_CODE = "code";
    public static final String C_DESC = "desc";
    public static final String C_NAME = "name";
    public static final String C_PRICE = "price";
    public static final String C_SUBSCRIBE = "subscribe";

    private static final String[] COLUMNS = new String[] {
            C_AREA_CODE,
            C_CODE,
            C_DESC,
            C_NAME,
            C_PRICE,
            C_SUBSCRIBE
    };

    public static final String TABLE_NAME = "adv_alert";

//    private static final String TAG = DbTableNews.class.getSimpleName();


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_AREA_CODE + " TEXT PRIMARY KEY, " +
                C_CODE + " TEXT, " +
                C_DESC + " TEXT, " +
                C_NAME + " TEXT, " +
                C_PRICE + " TEXT, " +
                C_SUBSCRIBE + " TEXT)");
    }

    public static void delete()
    {
        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.delete(TABLE_NAME, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static Cursor read()
    {
        DbHelper.sDb.beginTransaction();

        Cursor c = DbHelper.sDb.query(TABLE_NAME, COLUMNS, null, null, null,
                null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();

        return c;
    }

    public static void update(ArrayList<DataAdvAlert> newsList)
    {
        DbHelper.sDb.beginTransaction();

        for (DataAdvAlert d : newsList) {
            ContentValues cv = new ContentValues();
            cv.put(C_AREA_CODE, d.areaCode);
            cv.put(C_CODE, d.code);
            cv.put(C_DESC, d.desc);
            cv.put(C_NAME, d.name);
            cv.put(C_PRICE, d.price);
            cv.put(C_SUBSCRIBE, d.subscribe);

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_AREA_CODE + "=?",
                    new String[] {d.areaCode});
            if (tmp == 0) {
                DbHelper.sDb.insert(TABLE_NAME, null, cv);
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static void update(String areaCode, boolean subscribe)
    {
        DbHelper.sDb.beginTransaction();

        ContentValues cv = new ContentValues();
        if (subscribe) {
            cv.put(C_SUBSCRIBE, "1");
        } else {
            cv.put(C_SUBSCRIBE, "0");
        }
        DbHelper.sDb.update(TABLE_NAME, cv, C_AREA_CODE + "=?",
                new String[] {areaCode});

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
}
