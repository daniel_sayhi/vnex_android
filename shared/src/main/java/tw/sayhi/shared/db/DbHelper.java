package tw.sayhi.shared.db;


import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DbHelper extends SQLiteOpenHelper
{
    public static String DB_NAME = "";
    public static int DB_VER;
    private static final String TAG = DbHelper.class.getSimpleName();


    public static SQLiteDatabase sDb;


    public static void init(Context ctx, String dbName, int dbVer)
    {
        DB_NAME = dbName;
        DB_VER = dbVer;
        if (sDb == null || !sDb.isOpen()) {
            sDb = new DbHelper(ctx, null).getWritableDatabase();
        }
    }


    public DbHelper(Context ctx, CursorFactory factory) {
        super(ctx, DB_NAME, factory, DB_VER);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        DbTableAdv._create(db);
        DbTableServiceBranch._create(db);

/*
        DbTableAdvAlert._create(db);
        DbTableAdvHot._create(db);
        DbTableArea._create(db);
        DbTableCate._create(db);
        DbTableConv._create(db);
        DbTableFriend._create(db);
        DbTableHotTags._create(db);
        DbTableMsgNews._create(db);
        DbTableMsgAlertAndChat._create(db);
        DbTableNews._create(db);
        DbTableStore._create(db);
        DbTableStoreBranch._create(db);
        DbTableStoreCate._create(db);
        DbTableStoreLocation._create(db);
        DbTableStoreTop._create(db);
*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
        Log.d(TAG, "DB upgrade: " + oldVer + " -> " + newVer);
    }

    public final static void insertRow(Cursor from, MatrixCursor to) {
        final String columns[] = from.getColumnNames(), values[] = new String[columns.length];
        final int size = columns.length;

        for (int i = 0; i < size; i++) {
            values[i] = getStringFromColumn(from, columns[i]);
        }
        to.addRow(values);

    }

    private static String getStringFromColumn(Cursor c, String columnName) {
        int index = c.getColumnIndexOrThrow(columnName);
        String value = c.getString(index);
        if (value != null && value.length() > 0) {
            return value;
        } else {
            return null;
        }
    }

    public static String getString(Cursor c, String column) {
        String str = c.getString(c.getColumnIndex(column));
        if (str == null)
            str = "";
        return str;
    }
    public static double getDouble(Cursor c, String column) {
        return c.getDouble(c.getColumnIndex(column));
    }
    public static boolean isNull(Cursor c)
    {
        if(c != null && c.getCount() > 0)
            return false;
        return true;
    }

}
