package tw.sayhi.shared.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tw.sayhi.shared.result.ResultCateAreaList;

public class DbTableArea {

    public static final String C_CODE = "_id";
    public static final String C_IMG_URL = "img_url";
    public static final String C_NAME = "name";

    private static final String[] COLUMNS =
            new String[]
            {
                C_CODE,
                C_IMG_URL,
                C_NAME
            };

    private static final String TABLE_NAME = "area";

    private static final String TAG = DbTableArea.class.getSimpleName();


    static void _create(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                C_CODE + " TEXT PRIMARY KEY, " +
                C_IMG_URL + " TEXT, " +
                C_NAME + " TEXT)");
    }

    public static void delete()
    {
        DbHelper.sDb.beginTransaction();

        DbHelper.sDb.delete(TABLE_NAME, null, null);

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }

    public static Cursor read(String[] codes)
    {
        synchronized (DbHelper.class) {
            if (codes == null) {
                codes = new String[] {"%"};
            }

            String selection = "";
            for (String c : codes) {
                selection += "(" + C_CODE + " LIKE '" + c + "') OR ";
            }
            selection = selection.substring(0, selection.length() - 4);
//            MyLog.d(TAG, selection);

            return DbHelper.sDb.query(TABLE_NAME, COLUMNS, selection, null,
                    null, null, C_CODE);
        }
    }
    public static void update(ArrayList<ResultCateAreaList.Item> areaList)
    {
        DbHelper.sDb.beginTransaction();

        areaList.add(new ResultCateAreaList.Item("$5", "", "依距離排序"));
        areaList.add(new ResultCateAreaList.Item("$6", "", "台灣"));

        for (ResultCateAreaList.Item a : areaList) {
            ContentValues cv = new ContentValues();
            cv.put(C_CODE, a.code);
            cv.put(C_IMG_URL, a.imgUrl);
            cv.put(C_NAME, a.name);

            int tmp = DbHelper.sDb.update(TABLE_NAME, cv, C_CODE + "=?",
                    new String[] {a.code});
            if (tmp == 0) {
                DbHelper.sDb.insert(TABLE_NAME, null, cv);
            }
        }

        DbHelper.sDb.setTransactionSuccessful();
        DbHelper.sDb.endTransaction();
    }
}
