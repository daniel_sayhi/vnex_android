package tw.sayhi.shared;

        import android.app.Activity;
        import android.app.ActivityManager;
        import android.app.Application;
        import android.app.Dialog;
        import android.content.ActivityNotFoundException;
        import android.content.ClipData;
        import android.content.ClipboardManager;
        import android.content.ComponentName;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.graphics.Rect;
        import android.net.Uri;
        import android.os.Build;
        import android.os.Looper;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.view.WindowManager;
        import android.view.inputmethod.InputMethodManager;
        import android.widget.ImageView;
        import android.widget.ListAdapter;
        import android.widget.ListView;
        import android.widget.TextView;
        import android.widget.Toast;

        import java.text.ParseException;
        import java.text.SimpleDateFormat;
        import java.util.Calendar;
        import java.util.Date;
        import java.util.List;
        import java.util.Locale;
        import java.util.TimeZone;
        import java.util.concurrent.CountDownLatch;

        import androidx.appcompat.app.AlertDialog;
        import tw.sayhi.shared.util.MyLog;

        import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class Utils
{
    public static boolean enableMemoryStats = true;

    private static final String TAG = Utils.class.getSimpleName();
    public static final SimpleDateFormat SDF_0 =
            new SimpleDateFormat("MM/dd HH:mm", Locale.TAIWAN);
    private static final SimpleDateFormat SDF_1 =
            new SimpleDateFormat("MM/dd", Locale.TAIWAN);
    public static final SimpleDateFormat SDF_YYYY_MM_DD =
            new SimpleDateFormat("yyyy/MM/dd", Locale.TAIWAN);
    public static final SimpleDateFormat SDF_DASH =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.TAIWAN);
    public static final SimpleDateFormat SDF_SLASH =
            new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.TAIWAN);

    public static boolean isNull(String str) {
        return str == null || str.trim().equals("") || str.trim() == null || "".equals(str.trim()) ||
                str.trim().equals("null") || str.trim().equals(" ") || str.equals("　");
    }

    public static boolean isNull(Object str) {
        return str == null || isNull(str.toString());
    }


    public static void _assert(boolean b, String message)
    {
        if(BuildConfig.DEBUG && !b) {
            MyLog.i("Utils._assert()", message);
            throw new RuntimeException();
        }
    }
    public static void _assertUIThread(boolean b, String message)
    {
        boolean isUIThread = (Looper.myLooper() == Looper.getMainLooper());
        _assert(b == isUIThread, message);
    }
    public static String getDate(String dateTime)
    {
        String[] tokens = dateTime.split(" ");
        if(tokens.length > 0) {
            return tokens[0];
        }
        return "";
    }
    public static String getDateOrTimeString(long date)
    {
        String d = SDF_0.format(date);
        String today = SDF_1.format(new java.util.Date());
        if (d.startsWith(today)) {
            d = d.substring(6);
        }
        return d;
    }

    public static String getDateOrTimeString(String date)
    {
        return getDateOrTimeString(Long.parseLong(date));
    }

    public static String getDateStringYyyyMmDd(String stamp)
    {
        long time = Long.parseLong(stamp);
        return SDF_YYYY_MM_DD.format(time);
    }
    public static long getTimeStamp(SimpleDateFormat date_format, String date_time) {
        long time_stamp = 0;
        try {
            Date date = date_format.parse(date_time);
            time_stamp = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time_stamp;
    }
    public static long getTimeStamp(String date_time) {
        long time_stamp = 0;
        try {
            Date date = SDF_YYYY_MM_DD.parse(date_time);
            time_stamp = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time_stamp;
    }
    public static boolean isDateValid(String date)
    {
        try {
            SDF_YYYY_MM_DD.setLenient(false);
            SDF_YYYY_MM_DD.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
    public static boolean isInBackground(Application app)
    {
        ActivityManager am = (ActivityManager) app.getSystemService(
                Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topAct = tasks.get(0).topActivity;
            if (!topAct.getPackageName().equals(app.getPackageName())) {
                return true;
            }
        }

        return false;
    }
    public static void sleep(int nMil)
    {
        if(nMil > 0) {
            try {
                Thread.sleep(nMil);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    private static Toast mToast;
    public static void postMessage(final Activity activity, int str_id)
    {
        postMessage(activity, activity.getString(str_id), Toast.LENGTH_LONG);
    }
    public static void postMessage(final Activity activity, final CharSequence sMessage)
    {
        postMessage(activity, sMessage, Toast.LENGTH_LONG);
    }
    public static void postMessage(final Activity activity, final CharSequence sMessage,
                                   final int nDuration)
    {
        if(Utils.isInBackground(activity.getApplication()))
            return;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mToast != null) {
                    mToast.cancel();
                }
                mToast = Toast.makeText(activity, sMessage, nDuration);
                mToast.show();
            }
        });
    }
    public static void dialogSetNetwork(final Activity activity, final Runnable runnable) {
        String sMessage = "無法使用網路。請從\"設定\"中開啟行動網路或Wi-Fi";
        dialogShow(activity, GlobalStatic.appName,
                sMessage, R.drawable.error, "前往設定",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        runnable.run();
                    }
                }, true,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                        //activity.finish();
                    }
                }
        );
    }
    public static void dialogNetworkProblem(Activity activity)
    {
        dialogFinish(activity, "網路問題", "無法上網或連線品質不佳，請稍候再試");
    }
    public static void dialogFinish(final Activity activity, CharSequence sTitle, CharSequence sMessage) {
        if(sTitle.equals(""))
            sTitle = GlobalStatic.appName;
        dialogShow(activity, sTitle, sMessage, R.drawable.error, "離開",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //activity.finish();
                        System.exit(0);
                    }
                }, false, null);
    }
    public static void dialogWarning(final Activity activity, final CharSequence sMessage) {
        dialogShow(activity, GlobalStatic.appName,
                sMessage, R.drawable.warning, "確定", null, false, null);
    }

    public static void dialogBack(final Activity activity, final CharSequence sMessage) {
        Utils.dialogShow(activity, activity.getResources().getString(R.string.app_name),
                sMessage, R.drawable.warning,
                "確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.onBackPressed();
                    }
                }, false, null);
    }

    public static void waitFor(CountDownLatch latch)
    {
        try {
            while(latch == null) {
                sleep(500);
            }
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitFor(CountDownLatch latch, int timeout)
    {
        try {
            latch.await(timeout, MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitTable(final CountDownLatch latch, final Activity act,
                                 final Runnable workerThread, final Runnable uiThread)
    {
        new Thread() {
            @Override
            public void run() {
                try {
                    latch.await();
                    if(workerThread != null)
                        workerThread.run();
                    if(uiThread != null) {
                        act.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                uiThread.run();
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static void openWebPage(Activity act, String url) {

        if(isNull(url)) return;

        Uri webpage = Uri.parse(url);

        if(!url.startsWith("http://") && !url.startsWith("https://")) {
            webpage = Uri.parse("http://" + url);
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if(intent.resolveActivity(act.getPackageManager()) != null) {
            act.startActivity(intent);
        } else {
            Utils.postMessage(act, "請安裝網路流覽器");
        }
    }

    public static class AlertDialogEx extends AlertDialog
    {
        public AlertDialogEx(Context context)
        {
            super(context);
        }

        public AlertDialogEx(Context context, int theme)
        {
            super(context, theme);
        }
    }
    public static void dialogShow(final Activity activity, final CharSequence title,
                                  final CharSequence message, final int imageId,
                                  final String posText, final DialogInterface.OnClickListener posListener,
                                  final boolean negative, final DialogInterface.OnClickListener negListener)
    {
        if(activity.isFinishing()) {
            return;
        }
        final boolean hasTitle = (title.length() > 0);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LayoutInflater li = LayoutInflater.from(activity);
                View view;
                if(hasTitle) {
                    view = li.inflate(R.layout.dialog_with_title, (ViewGroup) activity.findViewById(R.id.ll_dialog));
                    ((TextView) view.findViewById(R.id.tv_title)).setText(title);
                } else
                    view = li.inflate(R.layout.dialog_message, (ViewGroup) activity.findViewById(R.id.w_root));
                ((ImageView) view.findViewById(R.id.w_image)).setImageResource(imageId);
                ((TextView) view.findViewById(R.id.w_text)).setText(message);

                int styleId = R.style.NoTitleDialog;
                AlertDialogEx dlg = new AlertDialogEx(activity, styleId);
                dlg.setCancelable(false);
                dlg.setView(view);
                dlg.setButton(DialogInterface.BUTTON_POSITIVE, posText, posListener);
                if(negative)
                    dlg.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", negListener);
                dlg.show();
               /*
               AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                       .setCancelable(false)
                       .setView(v);
               if(title.length() > 0)
                   builder.setTitle(title);
               builder.setPositiveButton(posText, posListener);
               if(negative)
                   builder.setNegativeButton("取消", null);
               AlertDialog dlg = builder.create();
               if(title.length() > 0) {
                   TextView mTitle = (TextView)dlg.findViewById(android.R.id.title);
                   if (mTitle != null)
                       mTitle.setBackgroundResource(R.color.dialog_bg);
               }
               try {    // Caller activity may not in foreground
                   builder.show();
               } catch (WindowManager.BadTokenException e) {
                   MyLog.e(TAG, "Activity not in foreground!?");
                   e.printStackTrace();
               } */
            }
        });
        /*
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(activity)
                        //.setTitle("發生問題")
                        .setMessage(sMessage)
                        .setNegativeButton("離開",
                            new DialogInterface.OnClickListener() {
							    @Override
                                public void onClick(DialogInterface dialoginterface, int i) {
                                    activity.finish();
                                }
                            })
                        .show();
            }
        });
        */
    }
    public static void dialogOK(final Activity act, final String title, final String msg,
                                final int icon_id, final String okay)
    {
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(act.isFinishing()) {
                    return;
                }
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(act);
                builder.setTitle(title);
                builder.setMessage(msg);
                if(icon_id > 0) {
                    builder.setIcon(icon_id);
                }

                builder.setPositiveButton(okay, null);

                Dialog dialog = builder.create();

                dialog.show();
            }
        });
    }
    public static void dialogOK(final Activity act, final int title_id, final int msg_id,
                                final int icon_id, final int ok_id) {
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(act.isFinishing()) {
                    return;
                }
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(act);
                if(title_id > 0) {
                    builder.setTitle(act.getString(title_id));
                }
                String msg = act.getString(msg_id);
                builder.setMessage(msg);
                if(icon_id > 0) {
                    builder.setIcon(icon_id);
                }

                builder.setPositiveButton(act.getString(ok_id), null);

                Dialog dialog = builder.create();

                dialog.show();
            }
        });
    }
    public static void dialogWarning2(final Activity act, final String title, final String msg,
                                      final String posText, final DialogInterface.OnClickListener posListener,
                                      final boolean negative, final DialogInterface.OnClickListener negListener,
                                      final int y_offset) {
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(act);
                builder.setIcon(R.drawable.warning);
                builder.setTitle(title);
                builder.setMessage(msg);

                builder.setPositiveButton(posText, posListener);
                if(negative)
                    builder.setNegativeButton("取消", negListener);

                Dialog dialog = builder.create();
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

//                wmlp.gravity = Gravity.TOP | Gravity.LEFT;
//                wmlp.x = 100;   //x position
                wmlp.y += y_offset;   //y position

                dialog.show();
            }
        });
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public static void hideSoftInput(Activity act) {
        View view = act.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)act.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    public static void showSoftInput(Activity act, View view)
    {
        InputMethodManager imm = (InputMethodManager)act.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }
    public static void putClipboard(Activity act, String text)
    {
        if (Build.VERSION.SDK_INT >= 11) {
            // Gets a handle to the clipboard service.
            ClipboardManager clipboard = (ClipboardManager)
                    act.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("simple text", text);
            // Set the clipboard's primary clip.
            clipboard.setPrimaryClip(clip);
            postMessage(act, "訊息已複製到剪貼簿");
        }
    }
    public static String formatMemory(long memory)
    {
        float memoryInMB = memory * 1f / 1024 / 1024;
        return String.format("%.1f MB", memoryInMB);
    }
    public static long getAvailMem(Activity act)
    {
        ActivityManager activityManager = (ActivityManager)act.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }
    public static void memStats(Activity act)
    {
        memStats(act, 0);
    }
    public static void memStats(Activity act, int nIndent)
    {
        if(!enableMemoryStats)
            return;

        String indent = "";
        if(nIndent > 0)
            indent = new String(new char[nIndent]).replace('\0', ' ');

        String tag = "examineMemory";

        ActivityManager activityManager = (ActivityManager)act.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);

        MyLog.i(tag, indent + "System availMem " + formatMemory(memoryInfo.availMem));
        MyLog.i(tag, indent + "       lowMemory " + memoryInfo.lowMemory);
        //MyLog.i(tag, indent + "       threshold " + formatMemoryText(memoryInfo.threshold));

        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses =
                activityManager.getRunningAppProcesses();

        int pid = -1;
        String processName = "";
        for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
            if(runningAppProcessInfo.processName.contains("tw.sayhi.vnex")) {
                pid = runningAppProcessInfo.pid;
                processName = runningAppProcessInfo.processName;
                break;
            }
        }
        if(pid == -1) {
            MyLog.i(tag, "Can not find process ID for etag.");
        }
        int pids[] = new int[1];
        pids[0] = pid;
        android.os.Debug.MemoryInfo[] memInfo = activityManager.getProcessMemoryInfo(pids);
        MyLog.i(tag, indent + processName + " " + pid);
        MyLog.i(tag, indent + "    getTotalPrivateDirty(): " +
                formatMemory(memInfo[0].getTotalPrivateDirty() * 1024));
        MyLog.i(tag, indent + "    getTotalPss(): " +
                formatMemory(memInfo[0].getTotalPss() * 1024));
        MyLog.i(tag, indent + "    getTotalSharedDirty(): " +
                formatMemory(memInfo[0].getTotalSharedDirty() * 1024));
    }
    public static String trimURL(String url)
    {
//        int iHttp = url.indexOf("//");
        int iQuestion = url.indexOf('?');
        String sTrim = (iQuestion >= 0) ? url.substring(0, iQuestion - 1) : url;
        return sTrim;
    }
    public static Rect locateView(View v)
    {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try
        {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe)
        {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }
    public static int parseInt(String str)
    {
        int num = 0;
        try {
            num = Integer.parseInt(str);
        } catch(NumberFormatException e) {
        }
        return num;
    }
    public static long parseLong(CharSequence s)
    {
        String str = s.toString();
        long num = 0;
        try {
            num = Long.parseLong(str);
        } catch(NumberFormatException e) {
        }
        return num;
    }
    public static float parseFloat(CharSequence s)
    {
        String str = s.toString();
        float num = 0;
        try {
            num = Float.parseFloat(str);
        } catch(NumberFormatException e) {
        }
        return num;
    }
/*
    public static double parseDouble(CharSequence s)
    {
        String str = s.toString();
        double num = 0;
        try {
            num = Double.parseDouble(str);
        } catch(NumberFormatException e) {
        }
        return num;
    }
*/
    public static String scaleAmount(String amount, float scale)
    {
        Float d_amount = parseFloat(amount) * scale;
        return d_amount.toString();
    }
    public static boolean inArray(String[] array, String search)
    {
        for(String item : array) {
            if(search.equals(item))
                return true;
        }
        return false;
    }
    // Date Format: YYYY/MM/DD
    public static boolean getYearMonthDay(String date, int[] nums)
    {
        String[] tokens = date.split("/");
        if(tokens.length < 3)
            return false;
        String year = tokens[0];
        String month = tokens[1];
        String day = tokens[2];
        if(year.length() != 4 || month.length() != 2 || day.length() != 2)
            return false;
        nums[0] = Utils.parseInt(year);
        nums[1] = Utils.parseInt(month);
        nums[2] = Utils.parseInt(day);
        return true;
    }
    // Birthday must be at least, say, 18 years
    public static boolean isValidBirthday(String birthdate, int min_age)
    {
        int[] birth_nums = new int[3];
        int[] curr_nums = new int[3];
        if(!getYearMonthDay(birthdate, birth_nums))
            return false;
        String curr_date = SDF_YYYY_MM_DD.format(System.currentTimeMillis());
        if(!getYearMonthDay(curr_date, curr_nums))
            return false;
        int year_diff = curr_nums[0] - birth_nums[0];
        if(year_diff > min_age)
            return true;
        else if(year_diff < min_age)
            return false;
        // of the same age, check month
        if(curr_nums[1] > birth_nums[1])
            return true;
        else if(curr_nums[1] < birth_nums[1])
            return false;
        // check day
        if(curr_nums[2] >= birth_nums[2])
            return true;
        else return false;
    }
    // Date must be greater than current date
    public static boolean isDateEffective(String date)
    {
        int[] date_nums = new int[3];
        int[] curr_nums = new int[3];
        if(!getYearMonthDay(date, date_nums))
            return false;
        String curr_date = SDF_YYYY_MM_DD.format(System.currentTimeMillis());
        if(!getYearMonthDay(curr_date, curr_nums))
            return false;
        if(date_nums[0] > curr_nums[0])
            return true;
        else if(date_nums[0] < curr_nums[0])
            return false;
        if(date_nums[1] > curr_nums[1])
            return true;
        else if(date_nums[1] < curr_nums[1])
            return false;
        if(date_nums[2] > curr_nums[2])
            return true;
        else if(date_nums[2] < curr_nums[2])
            return false;
        return false; // the same day
    }
    public static void _setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();

        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }

            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
    public static boolean nonNull(String str) {
        return !isNull(str);
    }
    public static boolean isAppRunning(final Context context, final String packageName)
    {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null)
        {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }
}
