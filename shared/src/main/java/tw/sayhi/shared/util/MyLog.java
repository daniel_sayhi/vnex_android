package tw.sayhi.shared.util;

import android.app.Activity;
import android.util.Log;

import java.util.ArrayList;


public class MyLog
{
    private static final String TAG = MyLog.class.getSimpleName();
    private static final boolean ENABLE_LOG = true;
    private static final int MSG_LEN_LIMIT = 2000;

    public static String getTag()
    {
        StackTraceElement[] traces = new Exception().getStackTrace();
        int index = traces.length - 1;
        String fullClassName,className;
        for(; index >= 0; index--) {
            fullClassName = traces[index].getClassName();
            className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
            if(className.equals(TAG))
                break;
        }
        index++;
        fullClassName = traces[index].getClassName();
        className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = traces[index].getMethodName();
        int lineNumber = traces[index].getLineNumber();

        return(className + "." + methodName + "():" + lineNumber);
    }
    public static void d(String tag, String msg)
    {
        d(msg);
    }
    public static void d(String msg)
    {
        if (ENABLE_LOG) {
            if (msg.length() > MSG_LEN_LIMIT) {
                Log.d(getTag(), msg.substring(0, MSG_LEN_LIMIT));
                MyLog.d(getTag(), msg.substring(MSG_LEN_LIMIT));
            } else {
                Log.d(getTag(), msg);
            }
        }
    }
    public static void e(String tag, String msg)
    {
        e(msg);
    }
    public static void e(String msg)
    {
        if (ENABLE_LOG) {
            if (msg.length() > MSG_LEN_LIMIT) {
                Log.e(getTag(), msg.substring(0, MSG_LEN_LIMIT));
                MyLog.e(getTag(), msg.substring(MSG_LEN_LIMIT));
            } else {
                Log.e(getTag(), msg);
            }
        }
    }
    public static void i(String tag, String msg)
    {
        i(msg);
    }
    public static void i() { i(""); }
    public static void i(String msg)
    {
        if (ENABLE_LOG) {
            if (msg.length() > MSG_LEN_LIMIT) {
                Log.i(getTag(), msg.substring(0, MSG_LEN_LIMIT));
                MyLog.i(getTag(), msg.substring(MSG_LEN_LIMIT));
            } else {
                Log.i(getTag(), msg);
            }
        }
    }
    public static void v(String tag, String msg)
    {
        v(msg);
    }
    public static void v(String msg)
    {
        if (ENABLE_LOG) {
            if (msg.length() > MSG_LEN_LIMIT) {
                Log.v(getTag(), msg.substring(0, MSG_LEN_LIMIT));
                MyLog.v(getTag(), msg.substring(MSG_LEN_LIMIT));
            } else {
                Log.v(getTag(), msg);
            }
        }
    }
    // Daniel Yan : May 26, 2016
    // Add timing profiler.
    public static class timeProfile
    {
        long mStartTime, mPrevTime;
        public timeProfile(long start) {
            mStartTime = start;
            mPrevTime = start;
        }
    }
    public static final ArrayList<timeProfile> aTimeProfile = new ArrayList<>(50);
    /** Typical usage
     * 1. MyLog.timeStart(getTag());
     *    JobsToProfile();
     *    MyLog.timeEnd("JobsToProfile()");
     *
     * 2. MyLog.timeStart(getTag());
     *    Job1();
     *    MyLog.timePrint("Job1()");
     *    Job2();
     *    MyLog.timePrint("Job2()");
     *    Job3();
     *    MyLog.timeEnd("Job1 + Job2 + Job3");
     *
     * 3. For multi-threaded situation
     *    Thread 1:
     *        int i = MyLog.timeStart(getTag());
     *        JobsToProfile();
     *        MyLog.timeEnd(i, "Thread 1");
     *    Thread 2:
     *        int j = MyLog.timeStart(getTag());
     *        JobsToProfile();
     *        MyLog.timeEnd(j, "Thread 2");
     */
    public static int timeStart(String tag)
    {
        return timeStart();
    }
    public static int timeStart()
    {
        if(ENABLE_LOG) {
            synchronized (aTimeProfile) {
                timeProfile prof = new timeProfile(System.currentTimeMillis());
                aTimeProfile.add(prof);
                return (aTimeProfile.size() - 1);
            }
        }
        return -1;
    }
    public static void timePrint(String msg)
    {
        timePrint(aTimeProfile.size() - 1, msg);
    }
    public static void timePrint(int t)
    {
        timePrint(t, "");
    }
    public static void timePrint(int i, String msg)
    {
        if(ENABLE_LOG) {
            synchronized (aTimeProfile) {
                long currTime = System.currentTimeMillis();
                timeProfile prof = aTimeProfile.get(i);
                printTime(msg, currTime - prof.mPrevTime);
                prof.mPrevTime = currTime;
            }
        }
    }
    public static void printTime(String msg, long elapseMillis)
    {
        if(ENABLE_LOG) {
            String header = "";
            float elapseSeconds = (float) elapseMillis / 1000;
            float elapseMinutes = elapseSeconds / 60;
            if (elapseMinutes > 5)
                header = String.format("%.2f mins in ", elapseMinutes);
            else
                header = String.format("%.2f secs in ", elapseSeconds);

            Log.i(getTag(), header + msg);
        }
    }
    public static void timeEnd(String msg)
    {
        timeEnd(aTimeProfile.size() - 1, msg);
    }
    public static void timeEnd(int t)
    {
        timeEnd(t, "");
    }
    public static void timeEnd(int i, String msg)
    {
        if(ENABLE_LOG) {
            synchronized (aTimeProfile) {
                long currTime = System.currentTimeMillis();
                timeProfile prof = aTimeProfile.get(i);
                printTime(msg, currTime - prof.mStartTime);
                prof.mPrevTime = currTime;
                prof.mStartTime = currTime;
            }
        }
    }
    public static void memStats(Activity act, String msg)
    {
        if(false && ENABLE_LOG) {
            String className = act.getComponentName().getClassName();
            String simpleClassName = "";
            int dot = className.lastIndexOf('.');
            if (dot != -1) {
                simpleClassName = className.substring(dot + 1);
            }

            Log.i(simpleClassName, msg);
            // Utils.memStats(act, 4);
        }
    }
}
