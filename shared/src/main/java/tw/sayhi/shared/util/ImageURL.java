package tw.sayhi.shared.util;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;
import java.util.concurrent.CountDownLatch;

import tw.sayhi.shared.R;
import tw.sayhi.shared.Utils;

public class ImageURL
{
    public static CountDownLatch latchInitImageLoader = new CountDownLatch(1);
    @SuppressWarnings("deprecation")
    public static final DisplayImageOptions DISPLAY_IMG_OPTS =
            new DisplayImageOptions.Builder()
                    .showStubImage(R.drawable.zzz__dummy)
                    .showImageForEmptyUri(R.drawable.zzz__dummy)
                    .showImageOnFail(R.drawable.zzz__dummy)
//                    .cacheInMemory()
//                    .cacheOnDisc()
//                    .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();
    public static final DisplayImageOptions OPTIONS_WITH_CACHE =
            new DisplayImageOptions.Builder()
                    .showStubImage(R.drawable.zzz__dummy)
                    .showImageForEmptyUri(R.drawable.zzz__dummy)
                    .showImageOnFail(R.drawable.zzz__dummy)
                    // No cache as we always want the most updated image from EEC Server
                    .cacheInMemory(true)
                    .cacheOnDisc(true)
//                    .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();
    public static void init(final Activity act, final CountDownLatch latch)
    {
        new Thread() {
            @Override
            public void run() {
                int i = MyLog.timeStart();
                ImageLoader.getInstance().init(
                        new ImageLoaderConfiguration.Builder(act)
                                .threadPoolSize(5)
                                .threadPriority(Thread.MAX_PRIORITY)
                                .denyCacheImageMultipleSizesInMemory()
                                //.discCacheFileNameGenerator(new Md5FileNameGenerator())
//                            .tasksProcessingOrder(QueueProcessingType.LIFO)
                                //.enableLogging() // Not necessary in common
                                .build());
                latch.countDown();
                latchInitImageLoader.countDown();
//                MyLog.timeEnd(i, "ImageLoader.init()");
            }
        }.start();
    }
    public static void loadInCache(String imgURL)
    {
        ImageLoader.getInstance().loadImage(imgURL, new SimpleImageLoadingListener());
    }
    public static void loadInCache(String imgURL, final Runnable runnable)
    {
        ImageLoader.getInstance().loadImage(imgURL, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                runnable.run();
            }
        });
    }
    public static void load(String imgURL, ImageView iv, final Activity act)
    {
        load(imgURL, iv, act, null, false);
    }
    public static void load(String imgURL, ImageView iv, final Activity act, final Runnable postThread, boolean useCache)
    {
        final DisplayImageOptions options = (useCache ? OPTIONS_WITH_CACHE : DISPLAY_IMG_OPTS);
        ImageLoader.getInstance().displayImage(imgURL, iv, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imgUri, View v) {
            }
            @Override
            public void onLoadingFailed(String imgUri, View v, FailReason failReason) {
            }
            @Override
            @SuppressWarnings("deprecation")
            public void onLoadingComplete(String imgUri, View v, Bitmap img) {
                final ImageView iv = (ImageView)v;
                final Bitmap bitmap = img;
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        iv.setImageBitmap(bitmap);
                        if(postThread != null)
                            postThread.run();
                    }
                });
            }
            @Override
            public void onLoadingCancelled(String imgUri, View v) {
            }
        });
    }
    public static void loadAdjustDimen(String imgUrl, ImageView iv, final Activity act)
    {
        ImageLoader.getInstance().displayImage(imgUrl, iv, DISPLAY_IMG_OPTS, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imgUri, View v) {
                    }

                    @Override
                    public void onLoadingFailed(String imgUri, View v,
                                                FailReason failReason) {
                    }

                    @Override
                    @SuppressWarnings("deprecation")
                    public void onLoadingComplete(String imgUri, View v,
                                                  Bitmap img) {

                        int w = (int) DevInfo.dipToPix(img.getWidth());
                        int h = (int) DevInfo.dipToPix(img.getHeight());

                        Display d = act.getWindowManager().getDefaultDisplay();
                        int screenWid = d.getWidth();
                        if (w > screenWid) {
                            float tmp = (float) screenWid / (float) w;
                            w = screenWid;
                            h = (int) (h * tmp);
                        }
                        ((ImageView) v).setImageBitmap(
                                Bitmap.createScaledBitmap(img, w, h, false));
                    }

                    @Override
                    public void onLoadingCancelled(String imgUri, View v) {
                    }
                }
        );
    }
    public static boolean loadImage(Activity act, String path_name, ImageView iv)
    {
        File imgFile = new  File(path_name);

        if(imgFile.exists()) {
            iv.setImageURI(Uri.fromFile(imgFile));
            /*
            String abs_path = imgFile.getAbsolutePath();
            Bitmap myBitmap = BitmapFactory.decodeFile(abs_path);
            Utils.postMessage(act, "w=" + myBitmap.getWidth() + " h=" + myBitmap.getHeight());
            iv.setImageBitmap(myBitmap); */
            return true;
        }
        return false;
    }
}
