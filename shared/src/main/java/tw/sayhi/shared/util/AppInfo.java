package tw.sayhi.shared.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;

import java.util.Locale;

import tw.sayhi.shared.Utils;

public class AppInfo
{
    public static boolean isLocationEnabled = false;
    public static String getVersionName(Context context)
    {
        String version = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }
    public static String getCurrentLauguage()
    {
        //获取系统当前使用的语言
        String mCurrentLanguage = Locale.getDefault().getLanguage();
        //设置成简体中文的时候，getLanguage()返回的是zh
        return mCurrentLanguage;
    }
    public static String getCurrentLauguageUseResources(Context context)
    {
        /**
         * 获得当前系统语言
         */
        Locale locale = context.getResources().getConfiguration().locale;
        String language = locale.getLanguage(); // 获得语言码
        return language;
    }
/*
    public static Location getLocation(Activity act)
    {
        double lat, lng;
        int t = MyLog.timeStart();
        Location location = Locator.getLocation();
        MyLog.timeEnd(t, "Locator.getLocation()");
        if (location == null) {
            MyLog.e("Cannot get location.");
            Utils.postMessage(act, "請至手機「設定」開啟「位置服務」，以查詢相關資訊！");
            lat = Pref.getLastLatitude();
            lng = Pref.getLastLongitude();

            location = new Location("");
            location.setLatitude(lat);
            location.setLongitude(lng);
        } else {
            isLocationEnabled = true;
            lat = location.getLatitude();
            lng = location.getLongitude();
            Pref.saveLastLatLng(lat, lng);
        }
        return location;
    } */
}
