package tw.sayhi.shared.util;

        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.os.AsyncTask;
        import android.widget.ImageView;

        import java.io.InputStream;
        import java.util.concurrent.CountDownLatch;

public class ImageAsyncTask extends AsyncTask<String, Void, Bitmap>
{
    private static final String TAG = ImageAsyncTask.class.getSimpleName();

    ImageView bmImage;
    CountDownLatch latchImageLoaded;

    public ImageAsyncTask(ImageView bmImage, CountDownLatch latch) {
        this.bmImage = bmImage;
        this.latchImageLoaded = latch;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap bitmap = null;
        try {
            int t = MyLog.timeStart(TAG);
            InputStream in = new java.net.URL(urldisplay).openStream();
            bitmap = BitmapFactory.decodeStream(in);
            MyLog.e(TAG, "------");
            MyLog.timeEnd(t, "ImageAsyncTask()" + urldisplay);
        } catch (Exception e) {
			MyLog.e("Error", e.getMessage());
            MyLog.i(TAG, urldisplay);
            e.printStackTrace();
        }
        if(latchImageLoaded != null)
            latchImageLoaded.countDown();
        return bitmap;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }
}
