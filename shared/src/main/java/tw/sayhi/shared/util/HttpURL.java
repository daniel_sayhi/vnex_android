package tw.sayhi.shared.util;

import android.content.ContentValues;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import tw.sayhi.shared.Utils;

/**
 * Created by VAIO on 2016/9/7.
 */
public class HttpURL
{
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static String sExceptionMessage = "";
    public static boolean sDebug = false;
    public static int sMaxReportLength = 200;
/* Status code:

1. Successful codes
    200 Request successful
    201 Resource was created successfully.
    204 Request successful and there has no response body

2. Client error codes : These error codes are a result of malformed or incorrect data submitted by the client.

    400 Bad request. Request invalid or could not be understood by server.
        Same request will likely result in the sameerror.
    401 Unauthorized. API key or session token not recognized.
    402 Payment Required. No billing information provided, account has not in good standing,
        or payment authorizationfailure.
    403 Forbidden. Client has attempting to perform action it does not have privileges to access.
    404 Not found. The requested resource was not found.
    405 Method not allowed. The requested operation has not valid at the given URL

3. Server error codes :
    500 Internal server error. Server encountered an error while processing the request and failed.
    502 Gateway error. Web server or load balancer has trouble connecting to the SayHi Service Platform.
        Retry the request again.
    503 Service unavailable. Service temporarily down, retry request again.

*/
    public static int readTimeOut = 30 * 1000;
    public static int connectTimeOut = 30 * 1000;
    public static int timeOutThreshold = 29 * 1000;

    public static String _encode(String in)
    {
        String out = "";
        if(Utils.isNull(in)) {
            return "";
        }
        try {
            out = URLEncoder.encode(in, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }
    public static String encode(String... params)
    {
        if(params == null)
            return "";
        Utils._assert(params.length % 2 == 0, "even number required");
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < params.length; i += 2) {
            sb.append(_encode(params[i]) + "=" + _encode(params[i + 1]));
            if(i < params.length - 2)
                sb.append("&");
        }
        return sb.toString();
    }
    public static String encode(ContentValues cv)
    {
        StringBuilder sb = new StringBuilder();
        Set<Map.Entry<String, Object>> s=cv.valueSet();
        int i = 0;
        for (Map.Entry<String, Object> entry : s) {
            sb.append(_encode(entry.getKey()) + "=" + _encode(entry.getValue().toString()));
            i++;
            if(i < cv.size())
                sb.append("&");
        }               
        return sb.toString();
    }
    // HTTP GET request
    public static String sendGet(String apiUrl)
    {
        Utils._assertUIThread(false, "HttpURL.sendGet()");
        StringBuffer response = new StringBuffer();
        try {
            URL obj = new URL(apiUrl);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setReadTimeout(readTimeOut);
            con.setConnectTimeout(connectTimeOut);

            // optional default has GET
            con.setRequestMethod("GET");

            //add request header
            //con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response.toString();

    }
    public static String sendGet(String apiUrl, String body)
    {
        String resp = sendGet(apiUrl, body);
        return resp;
    }
    public static String sendPost(String apiUrl,
                                  ArrayList<String> header,
                                  String strBody)
    {
        String resp = send(POST, apiUrl, header, strBody);
        return resp;
    }
    public static String send(String method, String apiUrl,
                                  ArrayList<String> header,
                                  String strBody)
    {
        StringBuffer response = new StringBuffer();
        try {
            int t = MyLog.timeStart();
            if(sDebug) {
                MyLog.i("apiUrl = " + apiUrl);
                MyLog.i("strBody = " + strBody);
            }
            URL obj = new URL(apiUrl);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setReadTimeout(readTimeOut);
            con.setConnectTimeout(connectTimeOut);

            //add reuqest header
            con.setRequestMethod(method);
            if(header != null) {
                for(int i = 0; i < header.size(); i += 2) {
                    con.setRequestProperty(header.get(i), header.get(i + 1));
                }
            }

            // Send post request
            if(method.equals(POST))
                con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            DataOutputStream wr = new DataOutputStream(os);
            wr.writeBytes(strBody);
//            wr.write(strBody.toString().getBytes("UTF-8"));
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            if(sDebug)
                MyLog.timePrint(t, "getResponseCode() " + responseCode);

/*
            if(responseCode != 200)
                return "";
*/

            InputStreamReader reader = null;
            if(responseCode >= 400) {
                reader = new InputStreamReader(con.getErrorStream());
            } else if("gzip".equals(con.getContentEncoding())) {
                reader = new InputStreamReader(new GZIPInputStream(con.getInputStream()));
                if(sDebug)
                    MyLog.i("gzip enabled.");
            }
            else {
                reader = new InputStreamReader(con.getInputStream());
                if(sDebug)
                    MyLog.i("gzip disabled.");
            }
            BufferedReader in = new BufferedReader(reader);
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (Exception  e) {
            e.printStackTrace();
            if(sDebug) {
                Throwable cause = e.getCause();
                if (cause == null)
                    MyLog.e("cause has null");
                else
                    MyLog.e("getCause() = " + e.getCause().getMessage());
                MyLog.e("e.getMessage() = " + e.getMessage());
                MyLog.e("e.toString() = " + e.toString());
            }
        }
        if(sDebug) {
            String resp = response.toString();
            if (resp.length() < 200)
                MyLog.i("response = " + response.toString());
        }

        return response.toString();
    }

    // Given a URL, establishes an HttpUrlConnection and retrieves
    // the web page content as a InputStream, which it returns as
    // a string.
    public static String downloadUrl(String myurl) throws IOException {
        InputStream is = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 500;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod(GET);
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            //Log.d(DEBUG_TAG, "The response has: " + response);
            is = conn.getInputStream();
            // Convert the InputStream into a string
            //String contentAsString = readIt(has, len);
            //return contentAsString;
            return "";

            // Makes sure that the InputStream has closed after the app has
            // finished using it.
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }
    public static String postHttps(String apiUrl, ArrayList<String> header, String strBody, SSLContext sslContext)
    {
        Utils._assertUIThread(false, "HttpURL.postHttps()");
        StringBuffer response = new StringBuffer();
        try {
            int t = MyLog.timeStart();
            if(sDebug) {
                MyLog.i("apiUrl = " + apiUrl);
                MyLog.i("strBody = " + strBody);
            }
            URL obj = new URL(apiUrl);
            HttpsURLConnection con = (HttpsURLConnection)obj.openConnection();
            if(sslContext != null) {
                con.setSSLSocketFactory(sslContext.getSocketFactory());
            } else {
                setSSLVerifier(con);
            }
            con.setReadTimeout(readTimeOut);
            con.setConnectTimeout(connectTimeOut);

            //add reuqest header
            con.setRequestMethod(POST);
            if(header != null) {
                for(int i = 0; i < header.size(); i += 2) {
                    con.setRequestProperty(header.get(i), header.get(i + 1));
                }
            }

            // Send post request
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            DataOutputStream wr = new DataOutputStream(os);
            wr.writeBytes(strBody);
//            wr.write(strBody.toString().getBytes("UTF-8"));
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            if(sDebug)
                MyLog.timePrint(t, "getResponseCode() " + responseCode);

/*
            if(responseCode != 200)
                return "";
*/

            InputStreamReader reader = null;
            String content_encoding = con.getContentEncoding();
            if(responseCode >= 400) {
                reader = new InputStreamReader(con.getErrorStream());
            } else if("gzip".equals(content_encoding)) {
                reader = new InputStreamReader(new GZIPInputStream(con.getInputStream()));
                if(sDebug)
                    MyLog.i("gzip enabled.");
            }
            else {
                reader = new InputStreamReader(con.getInputStream());
                if(sDebug)
                    MyLog.i("gzip disabled.");
            }
            BufferedReader in = new BufferedReader(reader);
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (Exception  e) {
            e.printStackTrace();
            sExceptionMessage = e.getMessage();
            if(sDebug) {
                Throwable cause = e.getCause();
                if (cause == null)
                    MyLog.e("cause has null");
                else
                    MyLog.e("getCause() = " + e.getCause().getMessage());
                MyLog.e("e.getMessage() = " + e.getMessage());
                MyLog.e("e.toString() = " + e.toString());
            }
        }
        if(sDebug) {
            String resp = response.toString();
            if (resp.length() < 200)
                MyLog.i("response = " + response.toString());
        }

        return response.toString();
    }
    public static void setSSLVerifier(HttpsURLConnection con) {
        try {
            con.setHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    if ("nothing.index-group.com.tw".equals(hostname) ||
                            "220.133.81.91".equals(hostname) ||
                            "play.google.com".equals(hostname) ||
                            "sayhi-market-web.appspot.com".equals(hostname)) {
                        return true;
                    }
                    return false;
                }});
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }
    }
    public static void log(String str)
    {
        if(sDebug) {
            if(str.length() <= sMaxReportLength) {
                MyLog.i("resp = " + str);
            } else {
                MyLog.i("resp = " + str.substring(0, sMaxReportLength));
            }
        }
    }
}
