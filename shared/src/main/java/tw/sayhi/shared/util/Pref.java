package tw.sayhi.shared.util;

        import android.content.Context;
        import android.content.SharedPreferences;

        import tw.sayhi.shared.Utils;

public class Pref {
    private static final String KEY_CODE = "code";
    private static final String KEY_FAV = "fav";
    private static final String KEY_GCM_ID = "gcm_id";
    private static final String KEY_INVITE_URL = "inviteUrl";
    private static final String KEY_NEXT_ADV_REFRESH_TIME = "next_adv_refresh_time";
    private static final String KEY_NEXT_STORE_REFRESH_TIME = "next_store_refresh_time";
    private static final String KEY_MSISDN = "msisdn";
    private static final String KEY_AREA_INDEX = "area_index";
    private static final String PREF_NAME = "pref";
    private static final String VISITED_ADV = "visited_adv";
    private static final String VISITED_STORE = "visited_store";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    public static final String LANGUAGE = "language";
    public static final String ORDER_PAID = "order_paid";
    private static final String DELETED_EUSO_ORDER = "deleted_euso_order";

//    private static final String TAG = Pref.class.getSimpleName();

    private static SharedPreferences sPref;
    private static String sSession = "";

    public static String get(String key) {
        return sPref.getString(key, "");
    }

    public static boolean has(String key) {
        return(!Utils.isNull(sPref.getString(key, "")));
    }
    public static int getInt(String key) {
        return sPref.getInt(key, -1);
    }

    public static String getCode(String defValue) {
        return sPref.getString(KEY_CODE, defValue);
    }

    public static String getFav(String defValue) {
        return sPref.getString(KEY_FAV, defValue);
    }

    public static String getGcmId(String defValue)
    {
        return sPref.getString(KEY_GCM_ID, defValue);
    }
    public static int getAreaIndex() { return sPref.getInt(KEY_AREA_INDEX, 0); }

    public static String getInviteUrl(String defValue) {
        return sPref.getString(KEY_INVITE_URL, defValue);
    }

    public static String getLanguage(String defValue) {
        return sPref.getString(LANGUAGE, defValue);
    }

    public static String getMsisdn(String defValue) {
        return sPref.getString(KEY_MSISDN, defValue);
    }

    public static long getNextAdvRefreshTime(long defValue) {
        return sPref.getLong(KEY_NEXT_ADV_REFRESH_TIME, defValue);
    }

    public static long getNextStoreRefreshTime(long defValue) {
        return sPref.getLong(KEY_NEXT_STORE_REFRESH_TIME, defValue);
    }

    public static String getSession() {
        return sSession;
    }

    public static void init(Context ctx) {
        sPref = ctx.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        sPref.edit().putString(VISITED_ADV, " ");
        sPref.edit().putString(VISITED_STORE, " ");
        sPref.edit().putString(DELETED_EUSO_ORDER, " ");
        sPref.edit().putString(ORDER_PAID, " ");
    }

    public static boolean setCode(String code) {
        return sPref.edit().putString(KEY_CODE, code).commit();
    }

    public static boolean set(String key, String value) {
        if(value == null) {
            value = "";
        }
        return sPref.edit().putString(key, value).commit();
    }

    public static boolean setInt(String key, int value) {
        return sPref.edit().putInt(key, value).commit();
    }

    public static boolean setFav(String code) {
        return sPref.edit().putString(KEY_FAV, code).commit();
    }

    public static boolean setGcmId(String id)
    {
        return sPref.edit().putString(KEY_GCM_ID, id).commit();
    }

    public static boolean setAreaIndex(int index) {
        return sPref.edit().putInt(KEY_AREA_INDEX, index).commit();
    }

    public static boolean setInviteUrl(String code) {
        return sPref.edit().putString(KEY_INVITE_URL, code).commit();
    }

    public static boolean setLanguage(String code) {
        return sPref.edit().putString(LANGUAGE, code).commit();
    }

    public static boolean setMsisdn(String msisdn) {
        return sPref.edit().putString(KEY_MSISDN, msisdn).commit();
    }

    public static boolean setNextAdvRefreshTime(long t) {
        return sPref.edit().putLong(KEY_NEXT_ADV_REFRESH_TIME, t).commit();
    }

    public static boolean setNextStoreRefreshTime(long t) {
        return sPref.edit().putLong(KEY_NEXT_STORE_REFRESH_TIME, t).commit();
    }

    public static void setSession(String sess) {
        sSession = sess;
    }

    public static void addOrderPaid(String euso_order)
    {
        set(ORDER_PAID + euso_order, "1");
    }
    public static boolean isOrderPaid(String euso_order)
    {
        return has(ORDER_PAID + euso_order);
    }
    public static void addVisitedAdv(String id)
    {
        if(isVisitedAdv(id))
            return;
        String visited = sPref.getString(VISITED_ADV, "");
        sPref.edit().putString(VISITED_ADV, visited + id + " ").commit();
    }
    public static boolean isVisitedAdv(String id)
    {
        String visited = sPref.getString(VISITED_ADV, "");
        return visited.contains(id + " ");
    }
    public static void addVisitedStore(String id)
    {
        if(isVisitedStore(id))
            return;
        String visited = sPref.getString(VISITED_STORE, "");
        sPref.edit().putString(VISITED_STORE, visited + id + " ").commit();
    }
    public static boolean isVisitedStore(String id)
    {
        String visited = sPref.getString(VISITED_STORE, "");
        return visited.contains(" " + id + " ");
    }
    public static void saveLastLatLng(double lat, double lng)
    {
        sPref.edit().putFloat(LATITUDE, (float)lat).commit();
        sPref.edit().putFloat(LONGITUDE, (float)lng).commit();
    }
    public static double getLastLatitude()
    {
        return (double)sPref.getFloat(LATITUDE, (float)25.047908);
    }
    public static double getLastLongitude()
    {
        return (double)sPref.getFloat(LONGITUDE, (float)121.517315);
    }
}
