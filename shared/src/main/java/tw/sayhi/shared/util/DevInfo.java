package tw.sayhi.shared.util;

        import android.app.Activity;
        import android.content.Context;
        import android.content.pm.PackageInfo;
        import android.content.pm.PackageManager;
        import android.content.res.Resources;
        import android.net.ConnectivityManager;
        import android.net.NetworkInfo;
        import android.os.Build;
        import android.provider.Settings;
        import android.util.DisplayMetrics;

        import java.util.Locale;
        import java.util.UUID;

        import tw.sayhi.shared.Utils;


public class DevInfo {

    private static final String TAG = DevInfo.class.getSimpleName();


    private static Context sCtx;
    private static String sImei;


    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     *
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float dipToPix(float dp){
        Resources resources = sCtx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }
    public static String getImei()
    {
        if(!Utils.isNull(sImei))
            return sImei;
        sImei = UUID.randomUUID().toString();
        if(!Utils.isNull(sImei))
            return sImei;
        // ANDROID_ID should be replaced by Google Advertising ID.
        sImei = Settings.Secure.getString(sCtx.getContentResolver(), Settings.Secure.ANDROID_ID);
        if(!Utils.isNull(sImei) && sImei.startsWith("000000"))
            sImei = null;
        return sImei;
        // According to https://developer.android.com/training/articles/user-data-ids.html,
        // #1: Avoid using hardware identifiers.
        /*
        TelephonyManager tm = (TelephonyManager) sCtx.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null && Build.VERSION.SDK_INT < 23)
        {
            sImei = tm.getDeviceId();
            if (sImei != null && sImei.startsWith("000000"))
                sImei = null;
        }
        */
        // Albeit MAC has globally unique, it's not recommended to use it.
        // In addition, it needs ACCESS_WIFI_STAT permission
        /*
        if(Utils.isNull(sImei)) {
            WifiManager wm = (WifiManager) sCtx.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (wm != null && requestPermission(activity, Manifest.permission.ACCESS_WIFI_STATE))
            {
                sImei = wm.getConnectionInfo().getMacAddress();
            }
        }
        */
    }
/*
    public static String getImei(Activity activity)
    {
        if (sImei != null) {
            return sImei;
        }

        Log.i(TAG, "Trying to get IMEI [1]...");
        TelephonyManager tm = (TelephonyManager) sCtx.getSystemService(
                Context.TELEPHONY_SERVICE);
        if (tm != null && requestPermission(activity, Manifest.permission.READ_PHONE_STATE))
        {
            sImei = tm.getDeviceId();
            if (sImei != null && sImei.startsWith("000000")) {
                sImei = null;
            }
        }

        if (TextUtils.isEmpty(sImei)) {
            Log.i(TAG, "Trying to get IMEI [2]...");
            sImei = Settings.Secure.getString(sCtx.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            if (sImei != null && sImei.startsWith("000000")) {
                sImei = null;
            }
        }

        if (TextUtils.isEmpty(sImei)) {
            Log.i(TAG, "Trying to get IMEI [3]...");
            sImei = UUID.randomUUID().toString();
        }

        if (TextUtils.isEmpty(sImei)) {
            Log.i(TAG, "Trying to get IMEI [4]...");
            WifiManager wm = (WifiManager) sCtx.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (wm != null && requestPermission(activity, Manifest.permission.ACCESS_WIFI_STATE))
            {
                sImei = wm.getConnectionInfo().getMacAddress();
            }
        }

        if (TextUtils.isEmpty(sImei)) {
            Log.e(TAG, "Failed to get IMEI!");
            return null;
        } else {
            Log.d(TAG, "Got IMEI: " + sImei);
            return sImei;
        }
    }
*/

    public static String getLocale() {
        return Locale.getDefault().toString();
    }

    public static String getVersionName() {
        try {
            PackageInfo pkgInfo = sCtx.getPackageManager().getPackageInfo(
                    sCtx.getPackageName(), 0);
            return pkgInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) sCtx.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isConnected()) {
            return ni.isConnected();
        } else {
            return false;
        }
    }

    public static void init(Context ctx) {
        sCtx = ctx;
    }
}
