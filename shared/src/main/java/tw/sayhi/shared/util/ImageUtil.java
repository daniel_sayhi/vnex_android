package tw.sayhi.shared.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.widget.ImageView;

import java.io.File;

//import rapid.decoder.BitmapDecoder;
//    https://github.com/suckgamony/RapidDecoder
public class ImageUtil
{
    // Scale preserving aspect
    // suppose width > height
    public static Bitmap scalePreserveAspect(Bitmap originalImage, int width, int height)
    {
        Bitmap background = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        float originalWidth = originalImage.getWidth();
        float originalHeight = originalImage.getHeight();

        Canvas canvas = new Canvas(background);

        float scale = width / originalWidth;
        float xTranslation = 0.0f;
        float yTranslation = (height - originalHeight * scale) / 2.0f;

        float aspect_old = (originalWidth / originalHeight);
        float aspect_new = ((float)width / (float)height);

        if(aspect_new > aspect_old) { // fit Y axis
            scale = height / originalHeight;
            xTranslation = (width - originalWidth * scale) / 2.0f;
            yTranslation = 0.0f;
        } else { // fit X
            scale = width / originalWidth;
            xTranslation = 0.0f;
            yTranslation = (height - originalHeight * scale) / 2.0f;
        }
        Matrix transformation = new Matrix();
        transformation.postTranslate(xTranslation, yTranslation);
        transformation.preScale(scale, scale);

        Paint paint = new Paint();
        paint.setFilterBitmap(true);

        canvas.drawBitmap(originalImage, transformation, paint);

        return background;
    }
    // Decodes bitmap from file
/*
    public static Bitmap decodeFile(String path_name, int width, int height) {
        Bitmap bitmap = BitmapDecoder.from(path_name).
                scale(width, height).
                decode();
        return bitmap;
    }
*/
    public static Bitmap createScaledBitmap(Bitmap bitmap,int newWidth,int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, bitmap.getConfig());

        float scaleX = newWidth / (float) bitmap.getWidth();
        float scaleY = newHeight / (float) bitmap.getHeight();

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX, scaleY, 0, 0);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setFilterBitmap(true);
        canvas.drawBitmap(bitmap, 0, 0, paint);

        return scaledBitmap;

    }
    public static Bitmap resize(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float ratioX = newWidth / (float) bitmap.getWidth();
        float ratioY = newHeight / (float) bitmap.getHeight();
        float middleX = newWidth / 2.0f;
        float middleY = newHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;

    }
    // Rotate
    public static Bitmap rotate(Bitmap bitmap, int degrees)
    {
        Matrix matrix = new Matrix();

        matrix.postRotate(degrees);

        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        return rotatedBitmap;
    }
    // Make bitmap landscape
    public static Bitmap landscape(Bitmap bitmap)
    {
        if(bitmap.getWidth() > bitmap.getHeight())
            return bitmap;
        return rotate(bitmap, 270);
    }
    public static boolean loadImageFile(String path_name, ImageView iv)
    {
        File imgFile = new File(path_name);

        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            iv.setImageBitmap(myBitmap);

            return true;

        }
        return false;
    }
}
