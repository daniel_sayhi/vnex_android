package tw.sayhi.shared;

        import android.Manifest;
        import android.app.Activity;
        import android.app.ActivityOptions;
        import android.app.AlarmManager;
        import android.app.Application;
        import android.app.PendingIntent;
        import android.content.Context;
        import android.content.Intent;
        import android.content.pm.PackageManager;
        import android.location.Location;
        import android.location.LocationManager;
        import android.os.Build;

        import com.google.android.gms.analytics.GoogleAnalytics;
//        import com.google.android.gms.vision.Tracker;

        import androidx.core.app.ActivityCompat;
        import tw.sayhi.shared.util.MyLog;

public class AppCtx extends Application
{
    private static final String TAG = AppCtx.class.getSimpleName();
    public static long STORE_REFRESH_INTERVAL = 2 * 60 * 60 * 1000; // 2 hours
    public static long ADV_REFRESH_INTERVAL = 10 * 60 * 1000; // 10 minutes

//    private static Tracker mTracker;

    protected static AppCtx instance;

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    /*
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(GlobalStatic.GA_account); // Grace's account
            mTracker.enableAdvertisingIdCollection(true);
        }
        return mTracker;
    } */

    public static void startActivity(Activity act, Intent i)
    {
        if (Build.VERSION.SDK_INT >= 21) {
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(act);
            act.startActivity(i, options.toBundle());
        } else {
            act.startActivity(i);
        }
    }
    public static void suicide()
    {
        // Finish all activities before exit() to avoid system from
        // auto-restarting application immediately.
        MyLog.d(TAG, "Activities to finish:");
/*
        Enumeration<Activity> al = sActiveActivities.elements();
        while (al.hasMoreElements()) {
            Activity a = al.nextElement();
            a.finish();
            MyLog.d(TAG, "\t" + a);
        }
*/

        // Totally kill application, including background processes.
        MyLog.i(TAG, "###################################################\n" +
                "# SayHi totally closed, see you again :)          #\n" +
                "###################################################");
        System.exit(0);
    }

    public static void restart(Context ctx, Class act)
    {
        // Register the first activity to restart from.
        PendingIntent pi = PendingIntent.getActivity(ctx, 5566,
                new Intent(ctx, act),
                PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) ctx.getSystemService(
                Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 200, pi);

        suicide();
    }
    public static Location getLocation(Activity act) {
        int t = MyLog.timeStart(TAG);
        if (ActivityCompat.checkSelfPermission(act, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(act, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        LocationManager lm = (LocationManager)act.getSystemService(LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        MyLog.timeEnd(t, "Locator.getLocation()");
        if (location == null) {
            MyLog.e(TAG, "Cannot get location :(");
            if(act != null) {
                Utils.postMessage(act, "請至手機「設定」開啟「位置服務」，以查詢相關資訊！");
            }
            return null;
        }
        return location;
    }
}
