package tw.sayhi.shared;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import tw.sayhi.shared.result.ResultAdvAlert;
import tw.sayhi.shared.result.ResultConv;
import tw.sayhi.shared.result.ResultFriend;
import tw.sayhi.shared.result.ResultHotTags;
import tw.sayhi.shared.result.ResultMboxRead;
import tw.sayhi.shared.result.ResultNewsList;
import tw.sayhi.shared.result.ResultNewsRead;
import tw.sayhi.shared.result.ResultProfileGet;
import tw.sayhi.shared.util.HttpURL;
import tw.sayhi.shared.result.ResultAdvList;
import tw.sayhi.shared.result.ResultAdvView;
import tw.sayhi.shared.result.ResultBase;
import tw.sayhi.shared.result.ResultCateAreaList;
import tw.sayhi.shared.result.ResultAuth;
import tw.sayhi.shared.result.ResultCityList;
import tw.sayhi.shared.result.ResultEmbedReg;
import tw.sayhi.shared.result.ResultSearch;
import tw.sayhi.shared.result.ResultServiceBranches;
import tw.sayhi.shared.result.ResultStoreBranches;
import tw.sayhi.shared.result.ResultStoreCouponList;
import tw.sayhi.shared.result.ResultStoreList;
import tw.sayhi.shared.result.ResultStoreLocations;
import tw.sayhi.shared.util.MyLog;

import static java.util.Arrays.asList;


@SuppressWarnings("deprecation")
public class Api
{
    public static final String GPLAY_URL = "https://play.google.com/store/apps/details?id=";
    public static final String GPLAY_URL_ETAG = GPLAY_URL + "tw.sayhi.etag";
    public static final String GPLAY_URL_HSRC = GPLAY_URL + "tw.sayhi.hsrc";
    public static final String GPLAY_URL_SAYHI = GPLAY_URL + "tw.sayhi.myMessenger";
    public static final String GPLAY_URL_FETLC = GPLAY_URL + "tw.sayhi.fetlc";
    public static final String GPLAY_URL_FETIF = GPLAY_URL + "tw.sayhi.fetif";
    public static final String GPLAY_URL_FETSMB = GPLAY_URL + "tw.sayhi.fetsmb";
    public static final String GPLAY_URL_FETIFIN = GPLAY_URL + "tw.sayhi.fetifin";
    public static final String GPLAY_URL_FETIFVI = GPLAY_URL + "tw.sayhi.fetifvi";
    public static final String SAYHI_APP_URL = "http://www.sayhi.tw/ss/d/";
    public static final String API_KEY_YOUTUBE =
            "AIzaSyCkL0OETh9_oA3sOCs3ZA2GDibEvfqomYc";
    public static final String GCM_SENDER_ID = "773573997694";
    private static final String API_URL_PREFIX =
//            "https://sayhicloud-11.appspot.com/c" :
            "https://sayhi-201.appspot.com/c";
    public static final String SESSION = "session";
    private static final String TAG = Api.class.getSimpleName();

    private static String sendGet(String apiUrl, String... params)
            throws IOException
    {
        String body = HttpURL.encode(params);
        return HttpURL.sendGet(apiUrl + "?" + body);
    }

    private static String sendPost(String apiUrl, String body)
            throws IOException
    {
        return HttpURL.sendPost(apiUrl, null, body);
    }
    public static ResultAdvAlert advAlert(String session)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", session);

        String resp = sendPost(API_URL_PREFIX + "2/advalert", body);
//        MyLog.d(TAG, resp);

        return new ResultAdvAlert(resp);
    }
    public static ResultAdvAlert advAlert(
            String session, String area, boolean subscribe)
            throws IOException, JSONException {
        String subscribe_id = (subscribe ? "1" : "0");
        String body = HttpURL.encode("Area", area,
                "Subscribe", subscribe_id,
                "session", session);

        String resp = sendPost(API_URL_PREFIX + "2/advalert", body);

        return new ResultAdvAlert(resp);
    }

    public static ResultCateAreaList advCateArea()
            throws IOException, JSONException
    {
        String body = HttpURL.encode("Level", "3");

        String resp = sendPost(API_URL_PREFIX + "2/advCateArea", body);

        return new ResultCateAreaList(resp);
    }
    public static ResultBase advContact(
            String sess, String contentId, String msg)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", sess,
                "ContentId", contentId,
                "Message", msg);

        String resp = sendPost(API_URL_PREFIX + "2/advContact", body);

        return new ResultBase(resp);
    }
    public static ResultAdvList advHot(
            String sess, String page, String pageSize)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", sess,
                "Page", page,
                "PageSize", pageSize);

        String resp = sendPost(API_URL_PREFIX + "2/advHot", body);
//        MyLog.d(TAG, resp);

        return new ResultAdvList(resp);
    }
    public static String advList(String cate, String page,
                                 String pageSize/*, String session*/)
            throws IOException, JSONException {
        ContentValues cv = new ContentValues();
        if(!Utils.isNull(cate)) {
            cv.put("Cate", cate);
        }
        cv.put("Page", page);
        cv.put("PageSize", pageSize);
        String body = HttpURL.encode(cv);

        String resp = sendPost(API_URL_PREFIX + "2/advList", body);

        return resp;
        //MyLog.timePrint(j, " http");
        //ResultAdvList resultAdvList = new ResultAdvList(resp);

        //MyLog.timePrint(j, " parse");

        //return resultAdvList;
    }
    public static ResultAdvList advPromo()
            throws IOException, JSONException
    {
        String resp = sendGet(API_URL_PREFIX + "2/advPromo");

        return new ResultAdvList(resp);
    }
    public static ResultAdvList advPromo(String sess)
            throws IOException, JSONException
    {
        String resp = sendGet(API_URL_PREFIX + "2/advPromo", SESSION, sess);

        return new ResultAdvList(resp);
    }
    public static ResultAdvList advTop()
            throws IOException, JSONException {

        String resp = sendPost(API_URL_PREFIX + "2/advTop", "");

        return new ResultAdvList(resp);
    }
    public static ResultAdvView advView(String advId, String session)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("ContentId", advId, SESSION, session);

        String resp = sendPost(API_URL_PREFIX + "2/advView", body);

        return new ResultAdvView(resp);
    }

    /**
     *
     * @param appId "sayhi", "etag", or "hsrc"
     * @param c2dmId ID of GCM (Google Cloud Messaging)
     * @param code The secret code gotten after registration
     * @param imei Mobile IMEI
     * @param msisdn MSISDN gotten after registration
     * @param ver E.g., "Android_3.1.4"
     *
     * @return :)
     *
     * @throws IOException
     * @throws JSONException
     */
    public static ResultAuth auth(String appId, String c2dmId, String code,
                                  String imei, String msisdn, String ver)
            throws IOException, JSONException
    {
        String body = HttpURL.encode(
                "appId", appId,
                "Brand", android.os.Build.BRAND,
                "C2DM", c2dmId,
                "Code", code,
                "IMEI", imei,
                "Model", android.os.Build.MODEL,
                "MSISDN", msisdn,
                "Ver", ver,
                "language", Locale.getDefault().toString());

        String resp = sendPost(API_URL_PREFIX + "2/auth", body);

        return new ResultAuth(resp);
    }
    public static ResultBase chat(
            String sess, ArrayList<String> recvrs, String msg)
            throws IOException, JSONException {
        ContentValues cv = new ContentValues();
        cv.put("session", sess);
        for (String recvr : recvrs) {
            cv.put("Phone", recvr);
        }
        cv.put("Message", msg);

        String body = HttpURL.encode(cv);

        String resp = sendPost(API_URL_PREFIX + "2/chat", body);
//        MyLog.d(TAG, resp);

        // TODO
        return new ResultBase(resp);
    }
    public static ResultBase chat(String sess, String convId, String msg)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", sess,
                "Reply", convId, "Message", msg);

        String resp = sendPost(API_URL_PREFIX + "2/chat", body);

        return new ResultBase(resp);
    }
    public static ResultConv conv(String item, String session)
            throws IOException, JSONException {
        String body = HttpURL.encode("Item", item, "session", session);

        String resp = sendPost(API_URL_PREFIX + "2/conv", body);

        return new ResultConv(resp);
    }
    // NOTE: input to msisdn has actually a mobile phone number
    public static ResultBase register(
            String sess, String msisdn, String c2dmId, String imei, String ver)
            throws IOException, JSONException
    {
        String body = HttpURL.encode(
            "session", sess,
            "MSISDN", msisdn,
            "C2DM", c2dmId,
            "IMEI", imei,
            "Ver", ver,
            "Brand", android.os.Build.BRAND,
            "Model", android.os.Build.MODEL,
            "language", Locale.getDefault().toString());

        String resp = sendPost(API_URL_PREFIX + "2/register", body);

        return new ResultBase(resp);
    }
    /**
     *
     * @param appId "sayhi", "etag", or "hsrc"
     * @param devId Google advertising ID
     * @param imei Mobile IMEI
     * @param ver E.g., "Android_3.1.4"
     *
     * @return :)
     *
     * @throws IOException
     * @throws JSONException
     */
    public static ResultEmbedReg embedReg_org(
            String appId, String devId, String imei, String ver)
            throws IOException, JSONException
    {
        String body = HttpURL.encode(
                "IMEI", imei,
                "Ver", ver,
                "deviceId", devId,
                "language", Locale.getDefault().toString());

        String resp = sendPost(
                API_URL_PREFIX + "2/embedReg?appId=" + appId, body);

//        MyLog.d(TAG, resp);
        return new ResultEmbedReg(resp);
    }
    public static ResultEmbedReg embedReg(
            String appId, String devId, String imei, String ver)
            throws IOException, JSONException
    {
        String body = HttpURL.encode(
                "IMEI", imei,
                "Ver", ver,
                "deviceId", appId + "_" + devId,
                "language", Locale.getDefault().toString());

        String resp = sendPost(
                API_URL_PREFIX + "2/embedReg?appId=" + appId, body);

//        MyLog.d(TAG, resp);
        return new ResultEmbedReg(resp);
    }

    public static ResultStoreList etagStoreList(String sess)
            throws IOException, JSONException
    {
        String body = HttpURL.encode(SESSION, sess);

        String resp = sendPost(
                API_URL_PREFIX + "2/etag/store/list", body);
//        MyLog.d(TAG, resp);

        return new ResultStoreList(resp);
    }

    public static ResultStoreList etagStorePage(String sess, int page, int page_size)
            throws IOException, JSONException
    {
        String body = HttpURL.encode(SESSION, sess,
                "Page", "" + page,
                "PageSize", "" + page_size);

        String resp = sendPost(
                API_URL_PREFIX + "2/etag/store/list", body);
//        MyLog.d(TAG, resp);

        return new ResultStoreList(resp);
    }
    public static ResultStoreList etagStoreTop(String sess)
            throws IOException, JSONException
    {
        String body = HttpURL.encode(SESSION, sess);

        String resp = sendPost(API_URL_PREFIX + "2/etag/store/top", body);
//        String resp = FSHttpURL.sendPost(API_URL_PREFIX + "2/etag/store/top", null, body);
//        MyLog.d(TAG, resp);

        return new ResultStoreList(resp);
    }
    public static ResultFriend friend(String sess, ArrayList<String> phones)
            throws IOException, JSONException {
        ContentValues cv = new ContentValues();
        cv.put("session", sess);
        for (String ph : phones) {
            cv.put("Phone", ph);
        }
        String body = HttpURL.encode(cv);

        String resp = sendPost(API_URL_PREFIX + "2/friend", body);
//        MyLog.d(TAG, resp);

        return new ResultFriend(resp);
    }
    public static ResultHotTags hotTags(String session, String count,
                                        String latitude, String longitude)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", session,
                "count", count,
                "latitude", latitude,
                "longitude", longitude);

        String resp = sendGet(API_URL_PREFIX + "2/hotTags?" + body);

        return new ResultHotTags(resp);
    }
    /**
     * Delete multiple conversations.
     *
     * @param sess   :)
     * @param convId The ID of conversation to delete
     * @return :)
     * @throws IOException
     * @throws JSONException
     */
    public static ResultBase mbox(String sess, String convId)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", sess,
                "Remove", "1",
                "Reply", convId);

        String resp = sendPost(API_URL_PREFIX + "2/mbox", body);
//        MyLog.d(TAG, resp);

        return new ResultBase(resp);
    }
    public static ResultMboxRead mboxRead(String session, String convId)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", session,
                "Reply", convId,
                "Item", "100");

        String resp = sendPost(API_URL_PREFIX + "2/mboxRead", body);
//        MyLog.d(TAG, resp);

        return new ResultMboxRead(resp);
    }
    public static ResultBase multi(
            String sess, String recvr, String msg)
            throws IOException, JSONException
    {
        String body = HttpURL.encode(SESSION, sess,
                "Phone", recvr,
                "Message", msg);

        String resp = sendPost(API_URL_PREFIX + "2/multi", body);

        return new ResultBase(resp);
    }
    public static ResultBase multi(
            String sess, ArrayList<String> recvrs, String msg)
            throws IOException, JSONException {
        ContentValues cv = new ContentValues();
        cv.put("session", sess);
        for (String recvr : recvrs) {
            cv.put("Phone", recvr);
        }
        cv.put("Message", msg);
        String body = HttpURL.encode(cv);

        String resp = sendPost(API_URL_PREFIX + "2/multi", body);
//        MyLog.d(TAG, resp);

        return new ResultBase(resp);
    }
    public static ResultBase multiReply(String sess, String convId, String msg)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", sess,
                "Reply", convId,
                "Message", msg);

        String resp = sendPost(API_URL_PREFIX + "2/multiReply", body);

        return new ResultBase(resp);
    }
    /**
     * Subscribe or un-subscribe a <b>News</b> according to <code>newsId</code>.
     *
     * @param session   :)
     * @param newsId    The ID of the <b>News</b> to subscribe or un-subscribe
     * @param subscribe Use <code>true</code> to subscribe, and vice versa.
     * @return
     * @throws IOException
     * @throws JSONException
     */
    public static ResultBase news(
            String session, String newsId, boolean subscribe)
            throws IOException, JSONException {
        String subscribe_id = (subscribe ? "1" : "0");
        String body = HttpURL.encode("session", session,
                "News", newsId,
                "Subscribe", subscribe_id);

        String resp = sendPost(API_URL_PREFIX + "2/news", body);

        return new ResultBase(resp);
    }
    public static ResultNewsList newsList(String session)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", session);

        String resp = sendPost(API_URL_PREFIX + "2/newsList", body);
//        MyLog.d(TAG, resp);

        return new ResultNewsList(resp);
    }
    public static ResultNewsRead newsRead(String session, String convId)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", session,
                "News", convId,
                "Item", "10");

        String resp = sendPost(API_URL_PREFIX + "2/newsRead", body);

        return new ResultNewsRead(resp);
    }
    public static ResultProfileGet profile(String sess)
            throws IOException, JSONException {

        String resp = sendGet(API_URL_PREFIX + "2/profile", "session", sess);

        return new ResultProfileGet(resp);
    }
    public static ResultBase profile(String sess, String nickname)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", sess, "nickname", nickname);

        String resp = sendPost(API_URL_PREFIX + "2/profile", body);

//        MyLog.d(TAG, resp);
        return new ResultBase(resp);
    }
    public static ResultSearch search(String sess, String keywords, String tag)
            throws IOException, JSONException {
        ContentValues cv = new ContentValues();
        cv.put("session", sess);
        cv.put("index", "1");
        cv.put("s", keywords);
        if (tag != null) {
            cv.put("tag", tag);
        }
        String body = HttpURL.encode(cv);

        String resp = HttpURL.sendGet(API_URL_PREFIX + "2/search?" + body);
//        MyLog.d(TAG, resp);

        return new ResultSearch(resp);
    }
    public static ResultSearch searchEtagStore(String sess, String keywords)
            throws IOException, JSONException
    {
        String body = HttpURL.encode(SESSION, sess, "s", keywords);
        String resp = HttpURL.sendGet(API_URL_PREFIX + "2/search?" + body + "%20UserStore%20ETC");

        return new ResultSearch(resp);
    }

    public static ResultStoreBranches storeBranches(String sess, String storeId)
            throws IOException, JSONException
    {
        String resp = sendGet(API_URL_PREFIX + "2/store/branches/" + storeId, SESSION, sess);

        return new ResultStoreBranches(resp);
    }

    public static ResultStoreCouponList storeCouponList(String sess, String id)
            throws IOException, JSONException
    {
        String resp = sendGet(API_URL_PREFIX + "2/storeCouponList", "id", id, SESSION, sess);

        return new ResultStoreCouponList(resp);
    }
    public static ResultStoreLocations storeLocations(String sess)
            throws IOException, JSONException
    {
        String body = HttpURL.encode(SESSION, sess);

        String resp = sendPost(API_URL_PREFIX + "2/store/locations", body);

        return new ResultStoreLocations(resp);
    }
    public static ResultStoreList storeList(String session)
            throws IOException, JSONException {
        int j = MyLog.timeStart("Api.storeList()");
        String body = HttpURL.encode("session", session);

        String resp = sendPost(API_URL_PREFIX + "2/store/list", body);
//        MyLog.d(TAG, resp);

        MyLog.timeEnd(j, " http");
        ResultStoreList resultStoreList = new ResultStoreList(resp);
        MyLog.timePrint(j, " parse");

        return resultStoreList;
    }
    public static ResultStoreList storePage(String session, String page)
            throws IOException, JSONException
    {
        int j = MyLog.timeStart("Api.storePage()");
        String body = HttpURL.encode("session", session);

        String resp = sendPost(API_URL_PREFIX + "2/store/list/" + page, body);
//        MyLog.d(TAG, resp);

        MyLog.timePrint(j, " http");
        ResultStoreList resultStoreList = new ResultStoreList(resp);
        MyLog.timePrint(j, " parse");

        return resultStoreList;
    }
    public static ResultStoreList storeTop(String sess)
            throws IOException, JSONException {
        String body = HttpURL.encode("session", sess);

        String resp = sendPost(API_URL_PREFIX + "2/store/top", body);
//        MyLog.d(TAG, resp);

        return new ResultStoreList(resp);
    }
    public static ResultAdvList userAdvList(String session, String key)
            throws IOException, JSONException
    {
        String body = HttpURL.encode("key", key);
        // to speed up, load AdvList before there has a session
//        SESSION, session));

        String resp = sendPost(API_URL_PREFIX + "2/userAdvList", body);

        return new ResultAdvList(resp);
    }

    // GET /c2/fet/store/list/{offset}/{limit}
    public static ResultServiceBranches serviceBranches(String session, int offset, int limit)
            throws IOException, JSONException
    {
        String url_suffix = String.format("2/fet/store/list/%d/%d", offset, limit);

        String resp = sendGet(API_URL_PREFIX + url_suffix, SESSION, session);

        return new ResultServiceBranches(resp);
    }

    // GET /c2/fet/store/list/{city-name}}
    public static ResultServiceBranches serviceBranches(String session, String city)
            throws IOException, JSONException
    {
        String url_suffix = String.format("2/fet/store/list/%s", city);

        String resp = sendGet(API_URL_PREFIX + url_suffix, SESSION, session);

        return new ResultServiceBranches(resp);
    }
    // GET /c2/fet/city/list
    public static ResultCityList cityList(String session)
            throws IOException, JSONException
    {
        String resp = sendGet(API_URL_PREFIX + "2/fet/city/list", SESSION, session);

        return new ResultCityList(resp);
    }
    public static String getGoogleAdId(Context context) {
        Utils._assertUIThread(false, "getGoogleAdId()");
        String adId = "";
        try {
            AdvertisingIdClient.Info info = AdvertisingIdClient.getAdvertisingIdInfo(context);
            if(info != null && !Utils.isNull(info.getId())) {
                adId = info.getId();
            }
//            MyLog.e("getAdvertisingIdInfo()=" + adId);
        } catch (GooglePlayServicesNotAvailableException |
                GooglePlayServicesRepairableException | IOException e) {
            e.printStackTrace();
            MyLog.e("getAdvertisingIdInfo() exception");
        }
        return adId;
    }
    public static String getGPlayHtml(String full_url)
            throws IOException, JSONException {
        String resp = HttpURL.sendGet(full_url);
        return resp;
    }
    public static String getUrl(String url)
    {
        ArrayList<String> header = new ArrayList<>();
        header.add("Accept-Encoding");
        header.add("gzip");
        header.add("User-Agent");
        header.add("gzip");
        String resp = HttpURL.postHttps(url, header, "", null);
        return resp;
    }
}
