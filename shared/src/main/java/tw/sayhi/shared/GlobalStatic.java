package tw.sayhi.shared;

/**
 * Created by user on 2017/4/21.
 */

public class GlobalStatic
{
    public static String appId;
    public static String appName;
    public static String apkName;
    public static String imsi;
    public static String imei;
    public static String android_id;
    public static String uuid;
    public static String sGoogleAdId;
    public static boolean isInitialized = false;
    public static boolean isEmulator;

    public static int drawableLogo;
    public static int colorPrimary;
    public static String GA_account;
}
