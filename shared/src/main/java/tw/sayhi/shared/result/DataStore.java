package tw.sayhi.shared.result;


public class DataStore
{
    public final String advCount;
    public final String area;
    public final String areaCodes;
    public final String category;
    public final String categoryCode;
    public final String clickCount;
    public final String dealDesc;
    public final String desc;
    public final String expiry;
    public final String imageUrl;
    public final String latitude;
    public final String linkUrl;
    public final String longitude;
    public final String mainAddress;
    public final String mainPhone;
    public final String nickName;
    public final String storeId;
    public final String storeUrl; 
    public final String title; 
    public final String updated;
    
    
    public DataStore(String advCount, String area, String areaCodes,
                     String category, String categoryCode, String clickCount,
                     String dealDesc, String desc, String expiry,
                     String imageUrl, String latitude,
                     String linkUrl, String longitude, String mainAddress,
                     String mainPhone, String nickName, String storeId, String storeUrl,
                     String title, String updated)
    {
        this.advCount = advCount;
        this.area = area;
        this.areaCodes = areaCodes;
        this.category = category;
        this.categoryCode = categoryCode;
        this.clickCount = clickCount;
        this.dealDesc = dealDesc;
        this.desc = desc;
        this.expiry = expiry;
        this.imageUrl = imageUrl;
        this.latitude = latitude;
        this.linkUrl = linkUrl;
        this.longitude = longitude;
        this.mainAddress = mainAddress;
        this.mainPhone = mainPhone;
        this.nickName = nickName;
        this.storeId = storeId;
        this.storeUrl = storeUrl;
        this.title = title;
        this.updated = updated;
    }
}

