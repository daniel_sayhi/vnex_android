package tw.sayhi.shared.result;


public class DataMsgOthers
{
    public final String contentId;
    public final String date;
    public final String message;
    public final String msgid;
    public final String neww;
    public final String notif;
    public final String phone;
    public final String receivers;
    public final String type;


    public DataMsgOthers(
            String contentId, String date, String message, String msgid,
            String neww, String notif, String phone, String receivers,
            String type)
    {
        this.contentId = contentId;
        this.date = date;
        this.message = message;
        this.msgid = msgid;
        this.neww = neww;
        this.notif = notif;
        this.phone = phone;
        this.receivers = receivers;
        this.type = type;
    }
}
