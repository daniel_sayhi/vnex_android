package tw.sayhi.shared.result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by user on 2017/1/18.
 */

public class ResultCityList extends ResultBase
{
    public final ArrayList<String> cityList = new ArrayList<>();

    public ResultCityList(String resp) throws JSONException
    {
        super(resp);

        JSONArray jsonArrAdv = jsonObj.optJSONArray("cities");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                String code = obj.optString("code");
                String name = obj.optString("name");
                cityList.add(name);
            }
        }
    }

}
