package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataAdv;
import tw.sayhi.shared.result.ResultBase;


public class ResultStoreCouponList extends ResultBase {

//    private static final String TAG = ResultSearch.class.getSimpleName();


    public final ArrayList<String> advIdList = new ArrayList<>();
    public final ArrayList<DataAdv> advList = new ArrayList<>();


    public ResultStoreCouponList(String resp) throws JSONException {
        super(resp);

        JSONArray jsonArrAdv = jsonObj.optJSONArray("advs");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                DataAdv pad = new DataAdv(obj);
                advIdList.add(pad.advId);
                advList.add(pad);
            }
        }
    }
}
