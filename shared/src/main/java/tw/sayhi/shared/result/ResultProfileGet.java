package tw.sayhi.shared.result;


import org.json.JSONException;

import tw.sayhi.shared.result.ResultBase;


public class ResultProfileGet extends ResultBase
{
//    private static final String TAG = ResultConv.class.getSimpleName();


    public final String bonus;
    public final String credit;
    public final String nickname;


    public ResultProfileGet(String resp) throws JSONException
    {
        super(resp);

        bonus = jsonObj.optString("bonus");
        credit = jsonObj.optString("credit");
        nickname = jsonObj.optString("nickname");
    }
}
