package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataAdv;
import tw.sayhi.shared.result.ResultBase;


public class ResultSearch extends ResultBase
{
//    private static final String TAG = ResultSearch.class.getSimpleName();


    public final ArrayList<String> advIdList = new ArrayList<>();
    public final ArrayList<DataAdv> advList = new ArrayList<>();
    public final ArrayList<String> storeIdList = new ArrayList<>();
    public final ArrayList<DataStore> storeList = new ArrayList<>();


    public ResultSearch(String resp) throws JSONException
    {
        super(resp);

        JSONArray jsonArrAdv = jsonObj.optJSONArray("advList");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                DataAdv pad = new DataAdv(obj);
                advIdList.add(obj.optString("advId"));
                advList.add(pad);
            }
        }

        JSONArray jsonArrStore = jsonObj.optJSONArray("storeList");
        if (jsonArrStore != null) {
            for (int i = 0; i < jsonArrStore.length(); i++) {
                JSONObject obj = jsonArrStore.optJSONObject(i);
                DataStore pd = new DataStore(
                        obj.optString("advCount"),
                        obj.optString("area"),
                        obj.optString("areaCodes"),
                        obj.optString("category"),
                        obj.optString("categoryCode"), 
                        obj.optString("clickCount"),
                        obj.optString("dealDesc"),
                        obj.optString("desc"),
                        obj.optString("expiry"),
                        obj.optString("imageUrl"),
                        obj.optString("latitude"),
                        obj.optString("linkUrl"),
                        obj.optString("longitude"),
                        obj.optString("mainAddress"),
                        obj.optString("mainPhone"),
                        obj.optString("nickName"),
                        obj.optString("storeId"),
                        obj.optString("storeUrl"), 
                        obj.optString("title"), 
                        obj.optString("updated"));

                storeIdList.add(obj.optString("storeId"));
                storeList.add(pd); 
            }
        }
    }
}
