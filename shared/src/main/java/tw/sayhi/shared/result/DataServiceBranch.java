package tw.sayhi.shared.result;

/*
        "code": "4556547383623680",
        "name": "木柵木新加盟門市",
        "address": "台北市文山區木新路三段167號  ",
        "city": "台北市",
        "direct": false,
        "hours": "週一 ~ 週日 11:00~22:00",
        "phone": "02-29386010",
        "longitude": 1215183,
        "latitude": 250322
        */
public class DataServiceBranch
{
    public final String address;
    public final String city;
    public final String code;
    public final String desc;
    public final String direct;
    public final String hours;
    public final String id;
    public final String lat;
    public final String lng;
    public final String name;
    public final String phone;
    public final String storeId;
    public final String title;


    public DataServiceBranch(String address, String city, String code, String desc, String direct,
                             String hours, String id,
                             String lat, String lng, String name, String phone,
                             String storeId, String title)
    {
        this.address = address;
        this.city = city;
        this.code = code;
        this.desc = desc;
        this.direct = direct;
        this.hours = hours;
        this.id = id;
        this.lat = lat;
        this.lng = lng;
        this.name = name;
        this.phone = phone;
        this.storeId = storeId;
        this.title = title;
    }
}

