package tw.sayhi.shared.result;


import org.json.JSONException;
import org.json.JSONObject;


public class ResultAdvView extends ResultBase {

    public final DataAdv adv;


    public ResultAdvView(String resp) throws JSONException {
        super(resp);

        JSONObject obj = jsonObj.optJSONObject("adv");
        if (!status.equals("SUCCESS")) {
            adv = null;
        } else {
            adv = new DataAdv(obj);
        }
    }
}
