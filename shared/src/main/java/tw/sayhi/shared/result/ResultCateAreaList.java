package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.ResultBase;


public class ResultCateAreaList extends ResultBase
{
    public static class Item
    {
        public final String code;
        public final String imgUrl;
        public final String name;


        public Item(String code, String imgUrl, String name)
        {
            this.code = code;
            this.imgUrl = imgUrl;
            this.name = name;
        }
    }


//    private static final String TAG = ResultCateAreaList.class.getSimpleName();


    public final ArrayList<Item> areaList = new ArrayList<>();
    public final ArrayList<Item> cateList = new ArrayList<>();
    public final ArrayList<Item> storeCateList = new ArrayList<>();


    public ResultCateAreaList(String resp) throws JSONException
    {
        super(resp);

        JSONObject jsonObj = new JSONObject(resp);
        JSONArray area = jsonObj.optJSONArray("area");
        if (area != null) {
            for (int i = 0; i < area.length(); i++) {
                JSONObject obj = area.optJSONObject(i);
                areaList.add(new Item(obj.optString("code"),
                        obj.optString("imgUrl"), obj.optString("name")));
            }
        }

        JSONArray cate = jsonObj.optJSONArray("cate");
        if (cate != null) {
            for (int i = 0; i < cate.length(); i++) {
                JSONObject obj = cate.optJSONObject(i);
                cateList.add(new Item(obj.optString("code"),
                        obj.optString("imgUrl"), obj.optString("name")));
            }
        }

        JSONArray userStoreCate = jsonObj.optJSONArray("userStoreCate");
        if (userStoreCate != null) {
            for (int i = 0; i < userStoreCate.length(); i++) {
                JSONObject obj = userStoreCate.optJSONObject(i);
                storeCateList.add(new Item(obj.optString("code"),
                        obj.optString("imgUrl"), obj.optString("name")));
            }
        }
    }
}
