package tw.sayhi.shared.result;


public class DataHotTag
{
    public final String code;
    public final String latitude;
    public final String longitude;
    public final String name;
    public final String refCount;
    public final String tag;


    public DataHotTag(String code, String latitude, String longitude,
            String name, String refCount, String tag)
    {
        this.code = code;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.refCount = refCount;
        this.tag = tag;
    }
}
