package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataStoreLocation;
import tw.sayhi.shared.result.ResultBase;


public class ResultStoreLocations extends ResultBase {

//    private static final String TAG = ResultSearch.class.getSimpleName();


    public final ArrayList<DataStoreLocation> locationList = new ArrayList<>();


    public ResultStoreLocations(String resp) throws JSONException
    {
        super(resp);

        JSONArray jsonArrAdv = jsonObj.optJSONArray("locations");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                DataStoreLocation dsb = new DataStoreLocation(
                        obj.optString("code"),
                        obj.optString("id"),
                        obj.optString("lat"),
                        obj.optString("lng"),
                        obj.optString("name"),
                        obj.optString("storeId")
                );

                locationList.add(dsb);
            }
        }
    }
}
