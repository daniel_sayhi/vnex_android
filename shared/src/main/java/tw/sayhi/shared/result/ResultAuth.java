package tw.sayhi.shared.result;


import org.json.JSONException;

import tw.sayhi.shared.result.ResultBase;


public class ResultAuth extends ResultBase
{
    public final String expiry;
    public final String inviteUrl;
    public final String nickname;
    public final String session;


    public ResultAuth(String resp) throws JSONException
    {
        super(resp);

        expiry = jsonObj.optString("expiry");
        inviteUrl = jsonObj.optString("inviteUrl");
        nickname = jsonObj.optString("nickname");
        session = jsonObj.optString("session");
    }
}
