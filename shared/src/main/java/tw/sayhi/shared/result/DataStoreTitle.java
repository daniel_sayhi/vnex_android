package tw.sayhi.shared.result;

import tw.sayhi.shared.result.DataStore;

/**
 * Created by VAIO on 2016/8/9.
 */
public class DataStoreTitle
{
    public String storeId;
    public String title;
    public DataStoreTitle(DataStore ds)
    {
        storeId = ds.storeId;
        title = ds.title;
    }
}

