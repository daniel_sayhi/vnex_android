package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataAdv;
import tw.sayhi.shared.result.ResultBase;


public class ResultAdvList extends ResultBase
{
//    private static final String TAG = ResultAdvList.class.getSimpleName();


    public final ArrayList<DataAdv> advList = new ArrayList<>();


    public ResultAdvList(String resp) throws JSONException
    {
        super(resp);

        JSONArray jsonArrAdv = jsonObj.optJSONArray("advs");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                DataAdv pad = new DataAdv(obj);
                advList.add(pad);
            }
        }
    }
}
