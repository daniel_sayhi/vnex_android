package tw.sayhi.shared.result;


public class DataAdvAlert
{
    public final String areaCode;
    public final String code;
    public final String desc;
    public final String name;
    public final String price;
    public final String subscribe;


    public DataAdvAlert(
            String areaCode, String code, String desc, String name,
            String price, String subscribe)
    {
        this.areaCode = areaCode;
        this.code = code;
        this.desc = desc;
        this.name = name;
        this.price = price;
        this.subscribe = subscribe;
    }
}
