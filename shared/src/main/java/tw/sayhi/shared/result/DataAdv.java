package tw.sayhi.shared.result;


import org.json.JSONObject;

public class DataAdv {

    public final String advId;
    public final String area;
    public final String areaCode;
    public final String areaPath; //
    public final String address; //
    public final String body; //
    public final String category;
    public final String categoryPath; //
    public final String cateCode;
    public final String clickCount;
    public final String code;
    public final String creatorName; //
    public final String displayPrice;
    public final String extKey;
    public final String imgUrl;
    public final String label;
    public final String latitude;
    public final String listPrice;
    public final String longitude;
    public final String name;
    public final String onTop;
    public final String phone;
    public final String position;
    public final String startTime;
    public final String stopTime;
    public final String subject;
    public final String tags;
    public final String time;
    public final String title;
    public final String url;
    public final String viewCount;
    public final String watchCount;
    
    /*
    public DataAdv(String advId, String area, String areaCode, String areaPath,
            String address, int awardedBonus, String body, String category, String categoryPath,
            String cateCode, String clickCount, String code, String creatorName,
            String displayPrice, String extKey, String imgUrl, String label,
            String latitude, String listPrice, String longitude, String name,
            String onTop, String phone, String position, String startTime,
            String stopTime, String subject, String tags, String time,
            String title, String url, String viewCount, String watchCount)
    {
        this.advId = advId;
        this.area = area;
        this.areaCode = areaCode;
        this.areaPath = areaPath;
        this.address = address;
        this.awardedBonus = awardedBonus;
        this.body = body;
        this.category = category;
        this.categoryPath = categoryPath;
        this.cateCode = cateCode;
        this.clickCount = clickCount;
        this.code = code;
        this.creatorName = creatorName;
        this.displayPrice = displayPrice;
        this.extKey = extKey;
        this.imgUrl = imgUrl;
        this.label = label;
        this.latitude = latitude;
        this.listPrice = listPrice;
        this.longitude = longitude;
        this.name = name;
        this.onTop = onTop;
        this.phone = phone;
        this.position = position;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.subject = subject;
        this.tags = tags;
        this.time = time;
        this.title = title;
        this.url = url;
        this.viewCount = viewCount;
        this.watchCount = watchCount;
    }
    */
    public DataAdv(JSONObject obj)
    {
        this.advId = obj.optString("advId");
        this.area = obj.optString("area");
        this.areaCode = obj.optString("areaCode");
        this.areaPath = obj.optString("areaPath");
        this.address = obj.optString("address");
        this.body = obj.optString("body");
        this.category = obj.optString("category");
        this.categoryPath = obj.optString("categoryPath");
        this.cateCode = obj.optString("cateCode");
        this.clickCount = obj.optString("clickCount");
        this.code = obj.optString("code");
        this.creatorName = obj.optString("creatorName");
        this.displayPrice = obj.optString("displayPrice");
        this.extKey = obj.optString("extKey");
        this.imgUrl = obj.optString("imgUrl");
        this.label = obj.optString("label");
        this.latitude = obj.optString("latitude");
        this.listPrice = obj.optString("listPrice");
        this.longitude = obj.optString("longitude");
        this.name = obj.optString("name");
        this.onTop = obj.optString("onTop");
        this.phone = obj.optString("phone");
        this.position = obj.optString("position");
        this.startTime = obj.optString("startTime");
        this.stopTime = obj.optString("stopTime");
        this.subject = obj.optString("subject");
        this.tags = obj.optString("tags");
        this.time = obj.optString("time");
        this.title = obj.optString("title");
        this.url = obj.optString("url");
        this.viewCount = obj.optString("viewCount");
        this.watchCount = obj.optString("watchCount");
    }
}
