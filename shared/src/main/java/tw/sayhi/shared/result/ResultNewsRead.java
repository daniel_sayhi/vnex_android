package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataMsgNews;
import tw.sayhi.shared.result.ResultBase;


public class ResultNewsRead extends ResultBase
{
//    private static final String TAG = ResultConv.class.getSimpleName();


    public final String bonus;
    public final String credit;
    public final ArrayList<DataMsgNews> news = new ArrayList<>();


    public ResultNewsRead(String resp) throws JSONException
    {
        super(resp);

        bonus = jsonObj.optString("bonus");
        credit = jsonObj.optString("credit");
        JSONArray jsonArrAdv = jsonObj.optJSONArray("news");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                DataMsgNews pad = new DataMsgNews(
                        obj.optString("code"),
                        obj.optString("contentId"),
                        obj.optString("date"),
                        obj.optString("message"),
                        obj.optString("name"),
                        obj.optString("newsId"),
                        obj.optString("notif"),
                        obj.optString("subject")
                );

                news.add(pad);
            }
        }
    }
}
