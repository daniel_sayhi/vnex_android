package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ResultServiceBranches extends ResultBase
{

//    private static final String TAG = ResultSearch.class.getSimpleName();


    public final ArrayList<DataServiceBranch> branchList = new ArrayList<>();


    public ResultServiceBranches(String resp) throws JSONException
    {
        super(resp);

        JSONArray jsonArrAdv = jsonObj.optJSONArray("stores");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                DataServiceBranch dsb = new DataServiceBranch(
                        obj.optString("address"),
                        obj.optString("city"),
                        obj.optString("code"),
                        obj.optString("desc"),
                        obj.optString("direct"),
                        obj.optString("hours"),
                        obj.optString("id"),
                        obj.optString("latitude"),
                        obj.optString("longitude"),
                        obj.optString("name"),
                        obj.optString("phone"),
                        obj.optString("storeId"),
                        obj.optString("title")
                );

                branchList.add(dsb);
            }
        }
    }
}
