package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataNews;
import tw.sayhi.shared.result.ResultBase;


public class ResultNewsList extends ResultBase
{
//    private static final String TAG = ResultAdvList.class.getSimpleName();


    public final String bonus;
    public final String credit;
    public final ArrayList<DataNews> newsList = new ArrayList<>();


    public ResultNewsList(String resp) throws JSONException
    {
        super(resp);

        bonus = jsonObj.optString("bonus");
        credit = jsonObj.optString("credit");
        JSONArray jsonArrAdv = jsonObj.optJSONArray("news");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                DataNews pad = new DataNews(
                        obj.optString("code"),
                        obj.optString("message"),
                        obj.optString("name"),
                        obj.optString("newsId"),
                        obj.optString("period"),
                        obj.optString("price"),
                        obj.optString("priceDesc"),
                        obj.optString("subject"),
                        obj.optString("subscribe")
                );

                newsList.add(pad);
            }
        }
    }
}
