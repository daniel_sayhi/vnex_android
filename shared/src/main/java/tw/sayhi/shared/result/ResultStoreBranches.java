package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataStoreBranch;
import tw.sayhi.shared.result.ResultBase;


public class ResultStoreBranches extends ResultBase {

//    private static final String TAG = ResultSearch.class.getSimpleName();


    public final ArrayList<DataStoreBranch> branchList = new ArrayList<>();


    public ResultStoreBranches(String resp) throws JSONException
    {
        super(resp);

        JSONArray jsonArrAdv = jsonObj.optJSONArray("branches");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                DataStoreBranch dsb = new DataStoreBranch(
                        obj.optString("address"),
                        obj.optString("code"),
                        obj.optString("desc"),
                        obj.optString("id"),
                        obj.optString("lat"),
                        obj.optString("lng"),
                        obj.optString("name"),
                        obj.optString("phone"),
                        obj.optString("storeId"),
                        obj.optString("title")
                );

                branchList.add(dsb);
            }
        }
    }
}
