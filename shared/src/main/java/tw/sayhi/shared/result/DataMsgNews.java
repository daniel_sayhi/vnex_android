package tw.sayhi.shared.result;


public class DataMsgNews
{
    public final String code;
    public final String contentId;
    public final String date;
    public final String message;
    public final String name;
    public final String newsId;
    public final String notif;
    public final String subject;


    public DataMsgNews(
            String code, String contentId, String date, String message,
            String name, String newsId, String notif, String subject)
    {
        this.code = code;
        this.contentId = contentId;
        this.date = date;
        this.message = message;
        this.name = name;
        this.newsId = newsId;
        this.notif = notif;
        this.subject = subject;
    }
}
