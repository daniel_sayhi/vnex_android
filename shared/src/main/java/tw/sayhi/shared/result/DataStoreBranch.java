package tw.sayhi.shared.result;


public class DataStoreBranch
{
    public final String address;
    public final String code;
    public final String desc;
    public final String id;
    public final String lat;
    public final String lng;
    public final String name;
    public final String phone;
    public final String storeId;
    public final String title;


    public DataStoreBranch(String address, String code, String desc, String id,
                           String lat, String lng, String name, String phone,
                           String storeId, String title)
    {
        this.address = address;
        this.code = code;
        this.desc = desc;
        this.id = id;
        this.lat = lat;
        this.lng = lng;
        this.name = name;
        this.phone = phone;
        this.storeId = storeId;
        this.title = title;
    }
}

