package tw.sayhi.shared.result;


public class DataFriend
{
    public final String code;
    public final String n;
    public final String name;
    public final String p;
    public final String s;


    public DataFriend(String code, String n, String name, String p, String s)
    {
        this.code = code;
        this.n = n;
        this.name = name;
        this.p = p;
        this.s = s;
    }
}
