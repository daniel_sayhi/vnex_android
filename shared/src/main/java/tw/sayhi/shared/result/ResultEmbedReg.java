package tw.sayhi.shared.result;


import org.json.JSONException;

import tw.sayhi.shared.result.ResultBase;


public class ResultEmbedReg extends ResultBase {

    public final String msisdn;


    public ResultEmbedReg(String resp) throws JSONException {
        super(resp);

        msisdn = jsonObj.optString("MSISDN");
    }
}
