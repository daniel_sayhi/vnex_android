package tw.sayhi.shared.result;


public class DataNews
{
    public final String code;
    public final String message;
    public final String name;
    public final String newsId;
    public final String period;
    public final String price;
    public final String priceDesc;
    public final String subject;
    public final String subscribe;


    public DataNews(
            String code, String message, String name, String newsId,
            String period, String price, String priceDesc, String subject,
            String subscribe)
    {
        this.code = code;
        this.message = message;
        this.name = name;
        this.newsId = newsId;
        this.period = period;
        this.price = price;
        this.priceDesc = priceDesc;
        this.subject = subject;
        this.subscribe = subscribe;
    }
}
