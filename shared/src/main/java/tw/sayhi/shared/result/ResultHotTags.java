package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataHotTag;
import tw.sayhi.shared.result.ResultBase;


public class ResultHotTags extends ResultBase
{
//    private static final String TAG = ResultHotTags.class.getSimpleName();


    public final ArrayList<DataHotTag> tags = new ArrayList<>();


    public ResultHotTags(String resp) throws JSONException
    {
        super(resp);

        JSONArray jsonArrAdv = jsonObj.optJSONArray("tags");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                DataHotTag pad = new DataHotTag(
                        obj.optString("code"),
                        obj.optString("latitude"),
                        obj.optString("longitude"),
                        obj.optString("name"),
                        obj.optString("refCount"),
                        obj.optString("tag"));

                tags.add(pad);
            }
        }
    }
}
