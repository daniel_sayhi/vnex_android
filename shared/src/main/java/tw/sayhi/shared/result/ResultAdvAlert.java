package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataAdvAlert;
import tw.sayhi.shared.result.ResultBase;


public class ResultAdvAlert extends ResultBase
{
//    private static final String TAG = ResultAdvList.class.getSimpleName();


    public final String bonus;
    public final String credit;
    public final ArrayList<DataAdvAlert> advAlertList = new ArrayList<>();


    public ResultAdvAlert(String resp) throws JSONException
    {
        super(resp);

        bonus = jsonObj.optString("bonus");
        credit = jsonObj.optString("credit");
        JSONArray jsonArrAdv = jsonObj.optJSONArray("advAlert");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                DataAdvAlert pad = new DataAdvAlert(
                        obj.optString("areaCode"),
                        obj.optString("code"),
                        obj.optString("desc"),
                        obj.optString("name"),
                        obj.optString("price"),
                        obj.optString("subscribe")
                );

                advAlertList.add(pad);
            }
        }
    }
}
