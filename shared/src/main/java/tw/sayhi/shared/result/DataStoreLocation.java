package tw.sayhi.shared.result;


public class DataStoreLocation
{
    public final String code;
    public final String id;
    public final String lat;
    public final String lng;
    public final String name;
    public final String storeId;


    public DataStoreLocation(String code, String id, String lat, String lng,
                             String name, String storeId)
    {
        this.code = code;
        this.id = id;
        this.lat = lat;
        this.lng = lng;
        this.name = name;
        this.storeId = storeId;
    }
}

