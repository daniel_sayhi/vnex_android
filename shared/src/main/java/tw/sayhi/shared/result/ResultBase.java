package tw.sayhi.shared.result;


import org.json.JSONException;
import org.json.JSONObject;


public class ResultBase
{
    public static final String STATUS_SUCCESS = "SUCCESS";


    public final String debug;
    final JSONObject jsonObj;
    public final String status;
    public final int bonus;
    public final int awardedBonus;


    public ResultBase(String resp) throws JSONException
    {
        jsonObj = new JSONObject(resp);
        debug = jsonObj.optString("debug");
        status = jsonObj.optString("status");
        bonus = jsonObj.isNull("bonus") ? 0 : jsonObj.optInt("bonus");
        awardedBonus = jsonObj.isNull("awardedBonus") ? 0 : jsonObj.optInt("awardedBonus");
    }
}

