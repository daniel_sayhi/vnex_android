package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataStore;
import tw.sayhi.shared.result.ResultBase;


public class ResultStoreList extends ResultBase
{
//    private static final String TAG = ResultSearch.class.getSimpleName();


    public final ArrayList<DataStore> storeList = new ArrayList<>();
    public final String visitCount;


    public ResultStoreList(String resp) throws JSONException
    {
        super(resp);

        JSONArray jsonArrStore = jsonObj.optJSONArray("stores");
        if (jsonArrStore != null) {
            for (int i = 0; i < jsonArrStore.length(); i++) {
                JSONObject obj = jsonArrStore.optJSONObject(i);
                DataStore pd = new DataStore(
                        obj.optString("advCount"),
                        obj.optString("area"),
                        obj.optString("areaCodes"),
                        obj.optString("category"),
                        obj.optString("categoryCode"), 
                        obj.optString("clickCount"),
                        obj.optString("dealDesc"),
                        obj.optString("desc"),
                        obj.optString("expiry"),
                        obj.optString("imageUrl"),
                        obj.optString("latitude"),
                        obj.optString("linkUrl"),
                        obj.optString("longitude"),
                        obj.optString("mainAddress"),
                        obj.optString("mainPhone"),
                        obj.optString("nickName"),
                        obj.optString("storeId"),
                        obj.optString("storeUrl"),
                        obj.optString("title"),
                        obj.optString("updated"));

                storeList.add(pd); 
            }
        }
        visitCount = jsonObj.optString("visitCount");
    }
}
