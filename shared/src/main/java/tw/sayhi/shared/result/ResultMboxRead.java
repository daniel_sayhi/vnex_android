package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ResultMboxRead extends ResultBase
{
    private static final String TAG = ResultMboxRead.class.getSimpleName();


    public final String bonus;
    public final String credit;
    public final ArrayList<DataMsgOthers> msgs = new ArrayList<>();


    public ResultMboxRead(String resp) throws JSONException
    {
        super(resp);

        bonus = jsonObj.optString("bonus");
        credit = jsonObj.optString("credit");
        JSONArray jsonArrAdv = jsonObj.optJSONArray("msgs");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);

                String receivers = "";
                JSONArray recvs = obj.optJSONArray("receivers");
                if (recvs != null) {
                    for (int j = 0; j < recvs.length(); j++) {
                        String recv = recvs.getString(j);
                        receivers += recv + " ";
                    }
                    receivers = receivers.trim();
                }
//                MyLog.d(TAG, "receivers = " + receivers);

                DataMsgOthers pad = new DataMsgOthers(
                        obj.optString("ContentId"),
                        obj.optString("date"),
                        obj.optString("message"),
                        obj.optString("msgid"),
                        obj.optString("new"),
                        obj.optString("notif"),
                        obj.optString("phone"),
                        receivers,
                        obj.optString("type")
                );

                msgs.add(pad);
            }
        }
    }
}
