package tw.sayhi.shared.result;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tw.sayhi.shared.result.DataFriend;
import tw.sayhi.shared.result.ResultBase;


public class ResultFriend extends ResultBase
{
//    private static final String TAG = ResultFriend.class.getSimpleName();


    public final ArrayList<DataFriend> friends = new ArrayList<>();


    public ResultFriend(String resp) throws JSONException
    {
        super(resp);

        JSONArray jsonArrAdv = jsonObj.optJSONArray("f");
        if (jsonArrAdv != null) {
            for (int i = 0; i < jsonArrAdv.length(); i++) {
                JSONObject obj = jsonArrAdv.optJSONObject(i);
                DataFriend pad = new DataFriend(
                        obj.optString("code"),
                        obj.optString("n"),
                        obj.optString("name"),
                        obj.optString("p"),
                        obj.optString("s")
                );

                friends.add(pad);
            }
        }
    }
}
