package tw.sayhi.shared.widget;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class DialogListItem extends Dialog
{
	int selectItem=0;
	public DialogListItem(Context context, String title, String[] arr)
	{
		super(context);
		Builder builder = new Builder(context);
		builder.setTitle(title);
		builder.setItems(arr, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				selectItem=which;
				selectInstance();
			}
		});
		Dialog noticeDialog = builder.create();
		noticeDialog.show();
	}

	
	public void selectInstance(){
		
	}
	public int getSelectItem(){
		return selectItem;
	}
}