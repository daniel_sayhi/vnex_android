package tw.sayhi.shared.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import tw.sayhi.shared.R;

public class ItemImageText extends LinearLayout
{
    private ItemImageText.OnClickListener onClickListener;
    public interface OnClickListener{
        public void onClick(View view);
    }
    private LinearLayout ll;
    private ImageView iv;
    private TextView tv;
    public ItemImageText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(attrs);
    }
    public ItemImageText(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }
    public void setOnClickListener(OnClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }
    private void init( AttributeSet attrs)
    {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MainViewItem);
        int imgId = a.getResourceId(R.styleable.MainViewItem_img_icon,0);
        int layoutId = a.getResourceId(R.styleable.MainViewItem_layout_id, R.layout.item_image_text);
        String title = a.getString(R.styleable.MainViewItem_title_text);
//        a.recycle();
        inflate(getContext(), layoutId, this);
        this.ll =(LinearLayout)findViewById(R.id.ll_mix);
        this.iv = (ImageView)findViewById(R.id.iv_mix);
        this.tv = (TextView)findViewById(R.id.tv_mix);

        iv.setImageResource(imgId);
//        Picasso.with(AppCtx.getContext()).load(imgId).into(iv);
        tv.setText(title);

        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener != null) {
                    onClickListener.onClick(ItemImageText.this);
                }
            }
        });
    }
}
