package tw.sayhi.shared.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;

public class ActionDialog extends Dialog
{
    public ActionDialog(Activity act, int title_id, String title_suffix, int msg_id, String msg_suffix, int icon_id, int ok_id, int cancel_id) {
        super(act);
        String title = act.getString(title_id);
        if(title_suffix != null) {
            title = title + title_suffix;
        }
        String msg = act.getString(msg_id);
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        if(icon_id > 0) {
            builder.setIcon(icon_id);
        }
        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setPositiveButton(act.getString(ok_id), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onOk();
            }
        });
        if(cancel_id > 0) {
            builder.setNegativeButton(act.getString(cancel_id), new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onCancel();
                }
            });
        }
        Dialog noticeDialog = builder.create();
        noticeDialog.setCancelable(false);
        noticeDialog.setCanceledOnTouchOutside(false);
        noticeDialog.show();
    }
    public void onOk() {}
    public void onCancel() {}
}
